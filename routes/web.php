<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    if(app()->getLocale() == 'ar')
    {
        echo 'f';
    }
    else
    {
        echo 'fs';
    }
   // dd(app()->getLocale());
    return view('welcome');
});




Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    
    // return what you want
    
});
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    
    // return what you want
    
});

Route::fallback(function () {

    return view("admin/error");

});
Route::group(['prefix' =>'/admin','namespace' => 'Dashboard', 'middleware' => 'auth:admin'], function()
{
    Route::get('/','AdminController@index');

Route::get('/logout','LoginAdminController@destroy');
});
Route::group(['prefix' =>'/admin', 'middleware' => 'auth:admin'], function()
{
        Route::resource('/users','UserController');

     // ================================== Start Route categorys ===================================//
        Route::resource('categorys', 'Dashboard\CategoryController')->only(['index']);
        Route::get('categorys/{id}/profile', 'Dashboard\CategoryController@show')->middleware('CheckRole:read-categorys');
        Route::resource('categorys', 'Dashboard\CategoryController')->only(['create','store'])->middleware('CheckRole:create-categorys');
        Route::resource('categorys', 'Dashboard\CategoryController')->only(['edit','update'])->middleware('CheckRole:edit-categorys');
        Route::resource('categorys', 'Dashboard\CategoryController')->only(['destroy'])->middleware('CheckRole:delete-categorys');
        Route::group(['middleware' => 'CheckRole:activate-categorys'], function(){
            Route::post('/categorys/{id}/activate', 'Dashboard\CategoryController@activate');
            Route::post('/categorys/{id}/deactivate', 'Dashboard\CategoryController@deactivate');
        });
        Route::post('/categorys/{id}/tags', 'Dashboard\CategoryController@addTag')->name('admin.addTag')->middleware('CheckRole:categorys-add-tag');
      // ==================================  End Route categorys ===================================//


     // =================================== Start Route products ===================================//
        Route::resource('products', 'Dashboard\ProductController')->only(['index']);
        Route::get('products/{id}/profile', 'Dashboard\ProductController@show')->middleware('CheckRole:read-products');
        Route::resource('products', 'Dashboard\ProductController')->only(['create','store'])->middleware('CheckRole:create-products');
        Route::resource('products', 'Dashboard\ProductController')->only(['edit','update'])->middleware('CheckRole:edit-products');
        Route::resource('products', 'Dashboard\ProductController')->only(['destroy'])->middleware('CheckRole:delete-products');
        Route::group(['middleware' => 'CheckRole:activate-products'], function(){
            Route::post('/products/{id}/activate', 'Dashboard\ProductController@activate');
            Route::post('/products/{id}/deactivate', 'Dashboard\ProductController@deactivate');
        });
        Route::group(['middleware' => 'CheckRole:star-products'], function(){
            Route::post('/products/{id}/add_star_product', 'Dashboard\ProductController@add_star_product');
            Route::post('/products/{id}/del_star_product', 'Dashboard\ProductController@del_star_product');
        });
        Route::post('/products/{id}/tags', 'Dashboard\ProductController@addTag')->name('admin.addTag')->middleware('CheckRole:products-add-tag');
     // =================================== End Route products ===================================//


      // ==================================  Start Route shop_coupons ===================================//

   Route::resource('shop_coupons', 'Dashboard\ShopCouponController')->only(['index']);
        Route::get('shop_coupons/{id}/profile', 'Dashboard\ShopCouponController@show')->middleware('CheckRole:read-shop-coupons');
        Route::resource('shop_coupons', 'Dashboard\ShopCouponController')->only(['create','store'])->middleware('CheckRole:create-shop-coupons');
        Route::resource('shop_coupons', 'Dashboard\ShopCouponController')->only(['edit','update'])->middleware('CheckRole:edit-shop-coupons');
        Route::resource('shop_coupons', 'Dashboard\ShopCouponController')->only(['destroy'])->middleware('CheckRole:delete-shop-coupons');
        Route::group(['middleware' => 'CheckRole:activate-shop-coupons'], function(){
            Route::post('/shop_coupons/{id}/activate', 'Dashboard\ShopCouponController@activate');
            Route::post('/shop_coupons/{id}/deactivate', 'Dashboard\ShopCouponController@deactivate');
        });
      // ==================================  End Route shop_coupons ===================================//




        Route::get('/getAnimals', 'Dashboard\ProductController@getAnimals')->name('admin.getAnimals');



     // ================================== Start Route tags ===================================//
        Route::resource('tags', 'Dashboard\BlogTagController')->only(['index']);
        Route::get('tags/{id}/profile', 'Dashboard\BlogTagController@show')->middleware('CheckRole:read-tags');
        Route::resource('tags', 'Dashboard\BlogTagController')->only(['create','store'])->middleware('CheckRole:create-tags');
        Route::resource('tags', 'Dashboard\BlogTagController')->only(['edit','update'])->middleware('CheckRole:edit-tags');
        Route::resource('tags', 'Dashboard\BlogTagController')->only(['destroy'])->middleware('CheckRole:delete-tags');
        Route::group(['middleware' => 'CheckRole:activate-tags'], function(){
            Route::post('/tags/{id}/activate', 'Dashboard\BlogTagController@activate');
            Route::post('/tags/{id}/deactivate', 'Dashboard\BlogTagController@deactivate');
        });
      // ==================================  End Route tags ===================================//



     // ================================== Start Route citys ===================================//
        Route::resource('citys', 'Dashboard\CityController')->only(['index']);
        Route::get('citys/{id}/profile', 'Dashboard\CityController@show')->middleware('CheckRole:read-citys');
        Route::resource('citys', 'Dashboard\CityController')->only(['create','store'])->middleware('CheckRole:create-citys');
        Route::resource('citys', 'Dashboard\CityController')->only(['edit','update'])->middleware('CheckRole:edit-citys');
        Route::resource('citys', 'Dashboard\CityController')->only(['destroy'])->middleware('CheckRole:delete-citys');
        Route::group(['middleware' => 'CheckRole:activate-citys'], function(){
            Route::post('/citys/{id}/activate', 'Dashboard\CityController@activate');
            Route::post('/citys/{id}/deactivate', 'Dashboard\CityController@deactivate');
        });
      // ==================================  End Route citys ===================================//


     // ================================== Start Route regions ===================================//
        Route::resource('regions', 'Dashboard\RegionController')->only(['index']);
        Route::get('regions/{id}/profile', 'Dashboard\RegionController@show')->middleware('CheckRole:read-regions');
        Route::resource('regions', 'Dashboard\RegionController')->only(['create','store'])->middleware('CheckRole:create-regions');
        Route::resource('regions', 'Dashboard\RegionController')->only(['edit','update'])->middleware('CheckRole:edit-regions');
        Route::resource('regions', 'Dashboard\RegionController')->only(['destroy'])->middleware('CheckRole:delete-regions');
        Route::group(['middleware' => 'CheckRole:activate-regions'], function(){
            Route::post('/regions/{id}/activate', 'Dashboard\RegionController@activate');
            Route::post('/regions/{id}/deactivate', 'Dashboard\RegionController@deactivate');
        });
      // ==================================  End Route regions ===================================//



     // ================================== Start Route locations ===================================//
        Route::resource('locations', 'Dashboard\LocationController')->only(['index']);
        Route::get('locations/{id}/profile', 'Dashboard\LocationController@show')->middleware('CheckRole:read-locations');
        Route::resource('locations', 'Dashboard\LocationController')->only(['create','store'])->middleware('CheckRole:create-locations');
        Route::resource('locations', 'Dashboard\LocationController')->only(['edit','update'])->middleware('CheckRole:edit-locations');
        Route::resource('locations', 'Dashboard\LocationController')->only(['destroy'])->middleware('CheckRole:delete-locations');
        Route::group(['middleware' => 'CheckRole:activate-locations'], function(){
            Route::post('/locations/{id}/activate', 'Dashboard\LocationController@activate');
            Route::post('/locations/{id}/deactivate', 'Dashboard\LocationController@deactivate');
        });
      // ==================================  End Route locations ===================================//





     // ==================================  Start Route Blogs ==================================//
        Route::resource('blogs', 'Dashboard\BlogController')->only(['index']);
        Route::get('blogs/{id}/profile', 'Dashboard\BlogController@show')->middleware('CheckRole:read-blog');
        Route::resource('blogs', 'Dashboard\BlogController')->only(['create','store'])->middleware('CheckRole:create-blog');
        Route::resource('blogs', 'Dashboard\BlogController')->only(['edit','update'])->middleware('CheckRole:edit-blog');
        Route::resource('blogs', 'Dashboard\BlogController')->only(['destroy'])->middleware('CheckRole:delete-blog');
        Route::group(['middleware' => 'CheckRole:activate-blogs'], function(){
            Route::post('/blogs/{id}/activate', 'Dashboard\BlogController@activate');
            Route::post('/blogs/{id}/deactivate', 'Dashboard\BlogController@deactivate');
        });
        Route::post('/blogs/{id}/tags', 'Dashboard\BlogController@addTag')->name('admin.addTag')->middleware('CheckRole:blog-add-tag');
     // ==================================  End Route Blogs ================================== //

     // ==================================  Start Route product_sellers ================================== //
        Route::resource('product_sellers', 'Dashboard\ProductSellerController')->only(['index']);
        Route::get('product_sellers/{id}/profile', 'Dashboard\ProductSellerController@show')->middleware('CheckRole:read-product_sellers');
        Route::resource('product_sellers', 'Dashboard\ProductSellerController')->only(['create','store'])->middleware('CheckRole:create-product_sellers');
        Route::resource('product_sellers', 'Dashboard\ProductSellerController')->only(['edit','update'])->middleware('CheckRole:edit-product_sellers');
        Route::resource('product_sellers', 'Dashboard\ProductSellerController')->only(['destroy'])->middleware('CheckRole:delete-product_sellers');
        Route::group(['middleware' => 'CheckRole:activate-sellers-products'], function(){
            Route::post('/product_sellers/{id}/activate', 'Dashboard\ProductSellerController@activate');
            Route::post('/product_sellers/{id}/deactivate', 'Dashboard\ProductSellerController@deactivate');
        });
     // ==================================  End Route product_sellers ================================== //


     // ==================================  Start Route product_arrivals ================================== //
        Route::resource('product_arrivals', 'Dashboard\ProductArrivalController')->only(['index']);
        Route::get('product_arrivals/{id}/profile', 'Dashboard\ProductArrivalController@show')->middleware('CheckRole:read-product_arrivals');
        Route::resource('product_arrivals', 'Dashboard\ProductArrivalController')->only(['create','store'])->middleware('CheckRole:create-product_arrivals');
        Route::resource('product_arrivals', 'Dashboard\ProductArrivalController')->only(['edit','update'])->middleware('CheckRole:edit-product_arrivals');
        Route::resource('product_arrivals', 'Dashboard\ProductArrivalController')->only(['destroy'])->middleware('CheckRole:delete-product_arrivals');
        Route::group(['middleware' => 'CheckRole:activate-arrivals-products'], function(){
            Route::post('/product_arrivals/{id}/activate', 'Dashboard\ProductArrivalController@activate');
            Route::post('/product_arrivals/{id}/deactivate', 'Dashboard\ProductArrivalController@deactivate');
        });
     // ==================================  End Route product_arrivals ================================== //



     // ==================================  Start Route gallerys ================================== //
        Route::resource('gallerys', 'Dashboard\SiteGalleryController')->only(['index']);
        Route::get('gallerys/{id}/profile', 'Dashboard\SiteGalleryController@show')->middleware('CheckRole:read-gallerys');
        Route::post('gallerys/save', 'Dashboard\SiteGalleryController@add_imgs')->middleware('CheckRole:create-gallerys')->name('site_gallerys.save');
        Route::get('gallerys/{id}/delete', 'Dashboard\SiteGalleryController@destroy')->middleware('CheckRole:delete-gallerys')->name('site_gallerys.delete');
        
     // ==================================  End Route gallerys ================================== //




     // ================================== Start Route animal_types ===================================//
        Route::resource('animal_types', 'Dashboard\AnimalTypeController')->only(['index']);
        Route::get('animal_types/{id}/profile', 'Dashboard\AnimalTypeController@show')->middleware('CheckRole:read-animal_types');
        Route::resource('animal_types', 'Dashboard\AnimalTypeController')->only(['create','store'])->middleware('CheckRole:create-animal_types');
        Route::resource('animal_types', 'Dashboard\AnimalTypeController')->only(['edit','update'])->middleware('CheckRole:edit-animal_types');
        Route::resource('animal_types', 'Dashboard\AnimalTypeController')->only(['destroy'])->middleware('CheckRole:delete-animal_types');
        Route::group(['middleware' => 'CheckRole:activate-animal_types'], function(){
            Route::post('/animal_types/{id}/activate', 'Dashboard\AnimalTypeController@activate');
            Route::post('/animal_types/{id}/deactivate', 'Dashboard\AnimalTypeController@deactivate');
        });
      // ==================================  End Route animal_types ===================================//





        Route::resource('shop_settings', 'Dashboard\ShopSettingController');
        Route::get('shop_statistics', 'Dashboard\ShopSettingController@shop_statistics');
        Route::resource('privacy_policy', 'Dashboard\PrivacyPolicyController');




     // ================================== Start Route clinics ===================================//
        Route::resource('clinics', 'Dashboard\ClinicController')->only(['index']);
        Route::get('clinics/{id}/profile', 'Dashboard\ClinicController@show')->middleware('CheckRole:read-clinics');
        Route::resource('clinics', 'Dashboard\ClinicController')->only(['edit','update'])->middleware('CheckRole:edit-clinics');
       Route::post('clinics', 'Dashboard\ClinicController@update')->middleware('CheckRole:edit-clinics')->name('admin.edit_clinic');
        Route::post('clinics/{id}/services', 'Dashboard\ClinicController@addService')->middleware('CheckRole:create-clinic_service')->name('admin.addService');

        Route::resource('clinics/{id}/services/delete', 'Dashboard\ClinicController')->only(['destroy_service'])->middleware('CheckRole:delete-clinic_service');
        Route::group(['middleware' => 'CheckRole:activate-clinic_service'], function(){
            Route::post('/clinics/{id}/services/activate', 'Dashboard\ClinicController@activate');
            Route::post('/clinics/{id}/services/deactivate', 'Dashboard\ClinicController@deactivate');
        });
      // ==================================  End Route clinics ===================================//



        // Route::resource('clinics', 'Dashboard\ClinicController');
        // Route::put('/clinics/{id}/update', 'Dashboard\ClinicController@update')->name('admin.edit_clinic');

        // Route::post('/clinics/{id}/services', 'Dashboard\ClinicController@addService')->name('admin.addService');
        // Route::post('/clinics/{id}/services/activate', 'Dashboard\ClinicController@activate');
        // Route::post('/clinics/{id}/services/deactivate', 'Dashboard\ClinicController@deactivate');
        // Route::any('/clinics/{id}/services/delete', 'Dashboard\ClinicController@destroy_service');




        Route::resource('hotels', 'Dashboard\HotelController');
        Route::put('/hotels/{id}/update', 'Dashboard\HotelController@update')->name('admin.edit_hotel');

        Route::post('/hotels/{id}/services', 'Dashboard\HotelController@addService')->name('admin.HoteladdService');
        Route::post('/hotels/{id}/services/activate', 'Dashboard\HotelController@activate');
        Route::post('/hotels/{id}/services/deactivate', 'Dashboard\HotelController@deactivate');
        Route::any('/hotels/{id}/services/delete', 'Dashboard\HotelController@destroy_service');




     // ================================== Start Route sliders ===================================//
        Route::resource('sliders', 'Dashboard\SliderController')->only(['index']);
        Route::get('sliders/{id}/profile', 'Dashboard\SliderController@show')->middleware('CheckRole:read-sliders');
        Route::resource('sliders', 'Dashboard\SliderController')->only(['create','store'])->middleware('CheckRole:create-sliders');
        Route::resource('sliders', 'Dashboard\SliderController')->only(['edit','update'])->middleware('CheckRole:edit-sliders');
        Route::resource('sliders', 'Dashboard\SliderController')->only(['destroy'])->middleware('CheckRole:delete-sliders');
        Route::group(['middleware' => 'CheckRole:activate-sliders'], function(){
            Route::post('/sliders/{id}/activate', 'Dashboard\SliderController@activate');
            Route::post('/sliders/{id}/deactivate', 'Dashboard\SliderController@deactivate');
        });
      // ==================================  End Route sliders ===================================//


     // ================================== Start Route trademarks ===================================//
        Route::resource('trademarks', 'Dashboard\TrademarkController')->only(['index']);
        Route::get('trademarks/{id}/profile', 'Dashboard\TrademarkController@show')->middleware('CheckRole:read-trademarks');
        Route::resource('trademarks', 'Dashboard\TrademarkController')->only(['create','store'])->middleware('CheckRole:create-trademarks');
        Route::resource('trademarks', 'Dashboard\TrademarkController')->only(['edit','update'])->middleware('CheckRole:edit-trademarks');
        Route::resource('trademarks', 'Dashboard\TrademarkController')->only(['destroy'])->middleware('CheckRole:delete-trademarks');
        Route::group(['middleware' => 'CheckRole:activate-trademarks'], function(){
            Route::post('/trademarks/{id}/activate', 'Dashboard\TrademarkController@activate');
            Route::post('/trademarks/{id}/deactivate', 'Dashboard\TrademarkController@deactivate');
        });
      // ==================================  End Route trademarks ===================================//


     // ================================== Start Route stores ===================================//
        Route::resource('stores', 'Dashboard\StoreController')->only(['index']);
        Route::get('stores/{id}/profile', 'Dashboard\StoreController@show')->middleware('CheckRole:read-stores');
        Route::resource('stores', 'Dashboard\StoreController')->only(['edit','update'])->middleware('CheckRole:edit-stores');
        Route::resource('stores', 'Dashboard\StoreController')->only(['destroy'])->middleware('CheckRole:delete-stores');
       
      // ==================================  End Route stores ===================================//


     // ================================== Start Route customers ===================================//
        Route::resource('customers', 'Dashboard\CustomerController')->only(['index']);
        Route::get('customers/{id}/profile', 'Dashboard\CustomerController@show')->middleware('CheckRole:read-customers');
        Route::resource('customers', 'Dashboard\CustomerController')->only(['create','store'])->middleware('CheckRole:create-customers');
        // Route::resource('customers', 'Dashboard\CustomerController')->only(['edit','update'])->middleware('CheckRole:edit-customers');
        // Route::resource('customers', 'Dashboard\CustomerController')->only(['destroy'])->middleware('CheckRole:delete-customers');
       
      // ==================================  End Route customers ===================================//


     // ================================== Start Route settings ===================================//
        
        Route::resource('settings', 'Dashboard\SettingController')->only(['index','update'])->middleware('CheckRole:edit-settings');
       
      // ==================================  End Route settings ===================================//

        
        Route::resource('customer_contacts', 'Dashboard\ContactController');


        Route::resource('customer_reviews', 'Dashboard\CustomerReviewController');


        Route::resource('users', 'Dashboard\UsersController');

        Route::resource('newsletters', 'Dashboard\NewsletterController');
});

Route::group(['namespace' => 'Dashboard','prefix' => 'admin', 'middleware' => 'guest:admin'], function()
{
    Route::get('/login','LoginAdminController@getLogin')->name( 'get.admin.login');
    Route::post('/login','LoginAdminController@login')->name( 'admin.login');
   

});

// Administrator & SuperAdministrator Control Panel Routes
Route::group(['prefix' => '/admin', 'middleware' => 'auth:admin', 'namespace' => 'Dashboard'], function () {
    Route::resource('/users', 'UsersController')->middleware('roles:superadministrator');
    Route::resource('/permission', 'PermissionController')->middleware('roles:superadministrator');
    Route::resource('/roles', 'RolesController')->middleware('roles:superadministrator');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// ============================================================================================================================================================================================================================================================== START WEBSITE ROUTES ==================================================================================== //



// ==================================  START WEBSITE ROUTES ================================== //
Route::group(['prefix' => LaravelLocalization::setLocale() ,'middleware' => ['localizationRedirect', 'localeViewPath']], function()
{

 
Route::get('comming-soon', function () {
    return view('site.comming-soon');
});


        Route::get('/','Website\SiteController@index')->name('site');
        Route::get('/about-us','Website\SiteController@get_about_page')->name('site.about-us');
        Route::get('/privacy-policy','Website\SiteController@get_privacy_page')->name('site.privacy-policy');


        Route::get('/contact-us','Website\SiteController@get_contact_page');
        Route::post('/contact-us/store','Website\SiteController@store_contact')->name('site.store_contact');

        Route::post('/newsletter/store','Website\SiteController@store_newsletter')->name('site.store_newsletter');

        Route::get('/clinic/{clinic_name}','Website\ClinicController@get_clinic')->name('site.clinic');

        Route::get('/hotel/{hotel_name}','Website\HotelController@get_hotel')->name('site.hotel');


        Route::get('/product/{code}', 'Website\ProductController@get_product');
        Route::get('/blogs', 'Website\BlogController@get_blogs')->name('site.sort1');
        Route::get('/blog/{blog_name}', 'Website\BlogController@get_singleblog');
        Route::get('/blogs/sortingP_p', 'Website\BlogController@sort2')->name('site.sort2');
       Route::get('/blogsTag/{tag_name}', 'Website\BlogController@get_blogs_tag');
       Route::get('/blogs-search', 'Website\BlogController@getBlogsSearch');

       Route::get('/search', 'Website\SiteController@getProductsSearch')->name('site.search');


Route::get('/category', 'Website\ProductController@get_category');
Route::get('/filter', 'Website\ProductController@get_category');
Route::get('/filter/{name}/{name2}', 'Website\ProductController@name');
Route::get('/filter/{name}', 'Website\ProductController@animal_name');

    Route::group(['middleware'=>'auth:customer'],function()
    {

        Route::get('/products/addFav', 'Website\ProductController@add_remove_product_to_fav')->name('site.add_fav');
       Route::get('/products/appraisal/{product_id}', 'Website\ProductController@store_appraisal')->name('site.store_appraisal');

        Route::get('/blog/comment/{blog_id}', 'Website\BlogController@store_comment')->name('site.store_comment');

        Route::get('/favourite', 'Website\ProductController@get_fav_products')->name('site.favourite_page');

        Route::get('/cart', 'Website\ProductController@get_cart_products')->name('site.cart_page');
        Route::get('/cart/update', 'Website\ProductController@update_cart')->name('site.update_cart');
        Route::get('/cart/removeCart', 'Website\ProductController@removeAllProductFromCart')->name('site.remove_cart');


        Route::post('/cart/update_bill', 'Website\ProductController@update_bill')->name('site.update_bill');

        Route::post('/cart/choose_payment_method', 'Website\ProductController@choose_payment_method')->name('site.choose_payment_method');
        Route::post('/cart/apply_discount', 'Website\ProductController@apply_discount')->name('apply_discount');




        Route::post('/clinic/add_more','Website\ClinicController@addMorePost');

        Route::post('/hotel/add_more','Website\HotelController@addMorePost');

        Route::post('/hotel/add_more','Website\HotelController@addMorePost');

        Route::get('/favourite/removeFav', 'Website\ProductController@removeProductFromFav')->name('site.remove_fav');

        Route::get('/cart/removeSingleCart', 'Website\ProductController@removeProductFromCart')->name('site.remove_single_cart');


        Route::get('/favourite/addCart', 'Website\ProductController@addProductToCart')->name('site.add_cart');

        
        Route::get('/profile', 'Website\CustomerController@get_profile');
        Route::any('/profile/{id}', 'Website\CustomerController@edit_customer')->name('edit-profile');
        Route::post('/update-photo/{id}', 'Website\CustomerController@update_photo')->name('update-photo');
        Route::post('/update-password/{id}', 'Website\CustomerController@update_password')->name('update-password');


        Route::get('/my-orders', 'Website\CustomerController@get_orders')->name('site.orders');
        Route::get('/my-orders-3', 'Website\CustomerController@get_last_3month')->name('site.orders-3');
        Route::get('/my-orders-6', 'Website\CustomerController@get_last_6month')->name('site.orders-6');


        Route::get('/my-reservations', 'Website\CustomerController@get_reservations')->name('site.reservations');
        Route::get('/my-reservations-3', 'Website\CustomerController@get_reserv_last_3month')->name('site.reservations-3');
        Route::get('/my-reservations-6', 'Website\CustomerController@get_reserv_last_6month')->name('site.reservations-6');
        Route::get('/my-reservations/removeReserv', 'Website\CustomerController@removeReservation')->name('site.remove_reserv');

        Route::get('/shippment-address', 'Website\CustomerController@shippment_address');
        Route::get('/shippment-address/regions/{id}', 'Website\CustomerController@get_ajax_region')->name('get_ajax_region');
        Route::get('/shippment-address/locations/{id}', 'Website\CustomerController@get_ajax_location');

        Route::post('/store-shippment-address', 'Website\CustomerController@store_shippment')->name('store-shippment-address');


            Route::get('/get_mail/{id}', 'Website\CustomerController@get_mail')->name('get-mail');

            Route::get('/update-email', 'Website\CustomerController@update_email')->name('site.updateEmail');

        Route::get('/logout','Website\CustomerController@logout');

    });
    Route::group(['middleware'=>'guest'],function()
    {

        Route::get('/signup','Website\SiteController@register');
        Route::post('/signup/create','Website\CustomerController@store')->name('site.store');
        Route::post('/customer_login','Website\CustomerController@login')->name('site.login');
        Route::get('/get_password', 'Website\CustomerController@get_password')->name('get-pass');
        Route::get('/new_password/{email}', 'Website\CustomerController@check_code')->name('site.checkCode');
        Route::post('/checking', 'Website\CustomerController@checking')->name('site.checking');

    });

 });