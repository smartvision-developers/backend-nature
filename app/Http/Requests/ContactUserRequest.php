<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
          'name'=> 'required|min:2',
          'email'=> 'required|email',
          'phone'           => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
          'message'=> 'required',
              ];
      }
  
      public function messages()
      {
      
          return [
            'name.required'=>__('translate.name-required'),
            'name.min' =>__('translate.name-min'),
            'email.email' =>__('translate.email-email'),
            'phone.required'=> __('translate.phone-required'),
            'phone.regex' =>  __('translate.phone-regex'),
            'email.required'=>__('translate.email-required'),
            'message.required'=>__('translate.message-required'),
              ];
      }
}
