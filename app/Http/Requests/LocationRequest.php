<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'location_name_ar' =>'required|min:2|max:50|unique:locations,location_name_ar',
            
              ];
      }
  
      public function messages()
      {
      
          return [
            'location_name_ar.required' =>'الكلمه الدلاليه مطلوب',
            'location_name_ar.unique' => 'هذا الرقم مستخدم من قبل ',
            'location_name_ar.min' =>'الكلمه الدلاليه يجب ان يكون اكبر من 2 حروف',
            'location_name_ar.max' =>'الكلمه الدلاليه  يجب ان يكون اقل من 50 حرف',
  
              ];
      }
}
