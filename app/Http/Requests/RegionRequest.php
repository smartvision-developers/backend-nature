<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'region_name_ar' =>'required|min:2|max:50|unique:regions,region_name_ar',
            
              ];
      }
  
      public function messages()
      {
      
          return [
            'region_name_ar.required' =>'الكلمه الدلاليه مطلوب',
            'region_name_ar.unique' => 'هذا الرقم مستخدم من قبل ',
            'region_name_ar.min' =>'الكلمه الدلاليه يجب ان يكون اكبر من 2 حروف',
            'region_name_ar.max' =>'الكلمه الدلاليه  يجب ان يكون اقل من 50 حرف',
  
              ];
      }
}
