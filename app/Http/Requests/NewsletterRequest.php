<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsletterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'email' =>'required|email|unique:newsletters,email',
              ];
      }
  
      public function messages()
      {
      
          return [
            'email.required' =>'البريد الالكتروني مطلوب',
            'email.unique' => 'هذا البريد الالكتروني مستخدم من قبل ',
            'email.email' =>'تأكد من ادخال بريد الكتروني صحيح',
  
              ];
      }
}
