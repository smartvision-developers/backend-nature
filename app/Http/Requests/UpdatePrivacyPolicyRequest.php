<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePrivacyPolicyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
            'title' =>'required|min:2|max:30',
            'description' =>'required',
  
              ];
      }
  
      public function messages()
      {
      
          return [
            'title.required' =>'عنوان الشروط والأحكام  الفرعي مطلوب',
            'title.min' =>'عنوان الشروط والأحكام يجب ان يكون اكبر من 2 حروف',
            'title.max' =>'عنوان الشروط والأحكام  يجب ان يكون اقل من 30 حرف',
            'description.required' =>'وصف الشرط مطلوبه',
  
              ];
      }
}
