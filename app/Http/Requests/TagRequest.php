<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'tag_ar' =>'required|min:2|max:50|unique:blog_tags,tag_ar',
            'tag_en' =>'required|min:2|max:50|unique:blog_tags,tag_en',
              ];
      }
  
      public function messages()
      {
      
          return [
            'tag_ar.required' =>'الكلمه الدلاليه مطلوب',
            'tag_ar.unique' => 'هذا الرقم مستخدم من قبل ',
            'tag_ar.min' =>'الكلمه الدلاليه يجب ان يكون اكبر من 2 حروف',
            'tag_ar.max' =>'الكلمه الدلاليه  يجب ان يكون اقل من 50 حرف',
            'tag_en.required' =>'اسم الحساب مطلوب',
            'tag_en.unique'=> 'هذا الحساب مستخدم من قبل',
            'tag_en.min' =>'الكلمه الدلاليه يجب ان يكون اكبر من 2 حروف',
            'tag_en.max' =>'الكلمه الدلاليه  يجب ان يكون اقل من 50 حرف',
  
              ];
      }
}
