<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'category_name_ar' =>'required|min:2|max:50|unique:categorys,category_name_ar',
            'category_name_en' =>'required|min:2|max:50|unique:categorys,category_name_en',
              ];
      }
  
      public function messages()
      {
      
          return [
            'category_name_ar.required' =>'اسم القسم مطلوب',
            'category_name_ar.unique' => 'هذا الرقم مستخدم من قبل ',
            'category_name_ar.min' =>'اسم القسم يجب ان يكون اكبر من 2 حروف',
            'category_name_ar.max' =>'اسم القسم  يجب ان يكون اقل من 50 حرف',
            'category_name_en.required' =>'اسم الحساب مطلوب',
            'category_name_en.unique'=> 'هذا الحساب مستخدم من قبل',
            'category_name_en.min' =>'اسم القسم يجب ان يكون اكبر من 2 حروف',
            'category_name_en.max' =>'اسم القسم  يجب ان يكون اقل من 50 حرف',
  
              ];
      }
}
