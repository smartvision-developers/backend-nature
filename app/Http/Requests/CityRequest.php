<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'city_name_ar' =>'required|min:2|max:50|unique:citys,city_name_ar',
              ];
      }
  
      public function messages()
      {
      
          return [
            'city_name_ar.required' =>'الكلمه الدلاليه مطلوب',
            'city_name_ar.unique' => 'هذا الرقم مستخدم من قبل ',
            'city_name_ar.min' =>'الكلمه الدلاليه يجب ان يكون اكبر من 2 حروف',
            'city_name_ar.max' =>'الكلمه الدلاليه  يجب ان يكون اقل من 50 حرف',
  
              ];
      }
}
