<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'app_name' =>'required|min:5|max:30',
            'logo_image' =>'required|image|mimes:jpg,svg,png',
            'our_service' =>'required|min:5',
            'about_us' =>'required',
            'phone' =>'required',
            'pledge' =>'required',
            'terms_conditions' =>'required',
            'download_app' =>'required',
            'link_facebook' =>'required',
            'link_twitter' =>'required',
            'link_instagram' =>'required',
  
              ];
      }
  
      public function messages()
      {
      
          return [
            'app_name.required' =>'اسم  الاعدادات مطلوب',
            'app_name.min' =>'اسم  يجب ان يكون اكبر من 5 حروف',
            'app_name.max' =>'اسم  يجب ان يكون اقل من 30 حرف',
            'logo_image.required' =>'اللوجو مطلوب',
            'our_service.required' =>'خدماتنا مطلوبه',
            'about_us.required' =>'عن التطبيق مطلوب',
            'download_app.required' =>' رابط التطبيق مطلوبه',
            'phone.required' =>' رقم الجوال مطلوب',
            'pledge.required' =>' التعهد مطلوب',
            'terms_conditions.required' =>' الشروط والاحكام مطلوبه',
            'link_facebook.required' =>' رابط الفيس بوك مطلوبه',
            'link_twitter.required' =>'رابط تويتر مطلوب',
            'link_instagram.required' =>'رابط الانستجرام مطلوبه',
  
              ];
      }
}
