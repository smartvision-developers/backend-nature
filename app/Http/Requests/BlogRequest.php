<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'blog_title_ar' =>'required|min:2|max:50|unique:blogs,blog_title_ar',
            'blog_title_en' =>'required|min:2|max:50|unique:blogs,blog_title_en',
           
            'blog_text_en' =>'required|min:2|max:500|unique:blogs,blog_text_en',
            'blog_text_en' =>'required|min:2|max:500|unique:blogs,blog_text_en',

            'blog_description_en' =>'required|min:2|unique:blogs,blog_description_en',
            'blog_description_en' =>'required|min:2|unique:blogs,blog_description_en',

              ];
      }
  
      public function messages()
      {
      
          return [
            'blog_title_ar.required' =>'عنوان المقاله مطلوب',
            'blog_title_ar.unique' => 'هذا الرقم مستخدم من قبل ',
            'blog_title_ar.min' =>'عنوان المقاله يجب ان يكون اكبر من 2 حروف',
            'blog_title_ar.max' =>'عنوان المقاله  يجب ان يكون اقل من 50 حرف',
            'blog_title_en.required' =>'اسم الحساب مطلوب',
            'blog_title_en.unique'=> 'هذا الحساب مستخدم من قبل',
            'blog_title_en.min' =>'عنوان المقاله يجب ان يكون اكبر من 2 حروف',
            'blog_title_en.max' =>'عنوان المقاله  يجب ان يكون اقل من 50 حرف',
            
            'blog_text_ar.required' =>'وصف بسيط مطلوب',
            'blog_text_ar.unique' => 'هذا الرقم مستخدم من قبل ',
            'blog_text_ar.min' =>'وصف بسيط يجب ان يكون اكبر من 2 حروف',
            'blog_text_ar.max' =>'وصف بسيط  يجب ان يكون اقل من 250 حرف',
            'blog_text_en.required' =>'اسم الحساب مطلوب',
            'blog_text_en.unique'=> 'هذا الحساب مستخدم من قبل',
            'blog_text_en.min' =>'وصف بسيط يجب ان يكون اكبر من 2 حروف',
            'blog_text_en.max' =>'وصف بسيط  يجب ان يكون اقل من 250 حرف',

            'blog_description_ar.required' =>'وصف مفصل مطلوب',
            'blog_description_ar.unique' => 'هذا الرقم مستخدم من قبل ',
            'blog_description_ar.min' =>'وصف مفصل يجب ان يكون اكبر من 2 حروف',
            'blog_description_en.required' =>'اسم الحساب مطلوب',
            'blog_description_en.unique'=> 'هذا الحساب مستخدم من قبل',
            'blog_description_en.min' =>'وصف مفصل يجب ان يكون اكبر من 2 حروف',


              ];
      }
}
