<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShopSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'shipping_info' =>'required|min:2|max:50|unique:shop_settings,shipping_info',
            'name_en' =>'required|min:2|max:50|unique:shop_settings,name_en',
              ];
      }
  
      public function messages()
      {
      
          return [
            'shipping_info.required' =>'اسم الحيوان مطلوب',
            'shipping_info.unique' => 'هذا الرقم مستخدم من قبل ',
            'shipping_info.min' =>'اسم الحيوان يجب ان يكون اكبر من 2 حروف',
            'shipping_info.max' =>'اسم الحيوان  يجب ان يكون اقل من 50 حرف',
            'name_en.required' =>'اسم الحساب مطلوب',
            'name_en.unique'=> 'هذا الحساب مستخدم من قبل',
            'name_en.min' =>'اسم الحيوان يجب ان يكون اكبر من 2 حروف',
            'name_en.max' =>'اسم الحيوان  يجب ان يكون اقل من 50 حرف',
  
              ];
      }
}
