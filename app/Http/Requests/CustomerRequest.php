<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' =>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:customers,phone',
            'password' =>'required|
               min:6|unique:customers,password|required_with:repassword|same:repassword',
            'repassword' => 'required|min:6'
              ];
      }
  
      public function messages()
      {
      
          return [
            'phone.required' =>'رقم الجوال مطلوب',
            'phone.unique' =>'هذا الرقم مسجل من قبل',
            'phone.regex' =>'تأكد من ادخال رقم جوال صحيح',
            'password.required' =>'كلمه المرور مطلوبه',
            'password.min' =>'كلمه المرور لابد ان تكون أكثر من 6 حروف أو أرقام',
            'password.required_with' =>'تأكد من تطابق كلمه المرور',
            'repassword.required' =>'تأكيد كلمه المرور مطلوبه',
            'repassword.min' =>'كلمه المرور لابد ان تكون أكثر من 6 حروف أو أرقام',

            ];
      }
}
