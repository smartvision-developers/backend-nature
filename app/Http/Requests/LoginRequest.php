<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'           => 'required|unique:customers,password',
            'phone'           => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            ];
    }

    public function messages()
    {
        return [
            'phone.required'     => __('translate.phone-required'),
            'phone.unique' => __('translate.phone-unique'),
            'phone.phone'   =>__('translate.phone-phone'),
            'password.required'  => __('translate.password-required'),
            // 'password.min'       => 'كلمة المرور لا تقل عن 6 أرقام',
            'password.unique' => __('translate.password-unique'),
            ];
    }
}
