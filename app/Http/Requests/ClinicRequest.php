<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClinicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'clinic_title_ar' =>'required|min:2|max:50|unique:clinics,clinic_title_ar',
            'clinic_title_en' =>'required|min:2|max:50|unique:clinics,clinic_title_en',
           
            'clinic_description_en' =>'required|min:2|unique:clinics,clinic_description_en',
            'clinic_description_en' =>'required|min:2|unique:clinics,clinic_description_en',

              ];
      }
  
      public function messages()
      {
      
          return [
            'clinic_title_ar.required' =>'عنوان العياده مطلوب',
            'clinic_title_ar.unique' => 'هذا العياده مستخدم من قبل ',
            'clinic_title_ar.min' =>'عنوان العياده يجب ان يكون اكبر من 2 حروف',
            'clinic_title_ar.max' =>'عنوان العياده  يجب ان يكون اقل من 50 حرف',
            'clinic_title_en.required' =>'اسم الحساب مطلوب',
            'clinic_title_en.unique'=> 'هذا الحساب مستخدم من قبل',
            'clinic_title_en.min' =>'عنوان العياده يجب ان يكون اكبر من 2 حروف',
            'clinic_title_en.max' =>'عنوان العياده  يجب ان يكون اقل من 50 حرف',
            
            'clinic_description_ar.required' =>'وصف مفصل مطلوب',
            'clinic_description_ar.unique' => 'هذا العياده مستخدم من قبل ',
            'clinic_description_ar.min' =>'وصف مفصل يجب ان يكون اكبر من 2 حروف',
            'clinic_description_en.required' =>'اسم الحساب مطلوب',
            'clinic_description_en.unique'=> 'هذا الحساب مستخدم من قبل',
            'clinic_description_en.min' =>'وصف مفصل يجب ان يكون اكبر من 2 حروف',


              ];
      }
}
