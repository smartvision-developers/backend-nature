<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TrademarkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'name' =>'required|min:2|max:50',
              ];
      }
  
      public function messages()
      {
      
          return [
            'name_ar.required' =>'اسم الحيوان مطلوب',
            'name_ar.min' =>'اسم الحيوان يجب ان يكون اكبر من 2 حروف',
            'name_ar.max' =>'اسم الحيوان  يجب ان يكون اقل من 50 حرف',  
              ];
      }
}
