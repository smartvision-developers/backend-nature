<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'slider_text_ar' =>'required|min:2|max:250',
            'slider_text_en' =>'required|min:2|max:250',
              ];
      }
  
      public function messages()
      {
      
          return [            
            'slider_text_ar.required' =>'وصف بسيط مطلوب',
            'slider_text_ar.min' =>'وصف بسيط يجب ان يكون اكبر من 2 حروف',
            'slider_text_ar.max' =>'وصف بسيط  يجب ان يكون اقل من 250 حرف',
            'slider_text_en.required' =>'اسم الحساب مطلوب',
            'slider_text_en.min' =>'وصف بسيط يجب ان يكون اكبر من 2 حروف',
            'slider_text_en.max' =>'وصف بسيط  يجب ان يكون اقل من 250 حرف',


              ];
      }
}
