<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShopCouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'coupon_code' =>'required|min:2|max:6|unique:categorys,coupon_code',
            'coupon_name' =>'required|min:2|max:50|unique:categorys,coupon_name',
            'coupon_discount' =>'required|numeric',

              ];
      }
  
      public function messages()
      {
      
          return [
            'coupon_code.required' =>'اسم القسم مطلوب',
            'coupon_code.unique' => 'هذا الرقم مستخدم من قبل ',
            'coupon_code.min' =>'اسم القسم يجب ان يكون اكبر من 2 حروف',
            'coupon_code.max' =>'اسم القسم  يجب ان يكون اقل من 6 حرف',
            'coupon_name.required' =>'اسم الحساب مطلوب',
            'coupon_name.unique'=> 'هذا الحساب مستخدم من قبل',
            'coupon_name.min' =>'اسم القسم يجب ان يكون اكبر من 2 حروف',
            'coupon_name.max' =>'اسم القسم  يجب ان يكون اقل من 50 حرف',
            'coupon_discount.required' =>'نسبه الخصم مطلوبه',
            'coupon_discount.numeric' =>'يوجد خطأ',
  
              ];
      }
}
