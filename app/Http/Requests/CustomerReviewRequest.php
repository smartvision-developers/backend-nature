<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'customer_name' =>'required|min:2|max:50',
            'customer_review' =>'required',
              ];
      }
  
      public function messages()
      {
      
          return [
            'customer_name.required' =>'اسم القسم مطلوب',
            'customer_name.min' =>'اسم القسم يجب ان يكون اكبر من 2 حروف',
            'customer_name.max' =>'اسم القسم  يجب ان يكون اقل من 50 حرف',
            'customer_review.required' =>'اسم الحساب مطلوب',
  
              ];
      }
}
