<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnimalTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'name_ar' =>'required|min:2|max:50|unique:animal_types,name_ar',
            'name_en' =>'required|min:2|max:50|unique:animal_types,name_en',
              ];
      }
  
      public function messages()
      {
      
          return [
            'name_ar.required' =>'اسم الحيوان مطلوب',
            'name_ar.unique' => 'هذا الرقم مستخدم من قبل ',
            'name_ar.min' =>'اسم الحيوان يجب ان يكون اكبر من 2 حروف',
            'name_ar.max' =>'اسم الحيوان  يجب ان يكون اقل من 50 حرف',
            'name_en.required' =>'اسم الحساب مطلوب',
            'name_en.unique'=> 'هذا الحساب مستخدم من قبل',
            'name_en.min' =>'اسم الحيوان يجب ان يكون اكبر من 2 حروف',
            'name_en.max' =>'اسم الحيوان  يجب ان يكون اقل من 50 حرف',
  
              ];
      }
}
