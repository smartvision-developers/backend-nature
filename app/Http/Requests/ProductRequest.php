<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'product_name_ar' =>'required|min:2|max:255',
            'product_name_en' =>'required|min:2|max:255',
              ];
      }
  
      public function messages()
      {
      
          return [
            'product_name_ar.required' =>'اسم القسم مطلوب',
            // 'product_name_ar.unique' => 'هذا الرقم مستخدم من قبل ',
            'product_name_ar.min' =>'اسم القسم يجب ان يكون اكبر من 2 حروف',
            'product_name_ar.max' =>'اسم القسم  يجب ان يكون اقل من 255 حرف',
            'product_name_en.required' =>'اسم الحساب مطلوب',
            // 'product_name_en.unique'=> 'هذا الحساب مستخدم من قبل',
            'product_name_en.min' =>'اسم القسم يجب ان يكون اكبر من 2 حروف',
            'product_name_en.max' =>'اسم القسم  يجب ان يكون اقل من 255 حرف',
  
              ];
      }
}
