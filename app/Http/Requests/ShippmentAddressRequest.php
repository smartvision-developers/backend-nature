<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShippmentAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' =>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:shippment_addresses,phone',
            'location_id' =>'required',
            'city_id' =>'required',
            'region_id' =>'required',
            'address_type' =>'required',
              ];
      }
  
      public function messages()
      {
      
          return [
            'phone.required' =>'رقم الجوال مطلوب',
            'phone.unique' =>'هذا الرقم مسجل من قبل',
            'phone.regex' =>'تأكد من ادخال رقم جوال صحيح',

            ];
      }
}
