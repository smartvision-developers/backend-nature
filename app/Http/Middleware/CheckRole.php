<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Request;
use Session;
use RealRashid\SweetAlert\Facades\Alert;
use Closure;
class CheckRole extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function handle($request, Closure $next, $role)
    {
        if (! $request->user()->isAbleTo($role)) {
            abort(401, 'This action is unauthorized.');
        }
        return $next($request);
    }
}
