<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Request;
use Session;
use RealRashid\SweetAlert\Facades\Alert;
class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
       if (! $request->expectsJson()) {
            if(Request::is('admin/*') )
            { 
                return route('admin.login');
            }
            else{
                Alert::error(session('error'),'لابد من التسجيل أولا');
            return route('site');
            //Session::flash('error', (trans('يجب التسجيل أولا')));
            }
        }
    }
}
