<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Clinic;
use App\Models\ClinicService;
use App\Http\Requests\ClinicRequest;
use Session;
use Auth;
class ClinicController extends Controller 
{

  public function index()
  {
    $clinic=Clinic::first();
       $get_services=ClinicService::get();

    return view('admin.clinics.index',compact('clinic','get_services'));   
  }

//  public function create()
//  {
//   return view('admin.clinics.create');   
//   }

// public function store(ClinicRequest $request)
// {
//  try{
//    $file_path="";
//    if($request->has('clinic_image'))
//    {
//     $file_path=$this->upload_image('clinics', $request->clinic_image);
//   }
//   else
//   {
//     $file_path='images/default2.png';
//   }
//   $addClinic=Clinic::create($request->except('user_id','clinic_image'));
//   $addClinic->user_id = Auth::user()->id;
//   $addClinic->clinic_image = $file_path;

//   $addClinic->save();
//   return redirect()->route('clinics.index')->with(['success' => 'تم الحفظ بنجاح']);
// }
// catch (\Exception $ex)
// {
//   return redirect()->route('clinics.create')->with(['error' => 'يوجد خطأ']);

// }
// }



  public function activate(Request $request, $id)
  {
    $status = ClinicService::findOrFail($id);
    $status->update(['status' => 1]);

    Session::flash('success','القسم مفعل الان');
    return Redirect('admin/clinics');
  }

  public function deactivate(Request $request, $id)
  {
   $status = ClinicService::findOrFail($id);
   $status->update(['status' => 0]);
   Session::flash('success','القسم غير مفعل الان');
   return Redirect('admin/clinics');
 }


public function addService(Request $request , $id)
{
   $clinic=Clinic::find($id);
  // dd($id);
   $get_services=ClinicService::where('clinic_id', $id)->get();
   if($get_services != null)
   {
     // dd($get_services);

    $add_tags=ClinicService::create([
      'service_description_ar' =>$request->service_description_ar,
      'service_description_en' =>$request->service_description_en,
      'service_price' =>$request->service_price,
      'clinic_id' => $id,
     ]);
  }
    return redirect()->back()->with(['success' => 'تم الحفظ بنجاح']);

}
public function edit($id)
{
 $clinic=Clinic::find($id);
 if(! $clinic)
 {
  return redirect()->route('clinics.index')->with(['error' => 'لا توجد رساله بهذا العنوان']);
} 
return view('admin.clinics.edit',compact('clinic'));

}

public function update(Request $request, $id)
{
  try{

    $clinic_data=Clinic::find($id);
 //dd( $clinic_data);
        //for save image
    $file_path=$clinic_data->clinic_image;
    if($request->has('clinic_image'))
    {
      $img=str_replace('public', '', $file_path);
      unlink(public_path() .'/'. $img );

      $file_path=$this->upload_image('clinics', $request->clinic_image);

    }
    Clinic::where('id',$id)->update([
      'clinic_title_ar' => $request['clinic_title_ar'],
      'clinic_title_en' => $request['clinic_title_en'],
      'clinic_description_ar' => $request['clinic_description_ar'],
      'clinic_description_en' => $request['clinic_description_en'],
      'clinic_image' => $file_path
    ]);

    return redirect()->route('clinics.index')->with(['success' => 'تم التعديل بنجاح']);
  }
  catch (\Exception $ex)
  {
    return redirect()->route('clinics.index')->with(['error' => 'يوجد خطأ']);

  }
}


// public function destroy($id)
// {
//  try{

//   $clinic=Clinic::find($id);
//   if( $clinic->clinic_image != 'images/default2.png')
//   {
//     $img=str_replace('public', '', $clinic->clinic_image);
//     unlink(public_path() .'/'. $img );

//             //   unlink(public_path() .'/'. $category->clinic_image );
//   }

//   if(! $clinic)
//   {
//     return  redirect()->route('clinics', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
//   } 

//   $clinic ->delete();

//   return redirect()->route('clinics.index')->with(['success' => 'تم الحذف بنجاح']);
// }
// catch (\Exception $ex)
// {
//   return redirect()->route('clinics.index')->with(['error' => 'يوجد خطأ']);

// }}

public function destroy_service($id)
{
 try{

  $service=ClinicService::find($id);
  if(! $service)
  {
    return  redirect()->route('clinics', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
  } 

  $service ->delete();

  return redirect()->route('clinics.index')->with(['success' => 'تم الحذف بنجاح']);
  }
  catch (\Exception $ex)
  {
    return redirect()->route('clinics.index')->with(['error' => 'يوجد خطأ']);

  }
}




function upload_image($folder, $image)
{
  $image->store('/', $folder);
  $filename=$image->hashName();
  $path= 'public/images/'. $folder .'/'. $filename;

  return $path;
}

}

?>