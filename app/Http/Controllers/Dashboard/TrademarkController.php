<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Trademark;
use App\Http\Requests\TrademarkRequest;
use Session;
class TrademarkController extends Controller 
{

  public function index()
  {
    $trademarks=Trademark::get();
    return view('admin.trademarks.index',compact('trademarks'));   
  }
  public function activate(Request $request, $id)
  {
    $status = Trademark::findOrFail($id);
    $status->update(['status' => 1]);

    Session::flash('success','القسم مفعل الان');
    return Redirect('admin/trademarks');
  }

  public function deactivate(Request $request, $id)
  {
   $status = Trademark::findOrFail($id);
   $status->update(['status' => 0]);
   Session::flash('success','القسم غير مفعل الان');
   return Redirect('admin/trademarks');
 }
 public function create()
 {
  return view('admin.trademarks.create');   
 }


  public function store(TrademarkRequest $request)
  {
   try{
     $file_path="";
   if($request->has('image'))
   {
    $file_path=$this->upload_image('trademarks', $request->image);
  }
  else
  {
    $file_path='images/default2.png';
  }
    $addtrademark=Trademark::create($request->except('image'));
    $addtrademark->image= $file_path;
    $addtrademark->save();
    return redirect()->route('trademarks.index')->with(['success' => 'تم الحفظ بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('trademarks.create')->with(['error' => 'يوجد خطأ']);

    }
  }
  

  public function edit($id)
  {
   $trademark=Trademark::find($id);
   if(! $trademark)
   {
      return redirect()->route('trademarks.index')->with(['error' => 'لا توجد رساله بهذا العنوان']);
    } 
    return view('admin.trademarks.edit',compact('trademark'));

  }

  public function update(Request $request, $id)
  {
    try{

      $trademark_data=Trademark::find($id);
        $file_path=$trademark_data->image;
    if($request->has('image'))
    {
      // $img=str_replace('public', '', $file_path);
      // unlink(public_path() .'/'. $img );

      $file_path=$this->upload_image('trademarks', $request->image);

    }
      if(! $trademark_data)
      {
        return  redirect()->route('trademarks.edit', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
      }
      Trademark::where('id',$id)->update([
        'name' => $request['name'],
        'image' => $file_path
      ]);

      return redirect()->route('trademarks.index')->with(['success' => 'تم التعديل بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('trademarks.index')->with(['error' => 'يوجد خطأ']);

    }
  }


  public function destroy($id)
  {
   try{

    $trademark=Trademark::find($id);
    if(! $trademark)
    {
      return  redirect()->route('trademarks', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
    } 

    $trademark ->delete();

      return redirect()->route('trademarks.index')->with(['success' => 'تم الحذف بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('trademarks.index')->with(['error' => 'يوجد خطأ']);

    }
  }

  function upload_image($folder, $image)
  {
    $image->store('/', $folder);
    $filename=$image->hashName();
    $path= 'public/images/'. $folder .'/'. $filename;

    return $path;
  }

}

?>