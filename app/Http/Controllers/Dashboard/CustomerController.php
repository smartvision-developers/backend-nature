<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Http\Requests\CustomerRequest;
use Session;
class CustomerController extends Controller 
{

  public function index()
  {
    $customers=Customer::get();
    return view('admin.customers.index',compact('customers'));   
  }
 
 public function create()
 {
  return view('admin.customers.create');   
 }


  public function store(CustomerRequest $request)
  {
   try{
    $addcustomer=Customer::create([
      'customer' => $request['customer'],
      'customer_name' => $request['customer_name'],
      'job_title' => $request['job_title'],
    ]);
    return redirect()->route('customers.index')->with(['success' => 'تم الحفظ بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('customers.create')->with(['error' => 'يوجد خطأ']);

    }
  }
  
    public function show($id)
  {
   $customer=Customer::find($id);
   
   if(! $customer)
   {
      return redirect()->route('customers.index')->with(['error' => 'لا توجد رساله بهذا العنوان']);
    } 
    return view('admin.customers.profile',compact('customer'));

  }


  public function edit($id)
  {
   $customer=Customer::find($id);
   if(! $customer)
   {
      return redirect()->route('customers.index')->with(['error' => 'لا توجد رساله بهذا العنوان']);
    } 
    return view('admin.customers.edit',compact('customer'));

  }

  public function update(Request $request, $id)
  {
    try{

      $customer_data=Customer::find($id);
      if(! $customer_data)
      {
        return  redirect()->route('customers.edit', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
      }
      Customer::where('id',$id)->update([
        'customer' => $request['customer'],
        'customer_name' => $request['customer_name'],
        'job_title' => $request['job_title'],
      ]);

      return redirect()->route('customers.index')->with(['success' => 'تم التعديل بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('customers.index')->with(['error' => 'يوجد خطأ']);

    }
  }


  public function destroy($id)
  {
   try{

    $customer=Customer::find($id);
    if(! $customer)
    {
      return  redirect()->route('customers', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
    } 

    $customer ->delete();

      return redirect()->route('customers.index')->with(['success' => 'تم الحذف بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('customers.index')->with(['error' => 'يوجد خطأ']);

    }
  }

}

?>