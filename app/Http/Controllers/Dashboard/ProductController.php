<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Store;
use App\Models\Cart;
use App\Models\Category_Product;
use App\Models\ProductDiscount;
use App\Models\ProductImage;
use App\Models\AnimalType;
use App\Models\Animal_Category;
use App\Http\Requests\ProductRequest;
use Session;
use Auth;
class ProductController extends Controller 
{

  public function index()
  {
    $products=Product::get();
    
    return view('admin.products.index',compact('products'));   
  }
  public function activate(Request $request, $id)
  {
    $status = Product::findOrFail($id);
    $status->update(['status' => 1]);

    Session::flash('success','القسم مفعل الان');
    return Redirect('admin/products');
  }

  public function deactivate(Request $request, $id)
  {
   $status = Product::findOrFail($id);
   $status->update(['status' => 0]);
   Session::flash('success','القسم غير مفعل الان');
   return Redirect('admin/products');
 }

   public function add_star_product(Request $request, $id)
  {
    $status = Product::findOrFail($id);
    $status->update(['star' => 1]);

    Session::flash('success','تم اضافه المنتج للمنتجات المميزه');
    return Redirect('admin/products');
  }

  public function del_star_product(Request $request, $id)
  {
   $status = Product::findOrFail($id);
   $status->update(['star' => 0]);
   Session::flash('success','تم حذف المنتج من المنتجات المميزه');
   return Redirect('admin/products');
 }


 public function create()
 {
  $categorys= Category::get();
    $animals= AnimalType::get();
    $animals_cat= Animal_Category::select('id','animal_type_id','category_id')->groupBy('animal_type_id')->get();

  return view('admin.products.create', compact('categorys','animals','animals_cat'));   
 }

  public function store(ProductRequest $request)
  {
    try{
    $addProduct=Product::create($request->except('user_id', 'exclusive'));
    $addProduct->user_id=Auth::user()->id;
    if($request->exclusive != null)
    {
    $addProduct->exclusive=1;
  }
    $addProduct->save();
  
    $quantProduct=Store::create([
      'product_quantity' => $request['product_quantity'],
      'product_id' => $addProduct->id,
    ]);

   $Category_Product=Category_Product::create([
      'category_id' => $request['category_id'],
      'product_id' => $addProduct->id,
      'animal_id' => $request['animal_id'],
    ]);

    $animal_cats=Animal_Category::where('category_id', $request['category_id'])->where('animal_type_id',$request['animal_id'])->first();
    if(!$animal_cats)
    {
       Animal_Category::create([
          'category_id' => $request['category_id'],
          'animal_type_id'  => $request['animal_id'],
       ]);
     }
    $num = 0;

    //dd($request->file('product_image') );
    foreach ($request->file('product_image') as $img) {
      $path = public_path();
            $destinationPath = $path.'/images/products/'; // upload path
            $extension = 'jpg'; // getting img extension
            $name = time().''.rand(11111,99999).'.'.$extension; // renameing img
            $img->move($destinationPath, $name); // uploading file to given path
            $final_name = $destinationPath.$name;
            $image = [
              'product_image'  => $name,
              'product_id' => $addProduct->id
            ];
            ProductImage::create($image);
            $num++;
          }
 
    return redirect()->route('products.index')->with(['success' => 'تم الحفظ بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('products.create')->with(['error' => 'يوجد خطأ']);

    }
  }
  
  public function show($id)
  {
     $product=Product::find($id);
     return view('admin.products.profile',compact('product'));

  }
  public function edit($id)
  {
      $categorys= Category::get();
    $animals= AnimalType::get();

   $product=Product::find($id);
   if(! $product)
   {
      return redirect()->route('products.index')->with(['error' => 'لا توجد رساله بهذا العنوان']);
    } 
    return view('admin.products.edit',compact('product','categorys', 'animals'));

  }

  public function update(Request $request, $id)
  {
   try{

      $product_data=Product::find($id);
      if(! $product_data)
      {
        return  redirect()->route('products.edit', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
      }
      $update_prod =Product::where('id',$id)->update([
        'product_name_ar' => $request['product_name_ar'],
        'product_name_en' => $request['product_name_en'],
        'product_code' => $request['product_code'],
        'product_unitprice' => $request['product_unitprice'],
        'product_description_ar' => $request['product_description_ar'],
        'product_description_en' => $request['product_description_en'],
        'product_longdescription_ar' => $request['product_longdescription_ar'],
        'product_longdescription_en' => $request['product_longdescription_en'],
        'product_oldprice' => $request['product_oldprice'],
      ]);
      if($request['exclusive'] != null)
      {
       Product::where('id',$id)->update([
        'exclusive' => 1
      ]);
      }
 
      $cart_pro=Cart::where('product_id',$id)->first();
      //dd($cart_pro);
      if(!$cart_pro)
      {
        $quantProduct=Store::where('product_id', $id)->update([
          'product_quantity' => $request['product_quantity'],
          'product_id' => $id,
        ]);
      }
      else
      {
        return  redirect()->route('products.edit', $id)->with(['error'=> 'لا يمكن تغيير كميه المنتج  لقدتم  استخدام المنتج']);
      }
  
     $Category_Product=Category_Product::where('product_id', $id)->update([
        'category_id' => $request['category_id'],
        'product_id' => $id,
        // 'animal_id' => $request['animal_id'],
      ]);

   $num = 0;

    //dd($request->file('product_image') );
   if($request->file('product_image'))
   {
    foreach ($request->file('product_image') as $img) {
      $path = public_path();
            $destinationPath = $path.'/images/products/'; // upload path
            $extension = 'jpg'; // getting img extension
            $name = time().''.rand(11111,99999).'.'.$extension; // renameing img
            $img->move($destinationPath, $name); // uploading file to given path
            $final_name = $destinationPath.$name;
            $image = [
              'product_image'  => $name,
              'product_id' => $id
            ];
            ProductImage::create($image);
            $num++;
          }
}


      return redirect()->route('products.index')->with(['success' => 'تم التعديل بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('products.index')->with(['error' => 'يوجد خطأ']);

    }
  }


  public function destroy($id)
  {
   try{

    $product=Product::find($id);
    if(! $product)
    {
      return  redirect()->route('products', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
    } 

    $product ->delete();

      return redirect()->route('products.index')->with(['success' => 'تم الحذف بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('products.index')->with(['error' => 'يوجد خطأ']);

    }
  }



    public function getAnimals( Request $request)
    {
        $html = view('admin.products._getanimals')->render();
             //   dd($html);

        return $html;
    }
}

?>