<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\City;
use App\Http\Requests\CityRequest;
use Session;
use Auth;
class CityController extends Controller 
{

  public function index()
  {
    $citys=City::orderBy('id','DESC')->get();
    return view('admin.citys.index',compact('citys'));   
  }
 //  public function activate(Request $request, $id)
 //  {
 //    $status = City::findOrFail($id);
 //    $status->update(['status' => 1]);

 //    Session::flash('success','الكلمه الدلاليه مفعل الان');
 //    return Redirect('admin/citys');
 //  }

 //  public function deactivate(Request $request, $id)
 //  {
 //   $status = City::findOrFail($id);
 //   $status->update(['status' => 0]);
 //   Session::flash('success','الكلمه الدلاليه غير مفعل الان');
 //   return Redirect('admin/citys');
 // }
 public function create()
 {
  return view('admin.citys.create');   
 }


  public function store(CityRequest $request)
  {
   try{
    $addCity=City::create([
      'city_name_ar' => $request['city_name_ar'],
    ]);
    return redirect()->route('citys.index')->with(['success' => 'تم الحفظ بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('citys.create')->with(['error' => 'يوجد خطأ']);

    }
  }
  

  public function edit($id)
  {
   $city=City::find($id);
   if(! $city)
   {
      return redirect()->route('citys.index')->with(['error' => 'لا توجد رساله بهذا العنوان']);
    } 
    return view('admin.citys.edit',compact('city'));

  }

  public function update(Request $request, $id)
  {
    try{

      $city_data=City::find($id);
      if(! $city_data)
      {
        return  redirect()->route('citys.edit', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
      }
      City::where('id',$id)->update([
        'city_name_ar' => $request['city_name_ar'],
      ]);

      return redirect()->route('citys.index')->with(['success' => 'تم التعديل بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('citys.index')->with(['error' => 'يوجد خطأ']);

    }
  }


  public function destroy($id)
  {
   try{

    $city=City::find($id);
    if(! $city)
    {
      return  redirect()->route('citys', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
    } 


    if(!($city->Region->isEmpty()))
    {
       return  redirect()->route('citys.index')->with(['error'=> 'لا يمكن الحذف هذه البيانات متعلقه بأخري ']);
    }
    $city ->delete();

      return redirect()->route('citys.index')->with(['success' => 'تم الحذف بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('citys.index')->with(['error' => 'يوجد خطأ']);

    }
  }

}

?>