<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CustomerReview;
use App\Http\Requests\CustomerReviewRequest;
use Session;
class CustomerReviewController extends Controller 
{

  public function index()
  {
    $customer_reviews=CustomerReview::get();
    return view('admin.customer_reviews.index',compact('customer_reviews'));   
  }
 
 public function create()
 {
  return view('admin.customer_reviews.create');   
 }


  public function store(CustomerReviewRequest $request)
  {
   try{
    $addcustomer_review=CustomerReview::create([
      'customer_review' => $request['customer_review'],
      'customer_name' => $request['customer_name'],
      'job_title' => $request['job_title'],
    ]);
    return redirect()->route('customer_reviews.index')->with(['success' => 'تم الحفظ بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('customer_reviews.create')->with(['error' => 'يوجد خطأ']);

    }
  }
  

  public function edit($id)
  {
   $customer_review=CustomerReview::find($id);
   if(! $customer_review)
   {
      return redirect()->route('customer_reviews.index')->with(['error' => 'لا توجد رساله بهذا العنوان']);
    } 
    return view('admin.customer_reviews.edit',compact('customer_review'));

  }

  public function update(Request $request, $id)
  {
    try{

      $customer_review_data=CustomerReview::find($id);
      if(! $customer_review_data)
      {
        return  redirect()->route('customer_reviews.edit', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
      }
      CustomerReview::where('id',$id)->update([
        'customer_review' => $request['customer_review'],
        'customer_name' => $request['customer_name'],
        'job_title' => $request['job_title'],
      ]);

      return redirect()->route('customer_reviews.index')->with(['success' => 'تم التعديل بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('customer_reviews.index')->with(['error' => 'يوجد خطأ']);

    }
  }


  public function destroy($id)
  {
   try{

    $customer_review=CustomerReview::find($id);
    if(! $customer_review)
    {
      return  redirect()->route('customer_reviews', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
    } 

    $customer_review ->delete();

      return redirect()->route('customer_reviews.index')->with(['success' => 'تم الحذف بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('customer_reviews.index')->with(['error' => 'يوجد خطأ']);

    }
  }

}

?>