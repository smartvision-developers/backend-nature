<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PrivacyPolicy;
use Session;
class PrivacyPolicyController extends Controller 
{

  public function index()
  {
    $privacy_policy=PrivacyPolicy::first();
    return view('admin.privacy_policy.index',compact('privacy_policy'));   
  }

  public function update(Request $request, $id)
  {
    try{

      $privacy_data=PrivacyPolicy::find($id);
      if(! $privacy_data)
      {
        return  redirect()->route('privacy_policy.edit', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
      }
      PrivacyPolicy::where('id',$id)->update([
        'privacy_policy_ar' => $request['privacy_policy_ar'],
        'privacy_policy_en' => $request['privacy_policy_en'],
        'term_of_use_ar' => $request['term_of_use_ar'],
        'term_of_use_en' => $request['term_of_use_en'],
      ]);

      return redirect()->route('privacy_policy.index')->with(['success' => 'تم التعديل بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('privacy_policy.index')->with(['error' => 'يوجد خطأ']);

    }
  }


  public function destroy($id)
  {
   try{

    $shop_setting=PrivacyPolicy::find($id);
    if(! $shop_setting)
    {
      return  redirect()->route('privacy_policy', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
    } 

    $shop_setting ->delete();

      return redirect()->route('privacy_policy.index')->with(['success' => 'تم الحذف بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('privacy_policy.index')->with(['error' => 'يوجد خطأ']);

    }
  }

}

?>