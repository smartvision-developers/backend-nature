<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\BlogTag;
use App\Models\BlogBlogTag;
use App\Http\Requests\BlogRequest;
use App\Models\BlogComment;
use Session;
use Auth;
class BlogController extends Controller 
{

  public function __construct()
    {
        //$this->middleware('role:users');
    }

  public function index()
  {
    $blogs=Blog::get();
    return view('admin.blogs.index',compact('blogs'));   
  }
  public function activate(Request $request, $id)
  {
    $status = Blog::findOrFail($id);
    $status->update(['status' => 1]);

    Session::flash('success','القسم مفعل الان');
    return Redirect('admin/blogs');
  }

  public function deactivate(Request $request, $id)
  {
   $status = Blog::findOrFail($id);
   $status->update(['status' => 0]);
   Session::flash('success','القسم غير مفعل الان');
   return Redirect('admin/blogs');
 }

   public function create()
   {
    $blogs= Blog::get();
    return view('admin.blogs.create', compact('blogs'));   
  }

  public function store(BlogRequest $request)
  {
   try{
     $file_path="";
     if($request->has('blog_image'))
     {
      $file_path=$this->upload_image('blogs', $request->blog_image);
    }
    else
    {
      $file_path='images/default2.png';
    }
    $addblog=Blog::create($request->except('user_id','blog_image'));
    $addblog->user_id = Auth::user()->id;
    $addblog->blog_image = $file_path;

    $addblog->save();
    return redirect()->route('blogs.index')->with(['success' => 'تم الحفظ بنجاح']);
  }
  catch (\Exception $ex)
  {
    return redirect()->route('blogs.create')->with(['error' => 'يوجد خطأ']);

  }
  }

  public function show($id)
  {
   $blog=Blog::find($id);
   $tags=BlogTag::get();
   $blog_tags=BlogBlogTag::where('blog_id', $id)->get();
   $blog_comments=BlogComment::where('blog_id' ,$id)->get();
   return view('admin.blogs.profile',compact('blog', 'tags' , 'blog_tags', 'blog_comments'));
  }
public function addTag(Request $request , $id)
{
   $blog=Blog::find($id);
  // dd($id);
   $get_tags=BlogBlogTag::where('blog_id', $id)->get();
   if($get_tags != null)
   {
     // dd($get_tags);

       foreach ($get_tags as $key => $value) {
          $value->delete();
        }
  
   foreach($request->blog_tag_id as $key=> $value)
      {
    $add_tags=BlogBlogTag::create([
      'blog_tag_id' =>$value,
      'blog_id' => $id,
     ]);

      }
  }
    return redirect()->back()->with(['success' => 'تم الحفظ بنجاح']);

}
public function edit($id)
{
 $blog=Blog::find($id);
 if(! $blog)
 {
  return redirect()->route('blogs.index')->with(['error' => 'لا توجد رساله بهذا العنوان']);
} 
return view('admin.blogs.edit',compact('blog'));

}

public function update(Request $request, $id)
{
  try{

    $blog_data=Blog::find($id);
    if(! $blog_data)
    {
      return  redirect()->route('blogs.edit', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
    }
        //for save image
    $file_path=$blog_data->blog_image;
    if($request->has('blog_image'))
    {
      $img=str_replace('public', '', $file_path);
      //unlink(public_path() .'/'. $img );

      $file_path=$this->upload_image('blogs', $request->blog_image);

    }
    Blog::where('id',$id)->update([
      'blog_title_ar' => $request['blog_title_ar'],
      'blog_title_en' => $request['blog_title_en'],
      'blog_description_ar' => $request['blog_description_ar'],
      'blog_description_en' => $request['blog_description_en'],
      'blog_text_ar' => $request['blog_text_ar'],
      'blog_text_en' => $request['blog_text_en'],
      'blog_image' => $file_path
    ]);

    return redirect()->route('blogs.index')->with(['success' => 'تم التعديل بنجاح']);
  }
  catch (\Exception $ex)
  {
    return redirect()->route('blogs.index')->with(['error' => 'يوجد خطأ']);

  }
}


public function destroy($id)
{
 try{

  $blog=Blog::find($id);
  if( $blog->blog_image != 'images/default2.png')
  {
    $img=str_replace('public', '', $blog->blog_image);
    unlink(public_path() .'/'. $img );

            //   unlink(public_path() .'/'. $category->blog_image );
  }

  if(! $blog)
  {
    return  redirect()->route('blogs', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
  } 

  $blog ->delete();

  return redirect()->route('blogs.index')->with(['success' => 'تم الحذف بنجاح']);
}
catch (\Exception $ex)
{
  return redirect()->route('blogs.index')->with(['error' => 'يوجد خطأ']);

}
}

function upload_image($folder, $image)
{
  $image->store('/', $folder);
  $filename=$image->hashName();
  $path= 'public/images/'. $folder .'/'. $filename;

  return $path;
}

}

?>