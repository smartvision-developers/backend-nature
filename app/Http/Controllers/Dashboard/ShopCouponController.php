<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ShopCoupon;
use App\Http\Requests\ShopCouponRequest;
use Session;
class ShopCouponController extends Controller 
{

  public function index()
  {
    $shop_coupons=ShopCoupon::orderBy('id','DESC')->get();
    return view('admin.shop_coupons.index',compact('shop_coupons'));   
  }
  public function activate(Request $request, $id)
  {
    $status = ShopCoupon::findOrFail($id);
    $status->update(['status' => 1]);

    Session::flash('success','القسم مفعل الان');
    return Redirect('admin/shop_coupons');
  }

  public function deactivate(Request $request, $id)
  {
   $status = ShopCoupon::findOrFail($id);
   $status->update(['status' => 0]);
   Session::flash('success','القسم غير مفعل الان');
   return Redirect('admin/shop_coupons');
 }
 public function create()
 {
    return view('admin.shop_coupons.create');   
 }


  public function store(ShopCouponRequest $request)
  {
   try{
    $addshop_coupon=ShopCoupon::create([
      'coupon_code' => $request['coupon_code'],
      'coupon_name' => $request['coupon_name'],
      'coupon_discount' => $request['coupon_discount'],
    ]);
    return redirect()->route('shop_coupons.index')->with(['success' => 'تم الحفظ بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('shop_coupons.create')->with(['error' => 'يوجد خطأ']);

    }
  }
  

  public function edit($id)
  {
   $shop_coupon=ShopCoupon::find($id);
   if(! $shop_coupon)
   {
      return redirect()->route('shop_coupons.index')->with(['error' => 'لا توجد رساله بهذا العنوان']);
    } 
    return view('admin.shop_coupons.edit',compact('shop_coupon'));

  }

  public function update(Request $request, $id)
  {
    try{

      $shop_coupon_data=ShopCoupon::find($id);
      if(! $shop_coupon_data)
      {
        return  redirect()->route('shop_coupons.edit', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
      }
      ShopCoupon::where('id',$id)->update([
        'coupon_code' => $request['coupon_code'],
        'coupon_name' => $request['coupon_name'],
        'coupon_discount' => $request['coupon_discount'],

      ]);

      return redirect()->route('shop_coupons.index')->with(['success' => 'تم التعديل بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('shop_coupons.index')->with(['error' => 'يوجد خطأ']);

    }
  }


  public function destroy($id)
  {
   try{

    $shop_coupon=ShopCoupon::find($id);
    if(! $shop_coupon)
    {
      return  redirect()->route('shop_coupons', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
    } 
        $shop_coupon ->delete();

      return redirect()->route('shop_coupons.index')->with(['success' => 'تم الحذف بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('shop_coupons.index')->with(['error' => 'يوجد خطأ']);

    }
  }

}

?>