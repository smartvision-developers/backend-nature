<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Newsletter;
use Session;
class NewsletterController extends Controller 
{

  public function index()
  {
    $newsletters=Newsletter::orderBy('id','DESC')->get();
    return view('admin.newsletters.index',compact('newsletters'));   
  }


  public function destroy($id)
  {
   try{

    $newsletter=Newsletter::find($id);
    if(! $newsletter)
    {
      return  redirect()->route('newsletters', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
    } 

    $newsletter ->delete();

      return redirect()->route('newsletters.index')->with(['success' => 'تم الحذف بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('newsletters.index')->with(['error' => 'يوجد خطأ']);

    }
  }
}

?>