<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CustomerReview;
use App\Models\Cart;
use App\Models\ProductBestSeller;
use App\Models\Product;
use App\Models\Contact;

use App\Models\Customer;
use App\Models\Blog;
use Session;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;
use Auth;
use Carbon\Carbon;
use DB;
class AdminController extends Controller 
{

  public function index()
  {
            $chart_options = [
                'chart_title' => 'عدد العملاء المسجلين شهريا',
                'report_type' => 'group_by_date',
                'model' => 'App\Models\User',
                'group_by_field' => 'created_at',
                'group_by_period' => 'month',
                'chart_type' => 'bar',
                'filter_field' => 'created_at',
                // 'filter_days' => 30, // show only last 30 days
            ];

            $chart1 = new LaravelChart($chart_options);


            $chart_options = [
                'chart_title' => 'عدد التعليقات داخل المقالات',
                'report_type' => 'group_by_relationship',
                'model' => 'App\Models\BlogComment',
                'relationship_name' => 'Blog',
                'group_by_field' => 'blog_title_ar',
                'chart_type' => 'pie',
                'filter_field' => 'created_at',
                    ];

            $chart2 = new LaravelChart($chart_options);



              $chart_options = [
                  'chart_title' => 'متابعه المنتجات المضافه للسله أسبوعيا',
                  'chart_type' => 'line',
                  'report_type' => 'group_by_relationship',
                  'model' => 'App\Models\BlogComment',

                  'relationship_name' => 'Blog', // represents function user() on Transaction model
                  'group_by_field' => 'blog_title_ar', // users.name
                  'filter_field' => 'created_at',
                  'filter_days' => 30, // show only transactions for last 30 days
                  'filter_period' => 'week', // show only transactions for this week
                  
            ];


            $chart3 = new LaravelChart($chart_options);


            $product_favs = DB::table('product_favourites')->whereNull('deleted_at')
             ->select('product_favourites.*',DB::raw('COUNT(product_id) as count'))
             ->groupBy('customer_id')
             ->orderBy('count', 'DESC')
             ->paginate(6);

             $count_customers= Customer::whereNull('deleted_at')->count();
             $count_all_blogs= Blog::whereNull('deleted_at')->count();
             $count_customer_reviews=CustomerReview::whereNull('deleted_at')->count();
             $count_all_contacts = Contact::count();

            return view('admin.index', compact('chart1','chart2','product_favs', 'chart3','count_customers', 'count_all_blogs','count_customer_reviews','count_all_contacts'));
  }
}

?>