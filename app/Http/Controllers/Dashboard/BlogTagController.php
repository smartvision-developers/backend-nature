<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BlogTag;
use App\Http\Requests\TagRequest;
use Session;
use Auth;
class BlogTagController extends Controller 
{

  public function index()
  {
    $tags=BlogTag::orderBy('id','DESC')->get();
    return view('admin.tags.index',compact('tags'));   
  }
  public function activate(Request $request, $id)
  {
    $status = BlogTag::findOrFail($id);
    $status->update(['status' => 1]);

    Session::flash('success','الكلمه الدلاليه مفعل الان');
    return Redirect('admin/tags');
  }

  public function deactivate(Request $request, $id)
  {
   $status = BlogTag::findOrFail($id);
   $status->update(['status' => 0]);
   Session::flash('success','الكلمه الدلاليه غير مفعل الان');
   return Redirect('admin/tags');
 }
 public function create()
 {
  return view('admin.tags.create');   
 }


  public function store(TagRequest $request)
  {
   try{
    $addTag=BlogTag::create([
      'tag_ar' => $request['tag_ar'],
      'tag_en' => $request['tag_en'],
      'user_id' => Auth::user()->id,
    ]);
    return redirect()->route('tags.index')->with(['success' => 'تم الحفظ بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('tags.create')->with(['error' => 'يوجد خطأ']);

    }
  }
  

  public function edit($id)
  {
   $tag=BlogTag::find($id);
   if(! $tag)
   {
      return redirect()->route('tags.index')->with(['error' => 'لا توجد رساله بهذا العنوان']);
    } 
    return view('admin.tags.edit',compact('tag'));

  }

  public function update(Request $request, $id)
  {
    try{

      $tag_data=BlogTag::find($id);
      if(! $tag_data)
      {
        return  redirect()->route('tags.edit', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
      }
      BlogTag::where('id',$id)->update([
        'tag_ar' => $request['tag_ar'],
        'tag_en' => $request['tag_en'],
      ]);

      return redirect()->route('tags.index')->with(['success' => 'تم التعديل بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('tags.index')->with(['error' => 'يوجد خطأ']);

    }
  }


  public function destroy($id)
  {
   try{

    $tag=BlogTag::find($id);
    if(! $tag)
    {
      return  redirect()->route('tags', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
    } 


    if(!($tag->BlogBlogTag->isEmpty()))
    {
       return  redirect()->route('tags.index')->with(['error'=> 'لا يمكن الحذف هذه البيانات متعلقه بأخري ']);
    }
    $tag ->delete();

      return redirect()->route('tags.index')->with(['success' => 'تم الحذف بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('tags.index')->with(['error' => 'يوجد خطأ']);

    }
  }

}

?>