<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductNewArrival;

use Session;
use Auth;
class ProductArrivalController extends Controller 
{

  public function index()
  {
    $product_arrivals=ProductNewArrival::select('id','product_id', 'user_id','status','created_at')->groupBy('product_id')->get();
    return view('admin.product_arrivals.index',compact('product_arrivals'));   
  }
  public function activate(Request $request, $id)
  {
    $status = ProductNewArrival::findOrFail($id);
    $status->update(['status' => 1]);

    Session::flash('success','القسم مفعل الان');
    return Redirect('admin/product_arrivals');
  }

  public function deactivate(Request $request, $id)
  {
   $status = ProductNewArrival::findOrFail($id);
   $status->update(['status' => 0]);
   Session::flash('success','القسم غير مفعل الان');
   return Redirect('admin/product_arrivals');
 }
 public function create()
 {
  $products= Product::get();
  return view('admin.product_arrivals.create', compact('products'));   
 }

  public function store(Request $request)
  {

    try{
          // to store multiple product best seller for specefic category
            foreach ($request->product_id as $key) {
              // to store all products to specific category
            if($key == "all")
            {
              $products=Product::all();
              foreach ($products as $value) {
                   $createProductSellers = $this->validate($request, ['product_id' => 'required']);
              $createProductSellers=ProductNewArrival::create([
                'user_id' => Auth::user()->id,
                'product_id' => $value->id,
                'status' => 1,

              ]);
                    }
            }
            else
            {

              $createProductSellers=ProductNewArrival::create([
                  'user_id' => Auth::user()->id,
                  'product_id' => $key,
                  'status' => 1,

              ]);
            }
        }

    return redirect()->route('product_arrivals.index')->with(['success' => 'تم الحفظ بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('product_arrivals.create')->with(['error' => 'يوجد خطأ']);

    }
  }

  

}

?>