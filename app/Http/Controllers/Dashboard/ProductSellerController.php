<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductBestSeller;

use Session;
use Auth;
class ProductSellerController extends Controller 
{

  public function index()
  {
    $product_sellers=ProductBestSeller::select('id','product_id', 'user_id','status','created_at')->groupBy('product_id')->get();
    return view('admin.product_sellers.index',compact('product_sellers'));   
  }
  public function activate(Request $request, $id)
  {
    $status = ProductBestSeller::findOrFail($id);
    $status->update(['status' => 1]);

    Session::flash('success','القسم مفعل الان');
    return Redirect('admin/product_sellers');
  }

  public function deactivate(Request $request, $id)
  {
   $status = ProductBestSeller::findOrFail($id);
   $status->update(['status' => 0]);
   Session::flash('success','القسم غير مفعل الان');
   return Redirect('admin/product_sellers');
 }
 public function create()
 {
  $products= Product::get();
  return view('admin.product_sellers.create', compact('products'));   
 }

  public function store(Request $request)
  {

    try{
          // to store multiple product best seller for specefic category
            foreach ($request->product_id as $key) {
              // to store all products to specific category
            if($key == "all")
            {
              $products=Product::all();
              foreach ($products as $value) {
                   $createProductSellers = $this->validate($request, ['product_id' => 'required']);
              $createProductSellers=ProductBestSeller::create([
                'user_id' => Auth::user()->id,
                'product_id' => $value->id,
                'status' => 1,

              ]);
                    }
            }
            else
            {

              $createProductSellers=ProductBestSeller::create([
                  'user_id' => Auth::user()->id,
                  'product_id' => $key,
                  'status' => 1,

              ]);
            }
        }

    return redirect()->route('product_sellers.index')->with(['success' => 'تم الحفظ بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('product_sellers.create')->with(['error' => 'يوجد خطأ']);

    }
  }

}

?>