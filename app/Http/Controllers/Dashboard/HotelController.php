<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Hotel;
use App\Models\HotelService;
use App\Http\Requests\HotelRequest;
use Session;
use Auth;
class HotelController extends Controller 
{

  public function index()
  {
    $hotel=Hotel::first();
       $get_services=HotelService::get();

    return view('admin.hotels.index',compact('hotel','get_services'));   
  }

//  public function create()
//  {
//   return view('admin.hotels.create');   
//   }

// public function store(HotelRequest $request)
// {
//  try{
//    $file_path="";
//    if($request->has('hotel_image'))
//    {
//     $file_path=$this->upload_image('clinics', $request->hotel_image);
//   }
//   else
//   {
//     $file_path='images/default2.png';
//   }
//   $addClinic=Clinic::create($request->except('user_id','hotel_image'));
//   $addClinic->user_id = Auth::user()->id;
//   $addClinic->hotel_image = $file_path;

//   $addClinic->save();
//   return redirect()->route('hotels.index')->with(['success' => 'تم الحفظ بنجاح']);
// }
// catch (\Exception $ex)
// {
//   return redirect()->route('hotels.create')->with(['error' => 'يوجد خطأ']);

// }
// }



  public function activate(Request $request, $id)
  {
    $status = HotelService::findOrFail($id);
    $status->update(['status' => 1]);

    Session::flash('success','القسم مفعل الان');
    return Redirect('admin/hotels');
  }

  public function deactivate(Request $request, $id)
  {
   $status = HotelService::findOrFail($id);
   $status->update(['status' => 0]);
   Session::flash('success','القسم غير مفعل الان');
   return Redirect('admin/hotels');
 }


public function addService(Request $request , $id)
{
   $hotel=Hotel::find($id);
  // dd($id);
   $get_services=HotelService::where('hotel_id', $id)->get();
   if($get_services != null)
   {
     // dd($get_services);

    $add_tags=HotelService::create([
      'service_description_ar' =>$request->service_description_ar,
      'service_description_en' =>$request->service_description_en,
      'service_price' =>$request->service_price,
      'hotel_id' => $id,
     ]);
  }
    return redirect()->back()->with(['success' => 'تم الحفظ بنجاح']);

}
public function edit($id)
{
 $hotel=Hotel::find($id);
 if(! $hotel)
 {
  return redirect()->route('hotels.index')->with(['error' => 'لا توجد رساله بهذا العنوان']);
} 
return view('admin.hotels.edit',compact('hotel'));

}

public function update(Request $request, $id)
{
  try{

    $hotel_data=Hotel::find($id);
 //dd( $hotel_data);
        //for save image
    $file_path=$hotel_data->hotel_image;
    if($request->has('hotel_image'))
    {
      $img=str_replace('public', '', $file_path);
      //unlink(public_path() .'/'. $img );

      $file_path=$this->upload_image('hotels', $request->hotel_image);

    }
    Hotel::where('id',$id)->update([
      'hotel_title_ar' => $request['hotel_title_ar'],
      'hotel_title_en' => $request['hotel_title_en'],
      'hotel_description_ar' => $request['hotel_description_ar'],
      'hotel_description_en' => $request['hotel_description_en'],
      'hotel_image' => $file_path
    ]);

    return redirect()->route('hotels.index')->with(['success' => 'تم التعديل بنجاح']);
  }
  catch (\Exception $ex)
  {
    return redirect()->route('hotels.index')->with(['error' => 'يوجد خطأ']);

  }
}


// public function destroy($id)
// {
//  try{

//   $clinic=Clinic::find($id);
//   if( $clinic->hotel_image != 'images/default2.png')
//   {
//     $img=str_replace('public', '', $clinic->hotel_image);
//     unlink(public_path() .'/'. $img );

//             //   unlink(public_path() .'/'. $category->hotel_image );
//   }

//   if(! $clinic)
//   {
//     return  redirect()->route('clinics', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
//   } 

//   $clinic ->delete();

//   return redirect()->route('hotels.index')->with(['success' => 'تم الحذف بنجاح']);
// }
// catch (\Exception $ex)
// {
//   return redirect()->route('hotels.index')->with(['error' => 'يوجد خطأ']);

// }}

public function destroy_service($id)
{
 try{

  $service=HotelService::find($id);
  if(! $service)
  {
    return  redirect()->route('hotels', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
  } 

  $service ->delete();

  return redirect()->route('hotels.index')->with(['success' => 'تم الحذف بنجاح']);
  }
  catch (\Exception $ex)
  {
    return redirect()->route('hotels.index')->with(['error' => 'يوجد خطأ']);

  }
}




function upload_image($folder, $image)
{
  $image->store('/', $folder);
  $filename=$image->hashName();
  $path= 'public/images/'. $folder .'/'. $filename;

  return $path;
}

}

?>