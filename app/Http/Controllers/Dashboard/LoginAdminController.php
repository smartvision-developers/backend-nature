<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;

class LoginAdminController extends Controller
{
    public function getLogin()
    {
        return view('admin.login');
    }

    public function login(Request $request)
    {
        $remember_me=$request->has('remember_me')? 'true' : 'false';
        if(auth()->guard('admin')->attempt(['name'=> $request->input('name'),'password'=>$request->input('password')]) )
        {
            return redirect('admin');
        }
        else{
            return redirect()->back()->with(['error' => 'يوجد خطأ في البريد الالكتروني أو كلمه المرور']);

        }
    }

public function destroy()
    {
        auth()->logout();
        return redirect('admin/login');
    }
}
