<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SiteGallery;

use Session;
class SiteGalleryController extends Controller 
{

  
  public function index()
  {
    $gallerys=SiteGallery::get();
  
    return view('admin.gallerys.index', compact('gallerys'));
  }

  public function add_imgs(Request $request)
  {
    $num = 0;
    foreach ($request->file('site_image') as $img) {
      $path = public_path();
            $destinationPath = $path.'/images/site_gallerys/'; // upload path
            $extension = 'jpg'; // getting img extension
            $name = 'public/images/site_gallerys/'.time().''.rand(11111,99999).'.'.$extension; // renameing img
            $img->move($destinationPath, $name); // uploading file to given path
            $final_name = $destinationPath.$name;
            $image = [
              'site_image'  => $name,
            ];
            SiteGallery::create($image);
            $num++;
          }
      return redirect()->back();
  }

  public function destroy($id)
  {
          try {
        $data = SiteGallery::find($id);
        $img=str_replace('public', '', $data->site_image);

        unLink(public_path().'/'.$img);

        if(!$data)
        return redirect('admin/gallerys')->with(['error' => 'هذا العمل غير موجود']);
          $data->delete();
        return redirect('admin/gallerys')->with(['success' => 'تم حذف العمل بنجاح']);
      } catch (\Exception $e) {
        return redirect('admin/gallerys')->with(['error' => 'حدث خطا ما برجاء المحاولة لاحقا']);
      }

    }
  
}

?>