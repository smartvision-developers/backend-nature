<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slider;
use App\Http\Requests\SliderRequest;
use Session;
use Auth;
class SliderController extends Controller 
{

  public function index()
  {
    $sliders=Slider::get();
    return view('admin.sliders.index',compact('sliders'));   
  }
  public function activate(Request $request, $id)
  {
    $status = Slider::findOrFail($id);
    $status->update(['status' => 1]);

    Session::flash('success','القسم مفعل الان');
    return Redirect('admin/sliders');
  }

  public function deactivate(Request $request, $id)
  {
   $status = Slider::findOrFail($id);
   $status->update(['status' => 0]);
   Session::flash('success','القسم غير مفعل الان');
   return Redirect('admin/sliders');
 }
 public function create()
 {
  $sliders= Slider::get();
  return view('admin.sliders.create', compact('sliders'));   
}

public function store(SliderRequest $request)
{
 try{
   $file_path="";
   if($request->has('slider_img'))
   {
    $file_path=$this->upload_image('sliders', $request->slider_img);
  }
  else
  {
    $file_path='images/default2.png';
  }
  $addslider=Slider::create($request->except('slider_img'));
  $addslider->slider_img = $file_path;

  $addslider->save();
  return redirect()->route('sliders.index')->with(['success' => 'تم الحفظ بنجاح']);
}
catch (\Exception $ex)
{
  return redirect()->route('sliders.create')->with(['error' => 'يوجد خطأ']);

 }
}

public function show($id)
{
 $slider=Slider::find($id);
 return view('admin.sliders.profile',compact('slider'));
}

public function edit($id)
{
 $slider=Slider::find($id);
 if(! $slider)
 {
  return redirect()->route('sliders.index')->with(['error' => 'لا توجد رساله بهذا العنوان']);
} 
return view('admin.sliders.edit',compact('slider'));

}

public function update(Request $request, $id)
{
  try{

    $slider_data=Slider::find($id);
    if(! $slider_data)
    {
      return  redirect()->route('sliders.edit', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
    }
        //for save image
    $file_path=$slider_data->slider_img;
    if($request->has('slider_img'))
    {
      $img=str_replace('public', '', $file_path);
      unlink(public_path() .'/'. $img );

      $file_path=$this->upload_image('sliders', $request->slider_img);

    }
    Slider::where('id',$id)->update([
      'slider_text_ar' => $request['slider_text_ar'],
      'slider_text_en' => $request['slider_text_en'],
      'slider_img' => $file_path
    ]);

    return redirect()->route('sliders.index')->with(['success' => 'تم التعديل بنجاح']);
  }
  catch (\Exception $ex)
  {
    return redirect()->route('sliders.index')->with(['error' => 'يوجد خطأ']);

  }
}


public function destroy($id)
{
 try{

  $slider=Slider::find($id);
  if( $slider->slider_img != 'images/default2.png')
  {
    $img=str_replace('public', '', $slider->slider_img);
    unlink(public_path() .'/'. $img );

            //   unlink(public_path() .'/'. $category->slider_img );
  }

  if(! $slider)
  {
    return  redirect()->route('sliders', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
  } 

  $slider ->delete();

  return redirect()->route('sliders.index')->with(['success' => 'تم الحذف بنجاح']);
}
catch (\Exception $ex)
{
  return redirect()->route('sliders.index')->with(['error' => 'يوجد خطأ']);

}
}

function upload_image($folder, $image)
{
  $image->store('/', $folder);
  $filename=$image->hashName();
  $path= 'public/images/'. $folder .'/'. $filename;

  return $path;
}

}

?>