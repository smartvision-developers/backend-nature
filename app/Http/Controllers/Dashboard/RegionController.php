<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Region;
use App\Models\City;

use App\Http\Requests\RegionRequest;
use Session;
use Auth;
class RegionController extends Controller 
{

  public function index()
  {
    $regions=Region::orderBy('id','DESC')->get();
    return view('admin.regions.index',compact('regions'));   
  }
 //  public function activate(Request $request, $id)
 //  {
 //    $status = Region::findOrFail($id);
 //    $status->update(['status' => 1]);

 //    Session::flash('success','الكلمه الدلاليه مفعل الان');
 //    return Redirect('admin/region');
 //  }

 //  public function deactivate(Request $request, $id)
 //  {
 //   $status = Region::findOrFail($id);
 //   $status->update(['status' => 0]);
 //   Session::flash('success','الكلمه الدلاليه غير مفعل الان');
 //   return Redirect('admin/region');
 // }
 public function create()
 {
  $citys=City::get();
  return view('admin.regions.create',compact('citys'));   
 }


  public function store(RegionRequest $request)
  {
   try{
    $addregion=Region::create([
      'region_name_ar' => $request['region_name_ar'],
              'city_id'  =>$request['city_id'],

    ]);
    return redirect()->route('regions.index')->with(['success' => 'تم الحفظ بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('regions.create')->with(['error' => 'يوجد خطأ']);

    }
  }
  

  public function edit($id)
  {
      $citys=City::get();

   $region=Region::find($id);
   if(! $region)
   {
      return redirect()->route('regions.index')->with(['error' => 'لا توجد رساله بهذا العنوان']);
    } 
    return view('admin.regions.edit',compact('region','citys'));

  }

  public function update(Request $request, $id)
  {
    try{

      $region_data=Region::find($id);
      if(! $region_data)
      {
        return  redirect()->route('regions.edit', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
      }
      Region::where('id',$id)->update([
        'region_name_ar' => $request['region_name_ar'],
        'city_id'  =>$request['city_id'],
      ]);

      return redirect()->route('regions.index')->with(['success' => 'تم التعديل بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('regions.index')->with(['error' => 'يوجد خطأ']);

    }
  }


  public function destroy($id)
  {
   try{

    $region=Region::find($id);
    if(! $region)
    {
      return  redirect()->route('region', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
    } 


    if(!($region->Location->isEmpty()))
    {
       return  redirect()->route('regions.index')->with(['error'=> 'لا يمكن الحذف هذه البيانات متعلقه بأخري ']);
    }
    $region ->delete();

      return redirect()->route('regions.index')->with(['success' => 'تم الحذف بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('regions.index')->with(['error' => 'يوجد خطأ']);

    }
  }

}

?>