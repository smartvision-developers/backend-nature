<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ShopSetting;
use App\Models\Cart;
use App\Models\ProductBestSeller;
use App\Models\Product;
use App\Models\ProductNewArrival;
use App\Models\Category;
use Session;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;
use Auth;
use Carbon\Carbon;
use DB;
class ShopSettingController extends Controller 
{

  public function index()
  {
    $shop_settings=ShopSetting::first();
    return view('admin.shop_settings.index',compact('shop_settings'));   
  }

  public function update(Request $request, $id)
  {
    try{

      $shop_setting_data=ShopSetting::find($id);
      if(! $shop_setting_data)
      {
        return  redirect()->route('shop_settings.edit', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
      }
      ShopSetting::where('id',$id)->update([
        'shipping_info_ar' => $request['shipping_info_ar'],
        'shipping_info_en' => $request['shipping_info_en'],
        'bank_name' => $request['bank_name'],
        'bank_account' => $request['bank_account'],
        'tax' => $request['tax'],
        'delivery_price' => $request['delivery_price'],
      ]);

      return redirect()->route('shop_settings.index')->with(['success' => 'تم التعديل بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('shop_settings.index')->with(['error' => 'يوجد خطأ']);

    }
  }


  public function shop_statistics()
    {
        if(Auth::check())
        {
            $chart_options = [
                'chart_title' => 'عدد المنتجات المضافه شهريا',
                'report_type' => 'group_by_date',
                'model' => 'App\Models\Product',
                'group_by_field' => 'created_at',
                'group_by_period' => 'month',
                'chart_type' => 'bar',
                'filter_field' => 'created_at',
                // 'filter_days' => 30, // show only last 30 days
            ];

            $chart1 = new LaravelChart($chart_options);


            $chart_options = [
                'chart_title' => 'عدد المنتجات داخل الأقسام',
                'report_type' => 'group_by_relationship',
                'model' => 'App\Models\Category_Product',
                'relationship_name' => 'Category',
                'group_by_field' => 'category_name_ar',
                'chart_type' => 'pie',
                'filter_field' => 'created_at',
                    ];

            $chart2 = new LaravelChart($chart_options);



              $chart_options = [
                  'chart_title' => 'متابعه المنتجات المضافه للسله أسبوعيا',
                  'chart_type' => 'line',
                  'report_type' => 'group_by_relationship',
                  'model' => 'App\Models\Cart',

                  'relationship_name' => 'product', // represents function user() on Transaction model
                  'group_by_field' => 'product_name_ar', // users.name
                  'filter_field' => 'created_at',
                  'filter_days' => 30, // show only transactions for last 30 days
                  'filter_period' => 'week', // show only transactions for this week
                  
            ];


            $chart3 = new LaravelChart($chart_options);


          $product_favs = DB::table('product_favourites')->whereNull('deleted_at')
             ->select('product_favourites.*',DB::raw('COUNT(product_id) as count'))
             ->groupBy('product_id')
             ->orderBy('count', 'DESC')
             ->paginate(6);

             $count_product_arrival= ProductNewArrival::count();
             $count_all_products= Product::whereNull('deleted_at')->count();
             $count_product_bestseller=ProductBestSeller::count();
             $count_all_categorys = Category::count();

            return view('admin.shop_statistics.index', compact('chart1','chart2','product_favs', 'chart3','count_product_arrival', 'count_all_products','count_product_bestseller','count_all_categorys'));

        }
    }


    public function get_data()
    {
      $get_product_id= Cart::get();
      foreach($get_product_id as $data)
      {
        $val= $data->product_id;
              return $val;

      }
    }

      public function get_data2()
    {
      $get_product_id2= Cart::get();
      foreach($get_product_id2 as $data2)
      {
        $val2= $data2->Product->product_name_ar;
      }
      return $val2;
    }
}

?>