<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\AnimalType;
use App\Models\Animal_Category;
use App\Http\Requests\CategoryRequest;
use Session;
class CategoryController extends Controller 
{

  public function index()
  {
    $categorys=Category::orderBy('id','DESC')->get();
    return view('admin.categorys.index',compact('categorys'));   
  }
  public function activate(Request $request, $id)
  {
    $status = Category::findOrFail($id);
    $status->update(['status' => 1]);

    Session::flash('success','القسم مفعل الان');
    return Redirect('admin/categorys');
  }

  public function deactivate(Request $request, $id)
  {
   $status = Category::findOrFail($id);
   $status->update(['status' => 0]);
   Session::flash('success','القسم غير مفعل الان');
   return Redirect('admin/categorys');
 }
 public function create()
 {
                $animals=AnimalType::get();

  return view('admin.categorys.create',compact('animals'));   
 }


  public function store(CategoryRequest $request)
  {
   try{
    $addCategory=Category::create([
      'category_name_ar' => $request['category_name_ar'],
      'category_name_en' => $request['category_name_en'],
    ]);

      // to store multiple product best seller for specefic category
            foreach ($request->animal_type_id as $key) {
              // to store all animals to specific category
            if($key == "all")
            {
              $animals=AnimalType::get();
              foreach ($animals as $value) {
                   $createAnimalCat = $this->validate($request, ['animal_type_id' => 'required']);
              $createAnimalCat=Animal_Category::create([
                'animal_type_id' => $value->id,
                'category_id' => $addCategory->id,

              ]);
                    }
            }
            else
            {

              $createAnimalCat=Animal_Category::create([
                  'animal_type_id' => $key,
                  'category_id' => $addCategory->id,

              ]);
            }
          }
    return redirect()->route('categorys.index')->with(['success' => 'تم الحفظ بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('categorys.create')->with(['error' => 'يوجد خطأ']);

    }
  }
  

  public function edit($id)
  {

    $animals=AnimalType::get();
   $category=Category::find($id);
   if(! $category)
   {
      return redirect()->route('categorys.index')->with(['error' => 'لا توجد رساله بهذا العنوان']);
    } 
    return view('admin.categorys.edit',compact('category','animals'));

  }

  public function update(Request $request, $id)
  {
    try{

      $category_data=Category::find($id);
      if(! $category_data)
      {
        return  redirect()->route('categorys.edit', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
      }
      Category::where('id',$id)->update([
        'category_name_ar' => $request['category_name_ar'],
        'category_name_en' => $request['category_name_en'],
      ]);


        if($request->has('animal_type_id'))
            {
            foreach ($request->animal_type_id as $key) {
                    $addAnimal_Category=Animal_Category::where('category_id',$id)->delete();

              // to store all animals to specific category
            if($key == "all")
            {

              $animals=AnimalType::get();
              foreach ($animals as $value) {
              $addAnimal_Category=Animal_Category::where('id',$id)->create([
                'category_id'=> $id,
                'animal_type_id' => $value->id,
              ]);
                    }
            }
            else
            {

              $addAnimal_Category=Animal_Category::where('id',$id)->create([
                'category_id'=> $id,
                'animal_type_id' => $key,
              ]);
            }
          }
        }

      return redirect()->route('categorys.index')->with(['success' => 'تم التعديل بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('categorys.index')->with(['error' => 'يوجد خطأ']);

    }
  }


  public function destroy($id)
  {
   try{

    $category=Category::find($id);
    if(! $category)
    {
      return  redirect()->route('categorys', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
    } 

    if(!($category->Animal_Category->isEmpty()) ||  !($category->Category_Product->isEmpty()))
    {
       return  redirect()->route('categorys.index')->with(['error'=> 'لا يمكن الحذف هذه البيانات متعلقه بأخري ']);
    }
        $category ->delete();

      return redirect()->route('categorys.index')->with(['success' => 'تم الحذف بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('categorys.index')->with(['error' => 'يوجد خطأ']);

    }
  }

}

?>