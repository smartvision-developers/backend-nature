<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Store;
class StoreController extends Controller 
{


  public function index()
  {
    $stores= Store::orderBy('id','DESC')->get();
    return view('admin.stores.index', compact('stores')); 
  }


  public function create()
  {
    
  }


  public function store(Request $request)
  {
    
  }


  public function show($id)
  {
    
  }


  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }
  
}

?>