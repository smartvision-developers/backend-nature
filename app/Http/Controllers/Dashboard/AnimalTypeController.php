<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AnimalType;
use App\Http\Requests\AnimalTypeRequest;
use Session;
class AnimalTypeController extends Controller 
{

  public function index()
  {
    $animal_types=AnimalType::orderBy('id','DESC')->get();
    return view('admin.animal_types.index',compact('animal_types'));   
  }
  public function activate(Request $request, $id)
  {
    $status = AnimalType::findOrFail($id);
    $status->update(['status' => 1]);

    Session::flash('success','القسم مفعل الان');
    return Redirect('admin/animal_types');
  }

  public function deactivate(Request $request, $id)
  {
   $status = AnimalType::findOrFail($id);
   $status->update(['status' => 0]);
   Session::flash('success','القسم غير مفعل الان');
   return Redirect('admin/animal_types');
 }
 public function create()
 {
  return view('admin.animal_types.create');   
 }


  public function store(AnimalTypeRequest $request)
  {
   try{
     $file_path="";
   if($request->has('image'))
   {
    $file_path=$this->upload_image('animals', $request->image);
  }
  else
  {
    $file_path='images/default2.png';
  }
    $addanimal_type=AnimalType::create($request->except('image'));
    $addanimal_type->image= $file_path;
    $addanimal_type->save();
    return redirect()->route('animal_types.index')->with(['success' => 'تم الحفظ بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('animal_types.create')->with(['error' => 'يوجد خطأ']);

    }
  }
  

  public function edit($id)
  {
   $animal_type=AnimalType::find($id);
   if(! $animal_type)
   {
      return redirect()->route('animal_types.index')->with(['error' => 'لا توجد رساله بهذا العنوان']);
    } 
    return view('admin.animal_types.edit',compact('animal_type'));

  }

  public function update(Request $request, $id)
  {
    try{

      $animal_type_data=AnimalType::find($id);
        $file_path=$animal_type_data->image;
    if($request->has('image'))
    {
      // $img=str_replace('public', '', $file_path);
      // unlink(public_path() .'/'. $img );

      $file_path=$this->upload_image('animals', $request->image);

    }
      if(! $animal_type_data)
      {
        return  redirect()->route('animal_types.edit', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
      }
      AnimalType::where('id',$id)->update([
        'name_ar' => $request['name_ar'],
        'name_en' => $request['name_en'],
        'image' => $file_path
      ]);

      return redirect()->route('animal_types.index')->with(['success' => 'تم التعديل بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('animal_types.index')->with(['error' => 'يوجد خطأ']);

    }
  }


  public function destroy($id)
  {
   try{

    $animal_type=AnimalType::find($id);
    if(! $animal_type)
    {
      return  redirect()->route('animal_types', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
    } 

    if(!($animal_type->Animal_Category->isEmpty()) ||  !($animal_type->Category_Product->isEmpty()))
    {
       return  redirect()->route('animal_types.index')->with(['error'=> 'لا يمكن الحذف هذه البيانات متعلقه بأخري ']);
    }

    $animal_type ->delete();

      return redirect()->route('animal_types.index')->with(['success' => 'تم الحذف بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('animal_types.index')->with(['error' => 'يوجد خطأ']);

    }
  }

  


function upload_image($folder, $image)
{
  $image->store('/', $folder);
  $filename=$image->hashName();
  $path= 'public/images/'. $folder .'/'. $filename;

  return $path;
}

}

?>