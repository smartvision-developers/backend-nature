<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;
use Session;
use Auth;
use Carbon\Carbon;
use DB;
class SettingController extends Controller 
{

  public function index()
  {
    $settings=Setting::first();
    return view('admin.settings.index',compact('settings'));   
  }

  public function update(Request $request, $id)
  {
    try{

      $setting_data=Setting::find($id);
      if(! $setting_data)
      {
        return  redirect()->route('settings.edit', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
      }
      Setting::where('id',$id)->update([
        'about_us_ar' => $request['about_us_ar'],
        'about_us_en' => $request['about_us_en'],
        'our_vision_ar' => $request['our_vision_ar'],
        'our_vision_en' => $request['our_vision_en'],
        'video' => $request['video'],
        'twitter_link' => $request['twitter_link'],
        'facebook_link' => $request['facebook_link'],
        'snapchat_link' => $request['snapchat_link'],
        'email' => $request['email'],
        'address_ar' => $request['address_ar'],
        'address_en' => $request['address_en'],
        'phone_no' => $request['phone_no'],
        'instagram_link' => $request['instagram_link'],
        'youtube_link' => $request['youtube_link'],
      ]);

      return redirect()->route('settings.index')->with(['success' => 'تم التعديل بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('settings.index')->with(['error' => 'يوجد خطأ']);

    }
  }

  
}

?>