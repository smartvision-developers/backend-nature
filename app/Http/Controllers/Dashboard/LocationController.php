<?php 

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Region;
use App\Models\City;
use App\Models\Location;
use App\Http\Requests\LocationRequest;
use Session;
use Auth;
class LocationController extends Controller 
{

  public function index()
  {
    $locations=Location::orderBy('id','DESC')->get();
    return view('admin.locations.index',compact('locations'));   
  }
 //  public function activate(Request $request, $id)
 //  {
 //    $status = Location::findOrFail($id);
 //    $status->update(['status' => 1]);

 //    Session::flash('success','الكلمه الدلاليه مفعل الان');
 //    return Redirect('admin/region');
 //  }

 //  public function deactivate(Request $request, $id)
 //  {
 //   $status = Location::findOrFail($id);
 //   $status->update(['status' => 0]);
 //   Session::flash('success','الكلمه الدلاليه غير مفعل الان');
 //   return Redirect('admin/region');
 // }
 public function create()
 {
  $citys=City::get();
  $regions=Region::get();
  return view('admin.locations.create',compact('citys','regions'));   
 }


  public function store(LocationRequest $request)
  {
   try{
    $addregion=Location::create([
      'location_name_ar' => $request['location_name_ar'],
       'region_id'  =>$request['region_id'],

    ]);
    return redirect()->route('locations.index')->with(['success' => 'تم الحفظ بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('locations.create')->with(['error' => 'يوجد خطأ']);

    }
  }
  

  public function edit($id)
  {
      $citys=City::get();
      $regions=Region::get();
   $location=Location::find($id);
   if(! $location)
   {
      return redirect()->route('locations.index')->with(['error' => 'لا توجد رساله بهذا العنوان']);
    } 
    return view('admin.locations.edit',compact('regions','citys','location'));

  }

  public function update(Request $request, $id)
  {
    try{

      $location_data=Location::find($id);
      if(! $location_data)
      {
        return  redirect()->route('locations.edit', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
      }
      Location::where('id',$id)->update([
        'location_name_ar' => $request['location_name_ar'],
        'region_id'  =>$request['region_id'],
      ]);

      return redirect()->route('locations.index')->with(['success' => 'تم التعديل بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('locations.index')->with(['error' => 'يوجد خطأ']);

    }
  }


  public function destroy($id)
  {
   try{

    $location=Location::find($id);
    if(! $location)
    {
      return  redirect()->route('region', $id)->with(['error'=> 'خطأ لا توجد لغه ']);
    } 

    $location ->delete();

      return redirect()->route('locations.index')->with(['success' => 'تم الحذف بنجاح']);
    }
    catch (\Exception $ex)
    {
      return redirect()->route('locations.index')->with(['error' => 'يوجد خطأ']);

    }
  }

}

?>