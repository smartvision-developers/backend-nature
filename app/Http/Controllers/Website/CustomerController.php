<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CustomerRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\ShippmentAddressRequest;
use RealRashid\SweetAlert\Facades\Alert;
use App\Models\Customer;
use App\Models\City;
use App\Models\ShippmentAddress;
use App\Models\ClinicReservation;
use App\Models\HotelReservation;
use Session;
use App\Models\ShopBill;
use DB;
use Validator;
use Response;
use Mail;
use Auth;
use Hash;
use Carbon\Carbon;
class CustomerController extends Controller
{
    public function store(Request $request)
    {
         $rules2 = [
            'name' =>'required',
            'phone' =>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:customers,phone',
            'password' =>'required|
               min:6|unique:customers,password|required_with:repassword|same:repassword',
            'repassword' => 'required|min:6',
            'email' => 'required|email|unique:customers,email'


        ];
        $rules2_msg=
        [
            'name.required' => 'اسم المستخدم مطلوب',
            'phone.required' =>'رقم الجوال مطلوب',
            'phone.unique' =>'هذا الرقم مسجل من قبل',
            'phone.regex' =>'تأكد من ادخال رقم جوال صحيح',
            'password.required' =>'كلمه المرور مطلوبه',
            'password.min' =>'كلمه المرور لابد ان تكون أكثر من 6 حروف أو أرقام',
            'password.required_with' =>'تأكد من تطابق كلمه المرور',
            'repassword.required' =>'تأكيد كلمه المرور مطلوبه',
            'repassword.min' =>'كلمه المرور لابد ان تكون أكثر من 6 حروف أو أرقام',
            'email.required' =>'البريد الالكتروني مطلوب',
            'email.unique' =>'هذا البريد مسجل من قبل',
            'email.email' =>'تأكد من ادخال البريد الالكتروني بشكل صحيح',

        ];

        $validator = Validator::make($request->all(), $rules2,$rules2_msg);
        if ($validator->fails()) {
            return response()->json(array(

                    'errors' => $validator->errors()->all(),
            ));
        } else {


        $customer=Customer::create($request->except('password','repassword'));
        $customer->password= bcrypt($request->password);
        $customer->repassword= bcrypt($request->repassword);
        $customer->code = rand(0, 9999);

        $customer->photo= '/public/images/avatar.png';
        $customer->save();
        // Alert::success(session('success'),'تم التسجيل');
        return 1;
    }
}
    
    public function login(Request $request) 
    {
              $rules = [
            'password'           => 'required|unique:customers,password',
            'phone'           => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',

        ];
        $rules_msg=
        [
            'phone.required'     => __('translate.phone-required'),
            'phone.unique' => __('translate.phone-unique'),
            'phone.phone'   =>__('translate.phone-phone'),
            'password.required'  => __('translate.password-required'),
            // 'password.min'       => 'كلمة المرور لا تقل عن 6 أرقام',
            'password.unique' => __('translate.password-unique'),

        ];

        $validator = Validator::make($request->all(), $rules,$rules_msg);
        if ($validator->fails()) {
            return response()->json(array(

                    'errors' => $validator->errors()->all(),
            ));
        } else {
        $rememberme = request('remember_me') == 1 ? true: false;
        if (auth()->guard('customer')->attempt(['phone' => $request->phone, 'password' => $request->password],$rememberme)) {
            $user=auth()->guard('customer')->user();
            //dd($user);
            return 1;
        }

    }
    }




    public function get_profile()
    {
        return view('site.profile');
    }

    public function edit_customer(Request $request, $id)
    {
        $data = Customer::find($id);
       // dd($data);
         $data->name      = isset($request->name) ? $request->name : $data->name;
         $data->phone     = isset($request->phone) ? $request->phone : $data->phone;
        // $data->email     = isset($request->email) ? $request->email : $data->email;
         $data->update();
         Alert::success(session('success'),'تم  تعديل بياناتك بنجاح');
          return back();
    }

    public function update_password(Request $request, $id)
    {
        $data = Customer::find($id);
        if(Hash::check($request->password ,$data->password))
        {
               Customer::where('id',$id)->update([
          'password' => bcrypt($request->new_password),
        ]);
            Alert::success(session('success'),'تم  تعديل بياناتك بنجاح');

            auth()->logout();

          return redirect('/');
        }
        else
        {
            Alert::error(session('error'),'خطأ في كلمه المرور');
          return back();

        }

    }
    public function update_photo(Request $request, $id){
        $data = Customer::find($id);

         $file_path=$data->photo;
        if($request->has('photo'))
        {
            //dd( $file_path=$this->upload_image('trademarks', $request->photo));
            $file_path=$this->upload_image('customers', $request->photo);

        }

        Customer::where('id',$id)->update([
          'photo' => $file_path,
        ]);
        Alert::success(session('success'),'تم  تعديل بياناتك بنجاح');
        return back();
     }
    public function get_mail(Request $request, $id)
    {
          $data = Customer::find($id);
           $data->spare_email= $request->email;
           $data->save();
            Mail::send('site.verfiy', ['email' => $data->email, 'id' => $id ,'link' => route('site.updateEmail')], function($message) use($request){
          $message->to(Auth::guard('customer')->user()->email);
          $message->subject('Change Email Notification');
          
      });
//dd($defdf);

         Alert::success(session('success'),'راجع البريد الالكتروني الخاص بك');
         return back();
    }
    
     public function update_email(Request $request)
    {
          $data = Customer::where('id',Auth::guard('customer')->user()->id)->first();
          $data->email= $data->spare_email;
          $data->spare_email=null;
          $data->save();
         Alert::success(session('success'),'تم تغيير البريد الالكتروني');
         return redirect('/');
    }
 public function get_password(Request $request)
    {
          $data = Customer::where('email',$request->email)->first();
          // $data->spare_email= $request->email;
           //$data->save();
           $email= $data->email;
            Mail::send('site.reset-password', ['email' => $email, 'code' => $data->code , 'id' => $data->id ,'link' => route('site.checkCode', $email)], function($message) use($email){
          $message->to($email);
          $message->subject('Reset Password Notification');
          
      });
//dd($defdf);
session()->put('user_signup',$email);
         Alert::success(session('success'),'راجع البريد الالكتروني الخاص بك');
         return back();
    }
    
     public function check_code(Request $request, $email)
    {
           $data = Customer::where('email', $email)->first();
       //  dd($data);
         return view('site.new_password', compact('data'));
    }

  public function checking(Request $request)
    {
           $data = Customer::where('email', $request->email)->first();
           if($data->forget_password == 0)
           {
           $x=[];
           foreach($request->code as $val)
            {
                  array_push($x, $val);

            }
            $arr= array_reverse($x);
            $user_code=implode("",$arr);
            //dd($user_code);
          if($data->code == $user_code)
          {
              $data->forget_password=1;
              $data->save();
            return back();
          }
            Alert::success(session('success'),'تأكد من ادخال الكود بشكل صحيح');
            return back();
           }
           else
           {
                $validator = Validator::make($request->all(), [
                    'new_password' => 'required|min:6|unique:customers,new_password|required_with:confirm_password|same:confirm_password',
                    'confirm_password' => 'required|min:6',
                ],
                [
                    'new_password.required' => 'كلمه المرور مطلوبه',
                    'confirm_password.required' => 'هذا الحقل مطلوب',
                    'new_password.min'=> 'كلمه المرور لا تقل عن 6 حروف او أرقام',
                    'confirm_password.min' => 'كلمه المرور لا تقل عن 6 حروف او أرقام',
                ]);
    
               $data->forget_password=0;
               if($request->confirm_password == $request->new_password)
               {
                    $data->password=bcrypt($request->new_password);
                    $data->repassword=bcrypt($request->confirm_password);
               }
               $data->code= rand(0, 9999);
               $data->save();
                Alert::success(session('success'),'تم تغيير كلمه المرور بنجاح');
                return redirect('/');
           }
    }

    public function shippment_address()
    {

        $citys=City::pluck('city_name_ar', 'id');
        $shippment_address=ShippmentAddress::where('customer_id',Auth::guard('customer')->user()->id)->get();
        return view('site.shippment-address', compact('citys','shippment_address'));
    }
    public function get_ajax_region($id)
    {
                $regions = DB::table("regions")
                        ->where("city_id",$id)
                        ->pluck("region_name_ar","id");
            return json_encode($regions);

    }
    public function get_ajax_location($id)
    {
                $locations = DB::table("locations")
                        ->where("region_id",$id)
                        ->pluck("location_name_ar","id");
            return json_encode($locations);

    }



    public function store_shippment(ShippmentAddressRequest $request)
    {
        if($request->city_id == null || $request->location_id == null || $request->region_id == null || $request->phone == null)
        {
        Alert::error(session('error'),'أكمل البيانات');
        }
        else
        {

        $create_shippment=ShippmentAddress::create($request->except('customer_id'));
        $create_shippment->customer_id= Auth::guard('customer')->user()->id;
        $create_shippment->save();
        Alert::success(session('success'),'تم  إضافه العنوان بنجاح');
        return back();
        }
    }
    public function logout()
    {
        auth()->logout();
        return redirect('/');
    }

    function upload_image($folder, $image)
    {
      $image->store('/', $folder);
      $filename=$image->hashName();
      $path= 'public/images/'. $folder .'/'. $filename;

      return $path;
    }



    public function get_orders()
    {
        $orders=ShopBill::where('order_total_price','>',0)->where('customer_id',Auth::guard('customer')->user()->id)->get();
        return view('site.orders', compact('orders'));
    }


    public function get_last_3month()
    {
        $orders = ShopBill::where('order_total_price','>',0)->where('customer_id',Auth::guard('customer')->user()->id)->where("created_at",">", Carbon::now()->subMonths(3))->get();
        return view('site.orders-3', compact('orders'));

    }

    public function get_last_6month()
    {
        $orders = ShopBill::where('order_total_price','>',0)->where('customer_id',Auth::guard('customer')->user()->id)->where("created_at",">", Carbon::now()->subMonths(6))->get();
        return view('site.orders-6', compact('orders'));

    }
    
    
    
    public function get_reservations()
    {
        $hotel_reservations=HotelReservation::groupBy('request_no')->where('customer_id',Auth::guard('customer')->user()->id)->get();
        $clinic_reservations=ClinicReservation::groupBy('request_no')->where('customer_id',Auth::guard('customer')->user()->id)->get();
        return view('site.reservations', compact('clinic_reservations','hotel_reservations'));
    }

   public function removeReservation(Request $request)
    {
            $hotel_reservations = HotelReservation::where('id', $request->id)->where('customer_id', Auth::guard('customer')->user()->id)->first();
            if($hotel_reservations)
            {
            $hotel_reservations->delete();
                }
                else
                {
            $clinic_reservations = ClinicReservation::where('id', $request->id)->where('customer_id', Auth::guard('customer')->user()->id)->first();
            if($clinic_reservations)
            {
            $clinic_reservations->delete();
        }}
            return 1;
       
    }

    public function get_reserv_last_3month()
    {
        $hotel_reservations=HotelReservation::groupBy('request_no')->where('customer_id',Auth::guard('customer')->user()->id)->where("created_at",">", Carbon::now()->subMonths(3))->get();
        $clinic_reservations=ClinicReservation::groupBy('request_no')->where('customer_id',Auth::guard('customer')->user()->id)->where("created_at",">", Carbon::now()->subMonths(3))->get();

        return view('site.reservations-3', compact('clinic_reservations','hotel_reservations'));

    }

    public function get_reserv_last_6month()
    {
        $hotel_reservations=HotelReservation::groupBy('request_no')->where('customer_id',Auth::guard('customer')->user()->id)->where("created_at",">", Carbon::now()->subMonths(6))->get();
        $clinic_reservations=ClinicReservation::groupBy('request_no')->where('customer_id',Auth::guard('customer')->user()->id)->where("created_at",">", Carbon::now()->subMonths(6))->get();

        return view('site.reservations-6', compact('clinic_reservations','hotel_reservations'));

    }
}
