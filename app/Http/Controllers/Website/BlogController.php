<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\BlogComment;
use App\Models\BlogBlogTag;
use App\Models\BlogTag;

use Auth;
class BlogController extends Controller
{
    public function get_blogs()
    {
        $blogs=Blog::getAllblogs();
        return view('site.blogs', compact('blogs'));
    }
  public function sort2()
    {
        $blogs=Blog::sort2();
        return view('site.blogs', compact('blogs'));
    }

    public function store_comment($blog_id, Request $request)
    {
        $blog=Blog::where('id',$blog_id)->first();

        $comment=BlogComment::create([
            'blog_id'=> $blog->id,
            'comment' => $request['comment'],
            'customer_id' =>Auth::guard('customer')->user()->id,
        ]);
        if($comment)
        {
        return response()->json($comment);
    }
    else
    {
        return 0;
    }
    }

    public function get_singleblog(Request $request, $blog_name)
    {
        $blog=Blog::getSingleblog($blog_name);
            // get previous blog id
        $previous = Blog::where('id', '<', $blog->id)->select('id','blog_image','blog_title_'.getLanguageCode().' as blog_title')->where('status',1)->first();
    // get next blog id
         $next = Blog::where('id', '>', $blog->id)->select('id','blog_image','blog_title_'.getLanguageCode().' as blog_title')->where('status',1)->first();
        //dd($next);
         $count_comment=BlogComment::where('blog_id', $blog->id)->count();

     $get_similar=Blog::where('status',1)->where('blog_description_ar','LIKE', '%' .$blog->blog_description_ar. '%' )->orwhere('blog_description_en','LIKE', '%' .$blog->blog_description_en. '%' )->orwhere('blog_text_ar','LIKE', '%' .$blog->blog_text_ar. '%' )->orwhere('blog_text_en','LIKE', '%' .$blog->blog_text_en. '%' )->select('created_at','id','blog_image','blog_title_'.getLanguageCode().' as blog_title', 'blog_text_'.getLanguageCode().' as blog_text')->take(5)->get();  
        
        return view('site.singleBlog', compact('blog','previous','next','get_similar','count_comment'));
    }


    public function get_blogs_tag($tag_name)
    {
        $get_tag= BlogTag::where('tag_ar', $tag_name)->orwhere('tag_en', $tag_name)->first();   
        $blog_tag=BlogBlogTag::where('blog_tag_id', $get_tag->id)->simplePaginate(8);
        return view('site.blogsTag', compact('blog_tag','get_tag'));

    }


 public function getBlogsSearch(Request $request)
  {
    $content=$request->get('content');
    if($request->has('content')){

    $get_blogs=Blog::where('status',1)->where('blog_title_ar','LIKE', '%' .$content. '%' )->orwhere('blog_title_en','LIKE', '%' .$content. '%' )->orwhere('blog_text_ar','LIKE', '%' .$content. '%' )->orwhere('blog_text_en','LIKE', '%' .$content. '%' )->select('created_at','id','blog_image','blog_title_'.getLanguageCode().' as blog_title', 'blog_text_'.getLanguageCode().' as blog_text')->simplePaginate(8);  
        }
    //dd($get_blogs);
    return view('site.blogs-search',compact('get_blogs','content'));
  }

}
