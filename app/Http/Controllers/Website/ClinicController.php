<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Clinic;
use App\Models\AnimalType;
use App\Models\ClinicReservation;
use Validator;
use App\Models\SiteGallery;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;
class ClinicController extends Controller
{
    public function get_clinic($clinic_name)
    {
        $clinic=Clinic::getclinic($clinic_name);
        $gallerys=SiteGallery::get();
        $animals=AnimalType::getanimalstype();


        return view('site.check_clinic', compact('clinic','gallerys','animals'));
    }

   public function addMorePost(Request $request)
    {
          $today=date('m/d/Y');
            $rules2 = [
            'more.*.service_id' => 'required',
            // 'more.*.service_price' => 'required',
            'more.*.animal_id'   => 'required',
            'reservation_date' => 'required|date|after_or_equal:'.$today,

        ];
        $rules2_msg=
        [
            'reservation_date.required'        => 'مطلوب ادخال تاريخ الحجز',
            'reservation_date.after_or_equal' => 'اختيار خطأ ',
            'more.*.animal_id.required'  => 'مطلوب اختيار نوع الحيوان',
            'more.*.service_id.required' => 'الخدمه مطلوبه',
            // 'more.*.service_price.required' => 'الخدمه مطلوبه',
        ];

        $validator = Validator::make($request->all(), $rules2,$rules2_msg);
        if ($validator->fails()) {
            return response()->json(array(

                    'errors' => $validator->errors()->all(),
            ));
        } else {

            foreach($request->more as $kedy => $value) {
                $store=ClinicReservation::create(['request_no' => mt_rand(100000, 999999),'customer_id'=> Auth::guard('customer')->user()->id,'reservation_date' =>$request->reservation_date , 'service_id'=>$value['service_id'],'service_price'=>$value['service_price'], 'animal_id'=>$value['animal_id'], 'animal_no' => $value['animal_no']]);
            }
            return 1;
            }    
        }

}
