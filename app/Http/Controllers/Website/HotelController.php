<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Hotel;
use App\Models\AnimalType;
use App\Models\HotelReservation;
use Validator;
use App\Models\SiteGallery;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;
class HotelController extends Controller
{
    public function get_hotel($hotel_name)
    {
        $hotel=Hotel::gethotel($hotel_name);
        $gallerys=SiteGallery::get();
        $animals=AnimalType::getanimalstype();

        return view('site.hotel', compact('hotel','gallerys','animals'));
    }

   public function addMorePost(Request $request)
    {
          $today=date('m/d/Y');
            $rules2 = [
            'more.*.service_id' => 'required',
            // 'more.*.service_price' => 'required',
            'more.*.animal_id'   => 'required',
            'reservation_start_date' => 'required|date|after_or_equal:'.$today,
            'reservation_end_date' => 'required|date|after_or_equal:'.$today,

        ];
        $rules2_msg=
        [
            'reservation_start_date.required'        => 'مطلوب ادخال تاريخ الحجز',
            'reservation_start_date.after_or_equal' => 'اختيار خطأ ',
            'reservation_end_date.required'        => 'مطلوب ادخال تاريخ الحجز',
            'reservation_end_date.after_or_equal' => 'اختيار خطأ ',
            'more.*.animal_id.required'  => 'مطلوب اختيار نوع الحيوان',
            'more.*.service_id.required' => 'الخدمه مطلوبه',
            // 'more.*.service_price.required' => 'الخدمه مطلوبه',
        ];

        $validator = Validator::make($request->all(), $rules2,$rules2_msg);
        if ($validator->fails()) {
            return response()->json(array(

                    'errors' => $validator->errors()->all(),
            ));
        } else {

            foreach($request->more as $kedy => $value) {
                $store=HotelReservation::create(['request_no' => mt_rand(1000000000, 9999999999), 'reservation_start_date'=>$request->reservation_start_date , 'reservation_end_date'=> $request->reservation_end_date , 'customer_id'=> Auth::guard('customer')->user()->id, 'service_id'=>$value['service_id'],'service_price'=>$value['service_price'], 'animal_id'=>$value['animal_id'], 'animal_no' => $value['animal_no']]);
            }
            return 1;
            }    
        }

}
