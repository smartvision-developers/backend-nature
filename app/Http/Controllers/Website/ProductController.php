<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\AnimalType;
use App\Models\Cart;
use App\Models\ShippmentAddress;
use App\Models\Order;
use App\Models\ShopBill;
use App\Models\Product_Favourite;
use App\Models\Category;
use App\Models\Category_Product;
use App\Models\ShopSetting;
use App\Models\ProductAppraisal;
use Auth;
use App\Models\ShopCoupon;

use Redirect;
use DB;
class ProductController extends Controller
{   


    public function get_cart_tax()
    {
        $cart_tax=Cart::where('customer_id', Auth::guard('customer')->user()->id)->select('tax')->first();
        return $cart_tax->tax;
    }
    public function get_category(Request $request) {
        
        if ($request->ajax() && isset($request->start)) {
            $start = $request->start; // min price value
            $end = $request->end; // max price value

            $products =Product::where('product_unitprice', '>=', $start)->where('status','1')->where('product_unitprice', '<=', $end)->orderby('product_unitprice', 'ASC')->simplePaginate(6);
             response()->json($products); //return to ajax
             
           //  dd($products);
            return view('site.catproduct_price', compact('products'));
        }
        else if(isset($request->category)){

           $category = $request->category; //category
           $animal_id = $request->animal_id; //category
          // dd($animal_id);
           $products = Category_Product::whereIN('animal_id',explode( ',', $animal_id))->whereIN('category_id', explode( ',', $category ))->simplePaginate(6);
            response()->json($products); //return to ajax            
     //   dd($products);
            return view('site.catproduct', compact('products'));
        }
        else {
            $products = Category_Product::simplePaginate(6); // now we are fetching all products

            $get_star_products=Product::where('status','1')->where('star','1')->orderBy('id','DESC')->take(5)->get();
            return view('site.category', compact('products','get_star_products'));
        }
    }


     public function name(Request $request) {
        
        if(isset($request->category)){

           $category = $request->category; //category
           $animal_id = $request->animal_id; //category
          // dd($animal_id);
           $products = Category_Product::whereIN('animal_id',explode( ',', $animal_id))->whereIN('category_id', explode( ',', $category ))->simplePaginate(6);
            response()->json($products); //return to ajax            
     //   dd($products);
            return view('site.catproduct', compact('products'));
        }
        else {

                       $name2 = $request->name2; //category
                       $cat_name=Category::where('category_name_en', $name2)->orwhere('category_name_ar', $name2)->where('status',1)->first();
                       $name=$request->name;
                      // dd($name);
                $animal_name=AnimalType::where('name_en', $name)->orwhere('name_ar', $name)->where('status',1)->first();
//dd(  $animal_name);
            $products = Category_Product::where('category_id',$cat_name->id)->where('animal_id', $animal_name->id)->simplePaginate(6); // now we are fetching all products
//dd($products);

            $get_star_products=Product::where('status','1')->where('star','1')->orderBy('id','DESC')->take(5)->get();
            return view('site.category', compact('products','get_star_products'));
        }
    }



 public function animal_name(Request $request) {
        
    
                       $name=$request->name;
                      // dd($name);
                $animal_name=AnimalType::where('name_en', $name)->orwhere('name_ar', $name)->where('status',1)->first();
//dd(  $animal_name);
            $products = Category_Product::where('animal_id', $animal_name->id)->simplePaginate(6); // now we are fetching all products
//dd($products);

            $get_star_products=Product::where('status','1')->where('star','1')->orderBy('id','DESC')->take(5)->get();
            return view('site.category', compact('products','get_star_products'));
        }

    public function store_appraisal($product_id, Request $request)
    {
        $product=Product::where('id',$product_id)->first();

        $appraisal=ProductAppraisal::create([
            'product_id'=> $product->id,
            'appraisal_num' => $request['appraisal_num'],
            'appraisal_content' => $request['appraisal_content'],
            'customer_id' =>Auth::guard('customer')->user()->id,
        ]);
        if($appraisal)
        {
        return response()->json($appraisal);
    }
    else
    {
        return 0;
    }
    }

    public function get_product($code)
    {
        $product=Product::with('ProductImage','Product_Favourite')->singleproduct($code);
        $get_similar_category=Category_Product::where('category_id', $product->Category_Product->category_id)->get();
        $get_shipping_info=ShopSetting::select('shipping_info_' . getLanguageCode().' as shipping_info')->first();

     //   dd($get_shipping_info->shipping_info);
    // <!--------------------------- Start avg rating star ---------------------------->
   // <!-- rating = (sum_of_rating * 5)/sum_of_max_rating_of_user_count   -->
       $total_user_rate=ProductAppraisal::where('product_id',$product->id)->count();
       if($total_user_rate > 0)
       {
            $sum_of_rating=ProductAppraisal::where('product_id',$product->id)->sum('appraisal_num'); 

            $max_rate=ProductAppraisal::where('product_id',$product->id)->max('appraisal_num');
            if($max_rate > 0)
            {

               $rating= round(($sum_of_rating* 5) / ($total_user_rate * $max_rate), 1);
            }
        }
    // <!---------------------------- End avg rating star ---------------------------->
        else
        {
            $rating = 0;
        }
        return view('site.product', compact('product','get_similar_category','get_shipping_info','rating'));
    }


    public function add_remove_product_to_fav(Request $request)
    {
        
        // $product=Product::singleproduct($code);
        // if ($product != null) {
            $product_fav = Product_Favourite::where('product_id', $request->product_id)->where('customer_id', Auth::guard('customer')->user()->id)->first();
            if (!$product_fav) {
               Product_Favourite::create([
            'customer_id' => Auth::guard('customer')->user()->id,
            'product_id'  => $request->product_id,
                     ]);
            
            return 1;
        }
        else
        {
            $product_fav->delete();

            return 0;
        }

        // } else {
        //     return 0;
        // }    
    }

   public function addProductToCart(Request $request)
    {
        $setting=ShopSetting::find(1);
        if($request->quantity != null)
        {
            $quantity= $request->quantity;
        }
        else
        {
            $quantity= 1;
        }
        $order= Order::where('customer_id', Auth::guard('customer')->user()->id)->first();

        $product_fav = Product_Favourite::where('customer_id', Auth::guard('customer')->user()->id)->first();

        // if ($product_fav != null) {
            $cart = Cart::where('price_after_quantity','>', 0)->where('product_id', $request->product_id)->where('customer_id', Auth::guard('customer')->user()->id)->first();
            

            if (!$cart) {
             if(!$order)
             {
            $order=Order::create([
                'customer_id' => Auth::guard('customer')->user()->id,
                     ]);

        }

             $cart=  Cart::create([
                'customer_id' => Auth::guard('customer')->user()->id,
                'product_id'  =>$request->product_id,
                'quantity'    => $quantity,
                'order_id' => $order->id,
                'price_after_quantity' => $request->price_after_quantity,
                'tax' => $setting->tax,
                     ]);
            $shop_bill=ShopBill::where('order_id', $order->id)->where('status','!=', 2)->first();

            if(!$shop_bill)
            {
            $shop_bill=ShopBill::create([
                'request_no' => mt_rand(1000000000, 9999999999),
                'order_id' => $order->id,
                'order_total_price' =>  $cart->price_after_quantity,
                'customer_id'=>Auth::guard('customer')->user()->id,
                'price_after_tax' => $cart->price_after_quantity +  (($cart->tax *  $cart->price_after_quantity)/ 100),

            ]);
             
        }
else{
                $product_price= $shop_bill->order_total_price + $cart->price_after_quantity;
            $product_price= $product_price + $cart->price_after_tax ;

            $shop_bill= ShopBill::where('order_id', $order->id)->where('status','!=', 2)->update([
                'order_id' => $order->id,
                'order_total_price' =>  $product_price,
                'price_after_tax' => $product_price+( ($this->get_cart_tax() * $product_price)/ 100),
                     ]);
        }
            return 1;
        }
        else
        {
            $cart->delete();
            return 0;
        }

        // } else {
        //     return 0;
        // }    
    }


    public function removeProductFromFav(Request $request)
    {
            $product_fav = Product_Favourite::where('product_id', $request->product_id)->where('customer_id', Auth::guard('customer')->user()->id)->first();
            $product_fav->delete();

            return 1;
       
    }

    public function removeProductFromCart(Request $request)
    {
            $product_cart = Cart::where('price_after_quantity','>', 0)->where('product_id', $request->product_id)->where('customer_id', Auth::guard('customer')->user()->id)->first();
                            $product_cart->delete();

              $cart = Cart::where('price_after_quantity','>', 0)->whereNull('deleted_at')->where('customer_id', Auth::guard('customer')->user()->id)->with('product')->get();
//dd($product_cart->price_after_quantity);

//echo $cart;
                $order= Order::where('customer_id', Auth::guard('customer')->user()->id)->first();
                $shop_bill=ShopBill::where('order_id', $order->id)->where('status','!=', 2)->first();
    $x1=0;

//dd($cart);
        foreach($cart as $key=> $value)
        {
            $x1+=$value->Product->product_unitprice;
               $shop_bill->update([
                    'order_total_price' =>  $x1,
                    'price_after_tax'   =>$value->Product->product_unitprice +  (($this->get_cart_tax() * $value->Product->product_unitprice)/ 100),

                     ]);
        }

                
         
            return 1;
       
    }

    public function removeAllProductFromCart()
    {
            $product_fav = Cart::where('price_after_quantity','>', 0)->where('customer_id', Auth::guard('customer')->user()->id)->get();
            foreach ($product_fav as $key => $value) {
                // code...
                $value->delete();

            }
            $shop_bill=ShopBill::where('customer_id',Auth::guard('customer')->user()->id)->where('status','!=', 2)->first();
            $shop_bill->delete();
            return 1;
       
    }


    public function get_fav_products()
    {
        $customer_id=Auth::guard('customer')->user()->id;
        $favs=Product_Favourite::where('customer_id', $customer_id)->get();
      //  dd($favs);
        return view('site.favourite',compact('favs'));
    }




    public function get_cart_products()
    {
        $customer_id=Auth::guard('customer')->user()->id;
        $carts=Cart::where('price_after_quantity','>', 0)->where('customer_id', $customer_id)->get();
        $cart_tax=Cart::where('price_after_quantity','>', 0)->where('customer_id', $customer_id)->select('tax')->first();

        $shop_bill=ShopBill::where('customer_id', $customer_id)->where('status','!=', 2)->first();
        $shippment_address=ShippmentAddress::where('customer_id',Auth::guard('customer')->user()->id)->get();

      //  dd($carts);
        return view('site.cart',compact('carts','shop_bill','cart_tax','shippment_address'));
    }


    public function update_cart( Request $request)
    {
                $order= Order::where('customer_id', Auth::guard('customer')->user()->id)->first();

        $product=Cart::where('customer_id',Auth::guard('customer')->user()->id)->get();
        $quantity=$request['quantity'];
        $exp_quantity= explode( ',', $quantity );
            $product_price=0;

        foreach($product as $key=> $value)
        {
        //    dd($key);
            $cart=Cart::where('product_id',$value->product_id)->update([
                'product_id'=> $value->product_id,
                'price_after_quantity' => $exp_quantity[$key] * $value->Product->product_unitprice,
                'quantity' => $exp_quantity[$key],
                'order_id' => $order->id,

            ]);

               $shop_bill=ShopBill::where('order_id', $order->id)->where('status','!=', 2)->first();
               $product_price= $product_price + $exp_quantity[$key] * $value->Product->product_unitprice ;
               $shop_bill= ShopBill::where('order_id', $order->id)->where('status','!=', 2)->update([
                    'order_id'          => $order->id,
                    'order_total_price' =>  $product_price,
                    'price_after_tax' =>$product_price +  (($this->get_cart_tax() * $product_price)/ 100),

                     ]);
         
        }

           if($cart)
            {
         

                return response()->json($cart);
            }
            else
            {
                return 0;
            }
    }






    public function update_bill( Request $request)
    {
        $order= Order::where('customer_id', Auth::guard('customer')->user()->id)->first();
        if($order)
        {
            $shop=ShopBill::where('customer_id', Auth::guard('customer')->user()->id)->where('order_id', $order->id)->where('status', '!=', 2)->first();
          //  dd($shop);
            $shop_bill= ShopBill::where('id', $shop->id)->update([
                'receive_type' => $request['receive_type'],
                'shippment_address' => $request['shippment_address'],
                'bill_address' => $request['bill_address'],
                'new_address' => $request['new_address'],
                'status' => 1,
                     ]);
            if($shop_bill)
            {
                return response()->json($shop_bill);
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;

        }
           
    }




    public function apply_discount( Request $request)
    {
        $coupon= ShopCoupon::where('coupon_code',$request->coupon_id)->where('status',1)->first();
       // dd($coupon);
        if($coupon != null)
        {
            $order= Order::where('customer_id', Auth::guard('customer')->user()->id)->first();
            if($order)
            {
                $shop=ShopBill::where('customer_id', Auth::guard('customer')->user()->id)->where('order_id', $order->id)->where('status', '!=', 2)->first();
              //  dd($shop);
                $shop_bill= ShopBill::where('id', $shop->id)->update([
                    'coupon_id' => $coupon->id,
                    'price_after_coupon' => $shop->price_after_tax -(($shop->price_after_tax * $coupon->coupon_discount)/100),
                
                ]);
                if($shop_bill)
                {
                    return response()->json($shop_bill);
                }
                else
                {
                    return 0;
                }
            }
        }
        else
        {
            return 2;

        }
           
    }






    public function choose_payment_method(Request $request)
    {
    $order= Order::where('customer_id', Auth::guard('customer')->user()->id)->first();
        if($order)
        {
            $shop=ShopBill::where('customer_id', Auth::guard('customer')->user()->id)->where('order_id', $order->id)->where('status', '!=', 2)->first();
          //  dd($shop);
            $shop_bill= ShopBill::where('id', $shop->id)->update([
                'payment_method' => $request['payment_method'],
                     ]);
            if($request['payment_method'] == 1)
            {
                return back();
            }
            else
            {
                $cart = Cart::where('customer_id', Auth::guard('customer')->user()->id)->get();

                $inItem=[];
foreach($cart as $item){
   array_push($inItem,[
      "ItemName"=> $item->Product->product_code,
      "Quantity"=>$item['quantity'],
      "UnitPrice"=> $item->Product->product_unitprice,
    "PriceAfterQuantity"=>$item['price_after_quantity'],

   ]);
}
$z=0;
foreach ($inItem as $key => $value) {
    $z+=$value['PriceAfterQuantity'];    

}
//dd($z);
$inn=json_encode($inItem);
$phone =Auth::guard('customer')->user()->phone;

     $token ="DcLI5_IKIL2z1wyxIx9fm9DMkZg127Qhyo3eaQpqjGd9Fr5bqFGW-FY1r_YdBGoymudW-rFzw8x80LalyETbsk1gi6dECVgQbGDN58EMiNefAgYiO-wnnjkLi7MJTTl8Mx-bTugS5NkSUnwKW4dUHRs9yduYz-Amy9Xd6pejMVvs3Rn1CrnCYqtUIzSb7xBZtQNaadP-F-ITra9ITIeADGV14fOmYXMNHwSdcAEBRqd9MJqfETQQ4dqnhmgQX4K707usSNNAuzQIbn2TJAsiUkg71haRABM9e2pGevOA7H1z6pqgOBXVMs5znLVIOXBOg5vr1PDsjWp_EBkSNv-rrHB9B775MtwDDPkTACRsG_sM0_JVqqy-xGFJno-bYC_bPTYnLGDfRNm_8YGzjM3JlqYhvdfNuM7C7CzhXOyO2Tfy-gpxH2AKUFAT6XjaDIX7eslLXEuWGjI6tGlCJXEvXtASFFgYagWe4aCvFLw8VQygqK51GBpg9Z3I7DEJMTy44UE8yT6i652AZXL60VP2A8LHpChtMldXrWtt3sIQUIKnnxxQSed4eZladcCotVywb1iK4tGLAv5EpLGFXKeFSjlrY-qXtUCgon-KSV2ndN5AzKENpmj5GPKHPvXAEX271rNTrWRHfK_NBxkxp7bcbW2ccAzdC9yXWIj06sjym8mS5mPMyjK4ZKvIn7ilx5Z-sAHYfQ"; #token value to be placed here;
$basURL = "https://api.myfatoorah.com/";
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL => "$basURL/v2/SendPayment",
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => '
    
    {
  "CustomerName": "'.Auth::guard('customer')->user()->name.'",
  "NotificationOption": "ALL",
  "MobileCountryCode":"+966",
  "CustomerMobile": "'.$phone.'",
  "CustomerEmail":"'.Auth::guard('customer')->user()->email.'",
  "InvoiceValue": "'.$z.'",
  "DisplayCurrencyIso": "SAR",
  "CallBackUrl": "https://google.com",
  "ErrorUrl": "https://google.com",
  "Language": "en",
  "CustomerReference" :"ref 1",
  "CustomerCivilId":12345678,
  "UserDefinedField": "Custom field",
  "ExpireDate": "",
  "CustomerAddress" :{
      "Block":"",
      "Street":"",
      "HouseBuildingNo":"",
      "Address":"",
      "AddressInstructions":""
  },
  "InvoiceItems": '.$inn.'
}
    
    
    ',    CURLOPT_HTTPHEADER => array("Authorization: Bearer $token","Content-Type: application/json"),

));
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

$response = curl_exec($curl);

$err = curl_error($curl);
$x=json_decode($response,true);

//dd($response);
curl_close($curl);
if ($err) {
    return redirect()->back()->with('toast_error', 'Product was Deleted Successfully');
} else {
           if($cart)
            {
            //$cart = session()->get('cart');
            $cart = Cart::where('customer_id', Auth::guard('customer')->user()->id)->get();

          $cources=[];
            foreach($cart as $item){
              array_push($cources,$item->product_id);
            }
            $pay=new \App\Models\PaySuccess;
            $pay->products=json_encode($cources);
            $pay->user_id=Auth::guard('customer')->user()->id;
            $pay->fatora_link=$x['Data']["InvoiceURL"];
            $pay->price=$z;
            $pay->done=0;
            $pay->save();
            //// destroy cart
           //   $cartt = new Cart(session()->get('cart'));
        // if($cartt)
        // {
        //     session()->forget('cart');
            
        // }
        ///////////////////////////
            return Redirect::to($x['Data']["InvoiceURL"]);
            
      }
}

            }
        }
        else
        {
            return 0;

        }

    }
}
