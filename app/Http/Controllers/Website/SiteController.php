<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ContactUserRequest;
use App\Http\Requests\NewsletterRequest;
use App\Models\Newsletter;
use App\Models\Slider;
use App\Models\Setting;
use App\Models\AnimalType;
use App\Models\SiteGallery;
use App\Models\PrivacyPolicy;
use App\Models\CustomerReview;
use App\Models\Blog;
use App\Models\Contact;
use App\Models\Clinic;
use App\Models\Hotel;
use App\Models\Product;
use App\Models\ProductBestSeller;
use App\Models\ProductNewArrival;
use App\Models\Visitor;

use Auth;
use RealRashid\SweetAlert\Facades\Alert;
class SiteController extends Controller
{
    public function index(Request $request)
    {
        $sliders=Slider::getsliders();
        $animal_types=AnimalType::getanimalstype();
        $customer_reviews= CustomerReview::get();
        $blogs= Blog::take(8)->GetAllBlogs();

        $exclusive_products = Product::where('exclusive' , 1)->get();
        $star_products = Product::where('star' , 1)->simplePaginate(4);
        $new_arrival_products = ProductNewArrival::where('status' , 1)->simplePaginate(4);
        $best_seller_products = ProductBestSeller::where('status' , 1)->simplePaginate(4);
        
        
        $Key =$request->ip();
        $sel=Visitor::where('ip_address',$Key)->first();
        if($sel)
        {
            $sel->update([
                'visitor_count' => $sel->visitor_count + 1,
            ]);
        }
        else
        {      
            Visitor::create([
                'ip_address' => $Key,
            ]);
        }

        return view('site.index', compact('sliders' , 'animal_types' , 'customer_reviews','blogs','best_seller_products','star_products','new_arrival_products','exclusive_products'));
    }

    public function get_about_page()
    {
        $about=Setting::aboutpage();
        $gallerys=SiteGallery::paginate(4);
        return view('site.about-us', compact('about','gallerys'));
    }

    public function get_privacy_page()
    {
        $privacy=PrivacyPolicy::privacypage();
        return view('site.privacy-policy', compact('privacy'));
    }

    public function get_contact_page()
    {
        return view('site.contact-us');
    }

    public function store_contact(ContactUserRequest $request)
    {
        $contact=Contact::create($request->all());

        if($contact)
        {
        Alert::success(session('success'),'تم ارسال  رسالتك بنجاح');
          return back();
        }
    
    }

    public function store_newsletter(NewsletterRequest $request)
    {
        $newsletter=Newsletter::create($request->all());

        if($newsletter)
        {
        Alert::success(session('success'),'تم اشتراكك بنجاح');
          return back();
        }
    
    }



     public function getProductsSearch(Request $request)
      {
        $content=$request->get('content');
        if($request->has('content')){

        $get_products=Product::where('status',1)->where('product_name_ar','LIKE', '%' .$content. '%' )->orwhere('product_name_en','LIKE', '%' .$content. '%' )->orwhere('product_description_ar','LIKE', '%' .$content. '%' )->orwhere('product_description_en','LIKE', '%' .$content. '%' )->select('created_at','id','product_code','product_name_'.getLanguageCode().' as product_name', 'product_description_'.getLanguageCode().' as product_description')->simplePaginate(8);  
            }
        //dd($get_products);
        return view('site.search',compact('get_products','content'));
      }
}
