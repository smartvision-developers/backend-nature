<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClinicSetting extends Model 
{

    protected $table = 'clinic_setting';
    public $timestamps = true;
    protected $fillable= array('email','phone', 'address','tax' ,'reserv_price' , 'clinic_id');
    public function Clinic()
    {
        return $this->belongsTo('App\Models\Clinic', 'clinic_id');
    }

}