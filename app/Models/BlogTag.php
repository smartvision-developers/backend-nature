<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogTag extends Model 
{

    protected $table = 'blog_tags';
    public $timestamps = true;

    protected $fillable= array('user_id', 'tag_en', 'tag_ar' , 'status');
    use SoftDeletes;

    protected $dates = ['deleted_at'];


   public function BlogBlogTag()
    {
        return $this->hasMany('App\Models\BlogBlogTag', 'blog_tag_id');
    }
}