<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiteGallery extends Model 
{

    protected $table = 'site_gallery';
    public $timestamps = true;

    protected $fillable=array('site_image');

}