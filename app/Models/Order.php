<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model 
{

    protected $table = 'orders';
    public $timestamps = true;

    protected $fillable=array('customer_id');

    public function Cart()
    {
        return $this->hasMany('App\Models\Cart', 'order_id');
    }

}