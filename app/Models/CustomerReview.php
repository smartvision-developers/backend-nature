<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerReview extends Model 
{

    protected $table = 'customer_reviews';
    public $timestamps = true;

    protected $fillable = array('customer_review','customer_name' , 'job_title');
    use SoftDeletes;

    protected $dates = ['deleted_at'];

}