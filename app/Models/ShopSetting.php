<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopSetting extends Model 
{

    protected $table = 'shop_settings';
    public $timestamps = true;

    protected $fillable = array('shipping_info_ar', 'shipping_info_en', 'bank_name' 
  , 'bank_account', 'tax' ,'delivery_price' );

}
