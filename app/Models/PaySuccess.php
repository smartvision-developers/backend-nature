<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaySuccess extends Model 
{

    protected $table = 'paysuccess';
    public $timestamps = true;
    protected $fillable=array('products','user_id','user_id' ,'fatora_link','price','done');
   public function User()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }


}