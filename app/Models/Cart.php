<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model 
{

    protected $table = 'carts';
    public $timestamps = true;

    use SoftDeletes;
    protected $fillable=array('product_id' ,'customer_id','price_after_quantity','quantity','order_id', 'tax');

    protected $dates = ['deleted_at'];

    public function Product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

   public function Customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }
    public function Order()
    {
        return $this->hasOne('App\Models\Order', 'order_id');
    }

}