<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotelReservation extends Model 
{

    protected $table = 'hotel_reservations';
    
    protected $fillable=array('customer_id','animal_id','animal_no','status','service_id','service_price','reservation_start_date','reservation_end_date','request_no');

    public function AnimalType()
    {
        return $this->belongsTo('App\Models\AnimalType', 'animal_id');
    }


    public function Customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }

    public function HotelService()
    {
        return $this->belongsTo('App\Models\HotelService', 'service_id');
    }

 
}