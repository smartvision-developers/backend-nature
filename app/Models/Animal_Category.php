<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Animal_Category extends Model 
{

    protected $table = 'animal_categorys';
    public $timestamps = true;
    protected $fillable= array('animal_type_id', 'category_id');
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function Category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function AnimalType()
    {
        return $this->belongsTo('App\Models\AnimalType', 'animal_type_id');
    }

    
}