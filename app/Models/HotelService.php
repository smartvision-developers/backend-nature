<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotelService extends Model 
{

    protected $table = 'hotel_services';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable= array('service_description_ar','service_description_en', 'status', 'hotel_id','service_price');
    public function Hotel()
    {
        return $this->belongsTo('App\Models\Hotel', 'hotel_id');
    }

}