<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductAppraisal extends Model 
{

    protected $table = 'product_appraisals';
    public $timestamps = true;

    use SoftDeletes;
    protected $fillable =array('appraisal_num','customer_id','product_id','appraisal_content');
    protected $dates = ['deleted_at'];

    public function Product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

      public function Customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }


}