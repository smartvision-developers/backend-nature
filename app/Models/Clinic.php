<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clinic extends Model 
{

    protected $table = 'clinics';
    public $timestamps = true;

    protected $fillable= array('clinic_image', 'user_id', 'clinic_title_ar', 'clinic_title_en', 'clinic_description_ar','clinic_description_en');
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function ClinicService()
    {
        return $this->hasMany('App\Models\ClinicService', 'clinic_id');
    }
    public function ClinicSetting()
    {
        return $this->hasOne('App\Models\ClinicSetting', 'clinic_id');
    }



  public function scopeGetClinic($query, $clinic_name)
    {
        return $query->select('id','clinic_image','created_at','user_id', 'clinic_title_' . getLanguageCode().' as clinic_title', 'clinic_description_' . getLanguageCode().' as clinic_description')->where('clinic_title_ar', $clinic_name)->orwhere('clinic_title_en',$clinic_name)->first();
    }

}