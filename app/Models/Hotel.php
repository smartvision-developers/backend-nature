<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hotel extends Model 
{

    protected $table = 'hotels';
    public $timestamps = true;

    protected $fillable= array('hotel_image', 'user_id', 'hotel_title_ar', 'hotel_title_en', 'hotel_description_ar','hotel_description_en');
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function HotelService()
    {
        return $this->hasMany('App\Models\HotelService', 'hotel_id');
    }
    public function HotelSetting()
    {
        return $this->hasOne('App\Models\HotelSetting', 'hotel_id');
    }



  public function scopeGetHotel($query, $hotel_name)
    {
        return $query->select('id','hotel_image','created_at','user_id', 'hotel_title_' . getLanguageCode().' as hotel_title', 'hotel_description_' . getLanguageCode().' as hotel_description')->where('hotel_title_ar', $hotel_name)->orwhere('hotel_title_en',$hotel_name)->first();
    }

}