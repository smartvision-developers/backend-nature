<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends Model 
{

    protected $table = 'stores';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable=array('product_id', 'product_quantity', 'status');

    public function Product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

}