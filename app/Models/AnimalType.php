<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnimalType extends Model 
{

    protected $table = 'animal_types';
    public $timestamps = true;
    protected $fillable=array('name_ar','name_en' , 'status', 'image');
    use SoftDeletes;

 public function Category_Product()
    {
        return $this->hasMany('App\Models\Category_Product', 'animal_id');
    }


     public function Animal_Category()
    {
        return $this->hasMany('App\Models\Animal_Category', 'animal_type_id');
    }
public function scopeAnimal($query)
    {
        return $query->select('id','image','name_' . getLanguageCode().' as name')->where('status', 1)->orderBy('id')->get();
    }

    public function scopeGetAnimalsType($query)
    {
        return $query->select('id','name_en','image','name_' . getLanguageCode().' as name')->where('status', 1)->orderBy('id', 'DESC')->with('Animal_Category')->take(6)->get();
    }

    protected $dates = ['deleted_at'];

}