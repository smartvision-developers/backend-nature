<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogComment extends Model 
{

    protected $table = 'blog_comments';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable= array('blog_id','comment', 'customer_id');
    public function Customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }

    public function Blog()
    {
        return $this->belongsTo('App\Models\Blog', 'blog_id');
    }

}