<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model 
{

    protected $table = 'settings';
    public $timestamps = true;

    protected $fillable=array('about_us_ar', 'about_us_en','our_vision_ar', 'our_vision_en','video', 'twitter_link','facebook_link','snapchat_link','email', 'address', 'phone_no','instagram_link','youtube_link');

  public function scopeAboutPage($query)
    {
        return $query->select('id','video', 'about_us_' . getLanguageCode().' as about_us', 'our_vision_' . getLanguageCode().' as our_vision')->first();
    }



}