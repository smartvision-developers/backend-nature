<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Trademark extends Model 
{

    protected $table = 'trademarks';
    public $timestamps = true;

    use SoftDeletes;

    protected $fillable= array('image', 'name','status');
    protected $dates = ['deleted_at'];

}