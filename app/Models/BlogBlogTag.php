<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogBlogTag extends Model 
{

    protected $table = 'blog_blogtags';
    public $timestamps = true;

    use SoftDeletes;
    protected $fillable= array('blog_id', 'blog_tag_id');
    protected $dates = ['deleted_at'];

    public function Blog()
    {
        return $this->belongsTo('App\Models\Blog', 'blog_id');
    }

    public function BlogTag()
    {
        return $this->belongsTo('App\Models\BlogTag', 'blog_tag_id');
    }

}