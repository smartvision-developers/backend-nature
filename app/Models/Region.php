<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model 
{

    protected $table = 'regions';
    
    protected $fillable=array('region_name_ar','city_id');
    public function City()
    {
        return $this->belongsTo('App\Models\City', 'city_id');
    }

}