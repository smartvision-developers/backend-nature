<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrivacyPolicy extends Model 
{

    protected $table = 'privacy_policys';
    public $timestamps = true;
public function scopePrivacyPage($query)
    {
        return $query->select('id', 'term_of_use_' . getLanguageCode().' as term_of_use', 'privacy_policy_' . getLanguageCode().' as privacy_policy')->first();
    }

}   