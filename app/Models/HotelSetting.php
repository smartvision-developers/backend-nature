<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotelSetting extends Model 
{

    protected $table = 'hotel_setting';
    public $timestamps = true;
    protected $fillable= array('email','phone', 'address' , 'hotel_id');
    public function Hotel()
    {
        return $this->belongsTo('App\Models\Hotel', 'hotel_id');
    }

}