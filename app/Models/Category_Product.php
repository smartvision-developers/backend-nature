<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category_Product extends Model 
{

    protected $table = 'category_products';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable=array('category_id','product_id','animal_id');

    public function Product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function Category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

     public function AnimalType()
    {
        return $this->belongsTo('App\Models\AnimalType', 'animal_id');
    }

}