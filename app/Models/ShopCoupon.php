<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopCoupon extends Model 
{

    protected $table = 'shop_coupons';
    public $timestamps = true;

    protected $fillable= array('coupon_code','coupon_name','coupon_discount', 'status','expire_date');
    use SoftDeletes;

    protected $dates = ['deleted_at'];

}