<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slider extends Model 
{

    protected $table = 'sliders';
    public $timestamps = true;
    protected $fillable=array('slider_img','slider_text_ar','slider_text_en', 'status');

    protected $dates = ['deleted_at'];
    public function scopeGetSliders($query)
    {
        return $query->select('id','slider_img','slider_text_' . getLanguageCode().' as slider_text')->where('status', 1)->orderBy('id', 'DESC')->get();
    }
}