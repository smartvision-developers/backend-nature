<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model 
{

    protected $table = 'categorys';
    public $timestamps = true;

    protected $fillable=array('category_name_ar','category_name_en', 'status');
    use SoftDeletes;

  public function scopeCategory($query)
    {
        return $query->select('id','category_name_' . getLanguageCode().' as category_name')->where('status', 1)->orderBy('id')->get();
    }

    public function Animal_Category()
    {
        return $this->hasMany('App\Models\Animal_Category', 'category_id');
    }

    public function Category_Product()
    {
        return $this->hasMany('App\Models\Category_Product', 'category_id');
    }
    protected $dates = ['deleted_at'];

}