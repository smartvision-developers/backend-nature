<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClinicService extends Model 
{

    protected $table = 'clinic_services';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable= array('service_description_ar','service_description_en', 'status', 'clinic_id','service_price');
    public function Clinic()
    {
        return $this->belongsTo('App\Models\Clinic', 'clinic_id');
    }

}