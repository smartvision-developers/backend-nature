<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopBill extends Model 
{

    protected $table = 'shop_bills';
    public $timestamps = true;

    use SoftDeletes;
    protected $fillable=array('request_no','order_total_price' ,'order_id','price_after_tax','customer_id','status','coupon_id','receive_type','new_address','bill_address','shippment_address','payment_method','price_after_coupon');

    protected $dates = ['deleted_at'];

    public function ShopCoupon()
    {
        return $this->belongsTo('App\Models\ShopCoupon', 'coupon_id');
    }
    
    public function Customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }
    public function ShippmentAddress()
    {
        return $this->belongsTo('App\Models\ShippmentAddress', 'shippment_address');
    }
    public function Order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }
}