<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model 
{

    protected $table = 'citys';
    public $timestamps = true;

    protected $fillable=array('city_name_ar');

    public function Region()
    {
        return $this->hasMany('App\Models\Region', 'city_id');
    }


}