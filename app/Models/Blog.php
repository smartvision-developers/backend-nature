<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model 
{

    protected $table = 'blogs';
    public $timestamps = true;
    protected $fillable=array('blog_title_en','blog_title_ar','user_id' ,'blog_description_ar','blog_description_en','blog_text_ar','blog_text_en', 'status', 'blog_image');

    use SoftDeletes;

    protected $dates = ['deleted_at'];

   public function User()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

 public function BlogComment()
    {
        return $this->hasMany('App\Models\BlogComment', 'blog_id');
    }

     public function BlogBlogTag()
    {
        return $this->hasMany('App\Models\BlogBlogTag', 'blog_id');
    }

  public function scopeGetAllBlogs($query)
    {
        return $query->select('id','blog_image','created_at','user_id', 'blog_title_' . getLanguageCode().' as blog_title', 'blog_description_' . getLanguageCode().' as blog_description', 'blog_text_' . getLanguageCode().' as blog_text')->where('status', 1)->orderBy('id', 'DESC')->paginate(8);
    }



  public function scopeGetSingleBlog($query, $blog_name)
    {
        return $query->select('id','blog_image','created_at','user_id', 'blog_title_' . getLanguageCode().' as blog_title', 'blog_description_' . getLanguageCode().' as blog_description', 'blog_text_' . getLanguageCode().' as blog_text')->where('status', 1)->where('blog_title_ar', $blog_name)->orwhere('blog_title_en',$blog_name)->orderBy('id', 'DESC')->first();
    }


    public function scopeSort2($query)
    {
        return $query->select('id','blog_image','created_at','user_id', 'blog_title_' . getLanguageCode().' as blog_title', 'blog_description_' . getLanguageCode().' as blog_description', 'blog_text_' . getLanguageCode().' as blog_text')->where('status', 1)->orderBy('id','ASC')->paginate(8);
    }

}