<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Customer extends Authenticatable 
{
        protected $guard = 'customer';

    protected $table = 'customers';
    public $timestamps = true;

    protected $fillable=array('name', 'phone', 'password', 'email','type' ,'repassword','photo','spare_email','code','forget_password');
    use SoftDeletes;

    protected $dates = ['deleted_at'];


    public function ShippmentAddress()
    {
        return $this->hasMany('App\Models\ShippmentAddress', 'customer_id');
    }

    public function ClinicReservation()
    {
        return $this->hasMany('App\Models\ClinicReservation', 'customer_id');
    }

    public function Cart()
    {
        return $this->hasMany('App\Models\Cart', 'customer_id');
    }
   public function HotelReservation()
    {
        return $this->hasMany('App\Models\HotelReservation', 'customer_id');
    }
}