<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product_Favourite extends Model 
{

    protected $table = 'product_favourites';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable= array('customer_id','product_id');
    public function Customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }

    public function Product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

}