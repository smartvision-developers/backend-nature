<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model 
{

    protected $table = 'products';
    public $timestamps = true;

    protected $fillable= array('product_name_ar','product_name_en', 'product_code',
    'product_longdescription_ar', 'product_longdescription_en' ,'views', 'status','star','product_description_ar', 'product_description_en' , 'user_id' ,'product_unitprice','product_oldprice','exclusive');
    use SoftDeletes;

    protected $dates = ['deleted_at'];


    public function ProductImage()
    {
        return $this->hasMany('App\Models\ProductImage', 'product_id');
    }

 public function ProductAppraisal()
    {
        return $this->hasMany('App\Models\ProductAppraisal', 'product_id');
    }

 public function Product_Favourite()
    {
        return $this->hasOne('App\Models\Product_Favourite', 'product_id');
    }
   public function Category_Product()
    {
        return $this->hasOne('App\Models\Category_Product', 'product_id');
    }

    public function Store()
    {
        return $this->hasOne('App\Models\Store', 'product_id');
    }

      public function scopeSingleProduct($query, $code)
    {
        return $query->select('id','product_code','product_oldprice','product_unitprice','product_name_' . getLanguageCode().' as product_name','product_description_' . getLanguageCode().' as product_description','product_longdescription_' . getLanguageCode().' as product_longdescription')->where('status', 1)->orderBy('id')->where('product_code', $code)->first();
    }
}