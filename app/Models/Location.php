<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model 
{

    protected $table = 'locations';
    
    protected $fillable=array('location_name_ar','region_id');

    public function Region()
    {
        return $this->belongsTo('App\Models\Region', 'region_id');
    }

}