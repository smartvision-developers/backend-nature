<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippmentAddress extends Model 
{

    protected $table = 'shippment_addresses';
    
    protected $fillable=array('customer_id','city_id','phone','status','address_details','address_type','location_id','region_id');

    public function Region()
    {
        return $this->belongsTo('App\Models\Region', 'region_id');
    }


    public function Customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }

    public function Location()
    {
        return $this->belongsTo('App\Models\Location', 'location_id');
    }

    public function City()
    {
        return $this->belongsTo('App\Models\City', 'city_id');
    }
}