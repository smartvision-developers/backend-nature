<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductDiscount extends Model 
{

    protected $table = 'product_discount';
    public $timestamps = true;

    use SoftDeletes;

    protected $fillable=array('product_id', 'product_discount_rate')
    protected $dates = ['deleted_at'];

    public function Product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

}