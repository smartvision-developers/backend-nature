<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductNewArrival extends Model 
{

    protected $table = 'product_new_arrivals';
    public $timestamps = true;

    protected $fillable= array( 'status' , 'user_id' ,'product_id');


    public function Product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

 public function User()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

}