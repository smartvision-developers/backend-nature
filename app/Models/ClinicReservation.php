<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClinicReservation extends Model 
{

    protected $table = 'clinic_reservations';
    
    protected $fillable=array('customer_id','animal_id','animal_no','status','service_id','service_price','reservation_date','clinic_id','request_no');

    public function AnimalType()
    {
        return $this->belongsTo('App\Models\AnimalType', 'animal_id');
    }


    public function Customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }


  public function Clinic()
    {
        return $this->belongsTo('App\Models\Clinic', 'clinic_id');
    }

    public function ClinicService()
    {
        return $this->belongsTo('App\Models\ClinicService', 'service_id');
    }

 
}