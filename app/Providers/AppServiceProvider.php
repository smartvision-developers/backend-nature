<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use App\Models\Product_Favourite;
use App\Models\Cart;
use App\Models\Trademark;
use App\Models\Setting;

use Auth;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
     view()->composer('*', function ($view) 
        {
            if (Auth::guard('customer')->check()) {
                $get_fav_count=Product_Favourite::where('customer_id', Auth::guard('customer')->user()->id)->count();
                $get_cart_count=Cart::where('customer_id', Auth::guard('customer')->user()->id)->count();

            }
            else {
                    $get_fav_count=0;

                    $get_cart_count=0;
               }   
         $get_trademarks=Trademark::get();
         $setting=Setting::findOrFail(1);
        $view->with(compact('get_fav_count','get_cart_count','get_trademarks','setting'));

        });
        

        Schema::defaultStringLength(191);
    }
}
