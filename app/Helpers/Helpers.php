<?php
use Illuminate\Support\Facades\Config;

function get_all_lang()
{
    return \App\Models\Language::where('active', 1)->get();     // return array of all lang in dashboard create form
}

function get_default_lang()
{
    return Config::get('app.locale'); //return default whick make in config app file
}

function upload_image($folder, $image)
{
    $image->store('/', $folder);
    $filename=$image->hashName();
    $path= 'images/'. $folder .'/'. $filename;
    
    return $path;
}


function getLanguageCode() {
    return LaravelLocalization::getCurrentLocale();
}

 function getIp(){
    foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
        if (array_key_exists($key, $_SERVER) === true){
            foreach (explode(',', $_SERVER[$key]) as $ip){
                $ip = trim($ip); // just to be safe
                if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                    return $ip;
                }
            }
        }
    }
}


?>