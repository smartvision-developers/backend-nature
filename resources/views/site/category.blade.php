@extends('site/layout/main')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />

@endsection

@php         $animals=\App\Models\AnimalType::get();

@endphp
@section('content')
<main class="main-content col-xs-12">
    <div class="bread-crumb col-xs-12" style="background-image: url({{URL('public/nature-sound')}}/images/hero-section/hero.png)">
        <div class="container">
            <h3>المتجر</h3>
            <ul>
                <li>
                    <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">{{__('translate.home')}}</a>
                </li>
                <li>
                    <a href="#">المتجر</a>
                </li>
<!--                 <li>الكلاب</li>
 -->            </ul>
        </div>
    </div>
    <div class="categ-wrap col-xs-12">
        <div class="container">
            <div class="cat-box col-md-9 col-xs-12" id="updateDiv">
                <div class="cat-toolbar col-xs-12">
                   <!--  <select class="form-control nice-select">
                        <option>ترتيب حسب</option>
                        <option>ترتيب حسب</option>
                        <option>ترتيب حسب</option>
                        <option>ترتيب حسب</option>
                    </select> -->
                    <div>
                        <button type="button" class="btn grid-view active">
                            <i class="la la-th"></i>
                        </button>
                        <button type="button" class="btn list-view">
                            <i class="la la-list"></i>
                        </button>
                    </div>
                </div>
                <div class="search-wrap col-xs-12 grid__">
                    @foreach($products as $key1 => $value1)

                    <div class="block col-md-4 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                        <div class="product-card">
                            <div class="p-inner">
                                <div class="p-img">
                                    <div class="p-actions">
                                      @php
                                      $cart=\App\Models\Cart::where('product_id', $value1->Product->id)->where('customer_id',auth()->guard('customer')->id())->first();
                                      @endphp

                                    <input type="hidden" class="quantity" value="1" name="quantity">

                                     <input type="hidden" class="price_after_quantity" value="{{$value1->Product->product_unitprice}}" name="price_after_quantity">
                                      @if(!$cart)
                                      <button class="add-pro cart_{{$value1->Product->id}}" type="button" onclick="addProductToCart(this, {{$value1->Product->id}}, {{$value1->Product->product_unitprice}})" data-tool="tooltip" title="اضف للسلة" data-placement="right">
                                        <i class="las la-shopping-bag"></i>
                                    </button>
                                    @else
                                    <button class="add-pro cart_{{$value1->Product->id}}" type="button" onclick="addProductToCart(this, {{$value1->Product->id}}, {{$value1->Product->product_unitprice}})" data-tool="tooltip" title="حذف من السله" data-placement="right">
                                        <i class="las la-trash" style="color:red;"></i>
                                    </button>
                                    @endif


                                    @php
                                    $fav=\App\Models\Product_Favourite::where('product_id', $value1->Product->id)->where('customer_id',auth()->guard('customer')->id())->first();
                                    @endphp
                                    @if(!$fav)
                                    <button type="button" class="add-fav fav_{{$value1->Product->id}}" onclick="addProductToFav(this, {{$value1->Product->id}})" data-tool="tooltip" title="اضف للمفضلة" data-placement="right">
                                        <i class="las la-heart"></i>
                                    </button>
                                    @else
                                    <button type="button" class="add-fav fav_{{$value1->Product->id}}" onclick="addProductToFav(this, {{$value1->Product->id}})" data-tool="tooltip" title="حذف من  لامفضلة" data-placement="right">
                                        <i class="las la-heart" style="color:red;"></i>
                                    </button>
                                    @endif


                                    <button type="button" data-tool="tooltip" title="عرض سريع" data-placement="right">
                                        <i class="las la-search"></i></button>
                                        <button type="button" data-tool="tooltip" title="اضف للمقارنة" data-placement="right">
                                            <i class="las la-sync"></i>
                                        </button>
                                    </div>
                                    <img  lazy="loading" src="{{URL('/public/images/products/'. $value1->Product->ProductImage[0]->product_image)}}" alt="{{$value1->Product->product_name_ar}}">
                                </div>
                                <div class="p-data">
                                    <a href="{{asset('/product/'.$value1->Product->product_code)}}">@if(app()->getLocale() == 'ar') {{$value1->Product->product_name_ar}} @elseif(app()->getLocale() == 'en'){{$value1->Product->product_name_en}}@endif
                                    </a>
                                    <p class="desc">
                                        @if(app()->getLocale() == 'ar') {{$value1->Product->product_description_ar}} @elseif(app()->getLocale() == 'en'){{$value1->Product->product_description_en}} @endif
                                        </p>
                                    <p>
                                        <span>{{$value1->Product->product_unitprice}} ريال</span>

                                        @if($value1->Product->product_oldprice)
                                        <span class="old-price">{{ $value1->Product->product_oldprice }} ريال</span>
                                    @endif                                            </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>

                <div class="g-more col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                    <button class="see-more btn" data-page="2" data-link="{{asset('/category')}}?page=" data-div=".grid__">{{__('translate.more')}}</button> 
                </div>

            </div>
            <div class="cat-sidebar col-md-3 col-xs-12">
                <div class="c-widget">
                    <h4>اقسام المنتجات</h4>
                    <div class="w-bdy">
                        <div class="w-deps">
                            <div class="panel-group" id="accordion">
                                @php $count= 1;@endphp
                                @foreach($animals as $key=> $value)
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        @php
                                                $new1 = str_replace(' ', '%20', $value->name_en);
                                     @endphp
                                        <a class="@if ($count != 1) collapsed @endif" >      
                                     </a>
                        <label><input class="animal_id" data-target="#{{$count}}" type="checkbox" name="animal_id" value="{{$value->id}}" data-toggle="collapse" data-parent="#accordion"  @if(str_contains(url()->current(), '/filter/'.$new1)) checked  @endif>  
                                <span>{{$value->name_ar}}</span>
                         </label>

                                                                            </div>
                                    <div id="{{$count}}" class="panel-collapse collapse @if(str_contains(url()->current(), '/filter/'.$new1)) in @endif">
                                        <div class="panel-body">
                                            <ul>
                                                @php   $categoryp = \App\Models\Animal_Category::where('animal_type_id', $value->id)->get();

                                                @endphp

                                                @foreach($value->Animal_Category as $value22)             @php             
                                                $new1 = str_replace(' ', '%20', $value22->AnimalType->name_en);

                                                $new = str_replace(' ', '%20', $value22->Category->category_name_en);


                                              
                                                @endphp
                                                <li>

                                                    <label>
                                                        <input type="checkbox" class="form-control try2"  value="{{$value22->category_id}}"  @if(str_contains(url()->current(), '/filter/'.$new1.'/'.$new)) checked @endif><span> {{$value22->Category->category_name_en}}</span></label>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    @php $count++;@endphp
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="c-widget">
                        <h4>السعر</h4>
                        <div class="w-bdy">
                            <div class="price-wrap">
                                <div id="slider-range"></div>
<!--                                     <input type="text" id="amount" readonly>
-->                                
<input class="amount" type="text" id="amount_start" name="start_price"
readonly="readonly" /></b>

<input class="amount" type="text" id="amount_end" name="end_price"  readonly="readonly"/></b>

</div>
</div>
</div>

<div class="c-widget">
    <div class="c-adds">
        <img  lazy="loading" src="images/ads-banner.png" alt="">
        <div class="ads-cap">
            <h5>
                توفير اكثر من <b>25%</b>
            </h5>
            <p>طعام واكسسورات الحيوانات</p>
            <a href="#" class="btn">تسوق الان</a>
        </div>
    </div>
</div>

<div class="c-widget">
    <h4>منتجات مميزة</h4>
    <div class="w-bdy">
        <div class="sp-products">
            <ul>
                @foreach($get_star_products as $s_product)
                <li>
                    <div class="sp-img">
                        <img  lazy="loading" src="{{URL('public/images/products/'. $s_product->ProductImage[0]->product_image)}}" alt="{{$s_product->product_name_ar}}">
                        <a href="{{asset('/product/'.$s_product->product_code)}}"></a>
                    </div>
                    <div class="sp-data">
                        <a href="{{asset('/product/'.$s_product->product_code)}}">{{$s_product->product_name_ar}}</a>
                                              <!--   <p>
                                                    <i class="la la-star active"></i>
                                                    <i class="la la-star active"></i>
                                                    <i class="la la-star active"></i>
                                                    <i class="la la-star active"></i>
                                                    <i class="la la-star active"></i>
                                                </p> -->
                                                <p>
                                                    <span>{{$s_product->product_unitprice}} ريال</span>
                                                    @if($s_product->product_oldprice)
                                                    <span class="old-price">{{ $s_product->product_oldprice }} ريال</span>
                                                @endif                                                </p>
                                            </div>
                                        </li>
                                        @endforeach

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @stop
            @section('js')
            <script src="{{URL('public/nature-sound')}}/js/jquery.nice-select.min.js"></script>
            <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
            <script src="{{URL('public/nature-sound')}}/js/aos.js"></script>

            <script>
                $('.grid-view').click(function (){
                    $('.search-wrap').addClass('grid__').removeClass('list__');
                    $(this).addClass('active');
                    $('.list-view').removeClass('active');
                });

                $('.list-view').click(function (){
                    $('.search-wrap').addClass('list__').removeClass('grid__');
                    $(this).addClass('active');
                    $('.grid-view').removeClass('active');
                });
                $(document).ready(function() {

                    $("#slider-range").slider({
                        range: true,
                        min: 0,
                        max: 10000,
                        values: [1, 10000],
                        slide: function (event, ui) {
                            $("#amount_start").val(ui.values[ 0 ]);
                            $("#amount_end").val(ui.values[ 1 ]);
                            var start = $('#amount_start').val();
                            var end = $('#amount_end').val();
                            $.ajax({
                                type: 'get',
                   // dataType: 'html',
                   url: '',
                   data: "start=" + start + "& end=" + end,
                   success: function (response) {
                       // console.log(response);
                       if (response.length == 0) {
                        $('#updateDiv').append('لا توجد بيانات للعرض');
                    } else {

                        $('#updateDiv').html(response);
                    }
                }
            });
                        }
                    });

                    $("#amount_start").val("ج.م" + $("#slider-range").slider("values", 0) + " - ج.م" + $("#slider-range").slider("values", 1));


                    
                    $('.try2').click(function(){
                        $('#updateDiv').empty();

          //  alert('hardeep');

          var category = [];
var animal=[];

          var animal_id = $('.animal_id').val();
          console.log(animal_id);

          $('.try2').each(function(){

            if($(this).is(":checked")){
                category.push($(this).val());
            }

//                console.log(category);

});

               $('.animal_id').each(function(){

            if($(this).is(":checked")){
                            animal.push($(this).val());

            }

//                console.log(category);

});
                    Finalanimal  = animal.toString();

          Finalcategory  = category.toString();


          if(category.length != 0)
          {
            $.ajax({
                type: 'get',
                  //  dataType: 'html',
                  url: '',
                  data: {
                   "category" :Finalcategory,
                   "animal_id" : Finalanimal
               },
               success: function (response) {


                $('#updateDiv').html(response);

            },

        });
        }
        else
        {
            $('#updateDiv').append('لا توجد بيانات للعرض');

        }
    });
                });



                $(".see-more").click(function() {
  $div = $($(this).data('div')); //div to append
  $link = $(this).data('link'); //current URL

  $page = $(this).data('page'); //get the next page #
  $href = $link + $page; //complete URL
  $.get($href, function(response) { //append data
    $html = $(response).find(".grid__").html(); 
    console.log($link);
    $div.append($html);
});

  $(this).data('page', (parseInt($page) + 1)); //update page #
});



</script>

@endsection