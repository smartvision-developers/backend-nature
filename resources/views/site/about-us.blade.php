@extends('site/layout/main')
@section('content')

<main class="main-content col-xs-12">
    <div class="bread-crumb col-xs-12" style="background-image: url({{URL('public/nature-sound/images/hero-section/hero.png')}})">
        <div class="container">
            <h3>{{__('translate.about-us')}} </h3>
            <ul>
                <li>
                    <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">{{__('translate.home')}}</a>
                </li>
                <li>{{__('translate.about-us')}} </li>
            </ul>
        </div>
    </div>
    <div class="about-wrap col-xs-12">
        <div class="ab-top col-xs-12">
            <div class="container">
                <div class="ab-img col-md-4 col-xs-12">
                    <img lazy="loading" src="{{URL('public/nature-sound')}}/images/hero-section/hero.png" alt="" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                    <img lazy="loading" src="{{URL('public/nature-sound')}}/images/blog.png" alt="" data-aos="fade-up-right" data-aos-duration="1000" data-aos-delay="400">
                </div>
                <div class="ab-data col-md-8 col-xs-12">
                    <div class="g-head" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                        <span>{{__('translate.about-us')}}</span>
                        <h3>{{__('translate.about-txt')}}</h3>
                    </div>
                    <p data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300"><?php echo $about->about_us; ?></p>
                    <h3 data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">{{__('translate.our-mission')}}</h3>
                    <p data-aos="fade-up" data-aos-duration="1000" data-aos-delay="500"> <?php echo $about->our_vision; ?></p>
                </div>
            </div>
        </div>
        <div class="ab-vid col-xs-12" style="background-image: url({{URL('public/nature-sound')}}/images/hero-section/hero.png)" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
            <a href="{{$about->video}}" data-fancybox>
                <i class="la la-play"></i>
            </a>
        </div>
        <div class="ab-gallery col-xs-12">
            <div class="container" id="site_gallery">
                <div class="g-head col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                    <span>{{__('translate.gallery')}}</span>
                    <h3>{{__('translate.photo-gallery')}}</h3>
                </div>
                <div class="g-body col-xs-12" id="gallerys">
                    @foreach($gallerys as $val)
                    <div class="block col-md-3 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                        <img lazy="loading" src="{{URL('/'.$val->site_image)}}" alt="">
                        <a href="{{URL('/'.$val->site_image)}}" data-fancybox="images">
                            <i class="la la-search"></i>
                        </a>
                    </div>
                    @endforeach
                    
                </div>
                <div class="g-more col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                    <button class="see-more btn" data-page="2" data-link="{{asset('/about-us')}}?page=" data-div="#gallerys">{{__('translate.more')}}</button> 
                </div>
            </div>
        </div>
    </div>
    @stop
    @section('js')
    <script src="{{URL('public/nature-sound')}}/js/jquery.nice-select.min.js"></script>
    <script>
        $('.grid-view').click(function (){
            $('.search-wrap').addClass('grid__').removeClass('list__');
            $(this).addClass('active');
            $('.list-view').removeClass('active');
        });
        
        $('.list-view').click(function (){
            $('.search-wrap').addClass('list__').removeClass('grid__');
            $(this).addClass('active');
            $('.grid-view').removeClass('active');
        });


        $(".see-more").click(function() {
  $div = $($(this).data('div')); //div to append
  $link = $(this).data('link'); //current URL

  $page = $(this).data('page'); //get the next page #
  $href = $link + $page; //complete URL
  $.get($href, function(response) { //append data
    $html = $(response).find("#gallerys").html(); 
    console.log($link);
    $div.append($html);
});

  $(this).data('page', (parseInt($page) + 1)); //update page #
});

</script>
@endsection