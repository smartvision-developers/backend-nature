@extends('site/layout/main')
@section('style')
<style type="text/css">
.blog-card
{
    height: 400px !important;
}
</style>
@endsection
@section('content')
<main class="main-content col-xs-12">
    <div class="bread-crumb col-xs-12" style="background-image: url({{URL('public/nature-sound/images/hero-section/hero.png')}})">
        <div class="container">
            <h3>{{__('translate.blog')}}</h3>
            <ul>
                <li>
                    <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">{{__('translate.home')}}</a>
                </li>
                <li>{{__('translate.blog')}}</li>
            </ul>
        </div>
    </div>
    <div class="categ-wrap blog-wrap col-xs-12">
        <div class="container">
            <div class="cat-toolbar col-xs-12">
                <select class="form-control nice-select" onchange="window.location.href=this.value;">
                    <option value="{{route('site.sort1')}}">{{__('translate.n-t-o')}}</option>
                    <option value="{{route('site.sort2')}}">{{__('translate.o-t-n')}}</option>

<!--                             <option>ترتيب حسب</option>
                            <option>ترتيب حسب</option>
                        -->                        </select>
                        <div>
                            <button type="button" class="btn grid-view active">
                                <i class="la la-th"></i>
                            </button>
                            <button type="button" class="btn list-view">
                                <i class="la la-list"></i>
                            </button>
                        </div>
                    </div>
                    <div class="search-wrap col-xs-12" id="blogs">
                        @foreach($blogs as $blog)
                        <div class="block col-md-3 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                            <div class="blog-card">
                                <div class="b-img">
                                    <img lazy="loading" src="{{URL('/'. $blog->blog_image)}}" alt="{{$blog->blog_title}}">
                                    <a href="{{URL('blog/'. $blog->blog_title)}}"></a>
                                </div>
                                <div class="b-data">
                                    <a href="{{URL('blog/'. $blog->blog_title)}}" class="title">{{$blog->blog_title}} </a>
                                    <p> {{$blog->blog_text}}</p>
                                    <p>
                                        <a href="{{URL('blog/'. $blog->blog_title)}}">
                                            {{__('translate.details')}}
                                            <i class="la la-angle-left"></i>
                                        </a>
                                        <b>{{($blog->created_at)->toDateString()}}</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="g-more col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="500">
                        <button class="see-more btn" data-page="2" data-link="{{asset('/blogs')}}?page=" data-div="#blogs">{{__('translate.more')}}</button> 
                    </div>


                </div>
            </div>

            @stop
            @section('js')
            <script src="{{URL('public/nature-sound')}}/js/jquery.nice-select.min.js"></script>
            <script>
                $('.grid-view').click(function (){
                    $('.search-wrap').addClass('grid__').removeClass('list__');
                    $(this).addClass('active');
                    $('.list-view').removeClass('active');
                });
                
                $('.list-view').click(function (){
                    $('.search-wrap').addClass('list__').removeClass('grid__');
                    $(this).addClass('active');
                    $('.grid-view').removeClass('active');
                });


                $(".see-more").click(function() {
  $div = $($(this).data('div')); //div to append
  $link = $(this).data('link'); //current URL

  $page = $(this).data('page'); //get the next page #
  $href = $link + $page; //complete URL
  $.get($href, function(response) { //append data
    $html = $(response).find("#blogs").html(); 
    console.log($link);
    $div.append($html);
});

  $(this).data('page', (parseInt($page) + 1)); //update page #
});

</script>

@endsection