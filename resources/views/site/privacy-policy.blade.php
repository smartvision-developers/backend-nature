@extends('site/layout/main')
@section('content')

<main class="main-content col-xs-12">
    <div class="bread-crumb col-xs-12" style="background-image: url({{URL('public/nature-sound/images/hero-section/hero.png')}})">
        <div class="container">
            <h3>{{__('translate.privacy')}} </h3>
            <ul>
                <li>
                    <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">{{__('translate.home')}}</a>
                </li>
                <li>{{__('translate.privacy')}} </li>
            </ul>
        </div>
    </div>
     <div class="privacy-wrap col-xs-12">
                <div class="container">
                    <h3>{{__('translate.privacy')}} </h3>
                    <?php echo $privacy->privacy_policy ?>
                    <h3>{{__('translate.terms')}}</h3>
                    <ul>
                                           <?php echo $privacy->term_of_use?>

                    </ul>
                </div>
            </div>
            
          
    @stop
    @section('js')
 @endsection