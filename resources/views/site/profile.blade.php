@extends('site/layout/main')
@section('content')
 <main class="main-content col-xs-12">
            <div class="bread-crumb col-xs-12" style="background-image: url({{URL('public/nature-sound')}}/images/hero-section/hero.png)">
                <div class="container">
                    <h3>{{__('translate.my-account')}}</h3>
                    <ul>
                        <li>
                            <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">{{__('translate.home')}}</a>
                        </li>
                        <li>{{__('translate.my-account')}}</li>
                    </ul>
                </div>
            </div>
            <div class="profile-wrap col-xs-12">
                <div class="container">
                    <a href="javascript:void(0)" class="op-sidebar">
                        <img lazy="loading" src="{{URL('/').Auth::guard('customer')->user()->photo}}" alt="{{Auth::guard('customer')->user()->name}}">
                    </a>
                    <div class="prof-sidebar col-md-3 col-xs-12">
                        <div class="prof-top">
                            <button type="button" class="clo-sidebar">
                                <i class="la la-close"></i>
                            </button>
                            <div class="s-img">
                        <img lazy="loading" src="{{URL('/')}}/{{Auth::guard('customer')->user()->photo}}" alt="{{Auth::guard('customer')->user()->name}}" id="blah1">

                          <form enctype="multipart/form-data" action="{{route('update-photo',  Auth::guard('customer')->user()->id)}}" method="post">
                                @csrf

                                <label>

                                    <input id="photo" type="file" name="photo" onchange="document.getElementById('blah1').src = window.URL.createObjectURL(this.files[0])">
                                    <i class="la la-camera"></i>
                                </label>
                                <button type="submit" class="i-btn">
                                    <i class="la la-check"></i>
                                </button>
                                <button type="button" class="i-btn i-red">
                                    <i class="la la-close"></i>
                                </button>
                            </form>
                            </div>
                            <h3>{{Auth::guard('customer')->user()->name }} </h3>
                            <p>{{Auth::guard('customer')->user()->email }}</p>
                        </div>
                        <ul>
                            <li class="active">
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/profile')}}">{{__('translate.profile')}}</a>
                            </li>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/my-orders')}}">مشترياتى</a>
                            </li>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/my-reservations')}}">حجوزاتى</a>
                            </li>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/shippment-address')}}">{{__('translate.shipping-addresses')}}</a>
                            </li>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/logout')}}">{{__('translate.logout')}}</a>
                            </li>
                        </ul>
                    </div>
                    <div class="prof-content col-md-9 col-sm-12 col-xs-12">
                        <div class="prof-head col-xs-12">
                            <h4>{{__('translate.profile')}}</h4>
                        </div>
                        <div class="prof-body col-xs-12">
                            <div class="personal-data">
                                <ul>
                                    <li>{{__('translate.name')}} :  {{Auth::guard('customer')->user()->name }}   </li>
                                    <li>{{__('translate.email')}} :  @if(Auth::guard('customer')->user()->email){{Auth::guard('customer')->user()->email }} @else  لا توجد @endif </li>
                                    <li>{{__('translate.phone')}} :{{Auth::guard('customer')->user()->phone }} </li>
                                    <li>{{__('translate.password')}} : *********</li>
                                </ul>
                                <a href="javascript:void(0)" class="btn edit__">{{__('translate.update')}}</a>
                            </div>
                            <div class="edit-personal">
                                <ul>
                                    <li>
                                        <div>
                                            <h4>{{__('translate.name')}}</h4>
                                            <span>{{Auth::guard('customer')->user()->name }} </span>
                                        </div>
                                        <a href="#" class="btn" data-target="#Nname_pop" data-toggle="modal">{{__('translate.update')}}</a>
                                    </li>
                                    <li>
                                        <div>
                                            <h4>{{__('translate.email')}}</h4>
                                            <span>@if(Auth::guard('customer')->user()->email) {{Auth::guard('customer')->user()->email }} @else لا توجد @endif</span>
                                        </div>
                                        <a href="#" class="btn" data-target="#Nemail_pop" data-toggle="modal">{{__('translate.update')}}</a>
                                    </li>
                                    <li>
                                        <div>
                                            <h4>{{__('translate.phone')}}</h4>
                                            <span>{{Auth::guard('customer')->user()->phone }} </span>
                                        </div>
                                        <a href="#" class="btn" data-target="#Nphone_pop" data-toggle="modal">{{__('translate.update')}}</a>
                                    </li>
                                    <li>
                                        <div>
                                            <h4>{{__('translate.password')}}</h4>
                                            <span>***********</span>
                                        </div>
                                        <a href="#" class="btn" data-target="#Npass_pop" data-toggle="modal">{{__('translate.update')}}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
               <div class="modal fade profile_modals" id="Nname_pop"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content col-xs-12">
                    <div class="modal-header col-xs-12">
                        <button type="button" class="close" data-dismiss="modal">
                            <i class="la la-close"></i>
                        </button>
                        <h4 class="modal-title">تغير الاسم</h4>
                    </div>
                    <div class="modal-body col-xs-12">
                        <div class="form-wrapo col-xs-12">
                            <form action="{{route('edit-profile',  Auth::guard('customer')->user()->id)}}" method="put">
                                @csrf
                                 {{ method_field('PUT') }}
                        <input type="hidden" name="_token" value="{{Auth::guard('customer')->user()->id}}">
                                <p>اذا كنت تريد تغيير الإسم  الخاص بك سوف يمكنك عمل ذلك أدناة
                                    تاكد من النقر على زر حفظ التغييرات عند الانتهاء.</p>
                                <div class="form-group col-xs-12">
                                    <h4>إسم جديد</h4>
                                    <input type="text" name="name" class="form-control" placeholder="إسم جديد" value="{{Auth::guard('customer')->user()->name }}">
                                </div>
                                <div class="form-group col-xs-12 has-btns">
                                    <button type="submit" class="btn btn-reg" data-toggle="modal" data-target="#done_pop">حفظ التغيرات</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->
        
        <div class="modal fade profile_modals" id="Nemail_pop"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content col-xs-12">
                    <div class="modal-header col-xs-12">
                        <button type="button" class="close" data-dismiss="modal">
                            <i class="la la-close"></i>
                        </button>
                        <h4 class="modal-title">تغيير عنوان البريد الإلكتروني الخاص بك</h4>
                    </div>
                    <div class="modal-body col-xs-12">
                        <div class="form-wrapo col-xs-12">
                            <form action="{{route('get-mail',  Auth::guard('customer')->user()->id)}}" method="put">
                                @csrf
                                 {{ method_field('PUT') }}
                        <input type="hidden" name="_token" value="{{Auth::guard('customer')->user()->id}}">
                                <p>{{Auth::guard('customer')->user()->email }}: عنوان البريد الإلكتروني الحالي</p>
                                <h5>أدخل عنوان البريد الإلكتروني الجديد الذي ترغب في ربطه بحسابك أدناه
                                    . سنرسل كلمة مرور لمرة واحدة  إلى هذا العنوان</h5>
                                <div class="form-group col-xs-12">
                                    <h4>البريد الالكترونى الجديد</h4>
                                    <input type="email" class="form-control" name="email" placeholder="البريد الالكترونى الجديد">
                                </div>
                                <div class="form-group col-xs-12 has-btns">
                                    <button type="submit" class="btn btn-reg" data-toggle="modal" data-target="#done_pop">حفظ التغيرات</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->
        
        <div class="modal fade profile_modals" id="Nphone_pop"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content col-xs-12">
                    <div class="modal-header col-xs-12">
                        <button type="button" class="close" data-dismiss="modal">
                            <i class="la la-close"></i>
                        </button>
                        <h4 class="modal-title">تغيير رقم الهاتف الجوال</h4>
                    </div>
                    <div class="modal-body col-xs-12">
                        <div class="form-wrapo col-xs-12">
                            <form action="{{route('edit-profile',  Auth::guard('customer')->user()->id)}}" method="put">
                                @csrf
                                 {{ method_field('PUT') }}
                        <input type="hidden" name="_token" value="{{Auth::guard('customer')->user()->id}}">
                                <p>رقم الهاتف المحمول القديم
                                    {{Auth::guard('customer')->user()->phone }}</p>
                                <div class="form-group col-xs-12">
                                    <h4>رقم لاهاتف الجديد</h4>
                                    <input type="text" class="form-control" name="phone">
                                </div>
                                <div class="form-group col-xs-12 has-btns">
                                    <button type="submit" class="btn btn-reg" data-toggle="modal" data-target="#done_pop" >حفظ التغيرات</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->
        
        <div class="modal fade profile_modals" id="Npass_pop"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content col-xs-12">
                    <div class="modal-header col-xs-12">
                        <button type="button" class="close" data-dismiss="modal">
                            <i class="la la-close"></i>
                        </button>
                        <h4 class="modal-title">تغيير كلمة السر</h4>
                    </div>
                    <div class="modal-body col-xs-12">
                        <div class="form-wrapo col-xs-12">
                            <form action="{{route('update-password',  Auth::guard('customer')->user()->id)}}" method="post">
                                @csrf
                                <p>استخدم النموذج أدناه لتغيير كلمة سر حسابك </p>
                                <div class="form-group col-xs-12">
                                    <h4>كلمة السر الحاليه</h4>
                                    <input type="password" class="form-control" name="password">
                                </div>
                                <div class="form-group col-xs-12">
                                    <h4>كلمة السر الجديده </h4>
                                    <input type="password" class="form-control" name="new_password">
                                </div>
                                <div class="form-group col-xs-12 has-btns">
                                    <button type="submit" class="btn btn-reg" data-toggle="modal">حفظ التغيرات</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->
        
        <div class="modal fade" id="done_pop" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content col-xs-12">
                    <div class="modal-header col-xs-12">
                        <button type="button" class="close" data-dismiss="modal">
                            <i class="la la-close"></i>
                        </button>
                    </div>
                    <div class="modal-body col-xs-12">
                        <div class="reserv-wrap">
                            <img lazy="loading" src="{{URL('public/nature-sound/')}}/images/success.svg" alt="">
                            <h5>تم التغير بنجاح</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->
        
@endsection

@section('js')
    <script src="{{URL('public/nature-sound')}}/js/jquery.fancybox.min.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/owl.carousel.min.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/jquery.nice-select.min.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/aos.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/intlTelInput.min.js"></script>

    <script>
        $('.edit__').click(function (){
            $('.personal-data').hide();
            $('.edit-personal').show();
        });
        
        $('.op-sidebar').click(function (){
            $('.prof-sidebar').addClass('active');
            $('html').addClass('off');
        });
        
        $('.clo-sidebar').click(function (){
            $('.prof-sidebar').removeClass('active');
            $('html').removeClass('off');
        });
        
        $('.s-img label').click(function (){
            $('.i-btn').fadeIn(1000);
            $(this).fadeOut(1000);
        });
        
        const input = document.querySelector("#phone");
        intlTelInput(input, {
            separateDialCode: true
        });
    </script>
    @endsection