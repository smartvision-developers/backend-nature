  <input type="hidden" data-id="{{$value->id}}"  value="{{$value->id}}" name="product_id" class="product">
                                                        <input type="hidden" value="1" class="quantity">
                                                            @php
                                                        $cart=\App\Models\Cart::where('product_id', $value->id)->where('customer_id',auth()->guard('customer')->id())->first();
                                                    @endphp
                                                    @if(!$cart)
                                                         <button class="add-pro" type="button" onclick="addProductToCart(this, {{$value->id}},{{$value->product_unitprice}})" data-tool="tooltip" title="اضف للسلة" data-placement="right">
                                                            <i class="las la-shopping-bag"></i>
                                                        </button>
                                                    @else
                                                       <button class="add-pro" type="button" onclick="addProductToCart(this, {{$value->id}},{{$value->product_unitprice}})" data-tool="tooltip" title="حذف من السله" data-placement="right">
                                                            <i class="las la-trash" style="color:red;"></i>
                                                        </button>
                                                    @endif
                                            

                                              @php
                                                        $fav=\App\Models\Product_Favourite::where('product_id', $value->id)->where('customer_id',auth()->guard('customer')->id())->first();
                                                    @endphp
                                                    @if(!$fav)
                                                          <button type="button" class="add-fav" onclick="addProductToFav(this, {{$value->id}})" data-tool="tooltip" title="اضف للمفضلة" data-placement="right">
                                                            <i class="las la-heart"></i>
                                                        </button>
                                                    @else
                                                       <button type="button" class="add-fav" onclick="addProductToFav(this, {{$value->id}})" data-tool="tooltip" title="حذف من  لامفضلة" data-placement="right">
                                                            <i class="las la-heart" style="color:red;"></i>
                                                        </button>
                                                    @endif
                                                        <button type="button" data-tool="tooltip" data-target-category="{{ $value->Category_Product->AnimalType->name_ar }}" data-target-product_description="{{ $value->product_description_ar }}"
                                                        data-target-product_code="{{ $value->product_code }}" data-target-product_name="{{ $value->product_name_ar }}"
                                                         data-target-product_img="{{ $value->ProductImage }}"
                                                        data-target-product_price="{{ $value->product_unitprice }}" title="عرض سريع" data-placement="right" data-toggle="modal" data-target="#preview_pop" data-target-id="{{ $value->id }}">
                                                             
                                                            <i class="las la-search"></i>
                                                        </button>

                                                        <!--<button type="button" data-tool="tooltip" title="اضف للمقارنة" data-placement="right">-->
                                                        <!--    <i class="las la-sync"></i>-->
                                                        <!--</button>-->