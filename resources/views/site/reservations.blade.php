@extends('site/layout/main')
@section('content')
 <main class="main-content col-xs-12">
            <div class="bread-crumb col-xs-12" style="background-image: url({{URL('public/nature-sound')}}/images/hero-section/hero.png)">
                <div class="container">
                    <h3>{{__('translate.my-account')}}</h3>
                    <ul>
                        <li>
                            <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">{{__('translate.home')}}</a>
                        </li>
                        <li>{{__('translate.my-account')}}</li>
                    </ul>
                </div>
            </div>
            <div class="profile-wrap col-xs-12">
                <div class="container">
                    <a href="javascript:void(0)" class="op-sidebar">
                        <img lazy="loading" src="{{URL('/').Auth::guard('customer')->user()->photo}}" alt="{{Auth::guard('customer')->user()->name}}">
                    </a>
                    <div class="prof-sidebar col-md-3 col-xs-12">
                        <div class="prof-top">
                            <button type="button" class="clo-sidebar">
                                <i class="la la-close"></i>
                            </button>
                            <div class="s-img">
                        <img lazy="loading" src="{{URL('/')}}/{{Auth::guard('customer')->user()->photo}}" alt="{{Auth::guard('customer')->user()->name}}" id="blah1">

                          <form enctype="multipart/form-data" action="{{route('update-photo',  Auth::guard('customer')->user()->id)}}" method="post">
                                @csrf

                                <label>

                                    <input id="photo" type="file" name="photo" onchange="document.getElementById('blah1').src = window.URL.createObjectURL(this.files[0])">
                                    <i class="la la-camera"></i>
                                </label>
                                <button type="submit" class="i-btn">
                                    <i class="la la-check"></i>
                                </button>
                                <button type="button" class="i-btn i-red">
                                    <i class="la la-close"></i>
                                </button>
                            </form>
                            </div>
                            <h3>{{Auth::guard('customer')->user()->name }} </h3>
                            <p>{{Auth::guard('customer')->user()->email }}</p>
                        </div>
                        <ul>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/profile')}}">{{__('translate.profile')}}</a>
                            </li>
                           <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/my-orders')}}">مشترياتى</a>
                            </li>
                            <li class="active">
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/my-reservations')}}">حجوزاتى</a>
                            </li>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/shippment-address')}}">{{__('translate.shipping-addresses')}}</a>
                            </li>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/logout')}}">{{__('translate.logout')}}</a>
                            </li>
                        </ul>
                    </div>
                       <div class="prof-content col-md-9 col-sm-12 col-xs-12">
                        <div class="prof-head col-xs-12">
                            <h4>الملف الشخصي</h4>
                            <div>
                                <div>
                                    <i class="la la-calendar"></i>
                                    <select class="form-control nice-select" onchange="top.location.href = this.options[this.selectedIndex].value">
                                        <option selected="">الكل</option>
                                        <option value="{{route('site.reservations-3')}}">اخر 3 شهور </option>
                                        <option value="{{route('site.reservations-6')}}">اخر 6 شهور</option>
                                    </select>
                                </div>
                                <div style="display:none;">
                                    <i class="las la-sort-amount-down-alt"></i>
                                    <select class="form-control nice-select">
                                        <option>الكل</option>
                                        <option>قيد التحضير</option>
                                        <option>تم استلامها</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="prof-body col-xs-12">
                            <div class="panel-group" id="accordion">
                                @php $count =0 ;@endphp
                               @foreach($clinic_reservations as $data)
                               @php   $clinic_price=\App\Models\ClinicReservation::where('request_no', $data->request_no)->groupBy('request_no')->sum('service_price');
                               $clinic_service=\App\Models\ClinicReservation::where('request_no', $data->request_no)->get();
                                @endphp
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a @if($count != 0) class="collapsed" @endif data-toggle="collapse" data-parent="#accordion" href="#cl{{$count}}">
                                            <span>نوع الحجز : عيادة</span>
                                            <span>تاريخ الحجز : {{$data->reservation_date}}</span>
                                            <span>رقم الحجز : {{$data->request_no}}</span>
                                            <span>التفاصيل</span>
                                        </a>
                                    </div>
                                    <div id="cl{{$count}}" class="panel-collapse collapse @if($count == 0) in @endif">
                                        <div class="panel-body">
                                            <div class="res-list">
                                                @foreach($clinic_service as $val)
                                                <div>
                                                <div>
                                                    <span>سعر الحجز : {{$val->service_price}} ريال</span>
                                                    <span>اسم الخدمه : {{$val->ClinicService->service_description_ar}} </span>
                                              </div>
                                                <div>
<!--                                                     <span>حاله الدفع : مدفوع</span>
 -->                                                    <span>عدد الحيوانات: {{$val->animal_no}}</span>
                                                </div>
                                                  <div>
                                                    <button type="button" class="r-red" onclick="removeReservation(this, {{$val->id}})">الغاء الحجز</button>
                                                </div>
                                            </div>
                                                @endforeach
                                                <div>
                                                    <span class="r-green">الاجمالى : {{$clinic_price}} ريال</span>
                                                </div>
                                              
                                            </div>
                                            <div class="serv-date">
                                                <!--<span></span>-->
                                                <span>موعد الخدمة: {{$data->reservation_date}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @php $count++; @endphp
                                @endforeach
                                 @php $count2=100; @endphp
                                @foreach($hotel_reservations as $data)
                                 @php   $hotel_price=\App\Models\HotelReservation::where('request_no', $data->request_no)->groupBy('request_no')->sum('service_price');
                               $hotel_service=\App\Models\HotelReservation::where('request_no', $data->request_no)->get();
                                @endphp
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#cl{{$count2}}">
                                            <span>نوع الحجز : فندقه</span>
                                            <span>تاريخ الحجز :  {{$data->reservation_start_date}}  -  {{$data->reservation_end_date}}</span>
                                            <span>رقم الحجز :  {{$data->request_no}}</span>
                                            <span>التفاصيل</span>
                                        </a>
                                    </div>
                                    <div id="cl{{$count2}}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="res-list">
                                                    @foreach($hotel_service as $val2)
                                                <div>
                                                <div>
                                                    <span>سعر الحجز : {{$val2->service_price}} ريال</span>
                                                    <span>نوع الدفع : {{$val2->HotelService->service_description_ar}}</span>
                                                </div>
                                                <div>
<!--                                                     <span>حاله الدفع : مدفوع</span>
 -->                                                    <span>عدد الحيوانات: {{$val2->animal_no}}</span>
                                                </div>
                                             <div>
                                                    <button type="button" class="r-red" onclick="removeReservation(this, {{$val2->id}})">الغاء الحجز</button>
                                                </div>
                                                </div>
                                            @endforeach
                                                <div>
                                                    <span class="r-green">الاجمالى : {{$hotel_price}} ريال</span>
                                                </div>
                                               
                                            </div>
                                            <div class="serv-date">
                                                <span>موعد الخدمة: {{$data->reservation_start_date}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 @php $count++; @endphp
                                @endforeach
                               
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        
@endsection

@section('js')
    <script src="{{URL('public/nature-sound')}}/js/jquery.fancybox.min.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/owl.carousel.min.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/jquery.nice-select.min.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/aos.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/intlTelInput.min.js"></script>
    <script type="text/javascript">
                function removeReservation(e, productID) {

            var x = productID;

            console.log(x);
            $.ajax({

                url: '{{route('site.remove_reserv')}}',
                data:{id : x},
                type: 'GET',
                dataType: 'json',
                success: function (result) {
                    if (result == 1) {
                    // $('.panel').parent().remove();            
                        swal({
                            icon: 'success',
                            title: 'تم إلغاء الحجز بنجاح'
                        });
       setTimeout(function() {
            window.location.href = ('{{url('/my-reservations')}}');
       }, 2000); // 2 second

                    } 
                },
                error: function (result) {
                    swal(
                        'Fail',
                        'يوجد خطأ',
                        'error',
                    );
                }
            });

        }


    </script>
    
    <script>
        $('.edit__').click(function (){
            $('.personal-data').hide();
            $('.edit-personal').show();
        });
        
        $('.op-sidebar').click(function (){
            $('.prof-sidebar').addClass('active');
            $('html').addClass('off');
        });
        
        $('.clo-sidebar').click(function (){
            $('.prof-sidebar').removeClass('active');
            $('html').removeClass('off');
        });
        
        $('.s-img label').click(function (){
            $('.i-btn').fadeIn(1000);
            $(this).fadeOut(1000);
        });
            
        $('.op-order-dets').click(function (){
            $(this).parent().next('.dets-table').stop().slideToggle();
        });
    </script>
    @endsection