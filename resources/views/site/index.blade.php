@extends('site/layout/main')
@php          $setting=\App\Models\Setting::findOrFail(1);
        $clinic=\App\Models\Clinic::select('clinic_title_ar','clinic_title_en')->find(1);            
        $hotel=\App\Models\Hotel::select('hotel_title_ar','hotel_title_en')->find(1);

@endphp
@section('content')
            <main class="main-content col-xs-12">
                   @if(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
                @endif
            <div class="heros col-xs-12">
                <div class="hero-slider owl-carousel">
                    @foreach($sliders as $slider)
                    <div class="item">
                        <img  lazy="loading" src="{{URL('/'.$slider->slider_img)}}" alt="">
                        <div class="h-cap">
                        <?php echo ($slider->slider_text != null) ? $slider->slider_text : '' ?> 
<!--                             <a href="#" class="btn">اكتشف المزيد</a>
 -->                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="hero-widgets col-xs-12" id="animalTypes">
                <div class="container">
                    @foreach($animal_types as $key => $type)
                    <div class="block col-md-2 col-sm-4 col-xs-6" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                        <a href="{{url('/')}}/filter/{{$type->name_en}}">

                            <img  lazy="loading" src="{{URL('/'.$type->image)}}" alt="{{$type->name}}">
                            <h4>{{ucwords($type->name)}}</h4>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="products col-xs-12">
                <div class="container">
                    <div class="g-head col-xs-12"  data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                        <span>{{__('translate.best-seller')}}</span>
                        <h3>{{__('translate.best-exclusive')}}</h3>
                    </div>
                    <div class="g-body col-xs-12"  data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                        <div class="pr-slider owl-carousel">
                           @foreach($exclusive_products as $key => $value)     
                            <div class="item">
                                <div class="product-card">
                                    <div class="p-inner">
                                        <div class="p-img">
                                            <div class="p-actions">
                                                 @include('site.partial._btnActions')
                                            </div>
                                             <img  lazy="loading" src="{{URL('/public/images/products/'.$value->ProductImage[0]->product_image)}}" alt="{{$value->product_name_ar}}">
                                        </div>
                                        <div class="p-data">
                                              <a href="{{URL('/product/'.$value->product_code)}}">
                                                    @if(app()->getLocale() == 'ar') {{$value->product_name_ar}} @elseif(app()->getLocale() == 'en'){{$value->product_name_en}} @endif
                                                </a>
                                                    <p>
                                                        <span>{{$value->product_unitprice}} {{__('translate.riyal')}}</span>
                                                        @if($value->product_oldprice)
                                                            <span class="old-price">{{ $value->product_oldprice }} {{__('translate.riyal')}}</span>
                                                        @endif
                                                   </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                @endforeach
                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="services col-xs-12" style="background-image: url({{URL('public/nature-sound')}}/images/services/bg.png)">
                <div class="container">
                    <div class="g-head col-xs-12"  data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                        <span>{{__('translate.advanced-clinic')}}</span>
                        <h3>{{__('translate.best-services')}} </h3>
                    </div>
                    <div class="g-body col-xs-12">
                        <div class="block col-md-6 col-xs-12"   data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                            <div class="inner">
                                <img  lazy="loading" src="{{URL('public/nature-sound')}}/images/services/1.png" alt="">
                                <div class="i-cap">
                                    <span>{{__('translate.advanced-clinic')}}</span>
                                    <p>هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</p>
                                    <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/clinic/'. $clinic->clinic_title_ar)}}" class="btn">{{__('translate.book-now')}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="block col-md-6 col-xs-12"   data-aos="fade-up" data-aos-duration="1000" data-aos-delay="600">
                            <div class="inner">
                                <img  lazy="loading" src="{{URL('public/nature-sound')}}/images/services/2.png" alt="">
                                <div class="i-cap">
                                    <span>{{__('translate.hotel-service')}}</span>
                                    <p>هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</p>
                                    <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/hotel/'.$hotel->hotel_title_ar)}}" class="btn">{{__('translate.book-now')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="best-sels col-xs-12">
                <div class="container">
                    <div class="g-head col-xs-12"   data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                        <span>{{__('translate.top-rated')}}</span>
                        <h3>{{__('translate.more-seller')}}</h3>
                    </div>
                    <div class="g-body col-xs-12">
                        <ul class="nav-tabs col-xs-12"   data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                            <li class="active">
                                <a href="#" data-toggle="tab" data-target="#t1">{{__('translate.premium-products')}}</a>
                            </li>
                            <li>
                                <a href="#" data-toggle="tab" data-target="#t2">{{__('translate.recently-arrived')}}</a>
                            </li>
                            <li>
                                <a href="#" data-toggle="tab" data-target="#t3">{{__('translate.more-seller')}}</a>
                            </li>
                            
                        </ul>
                        <div class="tab-content col-xs-12"   data-aos="fade-up" data-aos-duration="1000" data-aos-delay="600">
                            <div class="tab-pane fade active in" id="t1">
                                <div class="row" id="r1">
                                @foreach($star_products as $key => $value)
                                <div class="block col-md-3 col-sm-6 col-xs-12">
                                        <div class="product-card">
                                    <div class="p-inner">
                                        <div class="p-img">
                                            <div class="p-actions">
                                             @include('site.partial._btnActions')
                                            </div>
<!--                                             <span>20% خصم</span>
 -->                                      
                                             <img  lazy="loading" src="{{URL('/public/images/products/'.$value->ProductImage[0]->product_image)}}" alt="{{$value->product_name_ar}}">
                                        </div>
                                        <div class="p-data">
                                               <a href="{{URL('/product/'.$value->product_code)}}">
                                                   @if(app()->getLocale() == 'ar') {{$value->product_name_ar}} @elseif(app()->getLocale() == 'en'){{$value->product_name_en}} @endif
                                               </a>
                                                    <p>
                                                        <span>{{$value->product_unitprice}} {{__('translate.riyal')}}</span>
                                                        @if($value->product_oldprice)
                                                            <span class="old-price">{{ $value->product_oldprice }} {{__('translate.riyal')}}</span>
                                                        @endif 
                                                  </p>
                                        </div>
                                    </div>
                                </div>
                                    </div>
                                @endforeach
                              
                                </div>
                      <div class="g-more col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                    <button class="see-more btn" data-page="2" data-link="{{asset('/')}}?page=" data-div="#r1">{{__('translate.more')}}</button> 
                </div>                           

                            </div>
                            <div class="tab-pane fade" id="t2">
                                <div class="row" id="r2">
                                @foreach($new_arrival_products as $key => $value)
                                <div class="block col-md-3 col-sm-6 col-xs-12">
                                        <div class="product-card">
                                    <div class="p-inner">
                                        <div class="p-img">
                                            <div class="p-actions">
                                             @include('site.partial._btnActionsRelation')
                                            </div>
<!--                                             <span>20% خصم</span>
 -->                                      
                                             <img  lazy="loading" src="{{URL('/public/images/products/'.$value->Product->ProductImage[0]->product_image)}}" alt="{{$value->Product->product_name_ar}}">
                                        </div>
                                        <div class="p-data">
                                               <a href="{{URL('/product/'.$value->Product->product_code)}}"> @if(app()->getLocale() == 'ar') {{$value->Product->product_name_ar}} @elseif(app()->getLocale() == 'en'){{$value->Product->product_name_en}} @endif</a>
                                                    <p>
                                                        <span>{{$value->Product->product_unitprice}} {{__('translate.riyal')}}</span>
                                                        @if($value->Product->product_oldprice)
                                                            <span class="old-price">{{ $value->Product->product_oldprice }} {{__('translate.riyal')}}</span>
                                                        @endif
                                                    </p>
                                        </div>
                                    </div>
                                </div>
                                    </div>
                                @endforeach

                                </div>
                                  <div class="g-more col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                    <button class="see-more2 btn" data-page="2" data-link="{{asset('/')}}?page=" data-div="#r2">{{__('translate.more')}}</button> 
                </div>                           

                 </div>
                            <div class="tab-pane fade" id="t3">
                                <div class="row" id="r3">
                                      @foreach($best_seller_products as $key => $value)
                                <div class="block col-md-3 col-sm-6 col-xs-12">
                                        <div class="product-card">
                                    <div class="p-inner">
                                        <div class="p-img">
                                            <div class="p-actions">
                                             @include('site.partial._btnActionsRelation')
                                            </div>
<!--                                             <span>20% خصم</span>
 -->                                      
                                             <img  lazy="loading" src="{{URL('/public/images/products/'.$value->Product->ProductImage[0]->product_image)}}" alt="{{$value->Product->product_name_ar}}">
                                        </div>
                                        <div class="p-data">
                                               <a href="{{URL('/product/'.$value->Product->product_code)}}"> @if(app()->getLocale() == 'ar') {{$value->Product->product_name_ar}} @elseif(app()->getLocale() == 'en'){{$value->Product->product_name_en}} @endif</a>
                                                    <p>
                                                        <span>{{$value->Product->product_unitprice}} {{__('translate.riyal')}}</span>
                                                        @if($value->Product->product_oldprice)
                                                            <span class="old-price">{{ $value->Product->product_oldprice }} {{__('translate.riyal')}}</span>
                                                        @endif
                                                  </p>
                                        </div>
                                    </div>
                                </div>
                                    </div>
                                @endforeach
                                </div>
                            <div class="g-more col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                                <button class="see-more3 btn" data-page="2" data-link="{{asset('/')}}?page=" data-div="#r3">{{__('translate.more')}}</button> 
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          <!--   <div class="ex-prods col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="block col-md-6 col-xs-12"   data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                            <div class="inner">
                                <img  lazy="loading" src="images/ex1.png" alt="">
                                <div class="ex-cap">
                                    <h3>منتجات <br>الزواحف</h3>
                                    <p>%احصل على خصم 30</p>
                                    <a href="#" class="btn">تسوق الان</a>
                                </div>
                            </div>
                        </div>
                        <div class="block col-md-6 col-xs-12"   data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                            <div class="inner">
                                <img  lazy="loading" src="images/ex2.png" alt="">
                                <div class="ex-cap">
                                    <h3>لوازم <br>الطيور</h3>
                                    <p>%احصل على خصم 30</p>
                                    <a href="#" class="btn">تسوق الان</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="acces col-xs-12" style="background-image: url({{URL('/public/nature-sound')}}/images/services/bg.png)">
                <div class="container">
<!--                     <span   data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">%خصم 15</span>
 -->                    <h3   data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">اكسسورات حيوانك الاليف عندنا</h3>
                    <p   data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</p>
                    <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/comming-soon')}}" class="btn"   data-aos="fade-up" data-aos-duration="1000" data-aos-delay="500">{{__('translate.shop-now')}}</a>
                </div>
            </div>
                
            <div class="blogs col-xs-12">
                <div class="g-head col-xs-12"   data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                        <div class="container">
                            <span>{{__('translate.blog')}}</span>
                            <h3>{{__('translate.everything-need')}}
                                <a href="{{URL('blogs')}}">{{__('translate.watch-all')}}</a>
                            </h3>
                        </div>
                    </div>
                <div class="g-body col-xs-12"   data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                        <div class="blog-slider owl-carousel">
                            @foreach($blogs as $key => $blog)
                            <div class="item">
                                <div class="blog-card">
                                    <div class="b-img">
                                    <img  lazy="loading" src="{{URL('/'. $blog->blog_image)}}" alt="{{$blog->blog_title}}">                 
                                    <a href="{{URL('blog/'. $blog->blog_title)}}"></a>
                                    </div>
                                    <div class="b-data">
                                        <a href="{{URL('blog/'. $blog->blog_title)}}" class="title">{{$blog->blog_title}} </a>
                                        <p>{{$blog->blog_text}} </p>
                                        <p>
                                            <a href="{{URL('blog/'. $blog->blog_title)}}">
                                                التفاصيل
                                                <i class="la la-angle-left"></i>
                                            </a>
                                            <b>{{($blog->created_at)->toDateString()}}</b>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                           
                        </div>
                    </div>
            </div>
            <div class="testominals col-xs-12">
                <div class="container" id="customer_reviews">
                    <div class="g-head col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                        <span>   {{__('translate.customer-reviews')}}</span>
                        <h3> {{__('translate.said-about')}}</h3>
                    </div>
                    <div class="g-body col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                        <div class="teso-slider owl-carousel">
                           @foreach($customer_reviews as $review)
                            <div class="item">
                                <p> {{$review->customer_review}}
                                </p>
                                <h4>{{$review->customer_name}}</h4>
                                <span>{{$review->job_title}}</span>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
       

@stop

  @section('js')
    <script src="{{URL('public/nature-sound')}}/js/jquery.nice-select.min.js"></script>
    <script>

        $(".see-more").click(function() {
  $div = $($(this).data('div')); //div to append
  $link = $(this).data('link'); //current URL

  $page = $(this).data('page'); //get the next page #
  $href = $link + $page; //complete URL
  $.get($href, function(response) { //append data
    $html = $(response).find("#r1").html(); 
    console.log($link);
    $div.append($html);
});

  $(this).data('page', (parseInt($page) + 1)); //update page #
});


        $(".see-more2").click(function() {
  $div = $($(this).data('div')); //div to append
  $link = $(this).data('link'); //current URL

  $page = $(this).data('page'); //get the next page #
  $href = $link + $page; //complete URL
  $.get($href, function(response) { //append data
    $html = $(response).find("#r2").html(); 
    console.log($link);
    $div.append($html);
});

  $(this).data('page', (parseInt($page) + 1)); //update page #
});

        $(".see-more3").click(function() {
  $div = $($(this).data('div')); //div to append
  $link = $(this).data('link'); //current URL

  $page = $(this).data('page'); //get the next page #
  $href = $link + $page; //complete URL
  $.get($href, function(response) { //append data
    $html = $(response).find("#r3").html(); 
    console.log($link);
    $div.append($html);
});

  $(this).data('page', (parseInt($page) + 1)); //update page #
});
</script>
@endsection