@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />

@endsection
@php          $setting=\App\Models\Setting::findOrFail(1);
@endphp
            <div class="partners col-xs-12"   data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                <div class="container" id="site_trademarks">
                    <div class="part-slider owl-carousel">
                       @foreach($get_trademarks as $key=> $value)
                        <div class="itm">
                            <img src="{{URL($value->image)}}" alt="{{$value->name}}">
                        </div>
                        @endforeach
                      
                    </div>
                </div>
            </div>
</main>
        <footer class="main-footer col-xs-12">
            <div class="footer-top col-xs-12">
                <div class="newsletter col-md-4 col-xs-12" style="background-image: url(images/footer-bg.png)">
                    <div class="nw-head">
                        <i class="las la-envelope-open-text"></i>
                        <div>
                            <h4>{{__('translate.mailing-list')}}</h4>
                            <p>{{__('translate.subscibe')}}</p>
                        </div>
                    </div>
                    
                    <form action="{{route('site.store_newsletter')}}" method="POST">
                                    @csrf
                        <div class="form-group">
                             <input name="email" value="{{old('email')}}" type="email" class="form-control" placeholder="{{__('translate.email')}}" required="">
                                    @error('email')
                                    <div class="error-area">
                                      <p>{{$message}}</p>
                                    </div>
                                    @enderror
                            <button type="submit">
                                <i class="la la-arrow-left"></i>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="foot-item col-md-8 col-xs-12">
                    <div class="f-item col-md-6 col-xs-12">
                        <h4>{{__('translate.important-link')}}</h4>
                        <ul>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">{{__('translate.home')}}</a>
                            </li>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/about-us')}}">{{__('translate.about-us')}}</a>
                            </li>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/category')}}">{{__('translate.shop')}}</a>
                            </li>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/profile')}}">{{__('translate.my-account')}}</a>
                            </li>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/blogs')}}">{{__('translate.blog')}}</a>
                            </li>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/contact-us')}}">{{__('translate.contact-us')}}</a>
                            </li>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/privacy-policy')}}">{{__('translate.privacy')}}</a>
                            </li>
                        </ul>
                        <div class="social-s">
                            <a target="_blank" href="{{$setting->snapchat_link}}">
                                <i class="la la-snapchat-ghost"></i>
                            </a>
                            <a target="_blank" href="{{$setting->facebook_link}}">
                                <i class="la la-facebook"></i>
                            </a>
                            <a target="_blank" href="{{$setting->twitter_link}}">
                                <i class="la la-twitter"></i>
                            </a>
                            <a target="_blank" href="{{$setting->instagram_link}}">
                                <i class="la la-instagram"></i>
                            </a>
                            <a target="_blank" href="{{$setting->youtube_link}}">
                                <i class="la la-youtube"></i>
                            </a>
                        </div>
                    </div>
                     
                    <div class="f-item col-md-6 col-xs-12">
                        <h4>{{__('translate.contact-us')}}</h4>
                        <ul class="info-s">
                            <li> @if(app()->getLocale() == 'ar') {{$setting->address_ar}} @elseif(app()->getLocale() == 'en'){{$setting->address_en}} @endif
                            </li>
                            <li>{{__('translate.phone')}} : <a href="tel:{{$setting->phone_no}}">{{$setting->phone_no}} </a></li>
                            <li><a href="mailto:{{$setting->email}}">{{$setting->email}}</a> : {{__('translate.email')}}</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-bottom col-xs-12">
                <div class="container">
                    <p>{{__('translate.all-rights')}} <span>{{__('translate.nature-sound')}}</span></p>
                    <img src="{{URL('/public/nature-sound')}}/images/payment-method.png" alt="">
                    <a href="http://smartvision4p.com/">
                        <img src="{{URL('/public/nature-sound')}}/images/dev.svg" alt="SmartVision">
                    </a>
                </div>
            </div>
            <div class="toTop">
                <i class="la la-angle-up"></i>
            </div>
        </footer>
                                           
        <div class="modal fade" id="search_pop">
            <div class="modal-dialog">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="la la-close"></i>
                    </button>
                    <div class="modal-body">
                        <form action="{{url('/search/')}}" method="get">
                            @csrf
                            <div class="form-group">
                                <input type="search" required="" name="content" class="form-control" placeholder="{{__('translate.write-search')}}">
                                <button type="submit">
                                    <i class="la la-search"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->
        
        <div class="modal fade" id="login_pop">
            <div class="modal-dialog">
                <div class="modal-content col-xs-12">
                    <div class="modal-header col-xs-12">
                        <button type="button" class="close" data-dismiss="modal">
                            <i class="la la-close"></i>
                        </button>
                        <h4 class="modal-title">{{__('translate.login')}}</h4>
                    </div>
                    <div class="modal-body col-xs-12">
                        <form action="#" mehod="get">
                        <div class="form-wrapo col-xs-12">
                         <div class="alert alert-danger print-error-login form-group" style="display: none;">
                                <ul></ul>
                            </div>
                                <div class="form-group col-xs-12 @error('phone') is-invalid @enderror">
                                  <h4>{{__('translate.welcome-back')}}</h4>
                                    <input type="text" id="phone" name="phone" required="" class="form-control" placeholder="{{__('translate.phone')}}">
                                         @error('phone')
                                  <div class="error-area">
                                    <p>{{$message}}</p>
                                  </div>
                                  @enderror
                                </div>
                                <div class="form-group col-xs-12 @error('password') is-invalid @enderror" >
                                    <input id="password" type="password" required="" name="password" class="form-control" placeholder="{{__('translate.password')}}">
                                   @error('password')
                                  <div class="error-area">
                                    <p>{{$message}}</p>
                                  </div>
                                  @enderror
                                </div>
                                <div class="form-group col-xs-12">
                                    <div>
                                        <label>
                                            <input type="checkbox" name="remember_me">
                                            <span>{{__('translate.remember-me')}} </span>
                                        </label>
                                         <a href="#" data-toggle="modal" data-target="#forget_pop" data-dismiss="modal">هل فقدت كلمة المرور؟</a>
                                       </div>
                                </div>
                                <div class="form-group col-xs-12 has-btns">
                                    <button type="submit" class="btn login_customer">{{__('translate.login')}} </button>
                                    <button type="reset" class="btn btn-border reset">{{__('translate.cancel')}} </button>
                                </div>
                                <div class="form-group col-xs-12">
                                    <p>
                                       {{__('translate.donthave-account')}} 
                                        <a href="#" data-toggle="modal" data-target="#register_pop" data-dismiss="modal"> {{__('translate.singup')}}</a>
                                    </p>
                                </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->
        
        <div class="modal fade" id="register_pop">
            <div class="modal-dialog">
                <div class="modal-content col-xs-12">
                    <div class="modal-header col-xs-12">
                        <button type="button" class="close" data-dismiss="modal">
                            <i class="la la-close"></i>
                        </button>
                        <h4 class="modal-title">{{__('translate.signup')}}</h4>
                    </div>
                    <div class="modal-body col-xs-12">
                        <div class="form-wrapo col-xs-12">
                             <div class="alert alert-danger print-error form-group" style="display: none;    margin: 10px 30px;">
                                <ul></ul>
                            </div>
                             <div class="form-group col-xs-12 @error('name') is-invalid @enderror">
                                <h4> {{__('translate.welcome-back')}}</h4>
                                    <input type="text" required="" value="{{old('name')}}" name="name" id="name" class="form-control" placeholder="{{__('translate.name')}}">
                                 @error('name')
                                  <div class="error-area">
                                    <p>{{$message}}</p>
                                  </div>
                                  @enderror
                                </div>
                                <div class="form-group col-xs-12 @error('email') is-invalid @enderror">
                                    <input type="email" id="email_regis" required="" value="{{old('email')}}" name="email" class="form-control" placeholder="{{__('translate.email')}}">
                                 @error('email')
                                  <div class="error-area">
                                    <p>{{$message}}</p>
                                  </div>
                                  @enderror
                                </div>
                                <div class="form-group col-xs-12 @error('phone') is-invalid @enderror">
                                    <input type="text" id="phone_regis" required="" value="{{old('phone')}}" name="phone" class="form-control" placeholder="{{__('translate.phone')}}">
                                 @error('phone')
                                  <div class="error-area">
                                    <p>{{$message}}</p>
                                  </div>
                                  @enderror
                                </div>

                                <div class="form-group col-md-6 col-xs-12 @error('password') is-invalid @enderror">
                                    <input type="password" name="password" class="form-control" placeholder="{{__('translate.password')}}" id="password-field" required="">
                                    <button type="button" class="show-pass" toggle="#password-field">
                                        <i class="la la-eye-slash"></i>
                                    </button> 
                                 @error('password')
                                  <div class="error-area">
                                    <p>{{$message}}</p>
                                  </div>
                                  @enderror
                                </div>
                                <div class="form-group col-md-6 col-xs-12 @error('repassword') is-invalid @enderror">
                                    <input type="password" required="" name="repassword" class="form-control" placeholder="{{__('translate.repassword')}}" id="password-field1">
                                    <button type="button" class="show-pass" toggle="#password-field1">
                                        <i class="la la-eye-slash"></i>
                                    </button>
                                     @error('repassword')
                                  <div class="error-area">
                                    <p>{{$message}}</p>
                                  </div>
                                  @enderror
                                </div>
                                <div class="form-group col-xs-12 has-btns">
                                    <button type="submit" class="btn btn-reg store_customer">{{__('translate.signup')}}</button>
                                </div>
                                <div class="form-group col-xs-12">
                                    <p>
                                        {{__('translate.have-account')}} 
                                        <a href="#" data-toggle="modal" data-target="#login_pop" data-dismiss="modal">{{__('translate.login')}}</a>
                                    </p>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->
        
        <div class="modal fade" id="forget_pop">
            <div class="modal-dialog">
                <div class="modal-content col-xs-12">
                    <div class="modal-header col-xs-12">
                        <button type="button" class="close" data-dismiss="modal">
                            <i class="la la-close"></i>
                        </button>
                        <h4 class="modal-title">نسيت كلمة المرور</h4>
                    </div>
                    <div class="modal-body col-xs-12">
                        <div class="form-wrapo col-xs-12">
                            <form action="{{route('get-pass')}}" method="get">
                                @csrf
                                <h5>اكتب بريدك الالكتروني المسجل لدينا وستصلك رسالة
                                    بها كود مكون من أربعة ارقام</h5>
                                <div class="form-group col-xs-12">
                                    <input type="email" name="email" class="form-control" placeholder="{{__('translate.email')}}">
                                </div>
                                <div class="form-group col-xs-12 has-btns">
                                    <button type="submit" class="btn btn-reg">{{__('translate.send')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->
        
        <div class="modal fade" id="verify_pop">
            <div class="modal-dialog">
                <div class="modal-content col-xs-12">
                    <div class="modal-header col-xs-12">
                        <button type="button" class="close" data-dismiss="modal">
                            <i class="la la-close"></i>
                        </button>
                        <h4 class="modal-title">كود التحقق</h4>
                    </div>
                    <div class="modal-body col-xs-12">
                        <div class="form-wrapo col-xs-12">
                            <form action="#" method="get">
                                <h5>ادخل الكود المكون من أربعة ارقام الذى تم ارسالة
                                    الى هاتفك</h5>
                                <div class="form-group col-xs-12">
                                    <input type="text" class="form-control" placeholder="كود التحقق">
                                </div>
                                <div class="form-group col-xs-12 has-btns">
                                    <button type="submit" class="btn btn-reg">ارسال</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->
        
        <div class="modal fade" id="editPass_pop">
            <div class="modal-dialog">
                <div class="modal-content col-xs-12">
                    <div class="modal-header col-xs-12">
                        <button type="button" class="close" data-dismiss="modal">
                            <i class="la la-close"></i>
                        </button>
                        <h4 class="modal-title">تعديل كلمة السر</h4>
                    </div>
                    <div class="modal-body col-xs-12">
                        <div class="form-wrapo col-xs-12">
                            <form action="#" method="get">
                                <div class="form-group col-xs-12">
                                    <input type="password" class="form-control" placeholder="تعديل كلمة السر">
                                </div>
                                <div class="form-group col-xs-12">
                                    <input type="password" class="form-control" placeholder="الرقم السرى الجديد مرة اخرى">
                                </div>
                                <div class="form-group col-xs-12 has-btns">
                                    <button type="submit" class="btn btn-reg">حفظ</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->
        
        
                                    
        <div class="modal fade" id="preview_pop">
            <div class="modal-dialog modal-lg">
                <div class="modal-content col-xs-12">
                    <div class="modal-header col-xs-12">
                        <button type="button" class="close" data-dismiss="modal">
                            <i class="la la-close"></i>
                        </button>
                    </div>
                    <div class="modal-body col-xs-12">
                        <div class="product-wrap col-xs-12">
                            <input type="hidden" value="">
                                <div class="single-img col-md-6 col-xs-12">
                                    <div class="prev-slider owl-carousel product_img">
                                        <!--<div class="item">-->
                                        <!--    <img src="public/nature-sound/images/products/1.png" alt="">-->
                                        <!--</div>-->
                                        <!--<div class="item">-->
                                        <!--    <img src="public/nature-sound/images/products/2.png" alt="">-->
                                        <!--</div>-->
                                        <!--<div class="item">-->
                                        <!--    <img src="public/nature-sound/images/products/3.png" alt="">-->
                                        <!--</div>-->
                                        <!--<div class="item">-->
                                        <!--    <img src="public/nature-sound/images/products/4.png" alt="">-->
                                        <!--</div>-->
                                    </div>
                                </div>
                                <div class="single-data col-md-6 col-xs-12">
                                    <div class="sec-item">
                                        <span id="category">قسم </span>
                                        <b id="product_code"></b>
                                    </div>
                                    <div class="sec-info">
                                        <div class="info-top">
                                            <h1>
                                                <a id="product_name" href="#"></a>
                                            </h1>
                                            <div class="Stars" style="--rating: 2.3;"></div>
                                        </div>
                                        <p class="desc" id="product_description"> .
                                        </p>
                                    </div>
                                    <div class="sec-features">
                                        <ul>
                                            <li>
                                                <strong>:السعر</strong>
                                                <div>
                                                    <span id="product_price"> </span>
                                                    <!--<span class="old-price">250 ريال</span>-->
                                                </div>
                                            </li>
                                            <li>
                                                <strong>:الكمية</strong>
                                                <div>
                                                    <div class="number">
                                                        <a class="inc qtybutton waves-effect">+</a>
                                                        <input type="text" value="1" name="qtybutton" class="plus-minus-box" disabled="">
                                                        <a class="dec qtybutton waves-effect">-</a>
                                                    </div>
                                                    <button type="button" class="btn-fav">
                                                        <img src="public/nature-sound/images/heart.svg" alt="">
                                                    </button>
                                                    <b>   
                                                        <img src="public/nature-sound/images/check-circle.svg" alt="">
                                                        متاح في المخزن
                                                    </b>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="sec-actions"> 
                      
                                    
                                    
                                    
                                        <button type="button" class="btn btn-green" onclick="">
                                            <img src="public/nature-sound/images/shopping-cart.svg" alt="">
                                            اضف للسلة
                                        </button>
                                        <a href="#" class="btn btn-gray" id="url_details">مشاهدة التفاصيل</a>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal -->
    </div>


    <!-- Javascript Files -->
    <script src="{{URL('public/nature-sound')}}/js/jquery-2.2.2.min.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/jquery-ui.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/bootstrap.min.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/jquery.fancybox.min.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/owl.carousel.min.js"></script>
@yield('js')

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@include('sweetalert::alert')
 <script>
            $(document).ready(function () {
                $("#preview_pop").on("show.bs.modal", function (e) {
                    var id = $(e.relatedTarget).data('target-id');
                     var product_code = $(e.relatedTarget).data('target-product_code');
                      var product_name = $(e.relatedTarget).data('target-product_name');
                       var product_price = $(e.relatedTarget).data('target-product_price');
                    var product_description = $(e.relatedTarget).data('target-product_description');
                      var category = $(e.relatedTarget).data('target-category');
                    console.log(category);
                    $('#product_code').text('كود الصنف : '+ product_code);
                     $('#product_name').text( product_name);
                      $('#product_price').text(product_price + 'ريال');
                        $('#product_description').text(product_description);
                     $('#category').text('قسم '+ category);
                     
                     $('#url_details').attr('href', '/product/' + product_code); 
                    //  ///
                      $('.btn-green').attr('onclick','addProductToCart(this,'+ id +','+ product_price + ')');
                      $('.btn-green').addClass('cart_'+ id);
                      $('.btn-fav').attr('onclick','addProductToFav(this,'+ id +')');
                      $('.btn-fav').addClass('fav_'+ id);
                      
                      var product_img = $(e.relatedTarget).data('target-product_img');
                      $.each(product_img, function(index, value) {
                          console.log(value['product_image']);
                              $('.product_img').trigger('add.owl.carousel', ['<div class="item"><img src="{{URL('/')}}/public/images/products/'+ value['product_image'] +'"></div>'])
        .trigger('refresh.owl.carousel');
                          });

                     
                });
            });

</script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">

$('.store_customer').click(function(event){
            event.preventDefault();
            var name = $('#name').val();
            var phone = $('#phone_regis').val();
            var email = $('#email_regis').val();
            var password = $('#password-field').val();
            var repassword = $('#password-field1').val();
            $.ajax({

                type: "post",
                url: '{{route('site.store')}}',
                data: {    
              _token: '{{csrf_token()}}',  name:name,phone:phone,email:email, password:password, repassword:repassword },         
                dataType: "json",
                success: function(result){
                                    if ((result.errors)) {
                    printErrMsg(result.errors);
            }
                    if (result == 1) {
                        swal({
                            icon: 'success',
                            title: 'تم التسجيل بنجاح'
                        });
                     
                     setTimeout(function() {
                         $('#register_pop').modal('hide');
                          $('#login_pop').modal('show');
                                //   window.location.href = ('{{url('/')}}');
                                    
       }, 2000); // 2 second
  
        }
                },
                error: function (result) {
                    swal(
                        'Fail',
                        'يوجد خطأ',
                        'error',
                    );
                }
            });
        });
    

    $('.login_customer').click(function(event){
            event.preventDefault();
            var phone = $('#phone').val();
            var password = $('#password').val();
            $.ajax({

                type: "post",
                url: '{{route('site.login')}}',
                data: {    
              _token: '{{csrf_token()}}',phone:phone, password:password },         
                dataType: "json",
                success: function(result){
                 if ((result.errors)) {
                    printErrMsgLogin(result.errors);
                    }
                    if (result == 1) {
                        $('.error').addClass('hidden');
                        swal({
                            icon: 'success',
                            title: 'تم الدخول بنجاح'
                        });
       setTimeout(function() {
                                  window.location.href = ('{{url('/')}}');
       }, 2000); // 2 second

                    } 
                },
                error: function (result) {
                    swal(
                        'Fail',
                        'يوجد خطأ',
                        'error',
                    );
                }
            });
        });

    function printErrMsg(msg)
    {
        $(".print-error").find("ul").html('');
        $(".print-error").css('display','block');
        $.each(msg, function(key, value)
        {
           $(".print-error").find("ul").append('<li>' +value+ '</li>')
       })
    }
     function printErrMsgLogin(msg)
    {
        $(".print-error-login").find("ul").html('');
        $(".print-error-login").css('display','block');
        $.each(msg, function(key, value)
        {
           $(".print-error-login").find("ul").append('<li>' +value+ '</li>')
       })
    }
        function removeProductFromFav(e, productID) {

            var x = productID;

            console.log(x);
            $.ajax({

                url: '{{route('site.remove_fav')}}',
                data:{product_id : x},
                type: 'GET',
                dataType: 'json',
                success: function (result) {
                    if (result == 1) {
                    $('#codeRefer').parent().remove();            
                        swal({
                            icon: 'success',
                            title: 'تم حذف المنتج من المفضله'
                        });
                   var x= '<?php echo $get_fav_count ?>';
                   $('.heart').text(x);
       setTimeout(function() {
                                  window.location.href = ('{{url('/favourite')}}');
       }, 2000); // 2 second

                    } 
                },
                error: function (result) {
                    swal(
                        'Fail',
                        'يوجد خطأ',
                        'error',
                    );
                }
            });

        }


        function addProductToCart(e, productID, productPrice) {
              
                var x = $('.quantity').val();
                var x1 = $('.product').val();
                    var x1 = productID;

                if (x> 0)
                {
                    var x2= x * productPrice;
                }
                else
                {
                    var x2= 1 * productPrice;
                }
                console.log(x2);
            $.ajax({

                url: '{{route('site.add_cart')}}',
                type: 'GET',
                dataType: 'json',
                data:{ quantity : x , product_id : x1, price_after_quantity: x2} ,
                success: function (result) { 
                    if (result == 1) {
                     swal({
                            icon: 'success',
                            title: 'تم إضافه المنتج الي السله'
                        });
                    $('.cart_'+x1).html( '<i class="las la-trash" style="color:red;"></i>');
                  //  $('.la-trash').css('color', 'red');
                    $('.cart_'+x1).attr('data-original-title',"حذف من السله");

                    var y= '<?php echo $get_cart_count +1  ?>';
                   $('.shop_cart').text(y);

                    } 
                    else
                    {
                     swal({
                            icon: 'success',
                            title: 'تم الحذف من السله'
                        });
                       $('.cart_'+x1).html( '<i class="las la-shopping-bag"></i> ');
                       $('.cart_'+x1).attr('data-original-title',"إضافه للسله");

                   var y= '<?php echo $get_cart_count   ?>';
                   $('.shop_cart').text(y);

                    }
                },
                error: function (result) {
                    swal(
                        'Fail',
                        'لابد من تسجيل الدخول أولا',
                        'error',
                    );
                }
            });

        }

 function addProductToFav(e, productID) {
           
            var x1 = productID;
            console.log(x1);
            $.ajax({

                url: '{{route('site.add_fav')}}',
                type: 'GET',
                dataType: 'json',
                data:{ product_id : x1} ,
                success: function (da) {
                    if (da == 1) {
                        swal({
                            icon: 'success',
                            title: 'تم اضافة المنتج الي المفضلة'
                        });
                $('.fav_'+ x1).html( '<i class="las la-heart" style="color:red;"></i>');
                    $('.fav_'+ x1).attr('data-original-title',"حذف من المفضله");
                 var x= '<?php echo $get_fav_count +1  ?>';
                   $('.heart').text(x);

                    } else {
                         swal({
                            icon: 'success',
                            title: 'تم حذف المنتج من المفضله'
                        });

                    $('.fav_'+ x1).html( '<i class="las la-heart" ></i>');
                    $('.fav_'+ x1).attr('data-original-title',"إضافه للمفضله");

                   var x= '<?php echo $get_fav_count   ?>';
                   $('.heart').text(x);

                    }
                },
                error: function (da) {
                    swal(
                        'Fail',
                        'لابد من التسجيل أولا',
                        'error',
                    );
                }
            });

        }



</script>
    <script src="{{URL('public/nature-sound')}}/js/aos.js"></script>

    <script src="{{URL('public/nature-sound')}}/js/script.js"></script>

</body>

</html>    