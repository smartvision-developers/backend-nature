<!DOCTYPE html>
<html lang="ar" dir="rtl">

<head>
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta charset="utf-8">
    <meta name="author" content="Amir Nageh">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Css Files -->
    @if(get_default_lang() == 'ar')
  <link href="{{url('')}}/public/sadafa/css/style.css" rel="stylesheet">

  @elseif(get_default_lang() == 'en')
    <link href="{{url('')}}/public/sadafa/css/style.css" rel="stylesheet">

  <link href="{{url('')}}/public/sadafa/css/style-en.css" rel="stylesheet">
  @endif
    <link href="{{url('')}}/public/sadafa/css/bootstrap-rtl.min.css" rel="stylesheet">
    <link href="{{url('')}}/public/sadafa/css/style-res.css" rel="stylesheet">


    <!-- lavicons -->
    <link rel="shortcut icon" href="{{url('')}}/public/sadafa/images/faveicon.png">

</head>

<body>

    <div id="loading">
        <div class="loading">

        </div>
    </div>