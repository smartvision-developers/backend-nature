<!DOCTYPE html>
<html lang="ar" dir="rtl">
@php          $setting=\App\Models\Setting::findOrFail(1);
        $clinic=\App\Models\Clinic::select('clinic_title_ar','clinic_title_en')->find(1);            
        $hotel=\App\Models\Hotel::select('hotel_title_ar','hotel_title_en')->find(1);

@endphp
<head>
    <title>صوت الطبيعة | Nature Sound</title>
    <!-- og: data link website -->
    <meta property="og:title" content="صوت الطبيعه" />
    <meta property="og:type" content="e-commerce" />
    <meta property="og:url" content="{URL('/')}}" />
    <meta property="og:image" content="{{URL('/public/nature-sound/images/logo.png')}}" />
    <meta property="og:description" content="متجر صوت الطبيعه والحيوانات الأليفه. حيوانك الأليف صديقنا.. كل ما يحتاجه وأكثر " />
    <meta property="og:determiner" content="the" />
    <meta property="og:locale" content="ar_AR" />
    <meta property="og:locale:alternate" content="en_GB" />
    <meta property="og:site_name" content="صوت الطبيعه" />
    <!-- end -->
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta charset="utf-8">
    <meta name="author" content="Amir Nageh">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    @yield('og')

    <!-- Css Files -->
    <link href="{{URL('public/nature-sound')}}/css/style.css" rel="stylesheet">
    @if(get_default_lang() == 'en')
    <link href="{{URL('public/nature-sound')}}/css/style-en.css" rel="stylesheet">
    @else
    <link href="{{URL('public/nature-sound')}}/css/bootstrap-rtl.min.css" rel="stylesheet">

    @endif

    <link href="{{URL('public/nature-sound')}}/css/style-res.css" rel="stylesheet">
    @yield('style')
    
    <!-- lavicons -->
    <link rel="shortcut icon" href="{{URL('public/nature-sound')}}/images/faveicon.png">

</head>

<body>

    <div id="loading">
        <div class="loading">

        </div>
    </div>
    
    <div class="wrapper col-xs-12">
        <header class="main-head col-xs-12">
            <div class="top-head col-xs-12">
                <div class="container">
                    <div class="tr-extra">
                        <ul>  
                            @if(Auth::guard('customer')->check())          
                            <li class="menu-item-has-children">
                                <a href="">
                                    <i class="la la-user"></i>
                                    {{auth()->guard('customer')->user()->name}}
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/profile')}}">{{__('translate.my-account')}}</a>
                                    </li>
                                    <li>
                                        <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/logout')}}">{{__('translate.logout')}}</a>
                                    </li>
                                </ul>
                            </li>
                            @else         
                            <li class="menu-item-has-children">
                                <a href="#">
                                    <i class="la la-user"></i>
                                    {{__('translate.my-account')}}
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="" data-toggle="modal" data-target="#login_pop">{{__('translate.login')}}</a>
                                    </li>
                                    <li>
                                        <a href="" data-toggle="modal" data-target="#register_pop">{{__('translate.signup')}}</a>
                                    </li>
                                </ul>
                            </li>
                            @endif
                            <li class="menu-item-has-children">
                                <a href="javascript;">
                                    <i class="la la-globe"></i>
                                    {{__('translate.lang')}}
                                </a>
                                <ul class="sub-menu">
                                 @foreach(LaravelLocalization::getSupportedLocales() as $key => $value)
                                 <li>
                                    <a href="{{LaravelLocalization::getLocalizedUrl($key)}}">{{$value['native']}}</a>
                                </li>
                                @endforeach
                                <li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="tl-extra">
                        <div class="t-social">
                             <a target="_blank" href="{{$setting->snapchat_link}}">
                                <i class="la la-snapchat-ghost"></i>
                            </a>
                            <a target="_blank" href="{{$setting->facebook_link}}">
                                <i class="la la-facebook"></i>
                            </a>
                            <a target="_blank" href="{{$setting->twitter_link}}">
                                <i class="la la-twitter"></i>
                            </a>
                            <a target="_blank" href="{{$setting->instagram_link}}">
                                <i class="la la-instagram"></i>
                            </a>
                            <a target="_blank" href="{{$setting->youtube_link}}">
                                <i class="la la-youtube"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-head col-xs-12">
                <div class="container">
                    <div class="logo">
                        <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">
                            <img src="{{URL('/public/nature-sound')}}/images/logo.png" alt="لوجو صوت الطبيعه">
                        </a>
                    </div>
                    <div class="main-menu">
                        <ul>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">{{__('translate.home')}}</a>
                            </li>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/about-us')}}">{{__('translate.about-us')}}</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="javascript:void(0)">{{__('translate.shop')}}</a>
                                <ul class="sub-menu">
                                  
                                    @php         $animal_types=\App\Models\AnimalType::getanimalstype();
                                    @endphp
                                    @foreach($animal_types as $key=> $value)
                                    <li class="menu-item-has-children">
                                        <a href="javascript:void(0)">{{$value->name}}</a>
                                        <ul class="sub-menu">
                                          @foreach($value->Animal_Category as $cat)
                                          <li><a href="{{url('/')}}/filter/{{$value->name_en}}/{{$cat->Category->category_name_en}}">{{ucwords($cat->Category->category_name_en)}}</a></li>
                                          @endforeach
                                          
                                          
                                      </ul>
                                  </li>
                                  @endforeach
                                  
                              </ul>
                          </li>
                          <li class="menu-item-has-children">
                            <a href="javascript:void(0)">{{__('translate.our-services')}}</a>
                            <ul class="sub-menu">
                                @if(app()->getLocale() == 'ar') 
                                <li>
                                    <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/clinic/'. $clinic->clinic_title_ar)}}">{{__('translate.clinic-service')}}</a>
                                </li>
                                <li>
                                    <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/hotel/'.$hotel->hotel_title_ar)}}">{{__('translate.hotel-service')}}</a>
                                </li>                                
                                @elseif(app()->getLocale() == 'en')
                                <li>
                                    <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/clinic/'. $clinic->clinic_title_en)}}">{{__('translate.clinic-service')}}</a>
                                </li>
                                <li>
                                    <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/hotel/'.$hotel->hotel_title_en)}}">{{__('translate.hotel-service')}}</a>
                                </li>
                                @endif
                            </ul>
                        </li>
                        <li>
                            <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/blogs')}}">{{__('translate.blog')}}</a>
                        </li>
                        <li>
                            <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/contact-us')}}">{{__('translate.contact-us')}}</a>
                        </li>
                    </ul>
                </div>
                <div class="bl-extra">
                    <a href="#" data-toggle="modal" data-target="#search_pop">
                        <i class="la la-search"></i>
                    </a>
                    <ul class="notification-menu" style="display: none;">
                        <li class="menu-item-has-children">
                            <a href="javascript:void(0)">
                                <i class="la la-bell"></i>
                                <span class="badgo">3</span>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <div class="menu-inner">
                                        <div class="menu-top">
                                            <h4>الاشعارات</h4>
                                            <a href="#">
                                                <i class="la la-bell"></i>
                                            </a>
                                        </div>
                                        <div class="menu-content">
                                            <ul>
                                                <li>
                                                    <a href="#">
                                                        <img src="images/ex1.png" alt="">
                                                        <div class="a_user">
                                                            <h3>منى فاروق</h3>
                                                            <span>علقت على اعلانك</span>
                                                            <p>المنتج كويس بس ياريت تفاصيل اكتر</p>
                                                            <b>منذ 5 دقائق</b>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img src="images/ex2.png" alt="">
                                                        <div class="a_user">
                                                            <h3>منى فاروق</h3>
                                                            <span>علقت على اعلانك</span>
                                                            <p>المنتج كويس بس ياريت تفاصيل اكتر</p>
                                                            <b>منذ 5 دقائق</b>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img src="images/ex1.png" alt="">
                                                        <div class="a_user">
                                                            <h3>منى فاروق</h3>
                                                            <span>علقت على اعلانك</span>
                                                            <p>المنتج كويس بس ياريت تفاصيل اكتر</p>
                                                            <b>منذ 5 دقائق</b>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img src="images/ex2.png" alt="">
                                                        <div class="a_user">
                                                            <h3>منى فاروق</h3>
                                                            <span>علقت على اعلانك</span>
                                                            <p>المنتج كويس بس ياريت تفاصيل اكتر</p>
                                                            <b>منذ 5 دقائق</b>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img src="images/ex1.png" alt="">
                                                        <div class="a_user">
                                                            <h3>منى فاروق</h3>
                                                            <span>علقت على اعلانك</span>
                                                            <p>المنتج كويس بس ياريت تفاصيل اكتر</p>
                                                            <b>منذ 5 دقائق</b>
                                                        </div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    @if(Auth::guard('customer')->check())          
                    <a href="{{URL('/favourite')}}">
                        <b class="badgo heart">{{$get_fav_count}}</b>
                        <i class="la la-heart"></i>
                    </a>
                    <a href="{{URL('/cart')}}">
                        <b class="badgo shop_cart">{{$get_cart_count}}</b>
                        <i class="la la-shopping-cart"></i>
                    </a>
                    @else
                    <a href="{{URL('/favourite')}}">
                        <b class="badgo heart">0</b>
                        <i class="la la-heart"></i>
                    </a>
                    <a href="{{URL('/cart')}}">
                        <b class="badgo shop_cart">0</b>
                        <i class="la la-shopping-cart"></i>
                    </a>
                    @endif
                </div>
            </div>
        </div>
    </header>
    <header class="mob-head col-xs-12">

        <div class="m-top col-xs-12">
           @if(Session::has('success'))
           <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
           @endif

           @if(Session::has('error'))
           <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
           @endif
           <div class="container">
            <div class="logo">
                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">
                    <img src="{{URL('/public/nature-sound')}}/images/logo.png" alt="لوجو صوت الطبيعه">
                </a>
            </div>
            <form action="#" method="get">
                <input type="search" class="form-control" placeholder=" {{__('translate.write-search')}}">
                <button type="submit">
                    <i class="la la-search"></i>
                </button>
            </form>
        </div>
    </div>
    <div class="m-mid col-xs-12">
        <a href="javascript:void(0)" class="op-menu">
            <i class="la la-bars"></i>
            {{__('translate.discover')}}
        </a>
     
        <a href="javascript:void(0)" style="display: none;">
            <i class="la la-bell"></i>
            الاشعارات
            <span class="badgo">3</span>
        </a>
            @if(Auth::guard('customer')->check())          
                    <a href="{{URL('/favourite')}}">
<!--                         <b class="badgo heart">{{$get_fav_count}}</b>
 -->                        <i class="la la-heart"></i>
                        {{__('translate.my-fav')}}
                    </a>
                    <a href="{{URL('/cart')}}" class="op-cart">
<!--                         <b class="badgo shop_cart">{{$get_cart_count}}</b>
 -->                        <i class="la la-shopping-cart"></i>
                        {{__('translate.my-cart')}}
                    </a>
                    @else
                    <a href="{{URL('/favourite')}}">
<!--                         <b class="badgo heart">0</b>
 -->                        <i class="la la-heart"></i>
                        {{__('translate.my-fav')}}
                    </a>
                    <a href="{{URL('/cart')}}" class="op-cart">
<!--                         <b class="badgo shop_cart">0</b>
 -->                        <i class="la la-shopping-cart"></i>
                        {{__('translate.my-cart')}}
                    </a>
                    @endif
                </div>
    </div>
    <div class="main-sticky">
        <button type="button" class="off-menu">
            <i class="la la-close"></i>
        </button>
        <ul class="nav-tabs">
            <li class="active">
                <a href="#" data-toggle="tab" data-target="#t111">
                    <i class="la la-bars"></i>
                    {{__('translate.menu')}}
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tab" data-target="#t222">
                    <i class="la la-user"></i>
                    {{__('translate.my-account')}}
                </a>
            </li>
            <li>
                <a href="#" data-toggle="tab" data-target="#t333">
                    <i class="la la-cog"></i>
                 {{__('translate.setting')}}
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="t111">
                <ul>
                    <li>
                        <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">                    {{__('translate.home')}}</a>
                    </li>
                    <li>
                        <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/about-us')}}"> {{__('translate.about-us')}}</a>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="javascript:void(0)">{{__('translate.shop')}}</a>
                        <ul class="sub-menu">          
                                    @foreach($animal_types as $key=> $value)
                                    <li class="menu-item-has-children">
                                        <a href="javascript:void(0)">{{$value->name}}</a>
                                        <ul class="sub-menu">
                                          @foreach($value->Animal_Category as $cat)
                                          <li><a href="{{url('/')}}/filter/{{$cat->Category->category_name_en}}">{{ucwords($cat->Category->category_name_en)}}</a></li>
                                          @endforeach
                                          
                                          
                                      </ul>
                                  </li>
                                  @endforeach
                       </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="javascript:void(0)">{{__('translate.our-services')}}</a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/clinic/'. $clinic->clinic_title_ar)}}">{{__('translate.clinic-service')}}</a>
                            </li>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/clinic/'. $hotel->hotel_title_ar)}}">{{__('translate.hotel-service')}}</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/blogs')}}">{{__('translate.blog')}}</a>
                    </li>
                    <li>
                        <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/contact-us')}}">{{__('translate.contact-us')}}</a>
                    </li>
                </ul>
            </div>
            <div class="tab-pane fade" id="t222">
                <ul>
                     @if(Auth::guard('customer')->check())          
                            <li class="menu-item-has-children">
                                <a href="">
                                    <i class="la la-user"></i>
                                    {{auth()->guard('customer')->user()->name}}
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/profile')}}">{{__('translate.my-account')}}</a>
                                    </li>
                                    <li>
                                        <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/logout')}}">{{__('translate.logout')}}</a>
                                    </li>
                                </ul>
                            </li>
                            @else         
                            <li class="menu-item-has-children">
                                <a href="#">
                                    <i class="la la-user"></i>
                                    {{__('translate.my-account')}}
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="" data-toggle="modal" data-target="#login_pop">{{__('translate.login')}}</a>
                                    </li>
                                    <li>
                                        <a href="" data-toggle="modal" data-target="#register_pop">{{__('translate.signup')}}</a>
                                    </li>
                                </ul>
                            </li>
                            @endif
                </ul>
            </div>
            <div class="tab-pane fade" id="t333">
                <ul>
                    <li class="menu-item-has-children">
                        <a href="javascript:void(0)">
                            <i class="la la-globe"></i>
                            {{__('translate.lang')}} :
                        </a>
                          <ul class="sub-menu">
                                 @foreach(LaravelLocalization::getSupportedLocales() as $key => $value)
                                 <li>
                                    <a href="{{LaravelLocalization::getLocalizedUrl($key)}}">{{$value['native']}}</a>
                                </li>
                                @endforeach
                                <li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>  

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@include('sweetalert::alert')
