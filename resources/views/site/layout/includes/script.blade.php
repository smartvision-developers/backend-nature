    <!-- Javascript Files -->
    <script src="{{url('')}}/public/sadafa/js/jquery-2.2.2.min.js"></script>
    <script src="{{url('')}}/public/sadafa/js/bootstrap.min.js"></script>
    <script src="{{url('')}}/public/sadafa/js/jquery.fancybox.min.js"></script>
    <script src="{{url('')}}/public/sadafa/js/owl.carousel.min.js"></script>
    <script src="{{url('')}}/public/sadafa/js/aos.js"></script>
    <script src="{{url('')}}/public/sadafa/js/script.js"></script>

</body>

</html>
