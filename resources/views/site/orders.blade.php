@extends('site/layout/main')
@section('content')
 <main class="main-content col-xs-12">
            <div class="bread-crumb col-xs-12" style="background-image: url({{URL('public/nature-sound')}}/images/hero-section/hero.png)">
                <div class="container">
                    <h3>{{__('translate.my-account')}}</h3>
                    <ul>
                        <li>
                            <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">{{__('translate.home')}}</a>
                        </li>
                        <li>{{__('translate.my-account')}}</li>
                    </ul>
                </div>
            </div>
            <div class="profile-wrap col-xs-12">
                <div class="container">
                    <a href="javascript:void(0)" class="op-sidebar">
                        <img lazy="loading" src="{{URL('/').Auth::guard('customer')->user()->photo}}" alt="{{Auth::guard('customer')->user()->name}}">
                    </a>
                    <div class="prof-sidebar col-md-3 col-xs-12">
                        <div class="prof-top">
                            <button type="button" class="clo-sidebar">
                                <i class="la la-close"></i>
                            </button>
                            <div class="s-img">
                        <img lazy="loading" src="{{URL('/')}}/{{Auth::guard('customer')->user()->photo}}" alt="{{Auth::guard('customer')->user()->name}}" id="blah1">

                          <form enctype="multipart/form-data" action="{{route('update-photo',  Auth::guard('customer')->user()->id)}}" method="post">
                                @csrf

                                <label>

                                    <input id="photo" type="file" name="photo" onchange="document.getElementById('blah1').src = window.URL.createObjectURL(this.files[0])">
                                    <i class="la la-camera"></i>
                                </label>
                                <button type="submit" class="i-btn">
                                    <i class="la la-check"></i>
                                </button>
                                <button type="button" class="i-btn i-red">
                                    <i class="la la-close"></i>
                                </button>
                            </form>
                            </div>
                            <h3>{{Auth::guard('customer')->user()->name }} </h3>
                            <p>{{Auth::guard('customer')->user()->email }}</p>
                        </div>
                        <ul>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/profile')}}">{{__('translate.profile')}}</a>
                            </li>
                           <li class="active">
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/my-orders')}}">مشترياتى</a>
                            </li>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/my-reservations')}}">حجوزاتى</a>
                            </li>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/shippment-address')}}">{{__('translate.shipping-addresses')}}</a>
                            </li>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/logout')}}">{{__('translate.logout')}}</a>
                            </li>
                        </ul>
                    </div>
               
                       <div class="prof-content col-md-9 col-sm-12 col-xs-12">
                                   @if($orders->isEmpty())
                <div class="col-md-6 col-sm-offset-5">
                    <img lazy="loading" src="{{URL('/public/images/empty.png')}}" height="220px">
                </div>
                <div class="g-more col-xs-6 col-sm-offset-3" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                    <a class="see-more3 btn" href="{{asset('/category')}}">تسوق الان </a> 
                </div>
                @else
                        <div class="prof-head col-xs-12">
                            <h4>الملف الشخصي</h4>
                            <div>
                                <div>
                                    <i class="la la-calendar"></i>
                                    <select class="form-control nice-select" onchange="top.location.href = this.options[this.selectedIndex].value">
                                        <option selected="">الكل</option>
                                        <option value="{{route('site.orders-3')}}">اخر 3 شهور </option>
                                        <option value="{{route('site.orders-6')}}">اخر 6 شهور</option>
                                    </select>
                                </div>
                                <div style="display:none;">
                                    <i class="las la-sort-amount-down-alt"></i>
                                    <select class="form-control nice-select">
                                        <option>الكل</option>
                                        <option>قيد التحضير</option>
                                        <option>تم استلامها</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="prof-body col-xs-12">
                            @php $count=1;  @endphp
                            @foreach($orders as $value)
                            <div class="order-list">
                                <div class="order-head">
                                    <span>طلب رقم  {{$count}}</span>
                                  <!--   <b>الحاله : تم استلامها</b> -->
                                </div>
                                <div class="order-body">
                                    <ul>
                                        <li>رقم الطلب :{{$value->request_no}}</li>
                                        <li>تاريخ اجراء الطلب : {{($value->created_at)->toDayDateTimeString()}}</li>
<!--                                         <li>تاريخ تسليم الطلب :10 ابريل 2021</li>
 -->                                        <li>المستلم : {{$value->Customer->name}}</li>
                                        <li>طريقة الدفع : دفع عند الاستلام</li>
                                        <li>المجموع :{{$value->order_total_price}} ريال</li>
                                    </ul>
                                    <a href="javascript:void(0)" class="op-order-dets">
                                        تفاصيل الطلب
                                        <i class="la la-angle-down"></i>
                                    </a>
                                </div>
                                <div class="dets-table">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>تفاصيل المنتج</th>
                                                    <th>الحالة</th>
                                                    <th>السعر</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @foreach($value->Order->Cart as $vall)
                                                <tr>
                                                    <td>
                                                        <div class="t-product">
                                                            <div class="t-img">
                                                                <img src="{{URL('public/images/products/').'/'.$vall->Product->ProductImage[0]->product_image}}" alt="">
                                                                <a href="{{URL('product/').'/'.$vall->Product->product_code}}"></a>
                                                            </div>
                                                            <div class="t-data">
                                                                <a href="{{URL('product/').'/'.$vall->Product->product_code}}" class="title">{{$vall->Product->product_name_ar}}</a>
                                                                <span>{{($vall->Product->created_at)->toDayDateTimeString()}}</span>
<!--                                                                 <a href="#" class="remove">الغاء</a>
 -->                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>مرحلة الاعداد</td>
                                                    <td>
                                                        <span class="price"> {{$vall->price_after_quantity}} ر . س</span>
                                                    </td>
                                                </tr>
                                                
                                                @endforeach
                                             
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @php $count++;  @endphp
                            @endforeach
                         
                        </div>
                          @endif
                    </div>
                 
                </div>
            </div>
        
@endsection

@section('js')
    <script src="{{URL('public/nature-sound')}}/js/jquery.fancybox.min.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/owl.carousel.min.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/jquery.nice-select.min.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/aos.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/intlTelInput.min.js"></script>

    
    <script>
        $('.edit__').click(function (){
            $('.personal-data').hide();
            $('.edit-personal').show();
        });
        
        $('.op-sidebar').click(function (){
            $('.prof-sidebar').addClass('active');
            $('html').addClass('off');
        });
        
        $('.clo-sidebar').click(function (){
            $('.prof-sidebar').removeClass('active');
            $('html').removeClass('off');
        });
        
        $('.s-img label').click(function (){
            $('.i-btn').fadeIn(1000);
            $(this).fadeOut(1000);
        });
            
        $('.op-order-dets').click(function (){
            $(this).parent().next('.dets-table').stop().slideToggle();
        });
    </script>
    @endsection