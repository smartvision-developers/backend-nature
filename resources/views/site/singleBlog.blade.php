@extends('site/layout/main')

@section('og')
    <meta property="og:title" content="}" />
    <meta property="og:image" content="{{ $blog->blog_title }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $blog->blog_title }}" />
    <meta property="og:url" content="{{url('blogs/'. $blog->id) }}" />
    <meta property="og:image" content="{{asset('public/'.$blog->blog_image)}}" />
    <meta property="og:description" content="{{ $blog->blog_text }}" />
    <meta property="og:site_name" content="{{URL('/')}}" />

@endsection
@section('style')
<style type="text/css">
    
    .blog-card
    {
        height: 400px !important;
    }
</style>
@endsection
@section('content')
 <main class="main-content col-xs-12">
            <div class="bread-crumb col-xs-12" style="background-image: url({{URL('public/nature-sound')}}/images/hero-section/hero.png)">
                <div class="container">
                    <h3>{{__('translate.blog')}}</h3>
                    <ul>
                        <li>
                            <a href="">{{__('translate.home')}}</a>
                        </li>
                        <li>
                            <a href="#">{{__('translate.blog')}}</a>
                        </li>
                        <li>{{$blog->blog_title}}</li>
                    </ul>
                </div>
            </div>
            <!--=================== SINGLE BLOG ======================-->
            <div class="single-wrap col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="blog-box col-md-8 col-xs-12">
                            <div class="post">
                                <div class="post-title">
                                    <h4>{{$blog->blog_title}}</h4>
                                </div>
                                <div class="post-img">
                                    <img  lazy="loading" src="{{URL('/'. $blog->blog_image)}}" alt="{{$blog->blog_title}}">
                                </div>
                                
                                <div class="post-info">
                                    <ul>
                                        <li>
                                            <i class="la la-edit"></i>
                                            {{__('translate.by')}} {{$blog->User->name}}
                                        </li>
                                        <li>
                                            <i class="la la-clock"></i>
                                <?php echo $blog->created_at->toFormattedDateString(); ?>
                                        </li>
                                        <li>
                                            <i class="la la-comment"></i>
                                            {{ $count_comment}}
                                        </li>
                                    </ul>
                                    <div class="share-l">
                                        <span> :{{__('translate.share')}}</span>
                                        <!--<div>-->
                                        <!--    <a href="#">-->
                                        <!--        <i class="la la-twitter"></i>-->
                                        <!--    </a>-->
                                        <!--    <a href="#">-->
                                        <!--        <i class="la la-facebook"></i>-->
                                        <!--    </a>-->
                                        <!--    <a href="#">-->
                                        <!--        <i class="la la-instagram"></i>-->
                                        <!--    </a>-->
                                        <!--</div>-->
                                        <!-- ShareThis BEGIN --><div class="sharethis-inline-share-buttons"></div><!-- ShareThis END -->
                                    </div>
                                </div>
                                <div class="post-sections">
                                    <div class="section" id="sec__1">
                                                {!! $blog->blog_description !!}

                                    </div>
                                
                                </div>
                            </div>
                            <div class="post-others">
                                <div class="row">
                                    @isset($next)
                                    <div class="p-block col-md-6 col-xs-12">
                                        <span>{{__('translate.nex-blog')}}</span>
                                        <a href="{{url('blog/'.$next->blog_title)}}">
                                            <img  lazy="loading" src="{{URL('/'.$next->blog_image)}}" alt="">
                                            <p>{{$next->blog_title}}</p>

                                        </a>
                                    </div>
                                    @endisset
                                    @isset($previous)
                                    <div class="p-block col-md-6 col-xs-12">
                                        <span>{{__('translate.pre-blog')}}</span>
                                        <a href="{{url('blogs/'.$previous->blog_title)}}">
                                            <img  lazy="loading" src="{{URL('/'.$previous->blog_image)}}" alt="">
                                            <p>{{$previous->blog_title}}</p>
                                        </a>
                                    </div>
                                    @endisset
                                </div>
                            </div>
                            <div class="post-comments">
                                <ul>
                                    @foreach($blog->BlogComment as $key => $value3)
                                    <li>
                                        <div class="com-img">
                                    <img  lazy="loading" src="{{URL('/')}}/{{$value3->Customer->photo}}" alt="{{$value3->Customer->name}}- image">
                                        </div>
                                        <div class="com-data">
                                            <div class="d-top">
                                                <h4>{{$value3->Customer->name}}</h4>
                                                <span>
                                                    <i class="la la-clock"></i>
                                                  <?php echo $value3->created_at->toFormattedDateString(); ?>
                                                </span>
                                            </div>
                                            <div class="d-bot">
                                                <p>{{$value3->comment}}</p>
<!--                                                 <a href="#">رد</a>
 -->                                            </div>
                                        </div>
                                    </li>
                                    @endforeach
                                   
                                </ul>
                                <div class="post-add-comment">
                                    <div class="post-head">
                                        <h3>
                                            <i class="la la-comment"></i>
                                            <span>{{__('translate.leave-comment')}}</span> 
                                        </h3>
                                    </div>
                                    <div class="post-form">
                                             <div class="row">
                                                <input type="hidden" id="customer_id" name="customer_id" value="{{Auth::guard('customer')->check()?Auth::guard('customer')->user()->id:''}}">
                                                <input type="hidden" id="blog_id" name="blog_id" value="{{$blog->id}}">
                                            <div class="form-group col-md-12 col-xs-12">
                                                <textarea required="" name="comment" id="comment" class="form-control" placeholder="{{__('translate.your-comment')}}"></textarea>
                                            </div>
                                            <div class="form-group col-md-12 col-xs-12">
                                                <button type="submit" class="btn store_comment">{{__('translate.send')}}</button>
                                            </div>                                                                                   </div>                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog-sidebar col-md-4 col-xs-12">
                            <div class="side-widget">
                                <div class="w-head">
                                    <h4>{{__('translate.search-blog')}}</h4>
                                </div>
                                <div class="w-body">
                                    <div class="side-form">
                                       <form action="{{url('/blogs-search/')}}" method="get">
                                              @csrf
                                            <input type="search" required="" name="content" class="form-control" placeholder="{{__('translate.type-search')}}">
                                            <div class="form-group">
                                                <button type="submit" class="btn">{{__('translate.search')}}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="side-widget">
                                <div class="w-head">
                                    <h4>{{__('translate.other-blogs')}}</h4>
                                </div>
                                <div class="w-body">
                                    <div class="related-blogs">
                                        <ul>
                                            @foreach($get_similar as $key=> $value )
                                            <li>
                                                <div class="r-img">
                                                    <a href="{{URL('/blog/'.$value->blog_title)}}">
                                                        <img  lazy="loading"  src="{{URL('/'.$value->blog_image)}}" alt="{{$value->blog_text}}">
                                                    </a>
                                                </div>
                                                <div class="r-data">
                                                    <a href="{{URL('/blog/'.$value->blog_title)}}">{{$value->blog_title}}</a>
                                                    <p>
                                                    <?php echo $value->blog_text; ?></p>
                                                    <span>
                                                        <i class="la la-clock"></i>
                                                        {{($value->created_at)->toDateString()}} 
                                                    </span>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>

                           <!--  <div class="side-free-area">
                                <a href="#">
                                    <img  lazy="loading" src="https://via.placeholder.com/370x250.png/813970/fff?text=ads area 370*250" alt="">
                                </a>
                            </div> -->
                            @if($blog->BlogBlogTag->isnotEmpty())
                            <div class="side-widget" id="tags_section">
                                <div class="w-head">
                                    <h4>{{__('translate.tags')}}</h4>
                                </div>
                                <div class="w-body">
                                    <div class="side-tags">
                                        @foreach($blog->BlogBlogTag as $key => $value)
                                          @if(app()->getLocale() == 'ar')
                                            <a href="{{URL('blogsTag/'.$value->BlogTag->tag_ar)}}">{{$value->BlogTag->tag_ar}}</a>
                                           @elseif(app()->getLocale() == 'en')
                                            <a href="{{URL('blogsTag/'.$value->BlogTag->tag_en)}}">{{$value->BlogTag->tag_en}}</a>
                                          @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            @endif
                           <!--  <div class="side-free-area">
                                <a href="#">
                                    <img  lazy="loading" src="https://via.placeholder.com/370x700.png/813970/fff?text=ads area 370*700" alt="">
                                </a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
            <!--============================================-->


@stop
@section('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

   <script type="text/javascript">
       $(document).ready(function(){
        $('.store_comment').click(function(event){
            event.preventDefault();
            var comment = $('#comment').val();
            var blog_id = $('#blog_id').val();
            var customer_id = $('#customer_id').val();
            $.ajax({
                type: "get",
                url: '{{route('site.store_comment',$blog->id)}}',
                data: {    
                            comment:comment, blog_id:blog_id, customer_id:customer_id },         
                dataType: "json",
                success: function(result){
                    $('.post-comments ul').append('<li><div class="com-img"><img src="{{URL('/')}}/<?php echo Auth::guard('customer')->check()?Auth::guard('customer')->user()->photo: ''; ?>" alt="<?php echo Auth::guard('customer')->check()?Auth::guard('customer')->user()->name: ''; ?>"-image> </div> <div class="com-data"><div class="d-top"><h4>'+"<?php echo Auth::guard('customer')->check()? Auth::guard('customer')->user()->name: ''; ?>"+'</h4><span><i class="la la-clock"></i><?php  if($blog->BlogComment == null){echo $value3->created_at->toFormattedDateString(); }?> </span></div><div class="d-bot"><p>'+comment+'</p></div></li>');

                        swal({
                            icon: 'success',
                            title: 'تم إضافه تعليقك بنجاح'
                        });
                  
                },
                error: function (result) {
                    swal(
                        'Fail',
                        'لابد من تسجيل الدخول أولا',
                        'error',
                    );
                }
            });
        });
    });
   </script>
   <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5e695eafa853fd0012447981&product=sop' async='async'></script>
@endsection