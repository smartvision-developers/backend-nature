@extends('site/layout/main')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">

@section('content')

<main class="main-content col-xs-12">
    <div class="bread-crumb col-xs-12" style="background-image: url({{URL('public/nature-sound/images/hero-section/hero.png')}})">
        <div class="container">
            <h3>{{__('translate.hotel')}} </h3>
            <ul>
                <li>
                    <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">{{__('translate.home')}}</a>
                </li>
                <li>{{__('translate.hotel')}} </li>
            </ul>
        </div>
    </div>
    <div class="clinic-wrap col-xs-12">
        <div class="cl-top col-xs-12">
            <div class="container">
                <div class="cl-img col-md-3 col-xs-12">
                    <img lazy="loading" src="{{URL($hotel->hotel_image)}}" alt="{{$hotel->hotel_title}}">
                    <ul>
                        <li>
                            <i class="la la-map-marker"></i>
                            <a href="#">{{$hotel->HotelSetting->address}}</a>
                        </li>
                        <li>
                            <i class="la la-phone-volume"></i>
                            <a href="tel:{{$hotel->HotelSetting->phone}}">{{$hotel->HotelSetting->phone}}</a>
                        </li>
                        <li>
                            <i class="la la-envelope"></i>
                            <a href="mailto:{{$hotel->HotelSetting->email}}">{{$hotel->HotelSetting->email}}</a>
                        </li>
                    </ul>
                </div>
                <div class="cl-info col-md-9 col-xs-12">
                    <h3>{{__('translate.about')}} {{$hotel->hotel_title}}</h3>
                    <?php echo $hotel->hotel_description ;?>
                    <h3>{{__('translate.available-service')}}</h3>
                    <ul>
                        @foreach($hotel->HotelService as $val)
                        @if($val->status == 1)
                        <li>@if(app()->getLocale() == 'ar'){{$val->service_description_ar}} @elseif(app()->getLocale() == 'en'){{$val->service_description_en}}@endif    </li>
                        @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="cl-form col-xs-12">
            <div class="cl-form-head col-xs-12">
                <div class="container">
                    <h3>حجز خدمة عيادة</h3>
                </div>
            </div>
            <div class="cl-form-body col-xs-12">
                <div class="container">
                    <div class="alert alert-danger print-error form-group" style="display: none;    margin: 10px 30px;">
                                <ul></ul>
                            </div>
                    <form action="#" name="add_name" id="add_name" method="get">
                        <div class="row">
                            <div class="col-xs-12 row0" id="dynamic_field">
                            <div class="form-group col-md-4 col-xs-12">
                                <h4>اختر الخدمة</h4>
                                <select name="more[0][service_id]" class="form-control nice-select operations-supplier">
                                    <option value="">اختر الخدمة</option>
                                    @foreach($hotel->HotelService as $val)
                                        @if($val->status == 1)
                                        <option value="{{$val->id}}" data-capacity="{{$val->service_price}}"> @if(app()->getLocale() == 'ar') {{$val->service_description_ar}} @elseif(app()->getLocale() == 'en'){{$val->service_description_en}} @endif      </option>
                                        @endif
                                    @endforeach                                
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-xs-12">
                                <h4>نوع الحيوان الاليف</h4>
                                <select name="more[0][animal_id]" class="form-control nice-select">
                                    <option value="">اختر نوع الحيوان</option>
                                    @foreach($animals as $key=> $value)
                                        <option value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-2 col-xs-12">
                                <h4>اكتب العدد</h4>
                                <input name="more[0][animal_no]" type="number" class="form-control animal_no" placeholder="" value="1" min="1">
                            </div>
                            <input type="hidden" name="more[0][service_price]"  class="service_price operations-supplierCapacity" value="">

                            <div class="form-group col-md-2 col-xs-12">
                                <h4>g</h4>
                            <button type="button" name="add" id="add" class="btn btn-success">Add More</button>
                                </div>
                            </div>
                            <div class="form-group col-md-4 col-xs-12">
                                <h4>تاريخ بدايه الحجز</h4>
                                <input type="text" name="reservation_start_date" class="form-control date_inp" placeholder="تاريخ الحجز">
                                <i class="la la-calendar"></i>
                            </div>
                            <div class="form-group col-md-4 col-xs-12">
                                <h4>تاريخ نهايه الحجز</h4>
                                <input type="text" name="reservation_end_date" class="form-control date_inp" placeholder="تاريخ الحجز">
                                <i class="la la-calendar"></i>
                            </div>
                            <div class="form-group col-md-4 col-xs-12" style="display:none;">
                                <h4>طريقة الدفع</h4>
                                <select class="form-control nice-select">
                                    <option>دفع الكترونى</option>
                                    <option>دفع الكترونى</option>
                                    <option>دفع الكترونى</option>
                                </select>
                            </div>
                              <div class="form-group col-md-12">
                                <button type="button" name="submit" id="submit" class="btn operations-supplier22" data-toggle="modal" data-target="#reserv_pop">ادفع </button>
<!--                                     <button type="button" style="display:none;" name="submit" id="submit" class="btn operations-supplier221" data-toggle="modal" data-target="#reserv_pop">ادفع </button>
 -->
                                </div>
                            <div class="form-group has-pay col-xs-12" style="display:none;">
                                <h4>بطاقة الائتمان / الخصم</h4>
                                <div class="row">
                                    <div>
                                        <div class="form-group">
                                            <h4>رقم البطاقة <i>*</i></h4>
                                            <input type="text" class="form-control ccFormatMonitor" placeholder="رقم البطاقة" maxlength='19'>
                                        </div>
                                        <div class="form-group">
                                            <h4> اسم صاحب الكارت <i>*</i></h4>
                                            <input type="text" class="form-control" placeholder="اسم صاحب الكارت">
                                        </div>
                                        <div class="form-group">
                                            <h4>  انتهاء الصلاحية <i>*</i></h4>
                                            <input type="text" class="form-control" id="inputExpDate" placeholder="MM / YY" maxlength='7'>
                                        </div>
                                        <div class="form-group">
                                            <h4>CVV <i>*</i></h4>
                                            <input type="password" class="form-control cvv" placeholder="***" maxlength='3'>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="form-group">
                                            <button type="button" name="submit" id="submit" class="btn " data-toggle="modal" data-target="#reserv_pop">ادفع 300 ريال</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="ab-gallery col-xs-12">
            <div class="container">
                <div class="g-head col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                    <span>الجاليرى</span>
                    <h3>مكتبة الصور</h3>
                </div>
                <div class="g-body col-xs-12">
                    <div class="g-slider owl-carousel">
                     @foreach($gallerys as $data)
                        <div class="block">
                            <img lazy="loading" src="{{URL($data->site_image)}}" alt="">
                            <a href="{{URL($data->site_image)}}" data-fancybox="images">
                                <i class="la la-search"></i>
                            </a>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
    @section('js')
    <script src="{{URL('public/nature-sound')}}/js/jquery.fancybox.min.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/owl.carousel.min.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/jquery.nice-select.min.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/aos.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/visa-format.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        $(".date_inp").flatpickr();
    </script>
    <script type="text/javascript">
    $(document).ready(function(){      
      var postURL = "{{URL('/hotel/add_more')}}";
      var i=0;  
      var n_sum=0;
      var xy;
     $('.row0').change(function($x) {
            x=$('.animal_no').val();
        var capacityValue = $('select.operations-supplier').find(':selected').data('capacity');
     //   console.log(capacityValue);
        $('.operations-supplierCapacity').val(x *capacityValue);
         $('.operations-supplier22').text('ادفع ' + x *capacityValue + ' ريال');
         n_sum=( x * capacityValue);
        console.log(' record'+ n_sum);

    });

      $('#add').click(function(){  

        var t_sum=n_sum;

        var old_sum=0;
           i++;  
           $('#dynamic_field').append('<div id="row'+i+'" class="dynamic-added"><div class="form-group col-md-4 col-xs-12"><h4>اختر الخدمة</h4> <select name="more['+i+'][service_id]" class="form-control nice-select operations-supplier'+i+'"><option>اختر الخدمة</option> @foreach($hotel->HotelService as $val)@if($val->status == 1)<option value="{{$val->id}}" data-capacity="{{$val->service_price}}"> @if(app()->getLocale() == 'ar') {{$val->service_description_ar}} @elseif(app()->getLocale() == 'en'){{$val->service_description_en}} @endif </option> @endif @endforeach  </select></div><div class="form-group col-md-4 col-xs-12"><h4>نوع الحيوان الاليف</h4> <select name="more['+i+'][animal_id]" class="form-control nice-select"><option>اختر نوع الحيوان</option> @foreach($animals as $key=> $value)<option value="{{$value->id}}">{{$value->name}}</option>@endforeach</select></div> <div class="form-group col-md-2 col-xs-12"> <h4>اكتب العدد</h4> <input name="more['+i+'][animal_no]" type="number" class="form-control animal_no'+i+'" placeholder="" min="1" value="1"> </div><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button> <input type="hidden" name="more['+i+'][service_price]"  class="service_price operations-supplierCapacity'+i+'" value=""></div>');  
                    $('.nice-select').niceSelect('update');

                  console.log("old Price:  "+ t_sum);


  $('#row'+i+'').change(function($z) {
    //old_sum = t_sum;
    z=$('.animal_no'+i+'').val();
    console.log('dcfn'+z);
        var capacityValue2 = $('select.operations-supplier'+i+'').find(':selected').data('capacity');
        var m=$('.operations-supplierCapacity'+i+'').val(z *capacityValue2);
            console.log('price'+capacityValue2);

                console.log('su2m'+(z *capacityValue2));
          t_sum= ( z * capacityValue2);
        });
        console.log('totalsum'+$('select.operations-supplier1'));

    $('#row'+i+'').on('mouseleave',function($z) {

        var sum11 = 0;
        $('.service_price').each(function(){
            sum11 += Number($(this).val());
            console.log(sum11);
        });
$('.operations-supplier22').val(sum11);
     $('.operations-supplier22').text('ادفع ' + sum11+ ' ريال');
          });

      });  


      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           var q= $('.operations-supplierCapacity'+i+'').val();
                      var q2= $('.operations-supplier22').val();

           console.log('tttttttttt'+  $('.operations-supplierCapacity'+i+'').val());

           $('.operations-supplier22').val(q2 -q );
             $('.operations-supplier22').text('ادفع ' + (q2- q)+ ' ريال');

           $('#row'+button_id+'').remove();  
      });  


      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });


      $('#submit').click(function(){            
           $.ajax({  
                url:postURL,  
                method:"POST",  
                data:$('#add_name').serialize(),
                type:'json',
                  success: function(data){
              if ((data.errors)) {
                        printErrMsg(data.errors);
                    }
                    if (data == 1) {
                        swal({
                            icon: 'success',
                            title: 'تم  إضافه الحجز بنجاح '
                        });
                  }
                },
                error: function (data) {
                    swal(
                        'Fail',
                        'لابد من تسجيل الدخول أولا',
                        'error',
                    );
                }
              
           });  
      });  
          function printErrMsg(msg)
    {
        $(".print-error").find("ul").html('');
        $(".print-error").css('display','block');
        $.each(msg, function(key, value)
        {
           $(".print-error").find("ul").append('<li>' +value+ '</li>')
       })
    }

    });  

</script>

@endsection