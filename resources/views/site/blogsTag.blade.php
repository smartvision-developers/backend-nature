@extends('site/layout/main')
@section('style')
<style type="text/css">
    
    .blog-card
    {
        height: 400px !important;
    }
</style>
@endsection
@section('content')
       <main class="main-content col-xs-12">
            <div class="bread-crumb col-xs-12" style="background-image: url({{URL('public/nature-sound/images/hero-section/hero.png')}})">
                <div class="container">
                    <h3>{{__('translate.search-results')}}</h3>
                    <ul>
                        <li>
                            <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">{{__('translate.home')}}</a>
                        </li>
                        <li>{{__('translate.looking-for-blogs')}}  
                         @if(app()->getLocale() == 'ar') {{$get_tag->tag_ar}} @elseif(app()->getLocale() == 'en'){{$get_tag->tag_en}} @endif
                        </li>
                    </ul>
                </div>
            </div>
            <div class="search-wrap col-xs-12">
                <div class="container">
                    
                    @foreach($blog_tag as $key => $value)
                    <div class="block col-md-3 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                        <div class="product-card">
                            <div class="p-inner">
                                <div class="p-img">
                                    <img lazy="loading" src="{{URL('/'.$value->Blog->blog_image)}}" alt="{{$value->Blog->blog_title_ar}}">
                                </div>
                                <div class="p-data">
                                    @if(app()->getLocale() == 'ar') 
                                        <a href="{{asset('blog/'. $value->Blog->blog_title_ar)}}">{{$value->Blog->blog_title_ar}}</a>  
                                     <p> <?php echo $value->Blog->blog_text_ar ?> </p>
                                
                                    @elseif(app()->getLocale() == 'en')
                                        <a href="{{asset('blog/'. $value->Blog->blog_title_en)}}">{{$value->Blog->blog_title_en}}</a>                                      
                                        <p><?php echo $value->Blog->blog_text_en ?> </p>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
          
                    <div class="g-more col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="600">
                        <a href="#" class="btn">المزيد</a>
                    </div>
                </div>
            </div>
@stop
@section('js')

@endsection