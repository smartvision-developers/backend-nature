  
  <div class="cat-toolbar col-xs-12">
                          <!--   <select class="form-control nice-select">
                                <option>ترتيب حسب</option>
                                <option>ترتيب حسب</option>
                                <option>ترتيب حسب</option>
                                <option>ترتيب حسب</option>
                            </select> -->
                            <div>
                                <button type="button" class="btn grid-view active">
                                    <i class="la la-th"></i>
                                </button>
                                <button type="button" class="btn list-view">
                                    <i class="la la-list"></i>
                                </button>
                            </div>
                        </div>
                        <div class="search-wrap col-xs-12 grid__">
                            @foreach($products as $key1 => $value1)
                                 @if( $value1->Product->status == 1)
                            <div class="block col-md-4 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                                <div class="product-card">
                                    <div class="p-inner">
                                        <div class="p-img">
                                            <div class="p-actions">
                                                 @php
                                                        $cart=\App\Models\Cart::where('product_id', $value1->Product->id)->where('customer_id',auth()->guard('customer')->id())->first();
                                                    @endphp
                                                    @if(!$cart)
                                                         <button class="add-pro cart_{{$value1->Product->id}}" type="button" onclick="addProductToCart(this, {{$value1->Product->id}}, {{$value1->Product->product_unitprice}})" data-tool="tooltip" title="اضف للسلة" data-placement="right">
                                                            <i class="las la-shopping-bag"></i>
                                                        </button>
                                                    @else
                                                       <button class="add-pro cart_{{$value1->Product->id}}" type="button" onclick="addProductToCart(this, {{$value1->Product->id}}, {{$value1->Product->product_unitprice}})" data-tool="tooltip" title="حذف من السله" data-placement="right">
                                                            <i class="las la-trash" style="color:red;"></i>
                                                        </button>
                                                    @endif
                                            

                                              @php
                                                        $fav=\App\Models\Product_Favourite::where('product_id', $value1->Product->id)->where('customer_id',auth()->guard('customer')->id())->first();
                                                    @endphp
                                                    @if(!$fav)
                                                          <button type="button" class="add-fav fav_{{$value1->Product->id}}" onclick="addProductToFav(this, {{$value1->Product->id}})" data-tool="tooltip" title="اضف للمفضلة" data-placement="right">
                                                            <i class="las la-heart"></i>
                                                        </button>
                                                    @else
                                                       <button type="button" class="add-fav fav_{{$value1->Product->id}}" onclick="addProductToFav(this, {{$value1->Product->id}})" data-tool="tooltip" title="حذف من  لامفضلة" data-placement="right">
                                                            <i class="las la-heart" style="color:red;"></i>
                                                        </button>
                                                    @endif
                                               <button type="button" data-tool="tooltip" title="عرض سريع" data-placement="right">
                                                    <i class="las la-search"></i>
                                                </a>
                                               <button type="buttbuttonn" data-tool="tooltip" title="اضف للمقارنة" data-placement="right">
                                                    <i class="las la-sync"></i>
                                                </button>
                                            </div>
                                            <img  lazy="loading" src="{{URL('/public/images/products/'. $value1->Product->ProductImage[0]->product_image)}}" alt="{{$value1->Product->product_name_ar}}">
                                        </div>
                                        <div class="p-data">
                                            @if(app()->getLocale() == 'ar') 
                                                <a href="{{asset('/product/'.$value1->Product->product_code)}}">{{$value1->Product->product_name_ar}}</a>
                                                <p class="desc">{{$value1->Product->product_description_ar}}</p>
                                            @elseif(app()->getLocale() == 'en')
                                                <a href="{{asset('/product/'.$value1->Product->product_code)}}">{{$value1->Product->product_name_en}}</a>
                                                <p class="desc">{{$value1->Product->product_description_en}}</p>
                                            @endif
                                            <p>
                                                <span>{{$value1->Product->product_unitprice}} ريال</span>
                                                 @if($value1->Product->product_oldprice)
                                                <span class="old-price">{{ $value1->Product->product_oldprice }} ريال</span>
                                                @endif                                            
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                           @endforeach

                        </div>

                      <div class="g-more col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                    <button class="see-more btn" data-page="2" data-link="{{asset('/category')}}?page=" data-div=".grid__">{{__('translate.more')}}</button> 
                </div>
           
            <script src="{{URL('public/nature-sound')}}/js/jquery.nice-select.min.js"></script>

<script src="{{URL('public/nature-sound')}}/js/aos.js"></script>
<script src="{{URL('public/nature-sound')}}/js/script.js"></script>
<script>

                $('.grid-view').click(function (){
                    $('.search-wrap').addClass('grid__').removeClass('list__');
                    $(this).addClass('active');
                    $('.list-view').removeClass('active');
                });

                $('.list-view').click(function (){
                    $('.search-wrap').addClass('list__').removeClass('grid__');
                    $(this).addClass('active');
                    $('.grid-view').removeClass('active');
                });
                  $(".see-more").click(function() {
  $div = $($(this).data('div')); //div to append
  $link = $(this).data('link'); //current URL

  $page = $(this).data('page'); //get the next page #
  $href = $link + $page; //complete URL
  $.get($href, function(response) { //append data
    $html = $(response).find(".grid__").html(); 
    console.log($link);
    $div.append($html);
});

  $(this).data('page', (parseInt($page) + 1)); //update page #
});

    </script>
