@extends('site/layout/main')
@section('content')

<main class="main-content col-xs-12">
    <div class="bread-crumb col-xs-12" style="background-image: url({{URL('public/nature-sound/images/hero-section/hero.png')}})">
        <div class="container">
            <h3>{{__('translate.privacy')}} </h3>
            <ul>
                <li>
                    <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">{{__('translate.home')}}</a>
                </li>
                <li>{{__('translate.privacy')}} </li>
            </ul>
        </div>
    </div>
            <!-- start Verify -->
            <div class="auth-wrap col-xs-12">
                <div class="container">
                    <div class="c-card verify col-xs-12">
                    <div class="auth-top col-xs-12">
                    <img src="{{URL('/public/nature-sound')}}/images/verify.png" alt="">
                      @if($data->forget_password == 0)
                    <h3>كود التحقق</h3>
                     <p>تم ارسال كود التحقق المكون من أربعة
                     ارقام الى بريدك الالكترونى</p>
                     @else
                     <h3>تغير كلمة المرور</h3>
                     <p>يمكنك انشاء كلمة مرور جديدة</p>
                     @endif
                </div>
                <div class="auth-form col-xs-12">
                    @if($data->forget_password == 0)
                    <form action="{{route('site.checking')}}" method="post">
                        @csrf
                        <input type="hidden" name="email" type="email" value="{{$data->email}}">
                       
                        <div class="form-group col-xs-12">
                            
                            <input type="text" name="code[]" maxlength="1" min="0" class="form-control" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
                            <input type="text" name="code[]" maxlength="1" min="0" class="form-control" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
                            <input type="text" name="code[]" maxlength="1" min="0" class="form-control" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
                            <input type="text" name="code[]" maxlength="1" min="0" class="form-control" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
                        </div>
                        <div class="form-group col-xs-12">
                            <h5 id="timer">00:30</h5>
                        </div>
                        <div class="form-group col-xs-12">
                            <a href="#">
                                <i class="la la-redo-alt"></i>
                                إعادة ارسال
                            </a>
                        </div>
                        <div class="form-group col-xs-12 has-btn">
                            <button type="submit" class="btn">تسجيل</button>
                        </div>
                        </form>
                        @endif
                        @if($data->forget_password == 1)
                     <form action="{{route('site.checking')}}" method="post">
                        @csrf
                                                <input type="hidden" name="email" type="email" value="{{$data->email}}">

                        <div class="form-group n_form col-xs-12">
                            <h6>كلمة المرور الجديدة</h6>
                            <input type="password" name="new_password" class="form-control" id="password-field">
                            <button type="button" class="show-pass" toggle="#password-field">
                                <i class="la la-eye"></i>
                            </button>
                        </div>
                        <div class="form-group n_form col-xs-12">
                            <h6>كلمة المرور الجديدة مرة اخرى</h6>
                            <input type="password" name="confirm_password" class="form-control" id="password-field1">
                            <button type="button" class="show-pass" toggle="#password-field1">
                                <i class="la la-eye"></i>
                            </button>
                        </div>
                        <div class="form-group col-xs-12 has-btn">
                            <button type="submit" class="btn">تأكيد</button>
                        </div>
                    </form>
                    @endif
                </div>
                </div>
                </div>
            </div>
            
            <script>
    
    
time=5*60,r=document.getElementById('timer'),tmp=time;
setInterval(function(){
var c=tmp--,m=(c/60)>>0,s=(c-m*60)+'';
r.textContent='0' + m + ' : ' + (s.length>1?'':'0')+s
tmp!=0||(tmp=time);
},1000);


    </script>
            <!-- END Verify -->
    @stop
    @section('js')
 @endsection