@extends('site/layout/main')
@section('content')
        <main class="main-content col-xs-12">
            <div class="bread-crumb col-xs-12" style="background-image: url({{URL('public/nature-sound')}}/images/hero-section/hero.png)">
                <div class="container">
                    <h3>{{__('translate.favorite')}}</h3>
                    <ul>
                        <li>
                            <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">{{__('translate.home')}}</a>
                        </li>
                        <li>{{__('translate.favorite')}}</li>
                    </ul>
                </div>
            </div>
            <div class="cart-wrap fav-wrap col-xs-12">
                <div class="container">
                                    @if($favs->isEmpty())
                <div class="col-md-6 col-sm-offset-5">
                    <img lazy="loading" src="{{URL('/public/images/empty-wishlist.svg')}}" height="220px">
                </div>
                <div class="g-more col-xs-6 col-sm-offset-3" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                    <a class="see-more3 btn" href="{{asset('/category')}}">أضف منتجات للمفضله الأن </a> 
                </div>
                @else

                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>{{__('translate.removal')}}</th>
                                            <th>{{__('translate.product-details')}}</th>
                                            <th>{{__('translate.stock-status')}}</th>
                                            <th>{{__('translate.add-to-cart')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @isset($favs)
                                        @foreach($favs as $key=> $value)
                                        <tr>
                                       <input type="hidden" class="quantity" value="1" name="quantity">

                                        <input type="hidden" class="price_after_quantity" value="{{$value->Product->product_unitprice}}" name="price_after_quantity">

                                            <td id="codeRefer">
                                                <input type="hidden" value="{{$value->product_id}}" name="product_id">
                                                <button type="button" onclick="removeProductFromFav(this, {{$value->product_id}})" class="t-remove"><i class="la la-trash"></i></button>
                                            </td>
                                            <td>
                                                <div class="t-product">
                                                    <div class="t-img">
                                                        <img lazy="loading" src="{{URL('/public/images/products/'.$value->Product->ProductImage[0]->product_image)}}" alt="">
                                                        <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/product/'.$value->Product->product_code)}}"></a>
                                                    </div>
                                                    <div class="t-data">
                                                        <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/product/'.$value->Product->product_code)}}">{{$value->Product->product_name}}</a>
                                                        <span>{{$value->Product->product_description}}</span>
                                                        <p>{{$value->Product->product_unitprice}} ر.س</p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>متوفر</td>
                                            <td>

                                                  @php
                                      $cart=\App\Models\Cart::where('product_id', $value->Product->id)->where('customer_id',auth()->guard('customer')->id())->first();
                                      @endphp
                                            @if(!$cart)
                                                <button type="button" onclick="addProductToCart(this, {{$value->Product->id}}, {{$value->Product->product_unitprice}})" class="btn cart_{{$value->Product->id}}">{{__('translate.add-to-cart')}}</button>
                                                @else
                                                     <button type="button" onclick="addProductToCart(this, {{$value->Product->id}}, {{$value->Product->product_unitprice}})" class="btn cart_{{$value->Product->id}}">حذف من السله</button>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                      @endisset
                                    </tbody>
                                </table>
                            </div>
                            @endif
                </div>
            </div>
     
@stop
@section('js')
    <script src="{{URL('public/nature-sound')}}/js/jquery.nice-select.min.js"></script>
@endsection