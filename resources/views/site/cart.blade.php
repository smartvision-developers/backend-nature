@extends('site/layout/main')
@section('content')
 <main class="main-content col-xs-12">
            <div class="bread-crumb col-xs-12" style="background-image: url({{URL('public/nature-sound')}}/images/hero-section/hero.png)">
                <div class="container">
                    <h3>سلة الطلبات</h3>
                    <ul>
                        <li>
                            <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">{{__('translate.home')}}</a>
                        </li>
                        <li>سلة الطلبات</li>
                    </ul>
                </div>
            </div>
            <div class="cart-wrap col-xs-12">
                <div class="container">
                @if($carts->isEmpty())
                <div class="col-md-6 col-sm-offset-5">
                    <img lazy="loading" src="{{URL('/public/images/empty.png')}}" height="220px">
                </div>
                <div class="g-more col-xs-6 col-sm-offset-3" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                    <a class="see-more3 btn" href="{{asset('/category')}}">تسوق الان </a> 
                </div>
                @else
                    <ul class="nav-tabs col-xs-12">
                        <li class="active">
                            <a href="#" data-toggle="tab" data-target="#t1">
                                <i class="la la-check"></i>
                                سلة الطلبات
                            </a>
                        </li>
                        <li class="disabled">
                            <a href="#" data-toggle="tab" data-target="#t2">
                                <i class="la la-check"></i>
                                العنوان
                            </a>
                        </li>
                        <li class="disabled">
                            <a href="#" data-toggle="tab" data-target="#t3">
                                <i class="la la-check"></i>
                                الدفع
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content col-xs-12">
                        <div class="tab-pane fade active in" id="t1">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>تفاصيل المنتج</th>
                                            <th>الكمية</th>
                                            <th>الاجمالى</th>
                                            <th>ازالة</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                        @php $count= 1; @endphp
                                        @foreach($carts as $key => $value)
                                        <tr id="{{$value->product_id}}">
                                            <td>
                                                <div class="t-product">
                                                    <div class="t-img">
                                                        <img lazy="loading" src="{{URL('/public/images/products/'.$value->Product->ProductImage[0]->product_image)}}" alt="{{$value->Product->product_name_ar}}">
                                                        <a href="{{URL('product/'.$value->Product->product_code)}}"></a>
                                                    </div>
                                                    <div class="t-data">
                                                        <a href="{{URL('product/'.$value->Product->product_code)}}">
                                                         @if(app()->getLocale() == 'ar') {{$value->Product->product_name_ar}} @elseif(app()->getLocale() == 'en'){{$value->Product->product_name_en}}@endif
                                                            </a>
                                                        <p>{{$value->Product->product_unitprice}} ر.س</p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="number">
                                                    
 <button type="submit" class="inc qtybutton waves-effect" name="quantity[]" onclick="inc({{$value->Product->product_unitprice}})">+  </button>
     <input type="text" min="1" value="{{$value->quantity}}" name="quantity[]" id="quantity" class="plus-minus-box_{{$value->Product->product_unitprice}}  quantity">
 <button type="button" min="1" class="dec qtybutton waves-effect" name="quantity[]" onclick="dec({{$value->Product->product_unitprice}})">-</button>
                                                 
                                                </div>
                                            </td>
                                            <td name="price_after_quantity" id="price_after_quantity" class="total_price_{{$value->Product->product_unitprice}}">{{$value->price_after_quantity}} ر.س</td>
                                            <td>
                                                <button type="button" onclick="removeProductFromCart(this, {{$value->product_id}})" class="t-remove">
                                                    <i class="la la-trash"></i>
                                                </button>

                                            </td>
                                        </tr>
                                           @php $count++; @endphp
                                        @endforeach
                                       
                                    </tbody>
                                </table>
                            </div>
                            @if(!$carts->isEmpty())
                            <div class="table-btns">
                                <button type="button" class="btn" onclick="update_cart()">تحديث السلة</button>
                                <button type="button" onclick="removeAllProductFromCart(this)" class="btn btn-red">ازالة السلة</button>
                            </div>
                            @endif
                            <div class="table-extra">
                                <div class="total-list col-md-4 col-xs-12">
                                    <ul>
                                        <li>
                                            الاجمالى
                                            @isset($shop_bill)
                                            <span class="total">{{$shop_bill->order_total_price}} ر.س</span>
                                            @endisset
                                        </li>
                                        <li>
                                            الضريبة
                                            @if($cart_tax)
                                            <span>{{$cart_tax->tax}} % </span>
                                            @else
                                            <span>0</span>
                                            @endif
                                        </li>
                                         <li>
                                            الخصم
                                            @if($shop_bill->coupon_id)
                                            <span>{{$shop_bill->ShopCoupon->coupon_discount}} % </span>
                                            @else
                                            <span>0</span>
                                            @endif
                                        </li>
                                        <li>
                                            الاجمالى النهائي
                                            @isset($shop_bill)
                                            @if($shop_bill->price_after_coupon == 0)
                                            <span>{{$shop_bill->price_after_tax}} ر.س</span>
                                            @else
                                             <span>{{$shop_bill->price_after_coupon}} ر.س</span>
                                            @endif
                                            @endisset
                                        </li>
                                    </ul>
                                    <a href="#" class="btn next-step">استمرار</a>
                                </div>
                                <div class="discount-wrap col-md-4 col-xs-12">
                                    <h4>
                                        <img lazy="loading" src="images/sale.svg" alt="">
                                        خصم / البروموكود
                                    </h4>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="اكتب كود الخصم" required="" name="coupon_id" id="coupon_id">
                                            <input type="hidden" value="{{$shop_bill->price_after_tax}}" name="total_order_price" id="total_order_price">
                                            <button type="button" onclick="apply_discount()"  class="btn">تطبيق</button>
                                        </div>
                                 
                                </div>
                            </div>
                        </div>
                        
                        <div class="tab-pane fade" id="t2">
                            <div class="row">
                                <div class="address-form col-md-8 col-xs-12">
                                    <div class="inner col-xs-12">
                                        <!--<form method="post">-->
                                            @if(!empty($shippment_address))
                                            <div class="form-group col-md-12 col-xs-12">
                                                <h4> اختر عنوان الشحن </h4>
                                                @foreach($shippment_address as $value)
                                                <label>
                                                     <input type="radio" name="shippment_addrress" value="{{$value->id}}" id="shippment_address" class=" col-md-2">
                                                        
                                                <span>{{$value->address_type}} : {{$value->Location->location_name_ar}}- منطقه: {{$value->Region->region_name_ar}} - {{$value->City->city_name_ar}}- {{$value->address_details}}</span> 
                                                    
                                                   </label>
                                                @endforeach 
                                            </div>
                                            @else
                                            <form action="{{route('store-shippment-address')}}" method="post">
                                                @csrf
                                                <div class="form-group col-md-6 col-xs-12 @error('address_type') is-invalid @enderror">
                                                    <h4>{{__('translate.title-classification')}}</h4>
                                                    <select name="address_type" class="form-control nice-select" required="">
                                                        <option @if (old('address_type') == 'home') selected="selected" @endif value="home">السكن </option>
                                                        <option @if (old('address_type') == 'work') selected="selected" @endif value="work">العمل </option>
                                                    </select>
                                                    @error('address_type')
                                                      <div class="error-area">
                                                        <p>{{$message}}</p>
                                                      </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-6 col-xs-12">
                                                    <h4>{{__('translate.region')}}</h4>
                                                    <select name="city_id" class="form-control nice-select">
                                                        <option value="">--- {{__('translate.choose')}} {{__('translate.region')}} ---</option>
                                                      @php  $citys=\App\Models\City::pluck('city_name_ar', 'id'); @endphp
                                                            @foreach ($citys as $key => $value)
                                                                <option  @if (old('city_id') == $key) selected="selected" @endif value="{{ $key }}">{{ $value }}</option>
                                                            @endforeach
                                                    </select>
                                                       @error('city_id')
                                                      <div class="error-area">
                                                        <p>{{$message}}</p>
                                                      </div>
                                                    @enderror
                                                </div>
                                                  <div class="form-group col-md-6 col-xs-12">
                                                    <h4>{{__('translate.city')}}</h4>
                                                    <select name="region_id" class="form-control nice-select">
                                                         <option value="">--- {{__('translate.choose')}} {{__('translate.city')}} ---</option>

                                                    </select>
                                                       @error('region_id')
                                                      <div class="error-area">
                                                        <p>{{$message}}</p>
                                                      </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-6 col-xs-12">
                                                    <h4>{{__('translate.location')}}</h4>
                                                    <select name="location_id" class="form-control nice-select">
                                                        <option value="">--- {{__('translate.choose')}} {{__('translate.location')}} ---</option>

                                                    </select>
                                                       @error('location_id')
                                                      <div class="error-area">
                                                        <p>{{$message}}</p>
                                                      </div>
                                                    @enderror
                                                </div>
                                              
                                                <div class="form-group col-md-6 col-xs-12">
                                                    <h4>{{__('translate.phone')}}</h4>
                                                    <input type="text" name="phone" value="{{old('phone')}}" class="form-control" placeholder="{{__('translate.phone')}}">
                                                       @error('phone')
                                                      <div class="error-area">
                                                        <p>{{$message}}</p>
                                                      </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-6 col-xs-12">
                                                    <h4>{{__('translate.rest-address')}}</h4>
                                                    <input type="text" name="address_details" class="form-control" placeholder="{{__('translate.rest-address')}}">
                                                </div>
                                                <div class="form-group col-md-12 col-xs-12">
                                                    <button type="submit" class="btn">{{__('translate.add')}}</button>
                                                </div>
                                            </form>
                                            @endif
<!-- 
                                            <div class="form-group col-md-6 col-xs-12">
                                                <h4>  <i>*</i> الاسم الأول</h4>
                                                <input type="text" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6 col-xs-12">
                                                <h4>  <i>*</i> الاسم الاخير</h4>
                                                <input type="text" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6 col-xs-12">
                                                <h4>  <i>*</i> المدينة</h4>
                                                <select class="form-control nice-select">
                                                    <option>المدينة</option>
                                                    <option>المدينة</option>
                                                    <option>المدينة</option>
                                                    <option>المدينة</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6 col-xs-12">
                                                <h4>  <i>*</i> المحافظة</h4>
                                                <select class="form-control nice-select">
                                                    <option>المحافظة</option>
                                                    <option>المحافظة</option>
                                                    <option>المحافظة</option>
                                                    <option>المحافظة</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6 col-xs-12">
                                                <h4>  <i>*</i> الدولة</h4>
                                                <select class="form-control nice-select">
                                                    <option>الدولة</option>
                                                    <option>الدولة</option>
                                                    <option>الدولة</option>
                                                    <option>الدولة</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6 col-xs-12">
                                                <h4>  <i>*</i> الكود البريدى</h4>
                                                <input type="text" class="form-control">
                                            </div>
                                            <div class="form-group col-md-12 col-xs-12">
                                                <h4>  <i>*</i> العنوان بالكامل</h4>
                                                <input type="text" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6 col-xs-12">
                                                <h4>  <i>*</i> رقم الهاتف</h4>
                                                <input type="text" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6 col-xs-12">
                                                <h4> البريد الالكترونى</h4>
                                                <input type="email" class="form-control">
                                            </div> -->
                                            <div class="form-group col-md-12 col-xs-12">
                                                <label>
                                                    <input type="radio" id="bill_address" name="bill_address" value="1">
                                                    <span>.عنوان الفواتير هو نفس عنوان الشحن</span>
                                                </label>
                                                 <label>
                                                    <input type="radio" id="bill_address" name="bill_address" value="2">
                                                    <span>. العنوان الجديد</</span>
                                                </label>
                                                <h4>ادخل العنوان الجديد</h4>
                                                <input type="text" name="new_address" id="new_address" class="form-control">
                                            </div>
                                            <div class="form-group col-md-12 col-xs-12">
                                                <div class="ship-address">
                                                    <h5>اختر وضع التسليم</h5>
                                                    <ul>
                                                        <li>
                                                            <label>
                                                                <input type="radio" id="receive_type" name="receive_type" value="1">
                                                                <span>
                                                                    <img lazy="loading" src="images/truck.svg" alt="">
                                                                    <div>
                                                                        <span>التوصيل العادي <b>(مجانًا)</b></span>
                                                                        <p>سنرسل رسالة تحتوي على تفاصيل الشحن بعد إرسال الطلب</p>
                                                                    </div>
                                                                </span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" id="receive_type" name="receive_type" value="2">
                                                                <span>
                                                                    <img lazy="loading" src="images/home.svg" alt="">
                                                                    <div>
                                                                        <span>الاستلام من المتجر <a href="#"> احصل على الاتجاهات</a></span>
                                                                        <p>سوف نتصل لترتيب موعد بعد أن تصبح السلعة (العناصر) جاهزة للاستلام.</p>
                                                                    </div>
                                                                </span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        <button type="button" class="btn next-step" onclick="update_bill()">تأكيد</button>

                                        <!--</form>-->
                                    </div>

<!--                                     <button type="button" class="btn next-step">تأكيد</button>
 -->                                </div>
                                <div class="prods-list col-md-4 col-xs-12">
                                    <h4>المنتجات</h4>
                                    <ul class="p-list">
                                        @foreach($carts as $key => $value)
                                        <li>
                                         <img lazy="loading" src="{{URL('/public/images/products/'.$value->Product->ProductImage[0]->product_image)}}" alt="{{$value->Product->product_name_ar}}">
                                            <div>
                                                <span>@if(app()->getLocale() == 'ar') {{$value->Product->product_name_ar}} @elseif(app()->getLocale() == 'en'){{$value->Product->product_name_en}}@endif</span>
                                                <b>{{$value->Product->product_unitprice}} ر.س</b>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                    <ul class="ex-list">
                                        <li>
                                            الاجمالى
                                            @isset($shop_bill)
                                            <span class="total">{{$shop_bill->order_total_price}} ر.س</span>
                                            @endisset                                       
                                        </li>
                                       <!--  <li>
                                            التوصيل
                                            <span>120 ر.س</span>
                                        </li> -->
                                        <li>
                                            الضريبة
                                            @if($cart_tax)
                                            <span>{{$cart_tax->tax}} % </span>
                                            @else
                                            <span>0</span>
                                            @endif                                        
                                        </li>
                                        <li>
                                            الخصم
                                            @if($shop_bill->coupon_id)
                                            <span>{{$shop_bill->ShopCoupon->coupon_discount}} % </span>
                                            @else
                                            <span>0</span>
                                            @endif
                                        </li>
                                        <li>
                                            الاجمالى النهائي
                                            @isset($shop_bill)
                                            @if($shop_bill->price_after_coupon == 0)
                                            <span>{{$shop_bill->price_after_tax}} ر.س</span>
                                            @else
                                             <span>{{$shop_bill->price_after_coupon}} ر.س</span>
                                            @endif
                                            @endisset
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tab-pane fade" id="t3">
                            <div class="row">
                                <div class="address-form col-md-8 col-xs-12">
                                    <div class="pay-inner col-xs-12">
                                        <div class="addres-ship">
                                            <h4>عنوان الشحن</h4>
                                            <div>
                                                <span>{{Auth::guard('customer')->user()->name}}</span>
                                                @if($shop_bill->new_address != null)
                                                  <span>{{$shop_bill->new_address}} </span>
                                                @else
                                                @if($shop_bill->ShippmentAddress != null)
                                                 <span>{{$shop_bill->ShippmentAddress->Location->location_name_ar}}- منطقه: {{$shop_bill->ShippmentAddress->Region->region_name_ar}} - {{$shop_bill->ShippmentAddress->City->city_name_ar}}- {{$shop_bill->ShippmentAddress->address_details}}</span> 
                                                @endif
                                                @endif
                                            </div>
                                            <a href="#" style="display:none;">
                                                <i class="la la-edit"></i>
                                                تعديل العنوان
                                            </a>
                                        </div>
                                        <div class="pay-meth">
                                            <h3>إختيار طريقة الدفع</h3>
                                            <ul>
                                            <form method="post" action="{{route('site.choose_payment_method')}}">
                                                @csrf
                                                <li>
                                                    <label>
                                                        <input type="radio" name="payment_method" id="payment_method" value="1">
                                                        <span>
                                                            <img lazy="loading" src="{{URL('public/nature-sound')}}/images/cashe.svg" alt="">
                                                            الدفع عند التوصيل
                                                        </span>
                                                    </label>

                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="payment_method" id="payment_method" value="2">
                                                        <span>
                                                            <img lazy="loading" src="{{URL('public/nature-sound')}}/images/payo.svg" alt="">
                                                            بطاقة الائتمان / الخصم
                                                        </span>
                                                    </label>
                                                    <div class="row" style="display:none;">
                                                        <div class="form-group col-md-6 col-xs-12">
                                                            <h4>الاسم كما هو مكتوب فى البطاقة</h4>
                                                            <input type="text" class="form-control" placeholder="الاسم كما هو مكتوب فى البطاقة">
                                                        </div>
                                                        <div class="form-group col-md-6 col-xs-12">
                                                            <h4><i>*</i>رقم البطاقة</h4>
                                                            <input type="text" class="form-control ccFormatMonitor" placeholder="رقم البطاقة" maxlength='19'>
                                                        </div>
                                                        <div class="form-group col-md-6 col-xs-12">
                                                            <h4> (MM / YY) <i>*</i>انتهاء الصلاحية</h4>
                                                            <input type="text" class="form-control" id="inputExpDate" placeholder="MM / YY" maxlength='7'>
                                                        </div>
                                                        <div class="form-group col-md-6 col-xs-12">
                                                            <h4><i>*</i>CVV</h4>
                                                            <input type="password" class="form-control cvv" placeholder="***" maxlength='3'>
                                                        </div>
                                                    </div>
                                                </li>
                                                 <button type="submit" class="btn" >تأكيد الدفع </button>
                                                </form>
                                            </ul>
                                        </div>
                                    </div>
<!--                                     <button type="button" class="btn" data-toggle="modal" data-target="#reserv_pop">ادفع 200 ر.س</button>
 -->                                 
                                  </div>
                                <div class="prods-list col-md-4 col-xs-12">
                                    <h4>المنتجات</h4>
                                    <ul class="p-list">
                                        @foreach($carts as $key => $value)
                                        <li>
                                         <img lazy="loading" src="{{URL('/public/images/products/'.$value->Product->ProductImage[0]->product_image)}}" alt="{{$value->Product->product_name_ar}}">
                                            <div>
                                                <span>{{$value->Product->product_name_ar}}</span>
                                                <b>{{$value->Product->product_unitprice}} ر.س</b>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                    <ul class="ex-list">
                                        <li>
                                            الاجمالى
                                            @isset($shop_bill)
                                            <span class="total">{{$shop_bill->order_total_price}} ر.س</span>
                                            @endisset                                       
                                        </li>
                                       <!--  <li>
                                            التوصيل
                                            <span>120 ر.س</span>
                                        </li> -->
                                        <li>
                                            الضريبة
                                            @if($cart_tax)
                                            <span>{{$cart_tax->tax}} % </span>
                                            @else
                                            <span>0</span>
                                            @endif                                        
                                        </li>
                                         <li>
                                            الخصم
                                            @if($shop_bill->coupon_id)
                                            <span>{{$shop_bill->ShopCoupon->coupon_discount}} % </span>
                                            @else
                                            <span>0</span>
                                            @endif
                                        </li>
                                        <li>
                                            الاجمالى النهائي
                                            @isset($shop_bill)
                                            @if($shop_bill->price_after_coupon == 0)
                                            <span>{{$shop_bill->price_after_tax}} ر.س</span>
                                            @else
                                             <span>{{$shop_bill->price_after_coupon}} ر.س</span>
                                            @endif
                                            @endisset
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    @endif
                </div>
            </div>
@stop
@section('js')
    <script src="{{URL('public/nature-sound')}}/js/jquery.nice-select.min.js"></script>
    <script type="text/javascript">
    var count=1;
   function inc(id) {
        var ix=id;
        var val = $('.plus-minus-box_' + ix).val();
        var current = (++val);

         $('.total_price_'+ ix).html(current *  ix + ' ر. س');   
   }
 

    function dec(id) {
        var ix=id;
        var val = $('.plus-minus-box_' + ix).val();
        // console.log(val );
        if(val > 1){
        var current = (--val);
        
        $('.total_price_'+ ix).html(current *  ix + ' ر. س');
        }
   }
 


   function update_cart()
   {
            var quantity = [];
            $('.quantity').each(function(){

                    quantity.push($(this).val());   

                    console.log(quantity);

            });
            Finalquantity  = quantity.toString();
            if(quantity.length != 0)
            {
            var price_after_quantity = $('#price_after_quantity').val();
            var product_id = $('#product_id').val();
            $.ajax({
                type: "get",
                url: '{{route('site.update_cart')}}',
                data: {    
                            quantity:Finalquantity,  },         
                dataType: "json",
                success: function(result){
              
                        swal({
                            icon: 'success',
                            title: 'تم تحديث السله '
                        });
                   setTimeout(function() {
                                  window.location.href = ('{{url('/cart')}}');
                    }, 2000); // 2 second
                },
                error: function (result) {
                    swal(
                        'Fail',
                        'لابد من تسجيل الدخول أولا',
                        'error',
                    );
                }
            });
        }




}

 function removeAllProductFromCart(e) {
            $.ajax({

                url: '{{route('site.remove_cart')}}',
                type: 'GET',
                dataType: 'json',
                success: function (result) {
                    if (result == 1) {
                    $('#codeRefer').parent().remove();            
                        swal({
                            icon: 'success',
                            title: 'تم حذف المنتج من المفضله'
                        });
                               setTimeout(function() {
                                  window.location.href = ('{{url('/cart')}}');
       }, 2000); // 2 second

                   var x= 0;
                   $('.heart').text(x);
                    } 
                },
                error: function (result) {
                    swal(
                        'Fail',
                        'يوجد خطأ',
                        'error',
                    );
                }
            });

        }





function update_bill()
   {

        var shippment_address= $('#shippment_address').val();
        console.log('shippment_address' + shippment_address);
        var new_address = $('#new_address').val();
                console.log('new_address' + new_address);
        var payment_method = $('#payment_method').val();

        var bill_address = $('#bill_address').val();
                console.log('bill_address' + bill_address);

        var receive_type = $('#receive_type').val();
        console.log('receive_type' + receive_type);

            $.ajax({
                type: "post",
                url: '{{route('site.update_bill')}}',
                data: {    
                        _token: '{{csrf_token()}}',    shippment_address:shippment_address,  bill_address: bill_address, new_address: new_address , receive_type:receive_type, payment_method:payment_method},         
                dataType: "json",
                success: function(result){
              
                        swal({
                            icon: 'success',
                            title: 'تم تحديث السله '
                        });
               
                },
                error: function (result) {
                    swal(
                        'Fail',
                        'لابد من تسجيل الدخول أولا',
                        'error',
                    );
                }
            });
}





function apply_discount()
   {

        var coupon_id= $('#coupon_id').val();
        console.log('coupon_id' + coupon_id);
        var price_after_coupon = $('#total_order_price').val();
                console.log('price_after_coupon' + price_after_coupon);

            $.ajax({
                type: "post",
                url: '{{route('apply_discount')}}',
                data: {    
                      _token: '{{csrf_token()}}', coupon_id:coupon_id, price_after_coupon: price_after_coupon},         
                dataType: "json",
                success: function(result){
                    if(result == 1)
                    {
                        swal({
                            icon: 'success',
                            title: 'تم تحديث السله '
                        });
                    setTimeout(function() {
                                  window.location.href = ('{{url('/cart')}}');
                    }, 2000); // 2 second
                }
                  if(result == 2)
                    {
                            swal(  'Fail',
                        'لا يوجد كوبون بهذا الاسم',
                        'error',
                        );
                    }
                },
                error: function (result) {
                
                    swal(
                        'Fail',
                        'لابد من تسجيل الدخول أولا',
                        'error',
                    );
                }
            });
    }


        function removeProductFromCart(e, productID) {

            var x = productID;

            console.log(x);
            $.ajax({

                url: '{{route('site.remove_single_cart')}}',
                data:{product_id : x},
                type: 'GET',
                dataType: 'json',
                success: function (result) {
                    if (result == 1) {
                    $('#codeRefer').parent().remove();            
                        swal({
                            icon: 'success',
                            title: 'تم حذف المنتج من السله'
                        });
                   var x= '<?php echo $get_cart_count -1  ?>';
                   $('.heart').text(x);
                    setTimeout(function() {
                                  window.location.href = ('{{url('/cart')}}');
       }, 2000); // 2 second

                    } 
                },
                error: function (result) {
                    swal(
                        'Fail',
                        'يوجد خطأ',
                        'error',
                    );
                }
            });

        }



            $(document).ready(function() {
        $('select[name="city_id"]').on('change', function(e) {
                   e.preventDefault();

            var cityID = $(this).val();
            console.log(cityID);
            if(cityID) {
                $.ajax({
                    url: '{{URL('/')}}/shippment-address/regions/'+cityID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="region_id"]').empty();
                          $('select[name="region_id"]').append('<option value="">--- اختر المدينه ---</option>');
                        $.each(data, function(key, value) {
                            $('select[name="region_id"]').append('<option @if (old('region_id') == '+ key +') selected="selected" @endif value="'+ key +'">'+ value +'</option>');
                        });
                        $('select[name="region_id"]').niceSelect('update');


                    }
                });
            }else{
                $('select[name="region_id"]').empty();
            }
        });


           $('select[name="region_id"]').on('change', function(e) {
                   e.preventDefault();

            var regionID = $(this).val();
            console.log(regionID);
            if(regionID) {
                $.ajax({
                    url: '{{URL('/')}}/shippment-address/locations/'+regionID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="location_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="location_id"]').append('<option @if (old('location_id') == '+ key +') selected="selected" @endif  value="'+ key +'">'+ value +'</option>');
                        });
                        $('select[name="location_id"]').niceSelect('update');


                    }
                });
            }else{
                $('select[name="location_id"]').empty();
            }
        });
    });
    
        </script>
@endsection