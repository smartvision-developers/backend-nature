@extends('site/layout/main')
@section('og')
    <meta property="og:title" content="}" />
    <meta property="og:image" content="{{ $product->product_name }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $product->product_name }}" />
    <meta property="og:url" content="{{url('blogs/'. $product->id) }}" />
    <meta property="og:image" content="{{asset('public/'.$product->ProductImage[0]->product_image)}}" />
    <meta property="og:description" content="{{ $product->product_description }}" />
    <meta property="og:site_name" content="{{URL('/')}}" />

@endsection
@section('content')
<main class="main-content col-xs-12">
            <div class="bread-crumb col-xs-12" style="background-image: url({{URL('/public/nature-sound')}}/images/hero-section/hero.png)">
                <div class="container">
                    <h3> {{__('translate.store')}} </h3>
                    <ul>
                        <li>
                            <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">{{__('translate.home')}}</a>
                        </li>
                        <li> {{__('translate.store')}} </li>
                    </ul>
              
                </div>
            </div>
              @if(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
                @endif
            <div class="product-wrap col-xs-12">
                <div class="container">
                    <div class="single-img col-md-5 col-xs-12">
                        <div class="all">
                            <div class="slider">
                                <div class="owl-carousel owl-theme one">
                                  @foreach($product->ProductImage as $img)
                                    <div class="item-box">
                                       <img  lazy="loading" src="{{URL('/public/images/products/'.$img->product_image)}}" alt="">
                                    </div>
                                  @endforeach
                                </div>
                            </div>
                            <div class="slider-two">
                                <div class="owl-carousel owl-theme two">
                                   @foreach($product->ProductImage as $img)
                                    <div class="item">
                                       <img  lazy="loading" src="{{URL('/public/images/products/'.$img->product_image)}}" alt="">
                                    </div>
                                  @endforeach
                                  
                                </div>
                                <div class="left-t nonl-t">
                                    <i class="la la-angle-left"></i>
                                </div>
                                <div class="right-t">
                                    <i class="la la-angle-right"></i>
                                </div>
                            </div>
                        </div>
                        <div class="sec-share">
                            <span>{{__('translate.share-product')}}</span>
                          <!--   <div>
                                <a href="#" class="telg">
                                    <i class="la la-telegram"></i>
                                </a>
                                <a href="#" class="twit">
                                    <i class="la la-twitter"></i>
                                </a>
                                <a href="#" class="faceb">
                                    <i class="la la-facebook"></i>
                                </a>
                                <a href="#" class="insta">
                                    <i class="la la-instagram"></i>
                                </a>
                            </div> -->
                           <!-- ShareThis BEGIN --><div class="sharethis-inline-share-buttons"></div><!-- ShareThis END -->
                        </div>
                    </div>
                    <div class="single-data col-md-7 col-xs-12">
                        <div class="sec-item">
                            <span>{{__('translate.category')}}       @if(app()->getLocale() == 'ar') {{$product->Category_Product->AnimalType->name_ar}}   @elseif(app()->getLocale() == 'en'){{$product->Category_Product->AnimalType->name_en}}  @endif
                            </span>
                            <b>{{__('translate.product-code')}} : {{$product->product_code}}</b>
                        </div>
                        <div class="sec-info">
                            <h1>{{$product->product_name}}</h1>

                            <div class="Stars" style="--rating: {{$rating}};"></div>
                            <p>
                                :{{__('translate.price')}}
                                <span>{{$product->product_unitprice}} {{__('translate.riyal')}}</span>
                                @if($product->product_oldprice)
                                <span class="old-price">{{ $product->product_oldprice }} {{__('translate.riyal')}}</span>
                                @endif
                            </p>
                            <p class="desc">
                              <?php echo $product->product_description; ?>
                            </p>
                        </div>
                     <input type="hidden" data-id="{{$product->id}}"  value="{{$product->id}}" name="product_id" class="product">
                         <input type="hidden" class="price_after_quantity" value="{{$product->product_unitprice}}" name="price_after_quantity">

                        <div class="sec-actions">
                            <span>:{{__('translate.quantity')}}</span>
                            <div class="number">
                                <a class="inc qtybutton waves-effect">+</a>
                                <input type="text" value="1" min="1" id="" value="quantity" name="quantity" class="plus-minus-box quantity" >
                                <a class="dec qtybutton waves-effect">-</a>
                            </div>
                            @if(Auth::guard('customer')->check())
                         @php
                               $fav=\App\Models\Product_Favourite::where('product_id', $product->id)->where('customer_id',auth()->guard('customer')->id())->first();
                                                    @endphp
                                                    @if(!$fav)

                               <button onclick="addProductToFav(this, {{$product->id}})" type="button" class="add-fav fav_{{$product->id}}">
                                <i class="las la-heart"></i>
                            </button>
                                @else
                                      <button onclick="addProductToFav(this, {{$product->id}})" type="button" class="add-fav fav_{{$product->id}}">
                                <i class="las la-heart" style="color:red;"></i>
                            </button>
                                @endif
                            @else
                                 <button onclick="addProductToFav(this, {{$product->id}})" type="button" class="add-fav fav_{{$product->id}} ">
                                <i class="las la-heart"></i>
                            </button>
                            @endif
                            <b>
                                {{__('translate.available-stock')}}
                                <i class="la la-check-circle"></i>
                            </b>
                               @php
                                      $cart=\App\Models\Cart::where('product_id', $product->id)->where('customer_id',auth()->guard('customer')->id())->first();
                                      @endphp
                                        @if(!$cart)
                            <button type="button" class="btn add-pro cart_{{$product->id}}" onclick="addProductToCart(this, {{$product->id}}, {{$product->product_unitprice}})" >{{__('translate.add-cart')}}</button>
                            @else
                            <button type="button" class="btn add-pro cart_{{$product->id}}" onclick="addProductToCart(this, {{$product->id}}, {{$product->product_unitprice}})" >{{__('translate.del-cart')}}</button>

                            @endif
                        </div>
                    </div>
                    <div class="single-extra col-xs-12">
                        <ul class="nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" data-target="#p_desc"> {{__('translate.product-description')}}</a>
                            </li>
                            <li>
                                <a data-toggle="tab" data-target="#p_rate"> {{__('translate.rating')}}</a>
                            </li>
                             @if($get_shipping_info->shipping_info != null)
                            <li>
                                <a data-toggle="tab" data-target="#p_shippment"> {{__('translate.shipping-info')}}</a>
                            </li>
                            @endif
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="p_desc">
                             <?php echo $product->product_longdescription; ?>

                            </div>
                            <div class="tab-pane fade" id="p_rate">
                                <div class="clinic-reviews">
                                    <div class="rev-side">
                                        <div class="rev-list post-comments">
                                            <ul>
                                                @isset($product->ProductAppraisal)
                                                @foreach($product->ProductAppraisal as $key11=> $val2)
                                                <li>
                                                    <div class="Stars" style="--rating: {{$val2->appraisal_num}}"></div>
                                                    <b>                                                  <?php echo $val2->created_at->toFormattedDateString(); ?>
  {{$val2->Customer->name}} بواسطة
                                                    </b>
                                                    <p>{{$val2->appraisal_content}}</p>
                                                </li>
                                                @endforeach
                                                @endisset
                                            </ul>
                                        </div>
                                            <div class="form-group">
                                 <input type="hidden" id="customer_id" name="customer_id" value="{{Auth::guard('customer')->check()?Auth::guard('customer')->user()->id: ''}}">
                                <input type="hidden" id="product_id" name="product_id" value="{{$product->id}}">

                                                <div>
                                                    <span> {{__('translate.add-rating')}}</span>
                                                    <div id="rateYo"></div>        
                                                     <div class="rating"></div>
                                            </div>
                                            <input type="hidden" name="appraisal_num" id="appraisal_num">

                                                <h4> {{__('translate.add-comment')}}</h4>
                                                <textarea name="appraisal_content" id="appraisal_content" class="form-control" required ></textarea>
                                                <button type="submit" class="btn store_appraisal"> {{__('translate.send')}}</button>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            @isset($get_shipping_info)
                            <div class="tab-pane fade" id="p_shippment">
                               <?php echo $get_shipping_info->shipping_info; ?>
                            </div>
                            @endisset
                        </div>
                    </div>
                    <div class="single-related col-xs-12">
                        <div class="products-s col-xs-12">
                            <div class="g-head col-xs-12">
                                <h3>{{__('translate.related-product')}}</h3>
                            </div>
                            <div class="g-body col-xs-12">
                                <div class="products-slider owl-carousel">
                                    @foreach($get_similar_category as $key2=> $val)
                                    @if($val->Product->status == 1)
                                    <div class="item">
                                        <div class="product-card">
                                            <div class="p-inner">
                                                <div class="p-img">
                                                    <div class="p-actions">
                                                    <input type="hidden" data-id="{{$val->Product->id}}"  value="{{$val->Product->id}}" name="product_id" class="product">
                                                    <input type="hidden" class="price_after_quantity" value="{{$val->Product->product_unitprice}}" name="price_after_quantity">

                                                        <input type="hidden" value="1" class="quantity">
                                                            @php
                                                        $cart=\App\Models\Cart::where('product_id', $val->Product->id)->where('customer_id',auth()->guard('customer')->id())->first();
                                                    @endphp
                                                    @if(!$cart)
                                                         <button class="add-pro cart_{{$val->Product->id}}" type="button" onclick="addProductToCart(this, {{$val->Product->id}}, {{$val->Product->product_unitprice}})" data-tool="tooltip" title="اضف للسلة" data-placement="right">
                                                            <i class="las la-shopping-bag"></i>

                                                        </button>
                                                    @else
                                                       <button class="add-pro cart_{{$val->Product->id}}" type="button" onclick="addProductToCart(this, {{$val->Product->id}}, {{$val->Product->product_unitprice}})" data-tool="tooltip" title="حذف من السله" data-placement="right">
                                                            <i class="las la-trash" style="color:red;"></i>
                                                        </button>
                                                    @endif
                                            

                                              @php
                                                        $fav=\App\Models\Product_Favourite::where('product_id', $val->Product->id)->where('customer_id',auth()->guard('customer')->id())->first();
                                                    @endphp
                                                    @if(!$fav)
                                                          <button type="button" class="add-fav fav_{{$val->Product->id}}" onclick="addProductToFav(this, {{$val->Product->id}})" data-tool="tooltip" title="اضف للمفضلة" data-placement="right">
                                                            <i class="las la-heart"></i>
                                                        </button>
                                                    @else
                                                       <button type="button" class="add-fav fav_{{$val->Product->id}}" onclick="addProductToFav(this, {{$val->Product->id}})" data-tool="tooltip" title="حذف من  لامفضلة" data-placement="right">
                                                            <i class="las la-heart" style="color:red;"></i>
                                                        </button>
                                                    @endif
                                                        <button type="button" data-tool="tooltip" title="عرض سريع" data-placement="right">
                                                            <i class="las la-search"></i>
                                                        </button>
                                                        <button type="button" data-tool="tooltip" title="اضف للمقارنة" data-placement="right">
                                                            <i class="las la-sync"></i>
                                                        </button>
                                                    </div>
                                                    <img  lazy="loading" src="{{URL('/public/images/products/'.$val->Product->ProductImage[0]->product_image)}}" alt="{{$val->Product->product_name_ar}}">
                                                </div>
                                                <div class="p-data">
                                                    <a href="{{URL('/product/'.$val->Product->product_code)}}">
                                                        @if(app()->getLocale() == 'ar') {{$val->Product->product_name_ar}} @elseif(app()->getLocale() == 'en'){{$val->Product->product_name_en}} @endif
                                                    </a>
                                                    <p>
                                                        <span>{{$val->Product->product_unitprice}} {{__('translate.riyal')}}</span>
                                                        @if($val->Product->product_oldprice)
                                                            <span class="old-price">{{ $val->Product->product_oldprice }} {{__('translate.riyal')}}</span>
                                                        @endif
                                                   </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                  
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        
@stop
@section('js')
    <script src="{{URL('public/nature-sound')}}/js/jquery.nice-select.min.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/jquery.rateyo.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
           $("#rateYo").click(function(){
                $("#appraisal_num").val($(".rating").text())

});
  $('.store_appraisal').click(function(event){
            event.preventDefault();
            var appraisal_num = $('#appraisal_num').val();
            var appraisal_content = $('#appraisal_content').val();
            var product_id = $('#product_id').val();
            var customer_id = $('#customer_id').val();
            $.ajax({
                type: "get",
                url: '{{route('site.store_appraisal',$product->id)}}',
                data: {    
                            appraisal_num:appraisal_num,appraisal_content:appraisal_content, product_id:product_id, customer_id:customer_id },         
                dataType: "json",
                success: function(result){
                    $('.post-comments ul').append('<li><div class="Stars" style="--rating:'+appraisal_num+'"></div><b> <?php  if($product->ProductAppraisal == null){echo $val2->created_at->toFormattedDateString(); }?><?php echo Auth::guard('customer')->check()? Auth::guard('customer')->user()->name : ''; ?> بواسطة </b><p>'+appraisal_content+'</p></li>');

                        swal({
                            icon: 'success',
                            title: 'تم إضافه تعليقك بنجاح'
                        });
                  
                },
                error: function (result) {
                    swal(
                        'Fail',
                        'لابد من التسجيل أولا',
                        'error',
                    );
                }
            });
        });
    

</script>
   <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5e695eafa853fd0012447981&product=sop' async='async'></script>


@endsection