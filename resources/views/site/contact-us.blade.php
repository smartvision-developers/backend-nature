@extends('site/layout/main')
<style type="text/css">
    .error-area p
    {
        color: #813970 !important;
    }
</style>
@section('content')

  <main class="main-content col-xs-12">
            <div class="bread-crumb col-xs-12" style="background-image: url({{URL('public/nature-sound/images/hero-section/hero.png')}})">
                <div class="container">
                    <h3>{{__('translate.contact-us')}}</h3>
                    <ul>
                        <li>
                            <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">{{__('translate.home')}}</a>
                        </li>
                        <li>{{__('translate.contact-us')}}</li>
                    </ul>
                </div>
            </div>
            <div class="conto-wrap col-xs-12">
                <div class="conto-infos col-xs-12">
                    <div class="container">
                        <div class="block col-md-4 col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                            <div class="inner">
                                <i class="la la-map-marker"></i>
                                <div>
                                    <h4>{{__('translate.address')}}</h4>
                                    <p>{{$setting->address}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="block col-md-4 col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                            <div class="inner">
                                <i class="las la-phone-volume"></i>
                                <div>
                                    <h4>{{__('translate.phone-no')}}</h4>
                                    <p><a href="tel:{{$setting->phone}}">{{$setting->phone_no}}</a></p>
<!--                                     <p>+971 012 345 6789</p>
 -->                                </div>
                            </div>
                        </div>
                        <div class="block col-md-4 col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="600">
                            <div class="inner">
                                <i class="la la-at"></i>
                                <div>
                                    <h4>{{__('translate.email')}}</h4>
                                    <p>{{$setting->email}}</p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="conto-map col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d927755.0520175649!2d47.383032961783286!3d24.725398084702963!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e2f03890d489399%3A0xba974d1c98e79fd5!2z2KfZhNix2YrYp9i2INin2YTYs9i52YjYr9mK2Kk!5e0!3m2!1sar!2seg!4v1625849055331!5m2!1sar!2seg"></iframe>
                </div>
                <div class="conto-form col-xs-12">
                    <div class="container">
                        <div class="form-inner">
                            <div class="f-img">
                                <img lazy="loading" src="{{URL('public/nature-sound')}}/images/hero-section/hero.png" alt="">
                            </div>
                            <div class="f-inputs">
                                <form action="{{route('site.store_contact')}}" method="POST">
                                    @csrf
                                    <div class="form-group" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                                        <input name="name" value="{{old('name')}}" type="text" class="form-control" placeholder="{{__('translate.name')}}">
                                    @error('name')
                                    <div class="error-area">
                                      <p>{{$message}}</p>
                                    </div>
                                    @enderror

                                    </div>
                                    <div class="form-group" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">
                                        <input name="email" value="{{old('email')}}" type="email" class="form-control" placeholder="{{__('translate.email')}}">
                                           @error('email')
                                    <div class="error-area">
                                      <p>{{$message}}</p>
                                    </div>
                                    @enderror
                                    </div>
                                    <div class="form-group" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                                        <input name="phone" type="text" value="{{old('phone')}}" class="form-control" placeholder="{{__('translate.phone')}}">
                                           @error('phone')
                                    <div class="error-area">
                                      <p>{{$message}}</p>
                                    </div>
                                    @enderror
                                    </div>
                                    <div class="form-group" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="500">
                                        <textarea name="message" class="form-control" placeholder="{{__('translate.message')}}">{{old('message')}}</textarea>
                                       @error('message')
                                    <div class="error-area">
                                      <p>{{$message}}</p>
                                    </div>
                                    @enderror
                                    </div>
                                    <div class="form-group" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="600">
                                        <button type="submit" class="btn">{{__('translate.send')}}</button>

                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="form-social" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                            <a href="{{$setting->facebook_link}}">
                                <i class="la la-facebook"></i>
                            </a>
                            <a href="{{$setting->twitter_link}}">
                                <i class="la la-twitter"></i>
                            </a>
                            <a href="{{$setting->instagram_link}}">
                                <i class="la la-instagram"></i>
                            </a>
                            <a href="{{$setting->youtube_link}}">
                                <i class="la la-youtube"></i>
                            </a>
                            <a href="{{$setting->snapchat_link}}">
                                <i class="la la-snapchat-ghost"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
         
@stop
@section('js')

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@include('sweetalert::alert')

@endsection