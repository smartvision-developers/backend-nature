@extends('site/layout/main')
@section('content')
 <main class="main-content col-xs-12">
            <div class="bread-crumb col-xs-12" style="background-image: url({{URL('public/nature-sound')}}/images/hero-section/hero.png)">
                <div class="container">
                    <h3>{{__('translate.my-account')}}</h3>
                    <ul>
                        <li>
                            <a href="{{asset('/'.LaravelLocalization::getCurrentLocale())}}">{{__('translate.home')}}</a>
                        </li>
                        <li>{{__('translate.my-account')}}</li>
                    </ul>
                </div>
            </div>
            <div class="profile-wrap col-xs-12">
                <div class="container">
                    <a href="javascript:void(0)" class="op-sidebar">
                        <img lazy="loading" src="{{URL('/').Auth::guard('customer')->user()->photo}}" alt="{{Auth::guard('customer')->user()->name}}">
                    </a>
                    <div class="prof-sidebar col-md-3 col-xs-12">
                        <div class="prof-top">
                            <button type="button" class="clo-sidebar">
                                <i class="la la-close"></i>
                            </button>
                            <div class="s-img">
                        <img lazy="loading" src="{{URL('/')}}/{{Auth::guard('customer')->user()->photo}}" alt="{{Auth::guard('customer')->user()->name}}" id="blah1">

                          <form enctype="multipart/form-data" action="{{route('update-photo',  Auth::guard('customer')->user()->id)}}" method="post">
                                @csrf

                                <label>

                                    <input id="photo" type="file" name="photo" onchange="document.getElementById('blah1').src = window.URL.createObjectURL(this.files[0])">
                                    <i class="la la-camera"></i>
                                </label>
                                <button type="submit" class="i-btn">
                                    <i class="la la-check"></i>
                                </button>
                                <button type="button" class="i-btn i-red">
                                    <i class="la la-close"></i>
                                </button>
                            </form>
                            </div>
                            <h3>{{Auth::guard('customer')->user()->name }} </h3>
                            <p>{{Auth::guard('customer')->user()->email }}</p>
                        </div>
                        <ul>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/profile')}}">{{__('translate.profile')}}</a>
                            </li>
                          <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/my-orders')}}">مشترياتى</a>
                            </li>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/my-reservations')}}">حجوزاتى</a>
                            </li>
                            <li class="active">
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/shippment-address')}}">{{__('translate.shipping-addresses')}}</a>
                            </li>
                            <li>
                                <a href="{{asset('/'.LaravelLocalization::getCurrentLocale().'/logout')}}">{{__('translate.logout')}}</a>
                            </li>
                        </ul>
                    </div>
                  <div class="prof-content col-md-9 col-sm-12 col-xs-12">
                        <div class="prof-head col-xs-12">
                            <h4>{{__('translate.shipping-addresses')}}</h4>
                        </div>
                        <div class="prof-body col-xs-12">
                            <div class="shipp-adress">
                                <div class="old-address">
                                    @foreach($shippment_address as $value)
                                    <p>{{$value->address_type}} : {{$value->Location->location_name_ar}}- منطقه: {{$value->Region->region_name_ar}} - {{$value->City->city_name_ar}}- {{$value->address_details}} \ رقم الهاتف: 
                                    {{$value->phone}}</p>
                                    @endforeach
                                </div>
                                <div class="new-address">
                                    <a href="javascript:void(0)" class="btn op-new-address">{{__('translate.add-title')}}</a>
                                    <div class="new-form">
                                        <div class="row">
                                             <div class="alert alert-danger print-error-login form-group" style="display: none;">
                                <ul></ul>
                            </div>
                                            <form action="{{route('store-shippment-address')}}" method="post" validate>
                                                @csrf
                                                <div class="form-group col-md-6 col-xs-12 @error('address_type') is-invalid @enderror">
                                                    <h4>{{__('translate.title-classification')}}</h4>
                                                    <select name="address_type" class="form-control nice-select" required="">
                                                        <option @if (old('address_type') == 'home') selected="selected" @endif value="home">السكن </option>
                                                        <option @if (old('address_type') == 'work') selected="selected" @endif value="work">العمل </option>
                                                    </select>
                                                    @error('address_type')
                                                      <div class="error-area">
                                                        <p>{{$message}}</p>
                                                      </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-6 col-xs-12">
                                                    <h4>{{__('translate.region')}}</h4>
                                                    <select name="city_id" class="form-control nice-select" >
                                                        <option value="">--- {{__('translate.choose')}} {{__('translate.region')}} ---</option>
                                                            @foreach ($citys as $key => $value)
                                                                <option  @if (old('city_id') == $key) selected="selected" @endif value="{{ $key }}">{{ $value }}</option>
                                                            @endforeach
                                                    </select>
                                                       @error('city_id')
                                                      <div class="error-area">
                                                        <p>{{$message}}</p>
                                                      </div>
                                                    @enderror
                                                </div>
                                                  <div class="form-group col-md-6 col-xs-12">
                                                    <h4>{{__('translate.city')}}</h4>
                                                    <select name="region_id" class="form-control nice-select">
                                                         <option value="">--- {{__('translate.choose')}} {{__('translate.city')}} ---</option>

                                                    </select>
                                                       @error('region_id')
                                                      <div class="error-area">
                                                        <p>{{$message}}</p>
                                                      </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-6 col-xs-12">
                                                    <h4>{{__('translate.location')}}</h4>
                                                    <select name="location_id" class="form-control nice-select">
                                                        <option value="">--- {{__('translate.choose')}} {{__('translate.location')}} ---</option>

                                                    </select>
                                                       @error('location_id')
                                                      <div class="error-area">
                                                        <p>{{$message}}</p>
                                                      </div>
                                                    @enderror
                                                </div>
                                              
                                                <div class="form-group col-md-6 col-xs-12">
                                                    <h4>{{__('translate.phone')}}</h4>
                                                    <input type="text" name="phone" value="{{old('phone')}}" class="form-control" placeholder="{{__('translate.phone')}}">
                                                       @error('phone')
                                                      <div class="error-area">
                                                        <p>{{$message}}</p>
                                                      </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-6 col-xs-12">
                                                    <h4>{{__('translate.rest-address')}}</h4>
                                                    <input type="text" name="address_details" class="form-control" placeholder="{{__('translate.rest-address')}}">
                                                </div>
                                                <div class="form-group col-md-12 col-xs-12">
                                                    <button type="submit" class="btn">{{__('translate.add')}}</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
@endsection

@section('js')
    <script src="{{URL('public/nature-sound')}}/js/jquery.fancybox.min.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/owl.carousel.min.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/jquery.nice-select.min.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/aos.js"></script>
    <script src="{{URL('public/nature-sound')}}/js/intlTelInput.min.js"></script>

   <script>
        $('.edit__').click(function (){
            $('.personal-data').hide();
            $('.edit-personal').show();
        });
        
        $('.op-sidebar').click(function (){
            $('.prof-sidebar').addClass('active');
            $('html').addClass('off');
        });
        
        $('.clo-sidebar').click(function (){
            $('.prof-sidebar').removeClass('active');
            $('html').removeClass('off');
        });
        
        $('.s-img label').click(function (){
            $('.i-btn').fadeIn(1000);
            $(this).fadeOut(1000);
        });
        
        const input = document.querySelector("#phone");
        intlTelInput(input, {
            separateDialCode: true
        });
        
        $('.op-order-dets').click(function (){
            $(this).parent().next('.dets-table').stop().slideToggle();
        });
        
        $('.op-new-address').click(function (){
            $('.new-form').slideToggle();
        });



            $(document).ready(function() {
        $('select[name="city_id"]').on('change', function(e) {
                   e.preventDefault();

            var cityID = $(this).val();
            console.log(cityID);
            if(cityID) {
                $.ajax({
                    url: '{{URL('/')}}/shippment-address/regions/'+cityID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="region_id"]').empty();
                          $('select[name="region_id"]').append('<option value="">--- اختر المدينه ---</option>');
                        $.each(data, function(key, value) {
                            $('select[name="region_id"]').append('<option @if (old('region_id') == '+ key +') selected="selected" @endif value="'+ key +'">'+ value +'</option>');
                        });
                        $('select[name="region_id"]').niceSelect('update');


                    }
                });
            }else{
                $('select[name="region_id"]').empty();
            }
        });


           $('select[name="region_id"]').on('change', function(e) {
                   e.preventDefault();

            var regionID = $(this).val();
            console.log(regionID);
            if(regionID) {
                $.ajax({
                    url: '{{URL('/')}}/shippment-address/locations/'+regionID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="location_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="location_id"]').append('<option @if (old('location_id') == '+ key +') selected="selected" @endif  value="'+ key +'">'+ value +'</option>');
                        });
                        $('select[name="location_id"]').niceSelect('update');


                    }
                });
            }else{
                $('select[name="location_id"]').empty();
            }
        });
    });
    </script>

    @endsection