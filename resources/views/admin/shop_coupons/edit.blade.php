@extends('admin.layouts.master')
@section('css')
<!--- Internal Select2 css-->
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
<style type="text/css">
  form
  {
    margin-top: 3rem !important;
  }
  .main-content-label
  {
     font-size:large !important;
  }
  .btn-icon {
     display: initial !important;
  }
</style>
@endsection
@section('page-header')
        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
          <div class="my-auto">
            <div class="d-flex">
              <h4 class="content-title mb-0 my-auto">كوبونات الخصم</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ تعديل بيانات الكوبون</span>
            </div>
          </div>
          <div class="d-flex my-xl-auto right-content">
            <div class="pr-1 mb-3 mb-xl-0">
              <a href="{{URL('admin/shop_coupons')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i> عرض كوبونات الخصم</a>
            </div>
          
          </div>
        </div>
        <!-- breadcrumb -->
@endsection
@section('content')

        <!-- row -->
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="card">
              <div class="card-body">
                <div class="main-content-label mg-b-5">
                  تعديل بيانات  / {{$shop_coupon->coupon_name}}
                </div>
                <hr>
                 @if(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
                @endif
                 <form method="POST" class="parsley-style-1" id="selectForm2" name="selectForm2" enctype="multipart/form-data" role="form" action="{{route('shop_coupons.update', $shop_coupon->id)}}">
                    @csrf
                  {{ method_field('PUT') }}

                  <div class="">
                    <div class="row mg-b-20">
                      <div class="parsley-input col-md-5" id="coupon_code">
                        <label>كود الخصم: <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#coupon_code" name="coupon_code" value="{{$shop_coupon->coupon_code}}" required="" type="text">
                           @error('coupon_code')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="parsley-input col-md-5 mg-t-20 mg-md-t-0" id="coupon_name">
                        <label>اسم الكود الخاص بالخصم: <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#coupon_name" value="{{$shop_coupon->coupon_name}}" name="coupon_name" required="" type="text">
                           @error('coupon_name')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="parsley-input col-md-2 mg-t-20 mg-md-t-0" id="coupon_discount">
                        <label>نسبه الخصم: <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#coupon_discount" value="{{$shop_coupon->coupon_discount}}" name="coupon_discount"  required="" type="number"> %
                           @error('coupon_discount')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>

                    </div>
                  </div>
                          

                <div class="mg-t-30">
                    <button class="btn btn-main-primary pd-x-20" type="submit">حفظ</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- row closed -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
@endsection
@section('js')
<!--Internal  Select2 js -->
<script src="{{URL::asset('public/assets/plugins/select2/js/select2.min.js')}}"></script>
<!--Internal  Parsley.min js -->
<script src="{{URL::asset('public/assets/plugins/parsleyjs/parsley.min.js')}}"></script>
<!-- Internal Form-validation js -->
<script src="{{URL::asset('public/assets/js/form-validation.js')}}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){

    $("#show").click(function(){
    $(".hide").show();
  });
  });
</script>
@endsection