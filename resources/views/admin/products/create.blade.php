@extends('admin.layouts.master')
@section('css')
<!--- Internal Select2 css-->
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('public/assets/plugins/fancyuploder/fancy_fileupload.css')}}" rel="stylesheet" />
<link href="{{URL::asset('public/assets/plugins/quill/quill.snow.css')}}" rel="stylesheet">

<style type="text/css">
form
{
  margin-top: 3rem !important;
}
.main-content-label
{
 font-size:large !important;
}
.btn-icon {
 display: initial !important;
}
.bg-gray-100 {
  background-color: #fbfbfb47 !important;
}
hr {
    margin-top: 4rem !important;
    margin-bottom: 4rem !important;
    }

.select2
{
      width: 595px !important;
/*      margin-bottom: 20px !important;
*/}
}
</style>
@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="breadcrumb-header justify-content-between">
  <div class="my-auto">
    <div class="d-flex">
      <h4 class="content-title mb-0 my-auto">المنتجات</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ إضافه منتج جديد</span>
    </div>
  </div>
  <div class="d-flex my-xl-auto right-content">
    <div class="pr-1 mb-3 mb-xl-0">
      <a href="{{URL('admin/products')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i> عرض المنتجات</a>
    </div>

  </div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')

<!-- row -->
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="card">
      <div class="card-body">
        <div class="main-content-label mg-b-5">
          إضافه قسم جديد
        </div>
        <hr>
        @if(Session::has('success'))
        <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
        @endif
        <form action="{{route('products.store')}}" enctype="multipart/form-data" class="parsley-style-1" id="selectForm2" name="selectForm2"  method="POST">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="">
            <div class="wd-250" id="cbErrorContainer2"></div>
            <div class="row mg-b-20">

            <div class="parsley-select wd-250 mg-t-30 col-sm-6" id="slWrapper2">
             <label>اختر  القسم الأساسي: <span class="tx-danger">*</span></label>
              <select name="category_id" class="form-control select2" data-parsley-class-handler="#slWrapper2" data-placeholder="اختر قسم المنتج" required="" data-url="{{url('animal-select')}}" id="category_id" >
                <option value="">اختر قسم المنتج</option>
                @foreach($categorys as $category)
                <option value="{{$category->id}}">{{$category->category_name_ar}}</option>
                @endforeach
              </select>
              <div id="slErrorContainer2"></div>
            </div>



            <div class="parsley-select wd-250 mg-t-30 col-sm-6">
             <label>المنتج حصري أم لا : <span class="tx-danger"></span></label>
              <input type="checkbox" class="form-control ckbox" name="exclusive" style="width: 10%;">
            </div>


          </div>
            <div class="parsley-select wd-250 mg-t-30">
             <label>اختر  نوع الحيوان: <span class="tx-danger">*</span></label>
          <?php  $x=1 ?>
              <select name="animal_id" class="form-control select2" data-parsley-class-handler="#animal_id" data-placeholder="اختر قسم المنتج" required="" id="animal_id">
                <option value="">اختر نوع الحيوان</option>
                @foreach($animals_cat as $animal)

                <option value="{{$animal->AnimalType->id}}" >{{$animal->AnimalType->name_ar}}</option>
                @endforeach
              </select>
              <div id="slErrorContainer2"></div>
            </div>
               <div class="parsley-input wd-250 mg-t-20 mg-md-t-0" style="position:relative; top:-68px; right:51%" id="product_code">
                <label>كود المنتج: <span class="tx-danger">*</span></label>
                <input class="form-control" data-parsley-class-handler="#product_code" name="product_code" min="1" value="{{old('product_code')}}" placeholder="ادخل ا كود المنتج" required="" type="number">
                @error('product_code')
                <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                @enderror
              </div>
            <div class="row mg-b-20">
              <div class="parsley-input col-md-6" id="product_name_ar">
                <label>اسم القسم باللغه العربيه: <span class="tx-danger">*</span></label>
                <input class="form-control" data-parsley-class-handler="#product_name_ar" name="product_name_ar" value="{{old('product_name_ar')}}" placeholder="ادخل اسم القسم باللغه العربيه" required="" type="text">
                @error('product_name_ar')
                <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                @enderror
              </div>
              <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="product_name_en">
                <label>اسم القسم باللغه الانجليزيه: <span class="tx-danger">*</span></label>
                <input class="form-control" data-parsley-class-handler="#product_name_en" name="product_name_en" value="{{old('product_name_en')}}" placeholder="ادخل اسم القسم باللغه الانجليزيه" required="" type="text">
                @error('product_name_en')
                <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                @enderror
              </div>
            </div>
          </div>
        
          <div class="">
            <div class="row mg-b-20">
                 <div class="parsley-input col-md-3" id="product_oldprice">
                <label>السعر القديم : <span class="tx-danger"></span></label>
                <input class="form-control"  name="product_oldprice" value="{{old('product_oldprice')}}" min="1" placeholder="السعر القديم " required="" type="number">
                @error('product_oldprice')
                <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                @enderror
              </div>
              <div class="parsley-input col-md-3" id="product_unitprice">
                <label>سعر المنتج الحالي: <span class="tx-danger">*</span></label>
                <input class="form-control" data-parsley-class-handler="#product_unitprice" name="product_unitprice" value="{{old('product_unitprice')}}" min="1" placeholder="سعر المنتج الحالي" required="" type="number">
                @error('product_unitprice')
                <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                @enderror
              </div>
              
              <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="product_quantity">
                <label>الكميه المتاحه للمنتج: <span class="tx-danger">*</span></label>
                <input class="form-control" data-parsley-class-handler="#product_quantity" name="product_quantity" min="1" value="{{old('product_quantity')}}" placeholder="ادخل اسم القسم باللغه الانجليزيه" required="" type="number">
                @error('product_quantity')
                <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                @enderror
              </div>
            </div>
          </div>
        <hr>

          <div class="parsley-input row mb-4" id="product_image">
            <div class="col-sm-12 col-md-12">
               <label>صور المنتج: <span class="tx-danger">*</span></label>

              <input type="file" data-parsley-class-handler="#product_image" name="product_image[]" required="" accept=".jpg, .png, image/jpeg, image/png" multiple="multiple">
            </div>
          </div>
                  <hr>

                <!--   <p class="mg-b-10">What is your favorite browser? <span class="tx-danger">*</span></p>
                  <div class="parsley-checkbox wd-250 mg-b-0" id="cbWrapper2">
                    <label class="ckbox mg-b-5">
                      <input data-parsley-class-handler="#cbWrapper2" data-parsley-errors-container="#cbErrorContainer2" data-parsley-mincheck="2" name="browser[]" required="" type="checkbox" value="1">
                      <span>Firefox</span>
                    </label>
                    <label class="ckbox mg-b-5"><input name="browser[]" type="checkbox" value="2"><span>Chrome</span></label>
                    <label class="ckbox mg-b-5"><input name="browser[]" type="checkbox" value="3"><span>Safari</span></label>
                    <label class="ckbox"><input name="browser[]" type="checkbox" value="4"><span>Edge</span></label>
                  </div>
                  parsley-checkbox -->
<!--                   <div class="wd-250" id="cbErrorContainer2"></div>
                  <div class="parsley-select wd-250 mg-t-30" id="slWrapper2">
                    <select class="form-control select2" data-parsley-class-handler="#slWrapper2" data-parsley-errors-container="#slErrorContainer2" data-placeholder="Choose one" required="">
                      <option label="Choose one">
                      </option>
                      <option value="Firefox">
                        Firefox
                      </option>
                      <option value="Chrome">
                        Chrome
                      </option>
                      <option value="Safari">
                        Safari
                      </option>
                      <option value="Opera">
                        Opera
                      </option>
                      <option value="Internet Explorer">
                        Internet Explorer
                      </option>
                    </select>
                    <div id="slErrorContainer2"></div>
                  </div> -->
                  <div style="height: 270px;">
                    <div class=" row mg-b-20">
                      <div class="parsley-input col-sm-6" id="product_description_ar">
                        <label>وصف بسيط المنتج باللغه العربيه: <span class="tx-danger">*</span></label>

                        <textarea data-parsley-class-handler="#product_description_ar" class="ql-wrapper ql-wrapper-demo bg-gray-100" id="quillEditor" name="product_description_ar" required="">
                        </textarea>


                        @error('product_description_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="parsley-input col-sm-6" id="product_description_en">
                        <label>وصف بسيط المنتج باللغه الانجليزيه: <span class="tx-danger">*</span></label>

                        <textarea data-parsley-class-handler="#product_description_en" name="product_description_en" class="ql-wrapper ql-wrapper-demo bg-gray-100" id="editor" required="">
                        </textarea>
                        @error('product_description_en')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>
        <hr>

                <div style="height: 270px;">
                    <div class=" row mg-b-20">
                      <div class="parsley-input col-sm-6" id="product_longdescription_ar">
                        <label>وصف مفصل المنتج باللغه العربيه: <span class="tx-danger">*</span></label>

                        <textarea data-parsley-class-handler="#product_longdescription_ar" required="" class="ql-wrapper ql-wrapper-demo bg-gray-100" id="product_longdescription_ar1" name="product_longdescription_ar">
                        </textarea>


                        @error('product_description_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="parsley-input col-sm-6" id="product_longdescription_en">
                        <label>وصف مفصل المنتج باللغه الانجليزيه: <span class="tx-danger">*</span></label>

                        <textarea data-parsley-class-handler="#product_longdescription_en" required="" name="product_longdescription_en" class="ql-wrapper ql-wrapper-demo bg-gray-100" id="product_longdescription_en1">
                        </textarea>
                        @error('product_description_en')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>

        <hr>

                  <div class="mg-t-30">
                    <button class="btn btn-main-primary pd-x-20" type="submit">إضافه</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- row closed -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
    @endsection
    @section('js')
    <!--Internal  Select2 js -->
    <script src="{{URL::asset('public/assets/plugins/select2/js/select2.min.js')}}"></script>
    <!--Internal  Parsley.min js -->
    <script src="{{URL::asset('public/assets/plugins/parsleyjs/parsley.min.js')}}"></script>
    <!-- Internal Form-validation js -->
    <script src="{{URL::asset('public/assets/js/form-validation.js')}}"></script>

    <script src="{{URL::asset('public/assets/plugins/fileuploads/js/fileupload.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fileuploads/js/file-upload.js')}}"></script>
    <!--Internal Fancy uploader js-->
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.ui.widget.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.fileupload.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.iframe-transport.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.fancy-fileupload.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/fancy-uploader.js')}}"></script>

    <script src="{{URL::asset('public/assets/plugins/quill/quill.min.js')}}"></script>

    <script src="{{URL::asset('public/assets/js/form-editor.js')}}"></script>

    <script src="//cdn.quilljs.com/1.3.6/quill.js"></script>
    <script>
      var quill = new Quill('#editor', {
  theme: 'snow'   // Specify theme in configuration
});

        var quill = new Quill('#product_longdescription_ar1', {
  theme: 'snow'   // Specify theme in configuration
});

          var quill = new Quill('#product_longdescription_en1', {
  theme: 'snow'   // Specify theme in configuration
});
</script>    
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript">
  $("#animal_id option").hide();
  $(document).on("change","#category_id",function() {
    var value=$(this).val();
        x=value;
    $("#animal_id option").hide();    
    $("#data").attr('value', value);
        const method = 'GET';
            $.ajax({
                url: '{{route('admin.getAnimals')}}',
                type: 'GET',
                _method: method,
                data: {
                value: value,
                }, success: function (data) {
                  //console.log(data);
                    $('#animal_id').html(data);
                },
            });



      $("#animal_id option").show();

  });
</script>


@endsection