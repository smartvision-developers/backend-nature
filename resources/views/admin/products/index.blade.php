@extends('admin.layouts.master')
@section('css')
<style type="text/css">
.buttons-pdf , .buttons-excel
{
  display: none !important;
}

.main-content-label
{
 font-size:large !important;
}
.btn-icon {
 display: initial !important;
}
.chk-option .checkbox-fade
{
  display: inline-flex !important;
}
.pro-img-box img {
  height: 200px !important;
}
.ckbox{
  padding-right: 25px !important;
}
</style>
@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="breadcrumb-header justify-content-between">
  <div class="my-auto">
    <div class="d-flex">
      <h4 class="content-title mb-0 my-auto">المنتجات</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ عرض كل المنتجات</span>
    </div>
  </div>
  <div class="d-flex my-xl-auto right-content">
    @permission(['create-products'])
    <div class="pr-1 mb-3 mb-xl-0">
      <a href="{{URL('admin/products/create')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i>إضافه منتج</a>
    </div>
@endpermission
  </div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')
<!-- row -->
@if(Session::has('success'))
<div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
@endif

<div class="row row-sm">
  <div class="col-xl-12 col-lg-12 col-md-12">
    <div class="row row-sm">

      @foreach($products as $data)

      <div class="col-md-6 col-lg-6 col-xl-4  col-sm-6">
        <div class="card">
          <div class="card-body">
            <div class="pro-img-box">
              @permission(['read-products'])
              <a href="{{url('admin/products/'.$data->id.'/profile')}}"><img class="w-100" src="{{URL('/public/images/products/'.$data->ProductImage[0]->product_image)}}" alt="{{$data->product_name_ar}}"></a>
              @endpermission
            </div>
            <div class="text-center pt-3">
          @permission(['read-products'])
              <a href="{{url('admin/products/'.$data->id.'/profile')}}"><h3 class="h6 mb-2 mt-4 font-weight-bold text-uppercase">{{$data->product_name_ar}}</h3></a>
          @endpermission
              <?php 
              $total_user_rate=\App\Models\ProductAppraisal::where('product_id',$data->id)->count();
              if($total_user_rate > 0)
              {
                $sum_of_rating=\App\Models\ProductAppraisal::where('product_id',$data->id)->sum('appraisal_num'); 

                $max_rate=\App\Models\ProductAppraisal::where('product_id',$data->id)->max('appraisal_num');
               

                if($max_rate > 0)
                {

                 $rating= round(($sum_of_rating* 5) / ($total_user_rate * $max_rate), 1);
                 for($i=0; $i< ceil($rating) ; $i++)
                 {
                   echo '<i class="la la-star active" style="color:yellow;"></i>';
                 }

                 for($i=0; $i< 5-ceil($rating)  ; $i++)
                 {
                  echo '<i class="la la-star"></i>';
                }                                   

              }
              else
              {
                $rating=0;

              }
            }
            else{
              for($i=0; $i< 5  ; $i++)
              {
                echo '<i class="la la-star"></i>';
              }  
            }
      
        ?>

        <h4 class="h5 mb-0 mt-2 text-center font-weight-bold text-danger">{{$data->product_unitprice}} ر س <!--  <span class="text-secondary font-weight-normal tx-13 ml-1 prev-price">$59</span> --></h4>
      </div>
      <hr>
      <div class="row" style="margin-right: 33%; padding: 5px;">
        @permission(['edit-products'])
        <a href="{{url('admin/products/'.$data->id.'/edit')}}" class="btn btn-primary ti-pencil-alt"></a>
        @endpermission
        @permission(['delete-products'])
        <form method="post" action="{{url('admin/products/'.$data->id)}}">
          {{csrf_field()}}
          <a class="btn btn-danger ti-trash" data-id="{{$data->id}}" data-toggle="modal" data-target="#delete-data{{$data->id}}"></a>
          <!-- Modal -->
          <div class="modal modal-danger fade" id="delete-data{{$data->id}}" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
               <div class="modal-header" style="    display: inline;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title text-center" style="float: right;" id="exampleModalLabel">تأكيد الحذف</h4>
              </div>

              <div class="modal-body">
                <h5 class="text-center ">هل انت متأكد من حذف   {{$data->blog_title_ar}}؟</h5>                  
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline"  data-dismiss="modal">إلغاء</button>
                <input type="hidden" name="_method" value="delete">

                <button class="btn btn-outline danger delete pull-right">حذف</button>
              </div>
            </div>
          </div>
        </div>
      </form>
      @endpermission
      <br> 
      <hr>
      <div class="row" style="    padding: 20px 0px;">
       @permission(['activate-products'])
        <div class="row col-lg-12">
          @if($data->status == 0)
          <form method="POST" action="{{url('/admin/products/'.$data->id.'/activate')}}">
            @csrf

            <div class="chk-option">
              <div class="checkbox-fade fade-in-primary"> 
               <label class="ckbox mg-b-5">تفعيل المنتج في الموقع <input name="status" type="checkbox" onchange="this.form.submit()" ><span></span>
               </label>
             </div>
           </div>
         </form>
         @else
         <form method="POST" action="{{url('/admin/products/'.$data->id.'/deactivate')}}">
          @csrf

          <div class="chk-option">
            <div class="checkbox-fade fade-in-primary"> 
             <label class="ckbox mg-b-5">المنتج مفعل <input name="status" type="checkbox" onchange="this.form.submit()" checked><span></span></label>
           </div>
         </div>
       </form>
       @endif
     </div>
     @endpermission
     @permission(['star-products'])
     <div class="row col-lg-12">
      @if($data->star == 0)
      <form method="POST" action="{{url('/admin/products/'.$data->id.'/add_star_product')}}">
        @csrf

        <div class="chk-option">
          <div class="checkbox-fade fade-in-primary"> 
           <label class="ckbox mg-b-5"><input name="star" type="checkbox" onchange="this.form.submit()" ><span></span>إضافه المنتج الي المنتجات المميزه</label>

         </div>
       </div>
     </form>
     @else
     <form method="POST" action="{{url('/admin/products/'.$data->id.'/del_star_product')}}">
      @csrf

      <div class="chk-option">
        <div class="checkbox-fade fade-in-primary"> 
         <label class="ckbox mg-b-5"><input name="star" type="checkbox" onchange="this.form.submit()" checked><span></span>المنتج  مميز</label>

       </div>
     </div>
   </form>
   @endif
 </div>
@endpermission
</div>
</div>  
</div>
</div>
</div>
@endforeach
</div>
</div>
</div>
@endsection
@section('js')
@endsection