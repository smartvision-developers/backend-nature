@extends('admin.layouts.master')
@section('css')
<!--- Internal Select2 css-->
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('public/assets/plugins/fancyuploder/fancy_fileupload.css')}}" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">

<style type="text/css">
form
{
  margin-top: 3rem !important;
}
.main-content-label
{
 font-size:large !important;
}
.btn-icon {
 display: initial !important;
}
.bg-gray-100 {
  background-color: #fbfbfb47 !important;
}
hr {
    margin-top: 4rem !important;
    margin-bottom: 4rem !important;
    }

.select2
{
      width: 595px !important;
/*      margin-bottom: 20px !important;
*/}
}
</style>
@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="breadcrumb-header justify-content-between">
  <div class="my-auto">
    <div class="d-flex">
      <h4 class="content-title mb-0 my-auto">المنتجات</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ إضافه منتج جديد</span>
    </div>
  </div>
  <div class="d-flex my-xl-auto right-content">
    <div class="pr-1 mb-3 mb-xl-0">
      <a href="{{URL('admin/products')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i> عرض المنتجات</a>
    </div>

  </div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')

<!-- row -->
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="card">
      <div class="card-body">
        <div class="main-content-label mg-b-5">
          إضافه قسم جديد
        </div>
        <hr>
        @if(Session::has('success'))
        <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
        @endif
        <form action="{{route('products.update', $product->id)}}" enctype="multipart/form-data" class="parsley-style-1" id="selectForm2" name="selectForm2"  method="POST">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            {{ method_field('PUT') }}

          <div class="">
            <div class="wd-250" id="cbErrorContainer2"></div>
            <div class="row mg-b-20">

            <div class="parsley-select wd-250 mg-t-30 col-sm-6" id="slWrapper2">
             <label>اختر  القسم الأساسي: <span class="tx-danger">*</span></label>
              <select name="category_id" class="form-control select2" data-parsley-class-handler="#slWrapper2" data-placeholder="اختر قسم المنتج" required="">
                @foreach($categorys as $category)
                <option value="{{$category->id}}" @if($product->Category_Product->category_id == $category->id) selected @endif>{{$category->category_name_ar}}</option>
                @endforeach
              </select>
              <div id="slErrorContainer2"></div>
            </div>



            <div class="parsley-select wd-250 mg-t-30 col-sm-6">
             <label>المنتج حصري أم لا : <span class="tx-danger">*</span></label>
              <input type="checkbox" class="form-control ckbox mg-b-5" name="exclusive" style=" position: relative;left: 47%;"  @if($product->exclusive == 1) checked @endif >
            </div>


          </div>
          <!--   <div class="parsley-select wd-250 mg-t-30" id="slWrapper3">
             <label>اختر  نوع الحيوان: <span class="tx-danger">*</span></label>
              <select name="animal_id" class="form-control select2" data-parsley-class-handler="#slWrapper3" data-placeholder="اختر قسم المنتج" required="">
                @foreach($animals as $animal)
                <option value="{{$animal->id}}" @if($product->Category_Product->animal_id == $animal->id) selected @endif>{{$animal->name_ar}}</option>
                @endforeach
              </select>
              <div id="slErrorContainer2"></div>
            </div> -->

               <div class="parsley-input wd-250 mg-t-120 mg-md-t-0" style="position:relative; top: -94px;
    right: 66%;" id="product_code">
                <label>كود المنتج: <span class="tx-danger">*</span></label>
                <input class="form-control" data-parsley-class-handler="#product_code" name="product_code" min="1" value="{{$product->product_code}}" placeholder="ادخل ا كود المنتج" required="" type="number">
                @error('product_code')
                <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                @enderror
              </div>


            <div class="row mg-b-20">
              <div class="parsley-input col-md-6" id="product_name_ar">
                <label>اسم القسم باللغه العربيه: <span class="tx-danger">*</span></label>
                <input class="form-control" data-parsley-class-handler="#product_name_ar" name="product_name_ar" value="{{$product->product_name_ar}}" placeholder="ادخل اسم القسم باللغه العربيه" required="" type="text">
                @error('product_name_ar')
                <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                @enderror
              </div>
              <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="product_name_en">
                <label>اسم القسم باللغه الانجليزيه: <span class="tx-danger">*</span></label>
                <input class="form-control" data-parsley-class-handler="#product_name_en" name="product_name_en" value="{{$product->product_name_en}}" placeholder="ادخل اسم القسم باللغه الانجليزيه" required="" type="text">
                @error('product_name_en')
                <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                @enderror
              </div>
            </div>
          </div>
        
          <div class="">
            <div class="row mg-b-20">
                 <div class="parsley-input col-md-3" id="product_oldprice">
                <label>السعر القديم : <span class="tx-danger"></span></label>
                <input class="form-control" name="product_oldprice" value="{{$product->product_oldprice}}" placeholder="السعر القديم " required="" type="number">
                @error('product_oldprice')
                <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                @enderror
              </div>
              <div class="parsley-input col-md-3" id="product_unitprice">
                <label>سعر المنتج الحالي: <span class="tx-danger">*</span></label>
                <input class="form-control" data-parsley-class-handler="#product_unitprice" name="product_unitprice" value="{{$product->product_unitprice}}" min="1" placeholder="سعر المنتج الحالي" required="" type="number">
                @error('product_unitprice')
                <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                @enderror
              </div>
              
              <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="product_quantity">
                <label>الكميه المتاحه للمنتج: <span class="tx-danger">*</span></label>
                <input class="form-control" data-parsley-class-handler="#product_quantity" name="product_quantity" min="1" value="{{$product->Store->product_quantity}}" placeholder="ادخل اسم القسم باللغه الانجليزيه" required="" type="number">
                @error('product_quantity')
                <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                @enderror
              </div>
            </div>
          </div>
        <hr>

          <div class="parsley-input row mb-4" id="product_image">
            <div class="col-sm-12 col-md-12">
               <label>صور المنتج: </label>

              <input type="file" name="product_image[]" accept=".jpg, .png, image/jpeg, image/png" multiple="multiple">
            </div>

            <div class="col-md-8" style="padding: 20px; margin: 13px;
    border: 1px solid #ececec;"> 
              @foreach($product->ProductImage as $data)

                <img src="{{asset('/public/images/products/'. $data->product_image)}}" style="    width: 18%;">
              @endforeach
            </div>
          </div>
                  <hr>

                  <div style="height: 270px;">
                    <div class=" row mg-b-20">
                      <div class="parsley-input col-sm-6" id="product_description_ar">
                        <label>وصف بسيط المنتج باللغه العربيه: <span class="tx-danger">*</span></label>

                        <textarea data-parsley-class-handler="#product_description_ar" class="summernote"  name="product_description_ar" required="">
                          <?php echo $product->product_description_ar; ?>
                        </textarea>


                        @error('product_description_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="parsley-input col-sm-6" id="product_description_en">
                        <label>وصف بسيط المنتج باللغه الانجليزيه: <span class="tx-danger">*</span></label>

                        <textarea data-parsley-class-handler="#product_description_en" name="product_description_en" class="summernote"  required="">
                                                 <?php echo $product->product_description_en ; ?>
                        </textarea>
                        @error('product_description_en')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>
        <hr>

                <div style="height: 270px;">
                    <div class=" row mg-b-20">
                      <div class="parsley-input col-sm-6" id="product_longdescription_ar">
                        <label>وصف مفصل المنتج باللغه العربيه: <span class="tx-danger">*</span></label>

                        <textarea data-parsley-class-handler="#product_longdescription_ar" required="" class="summernote"  name="product_longdescription_ar">       <?php echo $product->product_longdescription_ar; ?>

                        </textarea>


                        @error('product_description_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="parsley-input col-sm-6" id="product_longdescription_en">
                        <label>وصف مفصل المنتج باللغه الانجليزيه: <span class="tx-danger">*</span></label>

                        <textarea data-parsley-class-handler="#product_longdescription_en" required="" name="product_longdescription_en" class="summernote"><?php echo $product->product_longdescription_en; ?>
                        </textarea>
                        @error('product_description_en')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>

        <hr>

                  <div class="mg-t-30">
                    <button class="btn btn-main-primary pd-x-20" type="submit">إضافه</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- row closed -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
    @endsection
    @section('js')
    <!--Internal  Select2 js -->
    <script src="{{URL::asset('public/assets/plugins/select2/js/select2.min.js')}}"></script>
    <!--Internal  Parsley.min js -->
    <script src="{{URL::asset('public/assets/plugins/parsleyjs/parsley.min.js')}}"></script>
    <!-- Internal Form-validation js -->
    <script src="{{URL::asset('public/assets/js/form-validation.js')}}"></script>

    <script src="{{URL::asset('public/assets/plugins/fileuploads/js/fileupload.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fileuploads/js/file-upload.js')}}"></script>
    <!--Internal Fancy uploader js-->
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.ui.widget.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.fileupload.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.iframe-transport.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.fancy-fileupload.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/fancy-uploader.js')}}"></script>
  <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
    <script type="text/javascript">
         $('.summernote').summernote({
               height: 200,
          });
    </script>
@endsection