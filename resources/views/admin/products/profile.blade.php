@extends('admin.layouts.master')
@section('css')
<style type="text/css">
  
.preview-thumbnail img {
    height: 50% !important;
}
.tab-pane img{
  height:280px;
}
strong
{
  color: red;
  font-weight: bold;
}
</style>
@endsection
@section('page-header')
        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
          <div class="my-auto">
            <div class="d-flex">
              <h4 class="content-title mb-0 my-auto">المنتجات</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ عرض تفاصيل المنتج</span>
            </div>
          </div>
          <div class="d-flex my-xl-auto right-content">
            <div class="pr-1 mb-3 mb-xl-0">
              <a href="{{URL('admin/products/create')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i>إضافه منتج</a>
            </div>
     
          </div>
        </div>
        <!-- breadcrumb -->
@endsection
@section('content')

          <!--div-->
          <div class="col-xl-12">
            <div class="card mg-b-20">
              <div class="card-header pb-0">
                <div class="d-flex justify-content-between">
                  <h4 class="card-title mg-b-0">عرض تفاصيل المنتج</h4>
                  <i class="mdi mdi-dots-horizontal text-gray"></i>
                </div>
                <hr>
              </div>
               @if(Session::has('success'))
          <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
          @endif

          @if(Session::has('error'))
          <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
          @endif
              <div class="card-body">
              <div class="panel panel-primary tabs-style-3">
  <div class="tab-menu-heading">
    <div class="tabs-menu ">
      <!-- Tabs -->
      <ul class="nav panel-tabs">
        <li class=""><a href="#tab11" class="active" data-toggle="tab"><i class="fa fa-laptop"></i>تفاصيل المنتج </a></li>
<!--         <li><a href="#tab12" data-toggle="tab"><i class="fa fa-cube"></i> تفاصيل المنتج باللغه الانجليزيه</a></li>
 -->        <li><a href="#tab13" data-toggle="tab"><i class="fa fa-cogs"></i> التقييمات</a></li>
      <!--   <li><a href="#tab14" data-toggle="tab"><i class="fa fa-tasks"></i> الكلمات الدلاليه المنتج</a></li> -->
      
      <li class="mr-auto">           <a href="{{url('admin/products/'.$product->id.'/edit')}}" class="btn btn-primary ti-pencil-alt"> تعديل المنتج </a></li>
      </ul>

    </div>
  </div>
  <div class="panel-body tabs-menu-body">
    <div class="tab-content">
      <div class="tab-pane active" id="tab11">
        <div class="row">
        <div class="col-md-6">
            <div class="preview-pic tab-content">
                                          @php $count=1; @endphp
                    @foreach($product->ProductImage as $imgs)
                      <div class="tab-pane @if($count == 1) active @endif" id="pic-{{$count}}"><img src="{{URL('/public/images/products/'.$imgs->product_image)}}" alt="image"/></div>
                                          @php $count++; @endphp
                      @endforeach
                    </div>
                    <ul class="preview-thumbnail nav nav-tabs">
                      @php $count=1; @endphp
                      @foreach($product->ProductImage as $imgs)
                      <li class=" @if($count == 1) active @endif"><a data-target="#pic-{{$count}}" data-toggle="tab"><img src="{{URL('/public/images/products/'.$imgs->product_image)}}" alt="image"/></a></li>
                                        @php $count++; @endphp
                       @endforeach
                    </ul>        
                  </div>
        <div class="col-md-6">
          <h5>كود المنتج : {{$product->product_code}}</h5>
          <h4>اسم المنتج : {{$product->product_name_ar}}</h4>
        <h5>وصف بسيط المنتج : </h5>   {{$product->product_description_ar}}
        <p> سعر المنتج : {{$product->product_unitprice}} ر.س</p>
        <p>الكميه : {{$product->Store->product_quantity}} قطعه</p>
        </div>
       <div class="col-lg-12">
          <br>
        <hr>
       </div>
        <div class="col-md-4">
          <h4> معلومات عن المنتج : </h4>
        </div>
        <div class="col-md-8">
          <?php echo $product->product_longdescription_ar ;?>
        </div>
      </div>
      </div>
  <!--     <div class="tab-pane" id="tab12">
       <div class="row">
        <div class="col-md-6">
        </div>
        <div class="col-md-6">
          <h3>عنوان المنتج : {{$product->product_title_en}}</h3>
        <h5>وصف بسيط المنتج : </h5>   {{$product->product_text_en}}
        </div>
       <div class="col-lg-12">
          <br>
        <hr>
       </div>
        <div class="col-md-4">
          <h3>          محتوي المنتج : </h3>
        </div>
        <div class="col-md-8">
          <?php echo $product->product_description_en ;?>
        </div>
      </div>
      </div> -->
      <div class="tab-pane" id="tab13">
          @if(count($product->ProductAppraisal) >0)
                @foreach($product->ProductAppraisal as $appraisal)

            <div class="row">
              <div class="col-sm-3">
                <p>اسم صاحب التقييم : </p>
                <p>نسبه التقييم : </p>
                <p>التعليق : </p>
              </div>
              <div class="col-sm-9">
                <p>{{$appraisal->Customer->name}}</p>
                <p><strong>{{($appraisal->appraisal_num)?$appraisal->appraisal_num: 0}} </strong>من  5 </p>
                <p>{{$appraisal->appraisal_content}}</p>

              </div>
            </div>
            <hr>
               @endforeach
          @else
           <p>لاتوجد تقييمات حتي الان </p>
                 </div>

           @endif
<!--       <div class="tab-pane" id="tab14">
          
      </div> -->
    </div>
  </div>
</div>

              </div>
            </div>
          </div>
          <!--/div-->
        </div>
        <!-- /row -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
@endsection
    @section('js')
@endsection