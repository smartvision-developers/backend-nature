@extends('admin.layouts.master2')
@section('css')
<!-- Sidemenu-respoansive-tabs css -->
<link href="{{URL::asset('assets/plugins/sidemenu-responsive-tabs/css/sidemenu-responsive-tabs.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="container-fluid">
      <div class="row no-gutter">
        <!-- The image half -->
        <div class="col-md-6 col-lg-6 col-xl-7 d-none d-md-flex bg-primary-transparent">
          <div class="row wd-100p mx-auto text-center">
            <div class="col-md-12 col-lg-12 col-xl-12 my-auto mx-auto wd-100p">
              <img src="{{URL::asset('public/nature-sound/images/logo.png')}}" class="my-auto ht-xl-35p wd-md-80p wd-xl-35p mx-auto" alt="logo">
            </div>
          </div>
        </div>
        <!-- The content half -->
        <div class="col-md-6 col-lg-6 col-xl-5 bg-white">
          <div class="login d-flex align-items-center py-2">
            <!-- Demo content-->
            <div class="container p-0">
              <div class="row">
                <div class="col-md-10 col-lg-10 col-xl-9 mx-auto">
                  <div class="card-sigin">
<!--                     <div class="mb-5 d-flex"> <a href="{{ url('/') }}"><img src="{{URL::asset('public/nature-sound/images/faveicon.png')}}" class="sign-favicon ht-40" alt="logo"></a><h3 class="">صوت <span>الطبيعه </span></h3></div>
 -->                    <div class="card-sigin">
                      <div class="main-signup-header">
                        <h2 style="color:#803870;">مرحبا بعودتك !</h2>
                        <h5 class="font-weight-semibold mb-4">من فضلك سجل دخولك للمتابعة.</h5>
                        <h2>@include('admin.layouts.massage')</h2>
                    <form action="{{route('admin.login')}}" method="post">
                           {{csrf_field()}}     
                          <div class="form-group">
                            <label>اسم المستخدم </label> <input class="form-control" placeholder="ادخل اسمك" name="name" type="text">
                          </div>
                          <div class="form-group">
                            <label>كلمه المرور</label> <input class="form-control" placeholder="ادخل كلمه المرور" name="password" type="password">
                          </div><button class="btn btn-main-primary btn-block" style="background-color:#a1be22">تسجيل الدخول</button>
                         
                        </form>
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- End -->
          </div>
        </div><!-- End -->
      </div>
    </div>
@endsection
@section('js')
@endsection