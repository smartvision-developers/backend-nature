@extends('admin.layouts.master')
@section('css')
<!--- Internal Select2 css-->
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
<style type="text/css">
form
{
    margin-top: 3rem !important;
}
.main-content-label
{
   font-size:large !important;
}
.btn-icon {
   display: initial !important;
}
</style>
@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="breadcrumb-header justify-content-between">
  <div class="my-auto">
    <div class="d-flex">
      <h4 class="content-title mb-0 my-auto">المديرين</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ إضافه  مدير جديد</span>
  </div>
</div>
<div class="d-flex my-xl-auto right-content">
    <div class="pr-1 mb-3 mb-xl-0">
      <a href="{{URL('admin/users')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i> عرض المديرين</a>
  </div>

</div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')

<!-- row -->
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="card">
      <div class="card-body">
        <div class="main-content-label mg-b-5">
          تعديل  بيانات {{$user->name}} 
      </div>
      <hr>
      @if(Session::has('success'))
      <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
      @endif

      @if(Session::has('error'))
      <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
      @endif
      <form method="post" action="{{ route('users.update',$user->id) }}" id="selectForm2" name="selectForm2" class="parsley-style-1" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">

          <div class="">
            <div class="row mg-b-20">
               <div class="col-md-6 parsley-input {{ $errors->has('name') ? ' has-error' : '' }} row" id="name">
                <label for="name"> الاسم <span class="tx-danger">*</span> </label>
                <input required="" type="text" value="{{$user->name}}" data-parsley-class-handler="#name" id="name" name="name" class="form-control col-md-7 col-xs-12"> @if ($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
                @endif
            </div>

            <div class="col-md-6 parsley-input {{ $errors->has('email') ? ' has-error' : '' }} row" id="email">
                <label for="email"> البريد الالكتروني<span class="tx-danger">*</span></label>
                <input required="" type="text" value="{{$user->email}}" data-parsley-class-handler="#email" id="email" name="email" class="form-control col-md-7 col-xs-12"> 

                @if ($errors->has('email'))
                <span class="help-block">{{ $errors->first('email') }}</span>
                @endif
            </div>
        </div>
    </div>

<div class="">

            <div class="row mg-b-20">
           <div class="col-md-6 parsley-input {{ $errors->has('phone') ? ' has-error' : '' }} row" id="phone">
                <label for="phone"> رقم الجوال<span class="tx-danger">*</span></label>
                <input required="" type="text" value="{{$user->phone}}" data-parsley-class-handler="#phone" id="phone" name="phone" class="form-control col-md-7 col-xs-12"> 

                @if ($errors->has('phone'))
                <span class="help-block">{{ $errors->first('phone') }}</span>
                @endif
            </div>
      

       <div class="col-md-6 parsley-input {{ $errors->has('photo') ? ' has-error' : '' }} row" id="photo">
                <label for="photo"> صوره المستخدم<span class="tx-danger">*</span></label>
                <input type="file" data-parsley-class-handler="#photo" id="photo" name="photo" class="form-control col-md-7 col-xs-12"> 

                @if ($errors->has('photo'))
                <span class="help-block">{{ $errors->first('photo') }}</span>
                @endif
                <img src="{{URL('/'). $user->photo}}" width="15%"> 
            </div>
        </div>
    </div>

           <div class="col-md-6 parsley-input {{ $errors->has('password') ? ' has-error' : '' }} row" id="password">
                <label for="password"> صوره المستخدم<span class="tx-danger">*</span></label>
                <input type="password" data-parsley-class-handler="#password" id="password" name="password" class="form-control col-md-7 col-xs-12"> 

                @if ($errors->has('password'))
                <span class="help-block">{{ $errors->first('password') }}</span>
                @endif
            </div>


        <div class=" col-md-6 parsley-input form-group{{ $errors->has('role_id') ? ' has-error' : '' }} row">
            <label class="col-sm-2 col-form-label" for="category_id">Role
                <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select class="form-control" id="role_id" name="role_id">
                    @if(count($roles))
                    @foreach($roles as $row)
                    <option value="{{$row->id}}" {{$row->id == $user->roles[0]->id ? 'selected="selected"' : ''}}>{{$row->name}}</option>
                    @endforeach
                    @endif
                </select>
                @if ($errors->has('role_id'))
                <span class="help-block">{{ $errors->first('role_id') }}</span>
                @endif
            </div>
        </div>

    <div class="ln_solid"></div>

    <div class="mg-t-30">
        <input type="hidden" name="_token" value="{{ Session::token() }}">
        <input name="_method" type="hidden" value="PUT">
        <button type="submit" class="btn btn-primary">حفظ</button>
    </div>
</form>
</div>
</div>
</div>
</div>
<!-- row closed -->
</div>
<!-- Container closed -->
</div>
<!-- main-content closed -->
@endsection
@section('js')
<!--Internal  Select2 js -->
<script src="{{URL::asset('public/assets/plugins/select2/js/select2.min.js')}}"></script>
<!--Internal  Parsley.min js -->
<script src="{{URL::asset('public/assets/plugins/parsleyjs/parsley.min.js')}}"></script>
<!-- Internal Form-validation js -->
<script src="{{URL::asset('public/assets/js/form-validation.js')}}"></script>
@endsection