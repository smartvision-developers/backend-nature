@extends('admin.layouts.master')
@section('css')
<!--- Internal Select2 css-->
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
<style type="text/css">
  form
  {
    margin-top: 3rem !important;
  }
  .main-content-label
  {
     font-size:large !important;
  }
  .btn-icon {
     display: initial !important;
  }
</style>
@endsection
@section('page-header')
        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
          <div class="my-auto">
            <div class="d-flex">
              <h4 class="content-title mb-0 my-auto">الإعدادات</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ تعديل اعدادات المتجر </span>
            </div>
          </div>
       
        </div>
        <!-- breadcrumb -->
@endsection
@section('content')

        <!-- row -->
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="card">
              <div class="card-body">
                <div class="main-content-label mg-b-5">
                  تعديل إعدادات المتجر  
                </div>
                <hr>
                 @if(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
                @endif
                 <form method="POST" class="parsley-style-1" id="selectForm2" name="selectForm2" enctype="multipart/form-data" role="form" action="{{route('shop_settings.update', $shop_settings->id)}}">
                    @csrf
                  {{ method_field('PUT') }}

                  <div class="">
                    <div class="row mg-b-20">
                      <div class="parsley-input col-md-6" id="bank_name">
                        <label>اسم  البنك: <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#bank_name" name="bank_name" value="{{$shop_settings->bank_name}}" placeholder="ادخل اسم البنك" required="" type="text">
                           @error('bank_name')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="bank_account">
                        <label> رقم الحساب البنكي: <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#bank_account" value="{{$shop_settings->bank_account}}" name="bank_account" placeholder="ادخل رقم الحساب" required="" type="number">
                           @error('bank_account')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>
              
                  <div class="">
                    <div class="row mg-b-20">
                      <div class="parsley-input col-md-6" id="tax">
                        <label>القيمه الضريبيه : <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#tax" name="tax" value="{{$shop_settings->tax}}" placeholder="ادخل قيمه الضريبه" required="" type="number">
                           @error('tax')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="delivery_price">
                        <label>سعر التوصيل : <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#delivery_price" value="{{$shop_settings->delivery_price}}" name="delivery_price" placeholder="ادخل سعر التوصيل" required="" type="number">
                           @error('delivery_price')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>
              
                     <div style="height: 270px;">
                    <div class="row mg-b-20" >
                      <div class="col-sm-6">
                        <label>معلومات الشحن والتوصيل باللغه العربيه: </label>

                        <textarea class="form-control" rows="10" name="shipping_info_ar"><?php echo $shop_settings->shipping_info_ar?>
                        </textarea>


                        @error('shipping_info_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="col-sm-6">
                        <label>معلومات الشحن والتوصيل باللغه الانجليزيه:</label>

                        <textarea name="shipping_info_en" rows="10" class="form-control">{{$shop_settings->shipping_info_en}}
                        </textarea>
                        @error('shipping_info_en')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>
                <div class="mg-t-30">
                    <button class="btn btn-main-primary pd-x-20" type="submit">حفظ</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- row closed -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
@endsection
@section('js')
<!--Internal  Select2 js -->
<script src="{{URL::asset('public/assets/plugins/select2/js/select2.min.js')}}"></script>
<!--Internal  Parsley.min js -->
<script src="{{URL::asset('public/assets/plugins/parsleyjs/parsley.min.js')}}"></script>
<!-- Internal Form-validation js -->
<script src="{{URL::asset('public/assets/js/form-validation.js')}}"></script>
@endsection