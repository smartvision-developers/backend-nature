@extends('admin.layouts.master')
@section('css')
<style type="text/css">
  form
  {
    margin-top: 3rem !important;
  }
  .main-content-label
  {
     font-size:large !important;
  }
  .btn-icon {
     display: initial !important;
  }
</style>
@endsection
@section('page-header')
        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
          <div class="my-auto">
            <div class="d-flex">
              <h4 class="content-title mb-0 my-auto">الكلمات الدلاليه</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ إضافه كلمه دلاليه جديد</span>
            </div>
          </div>
          <div class="d-flex my-xl-auto right-content">
            <div class="pr-1 mb-3 mb-xl-0">
              <a href="{{URL('admin/tags')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i> عرض الكلمات الدلاليه</a>
            </div>
          
          </div>
        </div>
        <!-- breadcrumb -->
@endsection
@section('content')

        <!-- row -->
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="card">
              <div class="card-body">
                <div class="main-content-label mg-b-5">
                  إضافه كلمه دلاليه جديد
                </div>
                <hr>
                 @if(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
                @endif
                <form action="{{route('tags.store')}}" class="parsley-style-1" id="selectForm2" name="selectForm2"  method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                  <div class="">
                    <div class="row mg-b-20">
                      <div class="parsley-input col-md-6" id="tag_ar">
                        <label>اسم الكلمه الدلاليه باللغه العربيه: <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#tag_ar" name="tag_ar" value="{{old('tag_ar')}}" placeholder="ادخل اسم الكلمه الدلاليه باللغه العربيه" required="" type="text">
                           @error('tag_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="tag_en">
                        <label>اسم الكلمه الدلاليه باللغه الانجليزيه: <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#tag_en" name="tag_en" value="{{old('tag_en')}}" placeholder="ادخل اسم الكلمه الدلاليه باللغه الانجليزيه" required="" type="text">
                           @error('tag_en')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>
             
                <div class="mg-t-30">
                    <button class="btn btn-main-primary pd-x-20" type="submit">إضافه</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- row closed -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
@endsection
@section('js')
<!--Internal  Parsley.min js -->
<script src="{{URL::asset('public/assets/plugins/parsleyjs/parsley.min.js')}}"></script>
<!-- Internal Form-validation js -->
<script src="{{URL::asset('public/assets/js/form-validation.js')}}"></script>
@endsection