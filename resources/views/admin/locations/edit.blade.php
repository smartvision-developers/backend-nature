
@extends('admin.layouts.master')
@section('css')
<style type="text/css">
  form
  {
    margin-top: 3rem !important;
  }
  .main-content-label
  {
     font-size:large !important;
  }
  .btn-icon {
     display: initial !important;
  }
</style>
@endsection
@section('page-header')
        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
          <div class="my-auto">
            <div class="d-flex">
              <h4 class="content-title mb-0 my-auto">الأحياء</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ تعديل بيانات الأحياء</span>
            </div>
          </div>
          <div class="d-flex my-xl-auto right-content">
            <div class="pr-1 mb-3 mb-xl-0">
              <a href="{{URL('admin/locations')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i> عرض الأحياء</a>
            </div>
          
          </div>
        </div>
        <!-- breadcrumb -->
@endsection
@section('content')

        <!-- row -->
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="card">
              <div class="card-body">
                <div class="main-content-label mg-b-5">
                  تعديل بيانات  / {{$location->location_name_ar}}
                </div>
                <hr>
                 @if(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
                @endif
                 <form method="POST" class="parsley-style-1" id="selectForm2" name="selectForm2" enctype="multipart/form-data" role="form" action="{{route('locations.update', $location->id)}}">
                    @csrf
                  {{ method_field('PUT') }}

                  <div class="">

            <div class="parsley-select wd-650 mg-t-30 col-sm-6" id="slWrapper2">
             <label>اختر  المنطقه: <span class="tx-danger">*</span></label>
              <select name="city_id" class="form-control select2" data-parsley-class-handler="#slWrapper2" data-placeholder="اختر المنطقه" required="">
                @foreach($citys as $city)
                <option value="{{$city->id}}" @if($location->city_id == $city->id) selected @endif>{{$city->city_name_ar}}</option>
                @endforeach
              </select>
              <div id="slErrorContainer2"></div>
            </div>
                        <div class="parsley-select wd-650 mg-t-30 col-sm-6" id="slWrapper2">
             <label>اختر  المنطقه: <span class="tx-danger">*</span></label>
              <select name="region_id" class="form-control select2" data-parsley-class-handler="#slWrapper2" data-placeholder="اختر المنطقه" required="">
                 @foreach($regions as $region)
                <option value="{{$region->id}}" @if($location->region_id == $region->id) selected @endif>{{$region->region_name_ar}}</option>
                @endforeach
              </select>
              <div id="slErrorContainer2"></div>
            </div>

                    <div class="row mg-b-20">
                      <div class="parsley-input col-md-6" id="location_name_ar">
                        <label>اسم المدينه: <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#location_name_ar" name="location_name_ar" value="{{$location->location_name_ar}}" placeholder="Enter firstname" required="" type="text">
                           @error('location_name_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      </div> 
                    </div>
                  </div>
                <div class="mg-t-30">
                    <button class="btn btn-main-primary pd-x-20" type="submit">حفظ</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- row closed -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
@endsection
@section('js')
<!--Internal  Parsley.min js -->
<script src="{{URL::asset('public/assets/plugins/parsleyjs/parsley.min.js')}}"></script>
<!-- Internal Form-validation js -->
<script src="{{URL::asset('public/assets/js/form-validation.js')}}"></script>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
            $(document).ready(function() {
              console.log('dfghj');
        $('select[name="city_id"]').on('change', function(e) {
                   e.preventDefault();

            var cityID = $(this).val();
            console.log(cityID);
            if(cityID) {
                $.ajax({
                    url: '{{URL('/')}}/shippment-address/regions/'+cityID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        $('select[name="region_id"]').empty();
                          $('select[name="region_id"]').append('<option value="">--- اختر المدينه ---</option>');
                        $.each(data, function(key, value) {
                            $('select[name="region_id"]').append('<option @if (old('region_id') == '+ key +') selected="selected" @endif value="'+ key +'">'+ value +'</option>');
                        });
                    }
                });
            }else{
                $('select[name="region_id"]').empty();
            }
        });
});
</script>
