@extends('admin.layouts.master')
@section('css')
<!-- Internal Data table css -->
<link href="{{URL::asset('public/assets/plugins/datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{URL::asset('public/assets/plugins/datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/datatable/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{URL::asset('public/assets/plugins/datatable/css/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/datatable/css/responsive.dataTables.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">

<style type="text/css">
.buttons-pdf , .buttons-excel
{
  display: none !important;
}

.main-content-label
{
 font-size:large !important;
}
.btn-icon {
 display: initial !important;
}
.chk-option .checkbox-fade
{
  display: inline-flex !important;
}
</style>
@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="breadcrumb-header justify-content-between">
  <div class="my-auto">
    <div class="d-flex">
      <h4 class="content-title mb-0 my-auto">المنتجات الأكثر مبيعا</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ عرض كل المنتجات الأكثر مبيعا</span>
    </div>
  </div>
  <div class="d-flex my-xl-auto right-content">
    @permission(['create-product_sellers'])
    <div class="pr-1 mb-3 mb-xl-0">
      <a href="{{URL('admin/product_sellers/create')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i>إضافه منتجات  وصل حديثا </a>
    </div>
    @endpermission
  </div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')

<!--div-->
<div class="col-xl-12">
  <div class="card mg-b-20">
    <div class="card-header pb-0">
      <div class="d-flex justify-content-between">
        <h4 class="card-title mg-b-0">عرض كل المنتجات الأكثر مبيعا</h4>
        
        <i class="mdi mdi-dots-horizontal text-gray"></i>
      </div>
      <hr>
    </div>
    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
    @endif

    @if(Session::has('error'))
    <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
    @endif
    <div class="card-body">
      <div class="table-responsive">
        <table id="example" class="table key-buttons text-md-nowrap">
          <thead>
            <tr>
              <th class="border-bottom-0 text-center">#</th>
              <th class="border-bottom-0 text-center">صوره المنتج :</th>

              <th class="border-bottom-0 text-center">اسم المنتج</th>
              <th class="border-bottom-0 text-center">بواسطه</th>

                @permission(['activate-sellers-products'])
              <th class="border-bottom-0 text-center">الحاله</th>
               @endpermission

              <th class="border-bottom-0 text-center">أنشأ في</th>

            </tr>
          </thead>
          <tbody>
            @isset($product_sellers)
            @php $count = 1; @endphp
            @foreach($product_sellers as $data)
            <tr>

              <td class="text-center">{{$count}}</td>
              <td class="text-center"><img style="width:20%;" src="{{asset('/public/images/products/'. $data->Product->ProductImage[0]->product_image)}}"></td>
              <td class="text-center"><a href="{{URL('/admin/products/'.$data->product_id)}}">{{$data->Product->product_name_ar}}</a></td>
              <td class="text-center">{{$data->User->name}}</td>
                              @permission(['activate-sellers-products'])
              <td class="text-center">
                @if($data->status == 0)
                <form method="POST" action="{{url('/admin/product_sellers/'.$data->id.'/activate')}}">
                  @csrf
                  
                  <div class="chk-option">
                    <div class="checkbox-fade fade-in-primary"> 
                     <label class="ckbox mg-b-5"><input name="status" type="checkbox" onchange="this.form.submit()" ><span></span></label>

                   </div>
                 </div>
               </form>
               @else
               <form method="POST" action="{{url('/admin/product_sellers/'.$data->id.'/deactivate')}}">
                @csrf

                <div class="chk-option">
                  <div class="checkbox-fade fade-in-primary"> 
                   <label class="ckbox mg-b-5"><input name="status" type="checkbox" onchange="this.form.submit()" checked><span></span></label>

                 </div>
               </div>
             </form>
             @endif
           </td>
           @endpermission
           <td class="text-center"> {{ ($data->created_at)->toDayDateTimeString()}}</td>

         </tr>
         @php $count ++; @endphp
         @endforeach

         @endisset
       </tbody>
     </table>
   </div>
 </div>
</div>
</div>
<!--/div-->
</div>
<!-- /row -->
</div>
<!-- Container closed -->
</div>
<!-- main-content closed -->
@endsection
@section('js')
<!-- Internal Data tables -->
<script src="{{URL::asset('public/assets/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/responsive.dataTables.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/jszip.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
<!--Internal  Datatable js -->
<script src="{{URL::asset('public/assets/js/table-data.js')}}"></script>
@endsection