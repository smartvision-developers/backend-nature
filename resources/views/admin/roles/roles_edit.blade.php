@extends('admin.layouts.master')
@section('css')
<!--- Internal Select2 css-->
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
<style type="text/css">
form
{
    margin-top: 3rem !important;
}
.main-content-label
{
   font-size:large !important;
}
.btn-icon {
   display: initial !important;
}
</style>
@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="breadcrumb-header justify-content-between">
  <div class="my-auto">
    <div class="d-flex">
      <h4 class="content-title mb-0 my-auto">Roles</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ إضافه  Role جديد</span>
  </div>
</div>
<div class="d-flex my-xl-auto right-content">
    <div class="pr-1 mb-3 mb-xl-0">
      <a href="{{URL('admin/roles')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i> عرض Roles</a>
  </div>

</div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')

<!-- row -->
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="card">
      <div class="card-body">
        <div class="main-content-label mg-b-5">
          تعديل  Role 
      </div>
      <hr>
      @if(Session::has('success'))
      <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
      @endif

      @if(Session::has('error'))
      <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
      @endif
      <form method="post" action="{{ route('roles.update', $role->id) }}" id="selectForm2" name="selectForm2" class="parsley-style-1">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">

          <div class="">
            <div class="row mg-b-20">
               <div class="col-md-6 parsley-input {{ $errors->has('name') ? ' has-error' : '' }} row" id="name">
                <label for="name"> الاسم <span class="tx-danger">*</span> </label>
                <input required="" type="text" value="{{$role->name}}" data-parsley-class-handler="#name" id="name" name="name" class="form-control col-md-7 col-xs-12"> @if ($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
                @endif
            </div>

            <div class="col-md-6 parsley-input {{ $errors->has('display_name') ? ' has-error' : '' }} row" id="display_name">
                <label for="display_name"> الاسم المستخدم<span class="tx-danger">*</span></label>
                <input required="" type="text" value="{{$role->display_name}}" data-parsley-class-handler="#display_name" id="display_name" name="display_name" class="form-control col-md-7 col-xs-12"> 

                @if ($errors->has('display_name'))
                <span class="help-block">{{ $errors->first('display_name') }}</span>
                @endif
            </div>
        </div>
    </div>

    <div class="">

        <div class="col-md-6 parsley-input {{ $errors->has('description') ? ' has-error' : '' }} row" id="description">
            <label for="description"> الوصف <span class="tx-danger">*</span></label>
            <input required="" type="text" value="{{$role->description}}" data-parsley-class-handler="#description" id="description" name="description" class="form-control col-md-7 col-xs-12"> @if ($errors->has('description'))
            <span class="help-block">{{ $errors->first('description') }}</span>
            @endif
        </div>
    </div>


    <div style="border: 1px solid #cccccc; padding: 20px;">
            <div class="col-md-6 parsley-input {{ $errors->has('permission_id') ? ' has-error' : '' }} row">
            <label for="permission_id" class="col-sm-2 col-form-label">Permission</label>
            <div class="col-sm-10">
                @if(count($permissions))
                    @foreach($permissions as $row)
                            <label>{{ Form::checkbox('permission_id[]', $row->id, in_array($row->id, $role_permissions) ? true : false, array('class' => 'name')) }}
                            {{ $row->display_name }}</label> <br/>
                    @endforeach
                @endif
                @if ($errors->has('permission_id'))
                <span class="help-block">{{ $errors->first('permission_id') }}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="ln_solid"></div>

    <div class="mg-t-30">
        <input type="hidden" name="_token" value="{{ Session::token() }}">
        <input name="_method" type="hidden" value="PUT">
        <button type="submit" class="btn btn-primary">حفظ</button>
    </div>
</form>
</div>
</div>
</div>
</div>
<!-- row closed -->
</div>
<!-- Container closed -->
</div>
<!-- main-content closed -->
@endsection
@section('js')
<!--Internal  Select2 js -->
<script src="{{URL::asset('public/assets/plugins/select2/js/select2.min.js')}}"></script>
<!--Internal  Parsley.min js -->
<script src="{{URL::asset('public/assets/plugins/parsleyjs/parsley.min.js')}}"></script>
<!-- Internal Form-validation js -->
<script src="{{URL::asset('public/assets/js/form-validation.js')}}"></script>
@endsection
