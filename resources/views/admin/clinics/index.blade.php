@extends('admin.layouts.master')
@section('css')
<!-- Internal Data table css -->
<link href="{{URL::asset('public/assets/plugins/datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{URL::asset('public/assets/plugins/datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/datatable/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{URL::asset('public/assets/plugins/datatable/css/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/datatable/css/responsive.dataTables.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">

<style type="text/css">
  .main-content-label
  {
     font-size:large !important;
  }
  .btn-icon {
     display: initial !important;
  }
.select2-container--default .select2-selection--multiple {
    width: 360px !important;
}
</style>
@endsection
@section('page-header')
        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
          <div class="my-auto">
            <div class="d-flex">
              <h4 class="content-title mb-0 my-auto">العياده</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ عرض تفاصيل العياده</span>
            </div>
          </div>
          <div class="d-flex my-xl-auto right-content">
            @permission(['edit_clinic'])
            <div class="pr-1 mb-3 mb-xl-0">
              <a href="{{URL('admin/clinics/1/edit')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i>تعديل عياده</a>
            </div>
            @endpermission
          </div>
        </div>
        <!-- breadcrumb -->
@endsection
@section('content')

          <!--div-->
          <div class="col-xl-12">
            <div class="card mg-b-20">
              <div class="card-header pb-0">
                <div class="d-flex justify-content-between">
                  <h4 class="card-title mg-b-0">عرض تفاصيل العياده</h4>
                  <i class="mdi mdi-dots-horizontal text-gray"></i>
                </div>
                <hr>
              </div>
               @if(Session::has('success'))
          <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
          @endif

          @if(Session::has('error'))
          <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
          @endif
              <div class="card-body">
              <div class="panel panel-primary tabs-style-3">
  <div class="tab-menu-heading">
    <div class="tabs-menu ">
      <!-- Tabs -->
      <ul class="nav panel-tabs">
        <li class=""><a href="#tab11" class="active" data-toggle="tab"><i class="fa fa-laptop"></i>تفاصيل العياده باللغه العربيه</a></li>
        <li><a href="#tab12" data-toggle="tab"><i class="fa fa-cube"></i> تفاصيل العياده باللغه الانجليزيه</a></li>
        <li><a href="#tab13" data-toggle="tab"><i class="fa fa-cogs"></i> الحجوزات</a></li>
        <li><a href="#tab14" data-toggle="tab"><i class="fa fa-tasks"></i> خدمات العياده</a></li>
      </ul>
    </div>
  </div>
  <div class="panel-body tabs-menu-body">
    <div class="tab-content">
      <div class="tab-pane active" id="tab11">
        <div class="row">
        <div class="col-md-4">
          <img height="300px" src="{{URL($clinic->clinic_image)}}">
        </div>
        <div class="col-md-8">
          <h3>عنوان العياده : {{$clinic->clinic_title_ar}}</h3>
        <h5>وصف بسيط للعياده : </h5>            <?php echo $clinic->clinic_description_ar ;?>

        </div>
     
      </div>
      </div>
      <div class="tab-pane" id="tab12">
       <div class="row">
        <div class="col-md-4">
          <img height="300px" src="{{URL($clinic->clinic_image)}}">
        </div>
        <div class="col-md-8">
          <h3>عنوان العياده : {{$clinic->clinic_title_en}}</h3>
        <h5>وصف بسيط للعياده : </h5>           <?php echo $clinic->clinic_description_en ;?>

        </div>
      
      </div>
      </div>
      <div class="tab-pane" id="tab13">
        <p>لاتوجد تعليقات حتي الان </p>
      </div>
      <div class="tab-pane" id="tab14">
        @permission(['create-clinic_service'])
          <form action="{{route('admin.addService', $clinic->id)}}" method="post">
            @csrf
                  <div class="row mg-b-20">
                      <div class="parsley-input col-md-4" id="service_description_ar">
                        <label>خدمه العياده باللغه العربيه: <span class="tx-danger">*</span></label>
                        <input type="text" name="service_description_ar" required="" class="form-control">
                           @error('service_description_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
               
                  <div class="parsley-input col-md-4" id="service_description_en">
                        <label>خدمه العياده باللغه الانجليزيه: <span class="tx-danger">*</span></label>
                        <input type="text" name="service_description_en" required="" class="form-control">
                           @error('service_description_en')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                            <div class="parsley-input col-md-2" id="service_price">
                        <label>سعر الخدمه: <span class="tx-danger">*</span></label>
                        <input type="number" name="service_price" required="" class="form-control">
                           @error('service_price')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>

                        <div class="col-md-2 mg-t-30">
                    <button class="btn btn-main-primary pd-x-20" type="submit">حفظ</button>
                  </div>  
                    </div>  
                  </form>
                  @endpermission
                    <hr>
                <div class="table-responsive">
                  <table id="example" class="table key-buttons text-md-nowrap">
                    <thead>
                      <tr>
                          <th class="border-bottom-0 text-center">#</th>
                          <th class="border-bottom-0 text-center">اسم الخدمه باللغه العربيه</th>
                          <th class="border-bottom-0 text-center">اسم الخدمه باللغه الانجليزيه</th>
                          <th class="border-bottom-0 text-center">سعر الخدمه</th>
                           @permission(['activate-clinic_service'])
                          <th class="border-bottom-0 text-center">حاله الخدمه</th>
                          @endpermission
                          <th class="border-bottom-0 text-center">أنشأ في</th>
                           @permission(['delete-clinic_service'])
                          <th class="border-bottom-0 text-center">حذف</th>
                           @endpermission
                      </tr>
                    </thead>
                    <tbody>
                      @isset($get_services)
                      @php $count = 1; @endphp
                      @foreach($get_services as $data)
                      <tr>
                         <td class="text-center">{{$count}}</td>
                        <td class="text-center">{{$data->service_description_ar}}</td>
                        <td class="text-center">{{$data->service_description_en}}</td>
                        <td class="text-center">{{$data->service_price}}ر. س</td>
                           @permission(['activate-clinic_service'])
                            <td class="text-center">
                              @if($data->status == 0)
                              <form method="POST" action="{{url('/admin/clinics/'.$data->id.'/services/activate')}}">
                                @csrf
                                
                                <div class="chk-option">
                                  <div class="checkbox-fade fade-in-primary"> 
                                       <label class="ckbox mg-b-5"><input name="status" type="checkbox" onchange="this.form.submit()" ><span></span></label>

                                   </div>
                                 </div>
                               </form>
                               @else
                               <form method="POST" action="{{url('/admin/clinics/'.$data->id.'/services/deactivate')}}">
                                @csrf

                                <div class="chk-option">
                                  <div class="checkbox-fade fade-in-primary"> 
                     <label class="ckbox mg-b-5"><input name="status" type="checkbox" onchange="this.form.submit()" checked><span></span></label>

                                 </div>
                               </div>
                             </form>
                             @endif
                           </td>
                           @endpermission
                           <td class="text-center"> {{ ($data->created_at)->toDayDateTimeString()}}</td>
<!--                            <td class="text-center">
                             <a href="{{url('admin/clinics/'.$data->id.'/edit')}}" class="btn btn-primary ti-pencil-alt"></a>
                           </td>

 -->                       
                            @permission(['delete-clinic_service'])
                            <td class="text-center">
                            <form method="post" action="{{url('admin/clinics/'.$data->id.'/services/delete')}}">
                              {{csrf_field()}}
                              <a class="btn btn-danger ti-trash" data-id="{{$data->id}}" data-toggle="modal" data-target="#delete-data{{$data->id}}"></a>
                              <!-- Modal -->
                              <div class="modal modal-danger fade" id="delete-data{{$data->id}}" role="dialog">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                   <div class="modal-header" style="    display: inline;">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title text-center" style="float: right;" id="exampleModalLabel">تأكيد الحذف</h4>
                                  </div>

                                  <div class="modal-body">
                                    <h5 class="text-center ">هل انت متأكد من حذف   {{$data->service_description_ar}}؟</h5>                  
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-outline"  data-dismiss="modal">إلغاء</button>
                                    <input type="hidden" name="_method" value="delete">

                                    <button class="btn btn-outline danger delete pull-right">حذف</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </form>     
                        </td>    
                          @endpermission
                                          </tr>
                      @php $count++; @endphp
                      @endforeach
                      @endisset
                    </tbody>
                    </table> 
                  </div>
                     
      </div>
    </div>
  </div>
</div>

              </div>
            </div>
          </div>
          <!--/div-->
        </div>
        <!-- /row -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
@endsection
    @section('js')
    <!--Internal  Select2 js -->
    <script src="{{URL::asset('public/assets/plugins/select2/js/select2.min.js')}}"></script>
    <!--Internal  Parsley.min js -->
    <script src="{{URL::asset('public/assets/plugins/parsleyjs/parsley.min.js')}}"></script>
    <!-- Internal Form-validation js -->
    <script src="{{URL::asset('public/assets/js/form-validation.js')}}"></script>
@endsection