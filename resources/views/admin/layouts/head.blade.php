<!-- Title -->
<title>صوت الطبيعه - لوحه التحكم </title>

<!-- og property -->
<meta property="og:title" content="صوت الطبيعه" />
<meta property="og:type" content="e-commerce" />
<meta property="og:url" content="{URL('/')}}" />
<meta property="og:image" content="{{URL('/public/nature-sound/images/logo.png')}}" />
<meta property="og:description" content="متجر صوت الطبيعه والحيوانات الأليفه. حيوانك الأليف صديقنا.. كل ما يحتاجه وأكثر " />
<meta property="og:determiner" content="the" />
<meta property="og:locale" content="ar_AR" />
<meta property="og:locale:alternate" content="en_GB" />
<meta property="og:site_name" content="صوت الطبيعه" />

<!-- Favicon -->
<link rel="icon" href="{{URL::asset('public/nature-sound/images/faveicon.png')}}" type="image/x-icon"/>
<!-- Icons css -->
<link href="{{URL::asset('public/assets/css/icons.css')}}" rel="stylesheet">
<!--  Custom Scroll bar-->
<link href="{{URL::asset('public/assets/plugins/mscrollbar/jquery.mCustomScrollbar.css')}}" rel="stylesheet"/>
<!--  Sidebar css -->
<link href="{{URL::asset('public/assets/plugins/sidebar/sidebar.css')}}" rel="stylesheet">
<!-- Sidemenu css -->
<link rel="stylesheet" href="{{URL::asset('public/assets/css-rtl/sidemenu.css')}}">
@yield('css')
<!--- Style css -->
<link href="{{URL::asset('public/assets/css-rtl/style.css')}}" rel="stylesheet">
<!--- Dark-mode css -->
<link href="{{URL::asset('public/assets/css-rtl/style-dark.css')}}" rel="stylesheet">
<!---Skinmodes css-->
<link href="{{URL::asset('public/assets/css-rtl/skin-modes.css')}}" rel="stylesheet">
  <style type="text/css">
@import url('https://fonts.googleapis.com/css2?family=Changa&display=swap');
   *,   h1,h2,h3,h4 , p

      {
          font-family: 'Changa', system-ui;
      }
  </style>