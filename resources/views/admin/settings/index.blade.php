@extends('admin.layouts.master')
@section('css')
<!--- Internal Select2 css-->
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">

<style type="text/css">
  form
  {
    margin-top: 3rem !important;
  }
  .main-content-label
  {
     font-size:large !important;
  }
  .btn-icon {
     display: initial !important;
  }
</style>
@endsection
@section('page-header')
        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
          <div class="my-auto">
            <div class="d-flex">
              <h4 class="content-title mb-0 my-auto">الإعدادات</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ تعديل اعدادات الموقع </span>
            </div>
          </div>
       
        </div>
        <!-- breadcrumb -->
@endsection
@section('content')

        <!-- row -->
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="card">
              <div class="card-body">
                <div class="main-content-label mg-b-5">
                  تعديل إعدادات الموقع  
                </div>
                <hr>
                 @if(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
                @endif
                 <form method="POST" class="parsley-style-1" id="selectForm2" name="selectForm2" enctype="multipart/form-data" role="form" action="{{route('settings.update', $settings->id)}}">
                    @csrf
                  {{ method_field('PUT') }}

                  <div class="">
                    <div class="row mg-b-20">
                      <div class="parsley-input col-md-6" id="email">
                        <label>البريد الالكتروني: <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#email" name="email" value="{{$settings->email}}" placeholder="ادخل البريد الالكتروني" required="" type="email">
                           @error('email')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="phone_no">
                        <label> رقم الجوال : <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#phone_no" value="{{$settings->phone_no}}" name="phone_no" placeholder="ادخل رقم  الجوال" required="" type="text">
                           @error('phone_no')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>
              
                  <div class="">
                    <div class="row mg-b-20">
                      <div class="parsley-input col-md-6" id="address_ar">
                        <label>العنوان باللغه العربيه   : <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#address_ar" name="address_ar" value="{{$settings->address_ar}}" placeholder="ادخل العنوان باللغه العربيه  " required="" type="text">
                           @error('address_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="parsley-input col-md-6" id="address_en">
                        <label>العنوان باللغه الانجليزيه  : <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#address_en" name="address_en" value="{{$settings->address_en}}" placeholder="ادخل العنوان باللغه الانجليزيه " required="" type="text">
                           @error('address_en')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                     
                    </div>
                  </div>

                      <div class="">
                    <div class="row mg-b-20">
                      <div class="parsley-input col-md-4" id="twitter_link">
                        <label>لينك تويتر : <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#twitter_link" name="twitter_link" value="{{$settings->twitter_link}}" placeholder="لينك تويتر" required="" type="text">
                           @error('twitter_link')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="parsley-input col-md-4 mg-t-20 mg-md-t-0" id="instagram_link">
                        <label>لينك انستجرام : <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#instagram_link" value="{{$settings->instagram_link}}" name="instagram_link" placeholder="لينك انستجرام" required="" type="text">
                           @error('instagram_link')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                       <div class="parsley-input col-md-4 mg-t-20 mg-md-t-0" id="youtube_link">
                        <label>لينك اليوتيوب : <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#youtube_link" value="{{$settings->youtube_link}}" name="youtube_link" placeholder="لينك اليوتيوب" required="" type="text">
                           @error('youtube_link')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>


                      <div class="">
                    <div class="row mg-b-20">
                      <div class="parsley-input col-md-6" id="facebook_link">
                        <label>لينك فيسبوك : <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#facebook_link" name="facebook_link" value="{{$settings->facebook_link}}" placeholder="لينك فيسبوك" required="" type="text">
                           @error('facebook_link')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="snapchat_link">
                        <label>لينك سناب شات : <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#snapchat_link" value="{{$settings->snapchat_link}}" name="snapchat_link" placeholder="لينك سناب شات" required="" type="text">
                           @error('snapchat_link')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>
              
                     <div style="height: 270px;">
                    <div class="row mg-b-20" >
                      <div class="col-sm-6">
                        <label>من نحن باللغه العربيه: </label>

                        <textarea class="form-control" rows="10" name="about_us_ar"><?php echo $settings->about_us_ar?>
                        </textarea>


                        @error('about_us_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="col-sm-6">
                        <label>من نحن باللغه الانجليزيه:</label>

                        <textarea name="about_us_en" rows="10" class="form-control">{{$settings->about_us_en}}
                        </textarea>
                        @error('about_us_en')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>


                   <div style="height: 270px;">
                    <div class="row mg-b-20" >
                      <div class="col-sm-6">
                        <label>رؤيتنا باللغه العربيه: </label>

                        <textarea class="form-control" rows="10" name="our_vision_ar"><?php echo $settings->our_vision_ar?>
                        </textarea>


                        @error('our_vision_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="col-sm-6">
                        <label>رؤيتنا باللغه الانجليزيه:</label>

                        <textarea name="our_vision_en" rows="10" class="form-control">{{$settings->our_vision_en}}
                        </textarea>
                        @error('our_vision_en')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>

                       <div style="height: 370px;">
                    <div class="row mg-b-20" >
                      <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="video" >
                        <label>وصف مفصل المقال باللغه العربيه: <span class="tx-danger">*</span></label>

                        <textarea class="summernote" data-parsley-class-handler="#video" required="" name="video">
                          <?php echo $settings->video; ?>
                        </textarea>


                        @error('video')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      
                    </div>

                  </div>

      

                <div class="mg-t-140">
                    <button class="btn btn-main-primary pd-x-20" type="submit">حفظ</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- row closed -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
@endsection
@section('js')
<!--Internal  Select2 js -->
<script src="{{URL::asset('public/assets/plugins/select2/js/select2.min.js')}}"></script>
<!--Internal  Parsley.min js -->
<script src="{{URL::asset('public/assets/plugins/parsleyjs/parsley.min.js')}}"></script>
<!-- Internal Form-validation js -->
<script src="{{URL::asset('public/assets/js/form-validation.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
    <script type="text/javascript">
         $('.summernote').summernote({
               height: 250,
          });
    </script>

@endsection