@extends('admin.layouts.master')
@section('css')
<style type="text/css">
  form
  {
    margin-top: 3rem !important;
  }
  .main-content-label
  {
     font-size:large !important;
  }
  .btn-icon {
     display: initial !important;
  }
</style>
@endsection
@section('page-header')
        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
          <div class="my-auto">
            <div class="d-flex">
              <h4 class="content-title mb-0 my-auto">المدن</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ إضافه مدينه جديد</span>
            </div>
          </div>
          <div class="d-flex my-xl-auto right-content">
            <div class="pr-1 mb-3 mb-xl-0">
              <a href="{{URL('admin/regions')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i> عرض المدن</a>
            </div>
          
          </div>
        </div>
        <!-- breadcrumb -->
@endsection
@section('content')

        <!-- row -->
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="card">
              <div class="card-body">
                <div class="main-content-label mg-b-5">
                  إضافه مدينه جديد
                </div>
                <hr>
                 @if(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
                @endif
                <form action="{{route('regions.store')}}" class="parsley-style-1" id="selectForm2" name="selectForm2"  method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                  <div class="">
                   <div class="parsley-select wd-650 mg-t-30 col-sm-6" id="slWrapper2">
                     <label>اختر  المنطقه: <span class="tx-danger">*</span></label>
                      <select name="city_id" class="form-control select2" data-parsley-class-handler="#slWrapper2" data-placeholder="اختر المنطقه" required="" id="city_id" >
                        <option value="">اختر المنطقه</option>
                        @foreach($citys as $city)
                        <option value="{{$city->id}}">{{$city->city_name_ar}}</option>
                        @endforeach
                      </select>
                      <div id="slErrorContainer2"></div>
                    </div>
                    <div class="row mg-b-20">
                      <div class="parsley-input col-md-6" id="region_name_ar">
                        <label>اسم المدينه : <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#region_name_ar" name="region_name_ar" value="{{old('region_name_ar')}}" placeholder="ادخل اسم المدينه " required="" type="text">
                           @error('region_name_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                     
                    </div>
      
                  </div>
             
                <div class="mg-t-30">
                    <button class="btn btn-main-primary pd-x-20" type="submit">إضافه</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- row closed -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
@endsection
@section('js')
<!--Internal  Parsley.min js -->
<script src="{{URL::asset('public/assets/plugins/parsleyjs/parsley.min.js')}}"></script>
<!-- Internal Form-validation js -->
<script src="{{URL::asset('public/assets/js/form-validation.js')}}"></script>
@endsection