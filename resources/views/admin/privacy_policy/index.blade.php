@extends('admin.layouts.master')
@section('css')
<!--- Internal Select2 css-->
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('public/assets/plugins/fancyuploder/fancy_fileupload.css')}}" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
<style type="text/css">
form
{
  margin-top: 3rem !important;
}
.main-content-label
{
 font-size:large !important;
}
.btn-icon {
 display: initial !important;
}
.bg-gray-100 {
  background-color: #fbfbfb47 !important;
}
hr {
    margin-top: 4rem !important;
    margin-bottom: 4rem !important;
    }

.select2
{
      width: 595px !important;
/*      margin-bottom: 20px !important;
*/}
.mg-t-30
{
  margin-top: 130px !important;
}
.img-subnail
{
      width: auto;
   height: 55%;
    margin-right: 17%;
    border: 1px solid #eceff6;
    margin-top: 29px;
    padding: 11px;
}

</style>

@endsection
@section('page-header')
        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
          <div class="my-auto">
            <div class="d-flex">
              <h4 class="content-title mb-0 my-auto">المقالات</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ تعديل بيانات المقال</span>
            </div>
          </div>
          <div class="d-flex my-xl-auto right-content">
            <div class="pr-1 mb-3 mb-xl-0">
              <a href="{{URL('admin/privacy_policy')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i> عرض المقالات</a>
            </div>
          
          </div>
        </div>
        <!-- breadcrumb -->
@endsection
@section('content')

        <!-- row -->
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="card">
              <div class="card-body">
                <div class="main-content-label mg-b-5">
                  تعديل بيانات 
                </div>
                <hr>
                 @if(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
                @endif
                 <form method="POST" class="parsley-style-1" id="selectForm2" name="selectForm2" enctype="multipart/form-data" role="form" action="{{route('privacy_policy.update', $privacy_policy->id)}}">
                    @csrf
                  {{ method_field('PUT') }}
      
                <div style="height: 270px;">
                    <div class="row mg-b-20" >
                      <div class="col-sm-6">
                        <label>سياسه الخصوصيه باللغه العربيه: <span class="tx-danger">*</span></label>

                        <textarea class="summernote" id="privacy_policy_ar" name="privacy_policy_ar"><?php echo $privacy_policy->privacy_policy_ar;?>
                        </textarea>


                        @error('privacy_policy_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="col-sm-6">
                        <label>سياسه الخصوصيه باللغه الانجليزيه: <span class="tx-danger">*</span></label>

                        <textarea name="privacy_policy_en" class="summernote" id="privacy_policy_en"><?php echo $privacy_policy->privacy_policy_en ;?>
                        </textarea>
                        @error('privacy_policy_en')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>

        <hr>

                <div style="height: 270px;">
                    <div class="row mg-b-20" >
                      <div class="col-sm-6">
                        <label>شروط الاستخدام باللغه العربيه: <span class="tx-danger">*</span></label>

                        <textarea class="summernote" id="term_of_use_ar" name="term_of_use_ar"><?php echo $privacy_policy->term_of_use_ar;?>
                        </textarea>


                        @error('term_of_use_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="col-sm-6">
                        <label>شروط الاستخدام باللغه الانجليزيه: <span class="tx-danger">*</span></label>

                        <textarea name="term_of_use_en" class="summernote" id="term_of_use_en"><?php echo $privacy_policy->term_of_use_en ;?>
                        </textarea>
                        @error('term_of_use_en')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>
                <div class="mg-t-30">
                    <button class="btn btn-main-primary pd-x-20" type="submit">حفظ</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- row closed -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
    @endsection
    @section('js')
    <!--Internal  Select2 js -->
    <script src="{{URL::asset('public/assets/plugins/select2/js/select2.min.js')}}"></script>
    <!--Internal  Parsley.min js -->
    <script src="{{URL::asset('public/assets/plugins/parsleyjs/parsley.min.js')}}"></script>
    <!-- Internal Form-validation js -->
    <script src="{{URL::asset('public/assets/js/form-validation.js')}}"></script>

    <script src="{{URL::asset('public/assets/plugins/fileuploads/js/fileupload.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fileuploads/js/file-upload.js')}}"></script>
    <!--Internal Fancy uploader js-->
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.ui.widget.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.fileupload.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.iframe-transport.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.fancy-fileupload.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/fancy-uploader.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
    <script type="text/javascript">
         $('.summernote').summernote({
               height: 250,
          });
    </script>
@endsection