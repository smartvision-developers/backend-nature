@extends('admin.layouts.master')
@section('css')
<!-- Internal Data table css -->
<link href="{{URL::asset('public/assets/plugins/datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{URL::asset('public/assets/plugins/datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/datatable/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{URL::asset('public/assets/plugins/datatable/css/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/datatable/css/responsive.dataTables.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">

<style type="text/css">
  .buttons-pdf , .buttons-excel
  {
    display: none !important;
  }

  .main-content-label
  {
     font-size:large !important;
  }
  .btn-icon {
     display: initial !important;
  }
  .chk-option .checkbox-fade
  {
    display: inline-flex !important;
  }
</style>
@endsection
@section('page-header')
        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
          <div class="my-auto">
            <div class="d-flex">
              <h4 class="content-title mb-0 my-auto">الأذونات </h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ عرض كل الأذونات </span>
            </div>
          </div>
          <div class="d-flex my-xl-auto right-content">
            <div class="pr-1 mb-3 mb-xl-0">
              <a href="{{route('permission.create')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i>إضافه إذن جديد </a>
            </div>
     
          </div>
        </div>
        <!-- breadcrumb -->
@endsection
@section('content')

          <!--div-->
          <div class="col-xl-12">
            <div class="card mg-b-20">
              <div class="card-header pb-0">
                <div class="d-flex justify-content-between">
                  <h4 class="card-title mg-b-0">عرض كل الأذونات </h4>
                  <i class="mdi mdi-dots-horizontal text-gray"></i>
                </div>
                <hr>
              </div>
               @if(Session::has('success'))
          <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
          @endif

          @if(Session::has('error'))
          <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
          @endif
              <div class="card-body">
                <div class="table-responsive">
                  <table id="example" class="table key-buttons text-md-nowrap">
                    <thead>
                      <tr>
                          <th class="border-bottom-0 text-center" width="100px;">#</th>
                          <th class="border-bottom-0 text-center" width="200px;">الاسم :</th>
                          <th class="border-bottom-0 text-center" width="200px;">الاسم المستخدم</th>
<!--                           <th class="border-bottom-0 text-center">الوصف</th>
 -->                          <th class="border-bottom-0 text-center">الاجراء</th>
                      </tr>
                    </thead>
                    <tbody>
                        @isset($permissions)
                          @php $count = 1; @endphp
                          @foreach($permissions as $row)
                          <tr>
                            <td class="text-center">{{$count}}</td>
                            <td class="text-center">{{ $row->name }}</td>
                            <td class="text-center">{{ $row->display_name }}</td>
<!--                             <td class="text-center">{{ $row->description }} </td>
 -->                            <td class="text-center">
                              <div class="btn-group">
                                <a class="btn btn-primary" href="{{ route('permission.edit',  $row->id) }}" class="btn btn-info btn-xs"><i class="ti ti-pencil" title="تعديل"></i> </a>
                                <a class="btn btn-danger" href="{{ route('permission.show',  $row->id) }}" class="btn btn-danger btn-xs"><i class="ti ti-trash" title="حذف"></i> </a>
                              </div>
                            </td>
                      </tr>
                      @php $count ++; @endphp
                      @endforeach

                      @endisset
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!--/div-->
        </div>
        <!-- /row -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
@endsection
@section('js')
<!-- Internal Data tables -->
<script src="{{URL::asset('public/assets/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/responsive.dataTables.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/jszip.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
<!--Internal  Datatable js -->
<script src="{{URL::asset('public/assets/js/table-data.js')}}"></script>
@endsection