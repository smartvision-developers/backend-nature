@extends('admin.layouts.master')
@section('css')
<!--- Internal Select2 css-->
<style type="text/css">
form
{
    margin-top: 3rem !important;
}
.main-content-label
{
   font-size:large !important;
}
.btn-icon {
   display: initial !important;
}
</style>
@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="breadcrumb-header justify-content-between">
  <div class="my-auto">
    <div class="d-flex">
      <h4 class="content-title mb-0 my-auto">الأذونات</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ إضافه  إذن جديد</span>
  </div>
</div>
<div class="d-flex my-xl-auto right-content">
    <div class="pr-1 mb-3 mb-xl-0">
      <a href="{{URL('admin/permission')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i> عرض الأذونات</a>
  </div>

</div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')

<!-- row -->
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="card">
      <div class="card-body">
        <div class="main-content-label mg-b-5">
          حذف إذن 
      </div>
      <hr>
    
       <div class="clearfix"></div>
    <h4>هل انت متأكد  من حذف إذن : 
        <strong>{{$permission->name}}</strong>
    </h4>

    <form method="POST" action="{{ route('permission.destroy',$permission->id) }}">
        <input type="hidden" name="_token" value="{{ Session::token() }}">
        <input name="_method" type="hidden" value="DELETE">
        <button type="submit" class="btn btn-danger">نعم  ، متأكد</button>
    </form>




</div>
</div>
</div>
</div>
<!-- row closed -->
</div>
<!-- Container closed -->
</div>
<!-- main-content closed -->
@endsection