@extends('admin.layouts.master')
@section('css')
<!--  Owl-carousel css-->
<link href="{{URL::asset('public/assets/plugins/owl-carousel/owl.carousel.css')}}" rel="stylesheet" />
<!-- Maps css -->
<link href="{{URL::asset('public/assets/plugins/jqvmap/jqvmap.min.css')}}" rel="stylesheet">
<style type="text/css">
  #aadd_almntgat_almdafh_shhrya
  {
    height: 360px !important;
  }
  #aadd_almntgat_dakhl_alaksam
  {
    height: 360px !important;
  }
</style>
@endsection
@section('page-header')
        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
          <div class="left-content">
            <div>
              <h2 class="main-content-title tx-24 mg-b-1 mg-b-lg-1">مرحبا بعودتك !</h2>
              <p class="mg-b-0">احصائيات المتجر .</p>
            </div>
          </div>
        <!--   <div class="main-dashboard-header-right">
            <div>
              <label class="tx-13">Customer Ratings</label>
              <div class="main-star">
                <i class="typcn typcn-star active"></i> <i class="typcn typcn-star active"></i> <i class="typcn typcn-star active"></i> <i class="typcn typcn-star active"></i> <i class="typcn typcn-star"></i> <span>(14,873)</span>
              </div>
            </div>
            <div>
              <label class="tx-13">Online Sales</label>
              <h5>563,275</h5>
            </div>
            <div>
              <label class="tx-13">Offline Sales</label>
              <h5>783,675</h5>
            </div>
          </div> -->
        </div>
        <!-- /breadcrumb -->
@endsection
@section('content')
        <!-- row -->
        <div class="row row-sm">
          <div class="col-lg-6 col-xl-3 col-md-6 col-12">
            <div class="card bg-primary-gradient text-white ">
              <div class="card-body">
                <div class="row">
                  <div class="col-6">
                    <div class="icon1 mt-2 text-center">
                      <i class="fe fe-users tx-40"></i>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="mt-0 text-center">
                      <span class="text-white">المنتجات الحديثه</span>
                      <h2 class="text-white mb-0">{{$count_product_arrival}}</h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-xl-3 col-md-6 col-12">
            <div class="card bg-danger-gradient text-white">
              <div class="card-body">
                <div class="row">
                  <div class="col-6">
                    <div class="icon1 mt-2 text-center">
                      <i class="fe fe-shopping-cart tx-40"></i>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="mt-0 text-center">
                      <span class="text-white" style="font-size:13px;">المنتجات الأكثر مبيعا</span>
                      <h2 class="text-white mb-0">{{$count_product_bestseller}}</h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-xl-3 col-md-6 col-12">
            <div class="card bg-success-gradient text-white">
              <div class="card-body">
                <div class="row">
                  <div class="col-6">
                    <div class="icon1 mt-2 text-center">
                      <i class="fe fe-bar-chart-2 tx-40"></i>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="mt-0 text-center">
                      <span class="text-white">كل  المنتجات</span>
                      <h2 class="text-white mb-0">{{$count_all_products}}</h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-xl-3 col-md-6 col-12">
            <div class="card bg-warning-gradient text-white">
              <div class="card-body">
                <div class="row">
                  <div class="col-6">
                    <div class="icon1 mt-2 text-center">
                      <i class="fe fe-pie-chart tx-40"></i>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="mt-0 text-center">
                      <span class="text-white">كل الأقسام</span>
                      <h2 class="text-white mb-0">{{$count_all_categorys}}</h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- row closed -->

        <!-- row opened -->
        <div class="row row-sm">
          <div class="col-md-12 col-lg-12 col-xl-7">
            <div class="card">
              <div class="card-body">
                    <h4>{{ $chart1->options['chart_title'] }}</h4>
                    {!! $chart1->renderHtml() !!}
              </div>
            </div>
          </div>
          <div class="col-lg-12 col-xl-5">
            <div class="card">
              <div class="card-body">
                    <h4>{{ $chart2->options['chart_title'] }}</h4>
                    {!! $chart2->renderHtml() !!}
              </div>
            </div>
          </div>
        </div>
        <!-- row closed -->

        <!-- row opened -->
     <!--    <div class="row row-sm">
          <div class="col-xl-4 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header pb-1">
                <h3 class="card-title mb-2">Recent Customers</h3>
                <p class="tx-12 mb-0 text-muted">A customer is an individual or business that purchases the goods service has evolved to include real-time</p>
              </div>
              <div class="card-body p-0 customers mt-1">
                <div class="list-group list-lg-group list-group-flush">
                  <div class="list-group-item list-group-item-action" href="#">
                    <div class="media mt-0">
                      <img class="avatar-lg rounded-circle ml-3 my-auto" src="{{URL::asset('public/assets/img/faces/3.jpg')}}" alt="Image description">
                      <div class="media-body">
                        <div class="d-flex align-items-center">
                          <div class="mt-0">
                            <h5 class="mb-1 tx-15">Samantha Melon</h5>
                            <p class="mb-0 tx-13 text-muted">User ID: #1234 <span class="text-success ml-2">Paid</span></p>
                          </div>
                          <span class="mr-auto wd-45p fs-16 mt-2">
                            <div id="spark1" class="wd-100p"></div>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="list-group-item list-group-item-action" href="#">
                    <div class="media mt-0">
                      <img class="avatar-lg rounded-circle ml-3 my-auto" src="{{URL::asset('public/assets/img/faces/11.jpg')}}" alt="Image description">
                      <div class="media-body">
                        <div class="d-flex align-items-center">
                          <div class="mt-1">
                            <h5 class="mb-1 tx-15">Jimmy Changa</h5>
                            <p class="mb-0 tx-13 text-muted">User ID: #1234 <span class="text-danger ml-2">Pending</span></p>
                          </div>
                          <span class="mr-auto wd-45p fs-16 mt-2">
                            <div id="spark2" class="wd-100p"></div>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="list-group-item list-group-item-action" href="#">
                    <div class="media mt-0">
                      <img class="avatar-lg rounded-circle ml-3 my-auto" src="{{URL::asset('public/assets/img/faces/17.jpg')}}" alt="Image description">
                      <div class="media-body">
                        <div class="d-flex align-items-center">
                          <div class="mt-1">
                            <h5 class="mb-1 tx-15">Gabe Lackmen</h5>
                            <p class="mb-0 tx-13 text-muted">User ID: #1234<span class="text-danger ml-2">Pending</span></p>
                          </div>
                          <span class="mr-auto wd-45p fs-16 mt-2">
                            <div id="spark3" class="wd-100p"></div>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="list-group-item list-group-item-action" href="#">
                    <div class="media mt-0">
                      <img class="avatar-lg rounded-circle ml-3 my-auto" src="{{URL::asset('public/assets/img/faces/15.jpg')}}" alt="Image description">
                      <div class="media-body">
                        <div class="d-flex align-items-center">
                          <div class="mt-1">
                            <h5 class="mb-1 tx-15">Manuel Labor</h5>
                            <p class="mb-0 tx-13 text-muted">User ID: #1234<span class="text-success ml-2">Paid</span></p>
                          </div>
                          <span class="mr-auto wd-45p fs-16 mt-2">
                            <div id="spark4" class="wd-100p"></div>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="list-group-item list-group-item-action br-br-7 br-bl-7" href="#">
                    <div class="media mt-0">
                      <img class="avatar-lg rounded-circle ml-3 my-auto" src="{{URL::asset('public/assets/img/faces/6.jpg')}}" alt="Image description">
                      <div class="media-body">
                        <div class="d-flex align-items-center">
                          <div class="mt-1">
                            <h5 class="mb-1 tx-15">Sharon Needles</h5>
                            <p class="b-0 tx-13 text-muted mb-0">User ID: #1234<span class="text-success ml-2">Paid</span></p>
                          </div>
                          <span class="mr-auto wd-45p fs-16 mt-2">
                            <div id="spark5" class="wd-100p"></div>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-md-12 col-lg-6">
            <div class="card">
              <div class="card-header pb-1">
                <h3 class="card-title mb-2">Sales Activity</h3>
                <p class="tx-12 mb-0 text-muted">Sales activities are the tactics that salespeople use to achieve their goals and objective</p>
              </div>
              <div class="product-timeline card-body pt-2 mt-1">
                <ul class="timeline-1 mb-0">
                  <li class="mt-0"> <i class="ti-pie-chart bg-primary-gradient text-white product-icon"></i> <span class="font-weight-semibold mb-4 tx-14 ">Total Products</span> <a href="#" class="float-left tx-11 text-muted">3 days ago</a>
                    <p class="mb-0 text-muted tx-12">1.3k New Products</p>
                  </li>
                  <li class="mt-0"> <i class="mdi mdi-cart-outline bg-danger-gradient text-white product-icon"></i> <span class="font-weight-semibold mb-4 tx-14 ">Total Sales</span> <a href="#" class="float-left tx-11 text-muted">35 mins ago</a>
                    <p class="mb-0 text-muted tx-12">1k New Sales</p>
                  </li>
                  <li class="mt-0"> <i class="ti-bar-chart-alt bg-success-gradient text-white product-icon"></i> <span class="font-weight-semibold mb-4 tx-14 ">Toatal Revenue</span> <a href="#" class="float-left tx-11 text-muted">50 mins ago</a>
                    <p class="mb-0 text-muted tx-12">23.5K New Revenue</p>
                  </li>
                  <li class="mt-0"> <i class="ti-wallet bg-warning-gradient text-white product-icon"></i> <span class="font-weight-semibold mb-4 tx-14 ">Toatal Profit</span> <a href="#" class="float-left tx-11 text-muted">1 hour ago</a>
                    <p class="mb-0 text-muted tx-12">3k New profit</p>
                  </li>
                  <li class="mt-0"> <i class="si si-eye bg-purple-gradient text-white product-icon"></i> <span class="font-weight-semibold mb-4 tx-14 ">Customer Visits</span> <a href="#" class="float-left tx-11 text-muted">1 day ago</a>
                    <p class="mb-0 text-muted tx-12">15% increased</p>
                  </li>
                  <li class="mt-0 mb-0"> <i class="icon-note icons bg-primary-gradient text-white product-icon"></i> <span class="font-weight-semibold mb-4 tx-14 ">Customer Reviews</span> <a href="#" class="float-left tx-11 text-muted">1 day ago</a>
                    <p class="mb-0 text-muted tx-12">1.5k reviews</p>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-md-12 col-lg-6">
            <div class="card">
              <div class="card-header pb-0">
                <h3 class="card-title mb-2">Recent Orders</h3>
                <p class="tx-12 mb-0 text-muted">An order is an investor's instructions to a broker or brokerage firm to purchase or sell</p>
              </div>
              <div class="card-body sales-info ot-0 pt-0 pb-0">
                <div id="chart" class="ht-150"></div>
                <div class="row sales-infomation pb-0 mb-0 mx-auto wd-100p">
                  <div class="col-md-6 col">
                    <p class="mb-0 d-flex"><span class="legend bg-primary brround"></span>Delivered</p>
                    <h3 class="mb-1">5238</h3>
                    <div class="d-flex">
                      <p class="text-muted ">Last 6 months</p>
                    </div>
                  </div>
                  <div class="col-md-6 col">
                    <p class="mb-0 d-flex"><span class="legend bg-info brround"></span>Cancelled</p>
                      <h3 class="mb-1">3467</h3>
                    <div class="d-flex">
                      <p class="text-muted">Last 6 months</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card ">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="d-flex align-items-center pb-2">
                      <p class="mb-0">Total Sales</p>
                    </div>
                    <h4 class="font-weight-bold mb-2">$7,590</h4>
                    <div class="progress progress-style progress-sm">
                      <div class="progress-bar bg-primary-gradient wd-80p" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="78"></div>
                    </div>
                  </div>
                  <div class="col-md-6 mt-4 mt-md-0">
                    <div class="d-flex align-items-center pb-2">
                      <p class="mb-0">Active Users</p>
                    </div>
                    <h4 class="font-weight-bold mb-2">$5,460</h4>
                    <div class="progress progress-style progress-sm">
                      <div class="progress-bar bg-danger-gradient wd-75" role="progressbar"  aria-valuenow="45" aria-valuemin="0" aria-valuemax="45"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> -->
        <!-- row close -->

        <!-- row opened -->
        <div class="row row-sm row-deck">
          <div class="col-md-12 col-lg-4 col-xl-4">
            <div class="card card-dashboard-eight pb-2">
              <h4 class="card-title">المنتجات المفضله</h4><span class="d-block mg-b-10 text-muted tx-12">المنتجات المفضله من قبل المستخدمين</span>
              <div class="list-group">
                @foreach($product_favs as $key=> $value)
                <div class="list-group-item border-top-0">
                  @php $prod=\App\Models\Product::where('id',$value->product_id )->first(); @endphp
                  <img src="{{URL('/public/images/products/'.$prod->ProductImage[0]->product_image)}}" style="   height: 40px;
    width: 23%;
object-fit: contain;">
                  <p>{{$prod->product_name_ar}}</p><span><a href="{{URL('admin/products/'.$value->product_id.'/profile')}}">تفاصيل المنتج </a></span>
                </div>
                @endforeach
           
              </div>
            </div>
          </div>
            <div class="col-md-12 col-lg-8 col-xl-8">
            <div class="card">
              <div class="card-body">
                    <h4>{{ $chart3->options['chart_title'] }}</h4>
                    {!! $chart3->renderHtml() !!}
              </div>
            </div>
          </div>
        <!--   <div class="col-md-12 col-lg-8 col-xl-8">
            <div class="card card-table-two">
              <div class="d-flex justify-content-between">
                <h4 class="card-title mb-1">Your Most Recent Earnings</h4>
                <i class="mdi mdi-dots-horizontal text-gray"></i>
              </div>
              <span class="tx-12 tx-muted mb-3 ">This is your most recent earnings for today's date.</span>
              <div class="table-responsive country-table">
                <table class="table table-striped table-bordered mb-0 text-sm-nowrap text-lg-nowrap text-xl-nowrap">
                  <thead>
                    <tr>
                      <th class="wd-lg-25p">Date</th>
                      <th class="wd-lg-25p tx-right">Sales Count</th>
                      <th class="wd-lg-25p tx-right">Earnings</th>
                      <th class="wd-lg-25p tx-right">Tax Witheld</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>05 Dec 2019</td>
                      <td class="tx-right tx-medium tx-inverse">34</td>
                      <td class="tx-right tx-medium tx-inverse">$658.20</td>
                      <td class="tx-right tx-medium tx-danger">-$45.10</td>
                    </tr>
                    <tr>
                      <td>06 Dec 2019</td>
                      <td class="tx-right tx-medium tx-inverse">26</td>
                      <td class="tx-right tx-medium tx-inverse">$453.25</td>
                      <td class="tx-right tx-medium tx-danger">-$15.02</td>
                    </tr>
                    <tr>
                      <td>07 Dec 2019</td>
                      <td class="tx-right tx-medium tx-inverse">34</td>
                      <td class="tx-right tx-medium tx-inverse">$653.12</td>
                      <td class="tx-right tx-medium tx-danger">-$13.45</td>
                    </tr>
                    <tr>
                      <td>08 Dec 2019</td>
                      <td class="tx-right tx-medium tx-inverse">45</td>
                      <td class="tx-right tx-medium tx-inverse">$546.47</td>
                      <td class="tx-right tx-medium tx-danger">-$24.22</td>
                    </tr>
                    <tr>
                      <td>09 Dec 2019</td>
                      <td class="tx-right tx-medium tx-inverse">31</td>
                      <td class="tx-right tx-medium tx-inverse">$425.72</td>
                      <td class="tx-right tx-medium tx-danger">-$25.01</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div> -->
        </div>
        <!-- /row -->
      </div>
    </div>
    <!-- Container closed -->
@endsection
@section('js')
<!--Internal  Chart.bundle js -->
<script src="{{URL::asset('public/assets/plugins/chart.js/Chart.bundle.min.js')}}"></script>
<!-- Moment js -->
<script src="{{URL::asset('public/assets/plugins/raphael/raphael.min.js')}}"></script>
<!--Internal  Flot js-->
<script src="{{URL::asset('public/assets/plugins/jquery.flot/jquery.flot.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/jquery.flot/jquery.flot.pie.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/jquery.flot/jquery.flot.resize.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/jquery.flot/jquery.flot.categories.js')}}"></script>
<script src="{{URL::asset('public/assets/js/dashboard.sampledata.js')}}"></script>
<script src="{{URL::asset('public/assets/js/chart.flot.sampledata.js')}}"></script>
<!--Internal Apexchart js-->
<script src="{{URL::asset('public/assets/js/apexcharts.js')}}"></script>
<!-- Internal Map -->
<script src="{{URL::asset('public/assets/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
<script src="{{URL::asset('public/assets/js/modal-popup.js')}}"></script>
<!--Internal  index js -->
<script src="{{URL::asset('public/assets/js/index.js')}}"></script>
<script src="{{URL::asset('public/assets/js/jquery.vmap.sampledata.js')}}"></script> 

{!! $chart1->renderChartJsLibrary() !!}
{!! $chart2->renderChartJsLibrary() !!}
{!! $chart3->renderChartJsLibrary() !!}

{!! $chart1->renderJs() !!}
{!! $chart2->renderJs() !!}
{!! $chart3->renderJs() !!}

@endsection