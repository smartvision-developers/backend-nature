@extends('admin.layouts.master')
@section('css')
<!--- Internal Select2 css-->
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('public/assets/plugins/fancyuploder/fancy_fileupload.css')}}" rel="stylesheet" />
<link href="{{URL::asset('public/assets/plugins/quill/quill.snow.css')}}" rel="stylesheet">

<style type="text/css">
form
{
  margin-top: 3rem !important;
}
.main-content-label
{
 font-size:large !important;
}
.btn-icon {
 display: initial !important;
}
.bg-gray-100 {
  background-color: #fbfbfb47 !important;
}
hr {
    margin-top: 4rem !important;
    margin-bottom: 4rem !important;
    }

select
{
      height: 595px !important;
}
option
{
  padding: 8px !important;
}
}
</style>
@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="breadcrumb-header justify-content-between">
  <div class="my-auto">
    <div class="d-flex">
      <h4 class="content-title mb-0 my-auto">المنتجات الحديثه</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ إضافه منتج جديد</span>
    </div>
  </div>
  <div class="d-flex my-xl-auto right-content">
    <div class="pr-1 mb-3 mb-xl-0">
      <a href="{{URL('admin/categorys')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i> عرض المنتجات الحديثه</a>
    </div>

  </div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')

<!-- row -->
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="card">
      <div class="card-body">
        <div class="main-content-label mg-b-5">
          إضافه منتج وصل حديثا جديد
        </div>
        <hr>
        @if(Session::has('success'))
        <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
        @endif
        <form action="{{route('product_arrivals.store')}}" enctype="multipart/form-data" class="parsley-style-1" id="selectForm2" name="selectForm2"  method="POST">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="">
            <div class="wd-250" id="cbErrorContainer2"></div>

            <div class="parsley-select wd-500 mg-t-30" id="slWrapper2">
             <label>اختر  المنتجات: <span class="tx-danger">*</span></label>
                  <select name="product_id[]" id="ingredients" multiple="multiple" class="form-control " style="overflow:auto;" data-parsley-class-handler="#slWrapper2" data-placeholder="اختر قسم المنتج" required="">
                    <option value="all" style="    font-size: 18px;
    color: red !important;
    font-weight: bolder;">الكل</option>

                  @foreach($products as $product)
                        <option value="{{$product->id}}">{{$product->product_name_ar}}</option>

                    @endforeach
                    </select>
              <div id="slErrorContainer2"></div>
            </div>
  
          </div>
                  <hr>
                    <div class="mg-t-30">
                    <button class="btn btn-main-primary pd-x-20" type="submit">إضافه</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- row closed -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
    @endsection
    @section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {

   // console.log('fdff');
        $('#ingredients').multiselect({});
    });
</script>

@endsection