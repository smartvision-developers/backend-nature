@extends('admin.layouts.master')
@section('css')
<!-- Internal Data table css -->
<link href="{{URL::asset('public/assets/plugins/datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{URL::asset('public/assets/plugins/datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/datatable/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{URL::asset('public/assets/plugins/datatable/css/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/datatable/css/responsive.dataTables.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">

<style type="text/css">
  .main-content-label
  {
   font-size:large !important;
  }
  .btn-icon {
   display: initial !important;
  }
  .select2-container--default .select2-selection--multiple {
    width: 360px !important;
  }
  .row-comment
  {
    margin-top: 30px;border:1px solid #dedede; margin-left: 0px; margin-bottom: 20px;width: 80%;
  }
</style>
@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="breadcrumb-header justify-content-between">
  <div class="my-auto">
    <div class="d-flex">
      <h4 class="content-title mb-0 my-auto">المقالات</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ عرض تفاصيل المقاله</span>
    </div>
  </div>
  <div class="d-flex my-xl-auto right-content">
    <div class="pr-1 mb-3 mb-xl-0">
      <a href="{{URL('admin/blogs/create')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i>إضافه مقال</a>
    </div>
    
  </div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')

<!--div-->
<div class="col-xl-12">
  <div class="card mg-b-20">
    <div class="card-header pb-0">
      <div class="d-flex justify-content-between">
        <h4 class="card-title mg-b-0">عرض تفاصيل المقاله</h4>
          <a class="btn btn-info" href="{{URL('/blog/'.$blog->blog_title_ar)}}">مشاهده داخل الموقع </a>
        <i class="mdi mdi-dots-horizontal text-gray"></i>
      </div>
      <hr>
    </div>
    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
    @endif

    @if(Session::has('error'))
    <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
    @endif
    <div class="card-body">
      <div class="panel panel-primary tabs-style-3">
        <div class="tab-menu-heading">
          <div class="tabs-menu ">
            <!-- Tabs -->
            <ul class="nav panel-tabs">
              <li class=""><a href="#tab11" class="active" data-toggle="tab"><i class="fa fa-laptop"></i>تفاصيل المقاله باللغه العربيه</a></li>
              <li><a href="#tab12" data-toggle="tab"><i class="fa fa-cube"></i> تفاصيل المقاله باللغه الانجليزيه</a></li>
              <li><a href="#tab13" data-toggle="tab"><i class="fa fa-cogs"></i> التعليقات</a></li>
              <li><a href="#tab14" data-toggle="tab"><i class="fa fa-tasks"></i> الكلمات الدلاليه للمقاله</a></li>
            </ul>
          </div>
        </div>
        <div class="panel-body tabs-menu-body">
          <div class="tab-content">
            <div class="tab-pane active" id="tab11">
              <div class="row">
                <div class="col-md-6">
                  <img src="{{URL($blog->blog_image)}}">
                </div>
                <div class="col-md-6">
                  <h3>عنوان المقاله : {{$blog->blog_title_ar}}</h3>
                  <h5>وصف بسيط للمقاله : </h5>   {{$blog->blog_text_ar}}
                </div>
                <div class="col-lg-12">
                  <br>
                  <hr>
                </div>
                <div class="col-md-4">
                  <h3>          محتوي المقاله : </h3>
                </div>
                <div class="col-md-8">
                  <?php echo $blog->blog_description_ar ;?>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tab12">
             <div class="row">
              <div class="col-md-6">
                <img src="{{URL($blog->blog_image)}}">
              </div>
              <div class="col-md-6">
                <h3>عنوان المقاله : {{$blog->blog_title_en}}</h3>
                <h5>وصف بسيط للمقاله : </h5>   {{$blog->blog_text_en}}
              </div>
              <div class="col-lg-12">
                <br>
                <hr>
              </div>
              <div class="col-md-4">
                <h3>          محتوي المقاله : </h3>
              </div>
              <div class="col-md-8">
                <?php echo $blog->blog_description_en ;?>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="tab13">
            <h3>         عدد التعليقات :  {{$blog_comments->count()}} تعليق</h3>
            <hr>
            @if($blog_comments->count() == 0)
            <p>لاتوجد تعليقات حتي الان </p>
            @else
            
            @foreach($blog_comments as $comment)
            
            <div class="row row-comment">
             <div class="col-md-2">
               <img src="{{asset('/')}}{{$comment->Customer->photo}}" style="width:80%;">
             </div>
             <div class="col-md-8">
              <h4>{{$comment->Customer->name}}</h4>
              <p>{{$comment->comment }}</p>
            </div>
          </div>
          @endforeach
          @endif
        </div>
        <div class="tab-pane" id="tab14">

          <form action="{{route('admin.addTag', $blog->id)}}" method="post">
            @csrf
            <div class="row mg-b-20">
              <div class="parsley-input col-md-6" id="tag_ar">
                <label>عنوان المقال باللغه العربيه: <span class="tx-danger">*</span></label>
                <select data-parsley-class-handler="#tag_ar" required="" class="form-control select2" name="blog_tag_id[]" multiple="multiple">
                  @foreach($tags as $tag)

                  <option value="{{$tag->id}}"                           @foreach($blog_tags as $data) @if($data->blog_tag_id == $tag->id) selected @endif @endforeach>{{$tag->tag_ar}}</option>
                  @endforeach
                </select>
                @error('tag_ar')
                <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                @enderror
              </div>
              
            </div>   

            <div class="mg-t-30">
              <button class="btn btn-main-primary pd-x-20" type="submit">حفظ</button>
              <a class="btn btn-info" href="{{URL('/blog/'.$blog->blog_title_ar.'#tags_section')}}">مشاهده داخل الموقع </a>
            </div>       
          </form>
        </div>
      </div>
    </div>
  </div>

</div>
</div>
</div>
<!--/div-->
</div>
<!-- /row -->
</div>
<!-- Container closed -->
</div>
<!-- main-content closed -->
@endsection
@section('js')
<!--Internal  Select2 js -->
<script src="{{URL::asset('public/assets/plugins/select2/js/select2.min.js')}}"></script>
<!--Internal  Parsley.min js -->
<script src="{{URL::asset('public/assets/plugins/parsleyjs/parsley.min.js')}}"></script>
<!-- Internal Form-validation js -->
<script src="{{URL::asset('public/assets/js/form-validation.js')}}"></script>
@endsection