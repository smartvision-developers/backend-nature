@extends('admin.layouts.master')
@section('css')
<!--- Internal Select2 css-->
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('public/assets/plugins/fancyuploder/fancy_fileupload.css')}}" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
<style type="text/css">
form
{
  margin-top: 3rem !important;
}
.main-content-label
{
 font-size:large !important;
}
.btn-icon {
 display: initial !important;
}
.bg-gray-100 {
  background-color: #fbfbfb47 !important;
}
hr {
    margin-top: 4rem !important;
    margin-bottom: 4rem !important;
    }

.select2
{
      width: 595px !important;
/*      margin-bottom: 20px !important;
*/}
.mg-t-30
{
  margin-top: 130px !important;
}
.img-subnail
{
      width: auto;
   height: 55%;
    margin-right: 17%;
    border: 1px solid #eceff6;
    margin-top: 29px;
    padding: 11px;
}

</style>

@endsection
@section('page-header')
        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
          <div class="my-auto">
            <div class="d-flex">
              <h4 class="content-title mb-0 my-auto">المقالات</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ تعديل بيانات المقال</span>
            </div>
          </div>
          <div class="d-flex my-xl-auto right-content">
            <div class="pr-1 mb-3 mb-xl-0">
              <a href="{{URL('admin/blogs')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i> عرض المقالات</a>
            </div>
          
          </div>
        </div>
        <!-- breadcrumb -->
@endsection
@section('content')

        <!-- row -->
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="card">
              <div class="card-body">
                <div class="main-content-label mg-b-5">
                  تعديل بيانات  / {{$blog->blog_title_ar}}
                </div>
                <hr>
                 @if(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
                @endif
                 <form method="POST" class="parsley-style-1" id="selectForm2" name="selectForm2" enctype="multipart/form-data" role="form" action="{{route('blogs.update', $blog->id)}}">
                    @csrf
                  {{ method_field('PUT') }}

                  <div class="">
                    <div class="row mg-b-20">
                      <div class="parsley-input col-md-6" id="blog_title_ar">
                        <label>عنوان المقال باللغه العربيه: <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#blog_title_ar" name="blog_title_ar" value="{{$blog->blog_title_ar}}" placeholder="Enter firstname" required="" type="text">
                           @error('blog_title_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="blog_title_en">
                        <label>عنوان المقال باللغه الانجليزيه: <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#blog_title_en" value="{{$blog->blog_title_en}}" name="blog_title_en" placeholder="Enter lastname" required="" type="text">
                           @error('blog_title_en')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>
          <hr>

          <div class="row mb-4">
            <div class="col-sm-6 col-md-6">
               <label>صوره المقال: <span class="tx-danger">*</span></label>

              <input class="dropify" data-height="200"  type="file" name="blog_image" accept=".jpg, .png, image/jpeg, image/png">
            </div>
                    @if($blog->blog_image != null)
                 <div class="col-sm-6 col-md-6">
              <label for="img"></label>
                <img src="{{asset($blog->blog_image)}}" class=" img-subnail" alt="">
            </div>

                            @endif
          </div>
                  <hr>

            
                  <div style="height: 270px;">
                    <div class="row mg-b-20" >
                      <div class="col-sm-6">
                        <label>وصف بسيط المقال باللغه العربيه: <span class="tx-danger">*</span></label>

                        <textarea class="form-control" rows="10" name="blog_text_ar"><?php echo $blog->blog_text_ar?>
                        </textarea>


                        @error('blog_text_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="col-sm-6">
                        <label>وصف بسيط المقال باللغه الانجليزيه: <span class="tx-danger">*</span></label>

                        <textarea name="blog_text_en" rows="10" class="form-control">{{$blog->blog_text_en}}
                        </textarea>
                        @error('blog_text_en')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>
        <hr>

                <div style="height: 270px;">
                    <div class="row mg-b-20" >
                      <div class="col-sm-6">
                        <label>وصف مفصل المقال باللغه العربيه: <span class="tx-danger">*</span></label>

                        <textarea class="summernote" id="blog_description_ar" name="blog_description_ar"><?php echo $blog->blog_description_ar;?>
                        </textarea>


                        @error('blog_description_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="col-sm-6">
                        <label>وصف مفصل المقال باللغه الانجليزيه: <span class="tx-danger">*</span></label>

                        <textarea name="blog_description_en" class="summernote" id="blog_description_en"><?php echo $blog->blog_description_en ;?>
                        </textarea>
                        @error('blog_description_en')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>

        <hr>

                <div class="mg-t-30">
                    <button class="btn btn-main-primary pd-x-20" type="submit">حفظ</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- row closed -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
    @endsection
    @section('js')
    <!--Internal  Select2 js -->
    <script src="{{URL::asset('public/assets/plugins/select2/js/select2.min.js')}}"></script>
    <!--Internal  Parsley.min js -->
    <script src="{{URL::asset('public/assets/plugins/parsleyjs/parsley.min.js')}}"></script>
    <!-- Internal Form-validation js -->
    <script src="{{URL::asset('public/assets/js/form-validation.js')}}"></script>

    <script src="{{URL::asset('public/assets/plugins/fileuploads/js/fileupload.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fileuploads/js/file-upload.js')}}"></script>
    <!--Internal Fancy uploader js-->
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.ui.widget.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.fileupload.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.iframe-transport.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.fancy-fileupload.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/fancy-uploader.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
    <script type="text/javascript">
         $('.summernote').summernote({
               height: 300,
          });
    </script>
@endsection