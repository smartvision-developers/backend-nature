@extends('admin.layouts.master')
@section('css')
<!--- Internal Select2 css-->
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">


<style type="text/css">
  .buttons-pdf , .buttons-excel
  {
    display: none !important;
  }

  .main-content-label
  {
     font-size:large !important;
  }
  .btn-icon {
     display: initial !important;
  }
  .chk-option .checkbox-fade
  {
    display: inline-flex !important;
  }
</style>
@endsection
@section('page-header')
        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
          <div class="my-auto">
            <div class="d-flex">
              <h4 class="content-title mb-0 my-auto">المقالات</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ عرض كل المقالات</span>
            </div>
          </div>
          <div class="d-flex my-xl-auto right-content">
            @permission(['create-blog'])
            <div class="pr-1 mb-3 mb-xl-0">
              <a href="{{URL('admin/blogs/create')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i>إضافه مقال</a>
            </div>
            @endpermission
          </div>
        </div>
        <!-- breadcrumb -->
@endsection
@section('content')

          <!--div-->
          <div class="col-xl-12">
            <div class="card mg-b-20">
              <div class="card-header pb-0">
                <div class="d-flex justify-content-between">
                  <h4 class="card-title mg-b-0">عرض كل المقالات</h4>
                  <i class="mdi mdi-dots-horizontal text-gray"></i>
                </div>
                <hr>
              </div>
               @if(Session::has('success'))
          <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
          @endif

          @if(Session::has('error'))
          <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
          @endif
              <div class="card-body">
                <div class="table-responsive">
                  <table id="example" class="table key-buttons text-md-nowrap">
                    <thead>
                      <tr>
                          <th class="border-bottom-0 text-center">#</th>
                          <th class="border-bottom-0 text-center">اسم المقال باللغه العربيه</th>
                         <th class="border-bottom-0 text-center">صوره المقال </th>
                         @permission(['activate-blog'])
                          <th class="border-bottom-0 text-center">حاله المقال</th>
                          @endpermission
                          <th class="border-bottom-0 text-center">أنشأ في</th>
                          @permission(['read-blog'])
                          <th class="border-bottom-0 text-center">تفاصيل</th>
                          @endpermission
                          <th class="border-bottom-0 text-center">إجراء</th>
                          
                           <th class="border-bottom-0 text-center"> </th>
                      </tr>
                    </thead>
                    <tbody>
                        @isset($blogs)
                          @php $count = 1; @endphp
                          @foreach($blogs as $data)
                          <tr>

                            <td class="text-center">{{$count}}</td>
                            <td class="text-center">{{$data->blog_title_ar}}</td>
                            <td class="text-center"><img width="160px" height="150px" src="{{URL($data->blog_image)}}">
                            </td>
                          @permission(['activate-blog'])
                            <td class="text-center">
                              @if($data->status == 0)
                              <form method="POST" action="{{url('/admin/blogs/'.$data->id.'/activate')}}">
                                @csrf
                                
                                <div class="chk-option">
                                  <div class="checkbox-fade fade-in-primary"> 
                                       <label class="ckbox mg-b-5"><input name="status" type="checkbox" onchange="this.form.submit()" ><span></span></label>

                                   </div>
                                 </div>
                               </form>
                               @else
                               <form method="POST" action="{{url('/admin/blogs/'.$data->id.'/deactivate')}}">
                                @csrf

                                <div class="chk-option">
                                  <div class="checkbox-fade fade-in-primary"> 
                     <label class="ckbox mg-b-5"><input name="status" type="checkbox" onchange="this.form.submit()" checked><span></span></label>

                                 </div>
                               </div>
                             </form>
                             @endif
                           </td>
                           @endpermission
                           <td class="text-center"> {{ ($data->created_at)->toDayDateTimeString()}}</td>
                           @permission(['read-blog'])
                              <td class="text-center">
                             <a href="{{url('admin/blogs/'.$data->id.'/profile')}}" class="btn btn-warning ti-more"></a>
                           </td>
                           @endpermission
                           <td class="text-center">
                             @permission(['edit-blog'])
                             <a href="{{url('admin/blogs/'.$data->id.'/edit')}}" class="btn btn-primary ti-pencil-alt"></a>
                             @endpermission

                             @permission(['delete-blog'])

                            <form method="post" action="{{url('admin/blogs/'.$data->id)}}">
                              {{csrf_field()}}
                              <a class="btn btn-danger ti-trash" data-id="{{$data->id}}" data-toggle="modal" data-target="#delete-data{{$data->id}}"></a>
                              <!-- Modal -->
                              <div class="modal modal-danger fade" id="delete-data{{$data->id}}" role="dialog">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                   <div class="modal-header" style="    display: inline;">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title text-center" style="float: right;" id="exampleModalLabel">تأكيد الحذف</h4>
                                  </div>

                                  <div class="modal-body">
                                    <h5 class="text-center ">هل انت متأكد من حذف   {{$data->blog_title_ar}}؟</h5>                  
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-outline"  data-dismiss="modal">إلغاء</button>
                                    <input type="hidden" name="_method" value="delete">

                                    <button class="btn btn-outline danger delete pull-right">حذف</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                                                                               @endpermission

                          </form>     
                        </td>
                        <td class="text-center"></td>

                      </tr>

                      @php $count ++; @endphp
                      @endforeach

                      @endisset
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!--/div-->
        </div>
        <!-- /row -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
@endsection
@section('js')
<!-- Internal Data tables -->
<script src="{{URL::asset('public/assets/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/responsive.dataTables.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/jszip.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
<!--Internal  Datatable js -->
<script src="{{URL::asset('public/assets/js/table-data.js')}}"></script>
@endsection