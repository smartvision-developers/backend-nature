@extends('admin.layouts.master')
@section('css')
<!--- Internal Select2 css-->
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('public/assets/plugins/fancyuploder/fancy_fileupload.css')}}" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">

<style type="text/css">
form
{
  margin-top: 3rem !important;
}
.main-content-label
{
 font-size:large !important;
}
.btn-icon {
 display: initial !important;
}
.bg-gray-100 {
  background-color: #fbfbfb47 !important;
}
hr {
    margin-top: 4rem !important;
    margin-bottom: 4rem !important;
    }

.select2
{
      width: 595px !important;
/*      margin-bottom: 20px !important;
*/}
.mg-t-30
{
  margin-top: 130px !important;
}
}
</style>
@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="breadcrumb-header justify-content-between">
  <div class="my-auto">
    <div class="d-flex">
      <h4 class="content-title mb-0 my-auto">المقالات</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ إضافه مقال جديد</span>
    </div>
  </div>
  <div class="d-flex my-xl-auto right-content">
    <div class="pr-1 mb-3 mb-xl-0">
      <a href="{{URL('admin/blogs')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i> عرض المقالات</a>
    </div>

  </div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')

<!-- row -->
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="card">
      <div class="card-body">
        <div class="main-content-label mg-b-5">
          إضافه مقال جديد
        </div>
        <hr>
        @if(Session::has('success'))
        <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
        @endif
        <form action="{{route('blogs.store')}}" enctype="multipart/form-data" class="parsley-style-1" id="selectForm2" name="selectForm2"  method="POST">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="">
            <div class="row mg-b-20">
              <div class="parsley-input col-md-6" id="blog_title_ar">
                <label>عنوان المقال باللغه العربيه: <span class="tx-danger">*</span></label>
                <input class="form-control" data-parsley-class-handler="#blog_title_ar" name="blog_title_ar" value="{{old('blog_title_ar')}}" placeholder="ادخل عنوان المقال باللغه العربيه" required="" type="text">
                @error('blog_title_ar')
                <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                @enderror
              </div>
              <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="blog_title_en">
                <label>عنوان المقال باللغه الانجليزيه: <span class="tx-danger">*</span></label>
                <input class="form-control" data-parsley-class-handler="#blog_title_en" name="blog_title_en" value="{{old('blog_title_en')}}" placeholder="ادخل عنوان المقال باللغه الانجليزيه" required="" type="text">
                @error('blog_title_en')
                <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                @enderror
              </div>
            </div>
          </div>
        
        <hr>

          <div class="row mb-4">
              <div class="parsley-input col-md-12 mg-t-20 mg-md-t-0" id="blog_image">
               <label>صور المقال: <span class="tx-danger">*</span></label>

              <input data-parsley-class-handler="#blog_image"  required="" class="dropify" data-height="200"  type="file" name="blog_image" accept=".jpg, .png, image/jpeg, image/png">
            </div>
          </div>
                  <hr>

            
                  <div style="height: 270px;">
                    <div class="row mg-b-20" >
                      <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="blog_text_ar">
                        <label>وصف بسيط المقال باللغه العربيه: <span class="tx-danger">*</span></label>

                        <textarea data-parsley-class-handler="#blog_text_ar"  rows="10" class="form-control" name="blog_text_ar" required="">
                      {{old('blog_text_ar')}}
                        </textarea>


                        @error('blog_text_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="blog_text_en">
                        <label>وصف بسيط المقال باللغه الانجليزيه: <span class="tx-danger">*</span></label>

                        <textarea rows="10" data-parsley-class-handler="#blog_text_en" name="blog_text_en" required="" class="form-control">
                        {{old('blog_text_en')}}
                        </textarea>
                        @error('blog_text_en')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>
        <hr>

                <div style="height: 270px;">
                    <div class="row mg-b-20" >
                      <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="blog_description_ar" >
                        <label>وصف مفصل المقال باللغه العربيه: <span class="tx-danger">*</span></label>

                        <textarea class="summernote" data-parsley-class-handler="#blog_description_ar" required="" name="blog_description_ar">{{old('blog_description_en')}}
                        </textarea>


                        @error('blog_description_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="blog_description_en">
                        <label>وصف مفصل المقال باللغه الانجليزيه: <span class="tx-danger">*</span></label>

                        <textarea data-parsley-class-handler="#blog_description_en" name="blog_description_en" class="summernote" required="">{{old('blog_description_ar')}}
                        </textarea>
                        @error('blog_description_en')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>

        <hr>

                  <div class="mg-t-30">
                    <button class="btn btn-main-primary pd-x-20" type="submit">إضافه</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- row closed -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
    @endsection
    @section('js')
    <!--Internal  Select2 js -->
    <script src="{{URL::asset('public/assets/plugins/select2/js/select2.min.js')}}"></script>
    <!--Internal  Parsley.min js -->
    <script src="{{URL::asset('public/assets/plugins/parsleyjs/parsley.min.js')}}"></script>
    <!-- Internal Form-validation js -->
    <script src="{{URL::asset('public/assets/js/form-validation.js')}}"></script>

    <script src="{{URL::asset('public/assets/plugins/fileuploads/js/fileupload.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fileuploads/js/file-upload.js')}}"></script>
    <!--Internal Fancy uploader js-->
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.ui.widget.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.fileupload.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.iframe-transport.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.fancy-fileupload.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/fancy-uploader.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
    <script type="text/javascript">
         $('.summernote').summernote({
               height: 300,
          });
    </script>
@endsection