@extends('admin.layouts.master')
@section('css')
<!--- Internal Select2 css-->
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">

<style type="text/css">
  form
  {
    margin-top: 3rem !important;
  }
  .main-content-label
  {
     font-size:large !important;
  }
  .btn-icon {
     display: initial !important;
  }
</style>
@endsection
@section('page-header')
        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
          <div class="my-auto">
            <div class="d-flex">
              <h4 class="content-title mb-0 my-auto">أراء عملائنا</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ إضافه رأي جديد</span>
            </div>
          </div>
          <div class="d-flex my-xl-auto right-content">
            <div class="pr-1 mb-3 mb-xl-0">
              <a href="{{URL('admin/customer_reviews')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i> عرض أراء عملائنا</a>
            </div>
          
          </div>
        </div>
        <!-- breadcrumb -->
@endsection
@section('content')

        <!-- row -->
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="card">
              <div class="card-body">
                <div class="main-content-label mg-b-5">
                  إضافه رأي جديد
                </div>
                <hr>
                 @if(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
                @endif
                <form action="{{route('customer_reviews.update', $customer_review->id)}}" class="parsley-style-1" id="selectForm2" name="selectForm2"  method="POST">
                  {{ method_field('PUT') }}

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                  <div class="">
                    <div class="row mg-b-20">
                      <div class="parsley-input col-md-6" id="customer_name">
                        <label>اسم العميل: <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#customer_name" name="customer_name" value="{{ $customer_review->customer_name}}" placeholder="ادخل اسم العميل" required="" type="text">
                           @error('customer_name')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="job_title">
                        <label>مهنه العميل: <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#job_title" name="job_title" value="{{ $customer_review->job_title}}" placeholder="ادخل مهنه العميل" required="" type="text">
                           @error('job_title')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>
               
                    <div class="row mg-b-20" >
                      <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="customer_review" >
                        <label>رأي العميل: <span class="tx-danger">*</span></label>

                        <textarea class="summernote" data-parsley-class-handler="#customer_review" required="" name="customer_review">
                          {{ $customer_review->customer_review}}
                        </textarea>


                        @error('customer_review')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                                       </div>
                <div class="mg-t-30">
                    <button class="btn btn-main-primary pd-x-20" type="submit">إضافه</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- row closed -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
@endsection
@section('js')
<!--Internal  Select2 js -->
<script src="{{URL::asset('public/assets/plugins/select2/js/select2.min.js')}}"></script>
<!--Internal  Parsley.min js -->
<script src="{{URL::asset('public/assets/plugins/parsleyjs/parsley.min.js')}}"></script>
<!-- Internal Form-validation js -->
<script src="{{URL::asset('public/assets/js/form-validation.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
    <script type="text/javascript">
         $('.summernote').summernote({
               height: 250,
          });
    </script>
@endsection