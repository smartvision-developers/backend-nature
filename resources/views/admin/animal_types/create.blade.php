@extends('admin.layouts.master')
@section('css')
<!--- Internal Select2 css-->
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('public/assets/plugins/fancyuploder/fancy_fileupload.css')}}" rel="stylesheet" />

<style type="text/css">
  form
  {
    margin-top: 3rem !important;
  }
  .main-content-label
  {
     font-size:large !important;
  }
  .btn-icon {
     display: initial !important;
  }
</style>
@endsection
@section('page-header')
        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
          <div class="my-auto">
            <div class="d-flex">
              <h4 class="content-title mb-0 my-auto">أنواع الحيوانات</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ إضافه نوع الحيوان جديد</span>
            </div>
          </div>
          <div class="d-flex my-xl-auto right-content">
            <div class="pr-1 mb-3 mb-xl-0">
              <a href="{{URL('admin/animal_types')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i> عرض أنواع الحيوانات</a>
            </div>
          
          </div>
        </div>
        <!-- breadcrumb -->
@endsection
@section('content')

        <!-- row -->
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="card">
              <div class="card-body">
                <div class="main-content-label mg-b-5">
                  إضافه نوع الحيوان جديد
                </div>
                <hr>
                 @if(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
                @endif
                <form action="{{route('animal_types.store')}}" class="parsley-style-1" id="selectForm2" name="selectForm2"  method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                  <div class="">
                    <div class="row mg-b-20">
                      <div class="parsley-input col-md-6" id="name_ar">
                        <label>اسم النوع الحيوان باللغه العربيه: <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#name_ar" name="name_ar" value="{{old('name_ar')}}" placeholder="ادخل اسم النوع الحيوان باللغه العربيه" required="" type="text">
                           @error('name_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="name_en">
                        <label>اسم النوع الحيوان باللغه الانجليزيه: <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#name_en" name="name_en" value="{{old('name_en')}}" placeholder="ادخل اسم النوع الحيوان باللغه الانجليزيه" required="" type="text">
                           @error('name_en')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>
<div class="row mb-4">
              <div class="parsley-input col-md-12 mg-t-20 mg-md-t-0" id="image">
               <label>صوره للحيوان : <span class="tx-danger">*</span></label>

              <input data-parsley-class-handler="#image"  required="" class="dropify" data-height="200"  type="file" name="image" accept=".jpg, .png, image/jpeg, image/png">
            </div>
          </div>
                  <hr>
                <div class="mg-t-30">
                    <button class="btn btn-main-primary pd-x-20" type="submit">إضافه</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- row closed -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
@endsection
@section('js')
<!--Internal  Select2 js -->
<script src="{{URL::asset('public/assets/plugins/select2/js/select2.min.js')}}"></script>
<!--Internal  Parsley.min js -->
<script src="{{URL::asset('public/assets/plugins/parsleyjs/parsley.min.js')}}"></script>
<!-- Internal Form-validation js -->
    <script src="{{URL::asset('public/assets/plugins/fileuploads/js/fileupload.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fileuploads/js/file-upload.js')}}"></script>



<script src="{{URL::asset('public/assets/js/form-validation.js')}}"></script>
@endsection