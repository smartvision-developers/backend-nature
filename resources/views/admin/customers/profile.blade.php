@extends('admin.layouts.master')
@section('css')
<!-- Internal Data table css -->
<link href="{{URL::asset('public/assets/plugins/datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{URL::asset('public/assets/plugins/datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/datatable/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{URL::asset('public/assets/plugins/datatable/css/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/datatable/css/responsive.dataTables.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">

<style type="text/css">
  .main-content-label
  {
   font-size:large !important;
  }
  .btn-icon {
   display: initial !important;
  }
  .select2-container--default .select2-selection--multiple {
    width: 360px !important;
  }
  .shippment
  {
    padding: 15px;
    border: 1px solid #cccccc;
    width: 100%;
  }
</style>
@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="breadcrumb-header justify-content-between">
  <div class="my-auto">
    <div class="d-flex">
      <h4 class="content-title mb-0 my-auto">عملائنا</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ عرض تفاصيل عميل </span>
    </div>
  </div>
  <div class="d-flex my-xl-auto right-content">
    <div class="pr-1 mb-3 mb-xl-0">
      <a href="{{URL('admin/customers')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i>عرض كل العملاء</a>
    </div>
    
  </div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')

<!--div-->
<div class="col-xl-12">
  <div class="card mg-b-20">
    <div class="card-header pb-0">
      <div class="d-flex justify-content-between">
        <h4 class="card-title mg-b-0">عرض تفاصيل عميل</h4>
        <i class="mdi mdi-dots-horizontal text-gray"></i>
      </div>
      <hr>
    </div>
    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
    @endif

    @if(Session::has('error'))
    <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
    @endif
    <div class="card-body">
      <div class="panel panel-primary tabs-style-3">
        <div class="tab-menu-heading">
          <div class="tabs-menu ">
            <!-- Tabs -->
            <ul class="nav panel-tabs">
              <li class=""><a href="#tab11" class="active" data-toggle="tab"><i class="fa fa-laptop"></i>عناوين الشحن </a></li>
              <li><a href="#tab12" data-toggle="tab"><i class="fa fa-cube"></i> حجوزات العياده</a></li>
              <li><a href="#tab13" data-toggle="tab"><i class="fa fa-cogs"></i> المشتريات </a></li>
              <li><a href="#tab14" data-toggle="tab"><i class="fa fa-tasks"></i> حجوزات الفندقه</a></li>
            </ul>
          </div>
        </div>
        <div class="panel-body tabs-menu-body">
          <div class="tab-content">
            <div class="tab-pane active" id="tab11">
              <div class="row">
                <ul class="col-sm-12">
                @foreach($customer->ShippmentAddress as $key=> $value)
                  <li class="shippment"><strong>عنوان الشحن  : </strong><p>{{$value->address_type}} : {{$value->Location->location_name_ar}}- منطقه: {{$value->Region->region_name_ar}} - {{$value->City->city_name_ar}}- {{$value->address_details}} </p>
                    <span>رقم الجوال : {{$value->phone}}</span>

                  </li>
                @endforeach
              </ul>
              </div>
            </div>
            <div class="tab-pane" id="tab12">
             <div class="row">

                <ol>
                @foreach($customer->ClinicReservation as $key=> $value)
                  <li> 
                    <div class="col-sm-10">
                      <ul>
                        <li>نوع الحيوان : {{$value->AnimalType->name_ar}}</li>
                        <li>اسم الخدمه : {{$value->ClinicService->service_description_ar}}</li>
                        <li>سعر الخدمه للحيوان الواحد : {{$value->ClinicService->service_price}} ريال </li>
                        <li>عدد الحيوانات: {{$value->animal_no}}</li>
                        <li>السعر الكلي للخدمه: {{$value->service_price}} ريال</li>
                        <li>تاريخ  الحجز: {{$value->reservation_date}}</li>
                      </ul>
                    </div>
                  </li>
                  <hr  style="width: 970px !important;">
                  <br>
                @endforeach
              </ol>

            </div>
          </div>
          <div class="tab-pane" id="tab13">
          <div class="row">
               <ul class="col-sm-12">
                @foreach($customer->Cart as $key=> $value)
                  <li class="shippment row">
                    <div class="col-sm-4">
                      <img src="{{URL('/public/images/products/'. $value->Product->ProductImage[0]->product_image)}}" height="200">
                    </div>
                    <div class="col-sm-8">
                      <span>كود الصنف : {{$value->Product->product_code}}</span>
                      <p>اسم المنتج : {{$value->Product->product_name_ar}}</p>
                      <span>السعر : {{$value->Product->product_unitprice}} ريال</span>
                      <br/>
                      <a class="btn btn-info" href="{{URL('admin/products/'.$value->Product->id.'/profile/')}}">تفاصيل المنتج</a>
                    </div>
                  </li>
                @endforeach
              </ul>
          </div>
       
        </div>
        <div class="tab-pane" id="tab14">
          <div class="row">
            
                <ol>
                @foreach($customer->HotelReservation as $key=> $value)
                  <li> 
                    <div class="col-sm-10">
                      <ul>
                        <li>نوع الحيوان : {{$value->AnimalType->name_ar}}</li>
                        <li>اسم الخدمه : {{$value->HotelService->service_description_ar}}</li>
                        <li>سعر الخدمه للحيوان الواحد : {{$value->HotelService->service_price}} ريال </li>
                        <li>عدد الحيوانات: {{$value->animal_no}}</li>
                        <li>السعر الكلي للخدمه: {{$value->service_price}} ريال</li>
                        <li>تاريخ بدايه الحجز: {{$value->reservation_start_date}} </li>
                        <li>تاريخ نهايه الحجز: {{$value->reservation_end_date}} </li>
                      </ul>
                    </div>
                  </li>
                  <hr  style="width: 970px !important;">
                  <br>
                @endforeach
              </ol>

          </div>
        </div>
      </div>
    </div>
  </div>

</div>
</div>
</div>
<!--/div-->
</div>
<!-- /row -->
</div>
<!-- Container closed -->
</div>
<!-- main-content closed -->
@endsection
@section('js')
<!--Internal  Select2 js -->
<script src="{{URL::asset('public/assets/plugins/select2/js/select2.min.js')}}"></script>
<!--Internal  Parsley.min js -->
<script src="{{URL::asset('public/assets/plugins/parsleyjs/parsley.min.js')}}"></script>
<!-- Internal Form-validation js -->
<script src="{{URL::asset('public/assets/js/form-validation.js')}}"></script>
@endsection