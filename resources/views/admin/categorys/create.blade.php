@extends('admin.layouts.master')
@section('css')
<!--- Internal Select2 css-->
<link href="{{URL::asset('public/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
<style type="text/css">
  form
  {
    margin-top: 3rem !important;
  }
  .main-content-label
  {
     font-size:large !important;
  }
  .btn-icon {
     display: initial !important;
  }
</style>
@endsection
@section('page-header')
        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
          <div class="my-auto">
            <div class="d-flex">
              <h4 class="content-title mb-0 my-auto">الأقسام</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ إضافه قسم جديد</span>
            </div>
          </div>
          <div class="d-flex my-xl-auto right-content">
            <div class="pr-1 mb-3 mb-xl-0">
              <a href="{{URL('admin/categorys')}}" class="btn btn-info btn-icon ml-2"><i class="mdi mdi-filter-variant"></i> عرض الأقسام</a>
            </div>
          
          </div>
        </div>
        <!-- breadcrumb -->
@endsection
@section('content')

        <!-- row -->
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="card">
              <div class="card-body">
                <div class="main-content-label mg-b-5">
                  إضافه قسم جديد
                </div>
                <hr>
                 @if(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
                @endif
                <form action="{{route('categorys.store')}}" class="parsley-style-1" id="selectForm2" name="selectForm2"  method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                  <div class="">
                    <div class="row mg-b-20">
                      <div class="parsley-input col-md-6" id="category_name_ar">
                        <label>اسم القسم باللغه العربيه: <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#category_name_ar" name="category_name_ar" value="{{old('category_name_ar')}}" placeholder="ادخل اسم القسم باللغه العربيه" required="" type="text">
                           @error('category_name_ar')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                      <div class="parsley-input col-md-6 mg-t-20 mg-md-t-0" id="category_name_en">
                        <label>اسم القسم باللغه الانجليزيه: <span class="tx-danger">*</span></label>
                        <input class="form-control" data-parsley-class-handler="#category_name_en" name="category_name_en" value="{{old('category_name_en')}}" placeholder="ادخل اسم القسم باللغه الانجليزيه" required="" type="text">
                           @error('category_name_en')
                        <strong class="help-block" style="color:#c01f0b">{{$message}}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>
                          <div class="parsley-select wd-500 mg-t-30" id="slWrapper2">
             <label>اختر  الحيوان: <span class="tx-danger">*</span></label>
                  <select name="animal_type_id[]" id="ingredients" multiple="multiple" class="form-control " style="overflow:auto;" data-parsley-class-handler="#slWrapper2" data-placeholder="اختر قسم المنتج" required="">
                    <option value="all" style="    font-size: 18px;
    color: red !important;
    font-weight: bolder;">الكل</option>

                  @foreach($animals as $animal)
                        <option value="{{$animal->id}}">{{$animal->name_ar}}</option>

                    @endforeach
                    </select>
              <div id="slErrorContainer2"></div>
            </div>
  
                <!--   <p class="mg-b-10">What is your favorite browser? <span class="tx-danger">*</span></p>
                  <div class="parsley-checkbox wd-250 mg-b-0" id="cbWrapper2">
                    <label class="ckbox mg-b-5">
                      <input data-parsley-class-handler="#cbWrapper2" data-parsley-errors-container="#cbErrorContainer2" data-parsley-mincheck="2" name="browser[]" required="" type="checkbox" value="1">
                      <span>Firefox</span>
                    </label>
                    <label class="ckbox mg-b-5"><input name="browser[]" type="checkbox" value="2"><span>Chrome</span></label>
                    <label class="ckbox mg-b-5"><input name="browser[]" type="checkbox" value="3"><span>Safari</span></label>
                    <label class="ckbox"><input name="browser[]" type="checkbox" value="4"><span>Edge</span></label>
                  </div>
                   parsley-checkbox -->
<!--                   <div class="wd-250" id="cbErrorContainer2"></div>
                  <div class="parsley-select wd-250 mg-t-30" id="slWrapper2">
                    <select class="form-control select2" data-parsley-class-handler="#slWrapper2" data-parsley-errors-container="#slErrorContainer2" data-placeholder="Choose one" required="">
                      <option label="Choose one">
                      </option>
                      <option value="Firefox">
                        Firefox
                      </option>
                      <option value="Chrome">
                        Chrome
                      </option>
                      <option value="Safari">
                        Safari
                      </option>
                      <option value="Opera">
                        Opera
                      </option>
                      <option value="Internet Explorer">
                        Internet Explorer
                      </option>
                    </select>
                    <div id="slErrorContainer2"></div>
                  </div> -->
                <div class="mg-t-30">
                    <button class="btn btn-main-primary pd-x-20" type="submit">إضافه</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- row closed -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
@endsection
@section('js')
<!--Internal  Select2 js -->
<script src="{{URL::asset('public/assets/plugins/select2/js/select2.min.js')}}"></script>
<!--Internal  Parsley.min js -->
<script src="{{URL::asset('public/assets/plugins/parsleyjs/parsley.min.js')}}"></script>
<!-- Internal Form-validation js -->
<script src="{{URL::asset('public/assets/js/form-validation.js')}}"></script>
@endsection