@extends('admin.layouts.master')
@section('css')
<!-- Internal Gallery css -->
<link href="{{URL::asset('public/assets/plugins/gallery/gallery.css')}}" rel="stylesheet">

<link href="{{URL::asset('public/assets/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('public/assets/plugins/fancyuploder/fancy_fileupload.css')}}" rel="stylesheet" />
@endsection
@section('page-header')
        <!-- breadcrumb -->
        <div class="breadcrumb-header justify-content-between">
          <div class="my-auto">
            <div class="d-flex">
              <h4 class="content-title mb-0 my-auto">الإعدادات</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ معرض الصور</span>
            </div>
          </div>
        </div>
        <!-- breadcrumb -->
@endsection
@section('content')

          <!--div-->
          <div class="col-xl-12">
            <div class="card mg-b-20">
              <div class="card-header pb-0">
                <div class="d-flex justify-content-between">
                  <h4 class="card-title mg-b-0">معرض الصور</h4>
                       <a class="btn btn-info" href="{{URL('/about-us#site_gallery')}}">مشاهده داخل الموقع </a>
                  <i class="mdi mdi-dots-horizontal text-gray"></i>
                </div>
                <hr>
              </div>
               @if(Session::has('success'))
          <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
          @endif

          @if(Session::has('error'))
          <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
          @endif
              <div class="card-body">
       @permission(['create-gallerys']) 
        <form action="{{route('site_gallerys.save')}}" enctype="multipart/form-data" class="parsley-style-1" id="selectForm2" name="selectForm2"  method="POST">
          @csrf
 <div class="row mb-4">
              <div class="parsley-input col-md-12 mg-t-20 mg-md-t-0" id="site_image">
               <label>إضافه صور للموقع<span class="tx-danger">*</span></label>

              <input data-parsley-class-handler="#site_image"  required="" class="dropify" data-height="200"  type="file" name="site_image[]" multiple="" accept=".jpg, .png, image/jpeg, image/png">
            </div>
          </div>
                 <div class="mg-t-30">
                    <button class="btn btn-main-primary pd-x-20" type="submit">حفظ</button>
                  </div>
                </form>
                @endpermission
                  <hr>
  <!-- Gallery -->
  @permission(['read-gallerys']) 
        <div class="demo-gallery">
          <ul id="lightgallery" class="list-unstyled row row-sm pr-0">
            @foreach($gallerys as $gallery)

            <li class="col-sm-6 col-lg-4" data-responsive="{{URL($gallery->site_image)}}" data-src="{{URL($gallery->site_image)}}" data-sub-html="<h4>Site image {{$gallery->id}}</h4>" >
              
                <img class="img-responsive" src="{{URL($gallery->site_image)}}" alt="site image {{$gallery->id}}">
              
@permission(['delete-gallerys']) 
<a href="{{route('site_gallerys.delete', $gallery->id)}}" class="rem">
  <i class="fa fa-trash"></i>
</a>
@endpermission
            </li>

            @endforeach
          </ul>
          <!-- /Gallery -->
                  </div>
                  @endpermission
        <!-- row closed -->
              </div>
            </div>
          </div>
          <!--/div-->
        </div>
        <!-- /row -->
      </div>
      <!-- Container closed -->
    </div>
    <!-- main-content closed -->
@endsection
@section('js')
<!--Internal  Datepicker js -->
<script src="{{URL::asset('public/assets/plugins/jquery-ui/ui/widgets/datepicker.js')}}"></script>
<!-- Internal Gallery js -->
<script src="{{URL::asset('public/assets/plugins/gallery/lightgallery-all.min.js')}}"></script>
<script src="{{URL::asset('public/assets/plugins/gallery/jquery.mousewheel.min.js')}}"></script>
<script src="{{URL::asset('public/assets/js/gallery.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fileuploads/js/fileupload.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fileuploads/js/file-upload.js')}}"></script>
    <!--Internal Fancy uploader js-->
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.ui.widget.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.fileupload.js')}}"></script>
<!--     <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.iframe-transport.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/fancyuploder/jquery.fancy-fileupload.js')}}"></script>
 --><!--     <script src="{{URL::asset('public/assets/plugins/fancyuploder/fancy-uploader.js')}}"></script>
 -->
 <script type="text/javascript">
   $('.rem').click(function (e){
    e.stopPropagation();
    
   });
 </script>
@endsection