<?php
	return [


        // Start navigation bar
		'home' 			=>  'Home',
		'about-us' 		=>  'About us',
		'more'  		=>  'More',
        'contact-us' 	=>  'Contact Us',
        'our-services'    =>  'Our Services',
        'clinic-service'  =>  'Clinic service',
        'hotel-service'   =>  'Hotel Service',
        'shop'            =>  'Shop',
        'my-account'      =>  'My Account',
        'lang'            =>  'Language',
        'login'           =>  'Login',
        'logout'          =>  'Logout',
        'signup'          =>  'Signup',
        'my-cart'         =>  'Cart',
        'my-fav'          =>  'Favourite',
        'menu'            =>  'Menu',
        'setting'         =>  'Setting',
        'discover'        =>  'Discover',
        'write-search'    => 'Type your search here',
         // end navigation bar


          // Strat Index Page

        'best-seller'   => 'Best seller',
        'best-exclusive'   => 'Best exclusive products',
        'best-services'   => 'The best services at the hands of specialists',
        'advanced-clinic'   => 'Advanced clinics',
        'shop-now'  => 'Shop now',
        'book-now'   => 'Book now',
        'top-rated'   => 'Top Rated',
        'more-seller'   => 'Best seller',
        'recently-arrived'   => 'Recently arrived',
        'premium-products'   => 'Premium Products',
        'everything-need'   => 'Everything you need to know about your pet',
        'watch-all'   => 'Watch all',
        'said-about'   => 'All that was said about us',
        'customer-reviews'   => 'Customer reviews',
          // End Index Page

      


		// Start About-Us page
        'our-mission' 	=>  'Our mission and vision',
        'about-txt' 	=>  'We can always make them happy',
        'photo-gallery' =>  'Photo Gallery',
        'gallery' 		=>  'Gallery',
		// End About-Us page

		// Start Contact-Us page
        'send' 			=>  'Send',
        'name' 			=>  'Name',
        'email'			=>  'Email',
        'phone-no' 		=>  'Call us on the number',
        'phone' 		=>  'Phone',
        'address' 		=>  'Address',
        'message' 		=>  'Your Message',
        'password'     =>  'Password',
        'repassword'     =>  'Repassword',
        'update'     =>  'Edit',

        //validation form

        'name-required' => 'Name field is required',
        'phone-required' => 'Phone field is required',
        'phone-regex' => 'Make sure the phone number is entered correctly',
        'email-email' => 'Make sure to enter the email correctly',
        'name-min' => 'The name must be more than two characters',
        'email-required' => 'Email field is required',
        'password-required' => 'Password field is required',
        'message-required' => 'Message field is required',
        'singup' => 'Sign Up',
        'have-account' => 'I already have an account?',
        'donthave-account' => 'I don\'t have an account?',
        'cancel' => 'Cancel',
        'remember-me'=> 'Remember Me',
        // end

		// End Contact-Us page


		// Start Blogs page
        'blog' 			=>  'Blog',
        'details' 		=>  'Details',
        'n-t-o' 		=>  'From newest to oldest',
        'o-t-n' 		=>  'From oldest to newest',
        'search-blog'   =>  'Search the blog',
        'type-search' 	=>  'Type your search here',
        'search' 		=>  'Search',
        'other-blogs' 	=>  'Other blogs',
        'leave-comment' =>  'Leave a comment',
        'your-comment' 	=>  'your comment',
        'pre-blog' 		=>  'Previous blog',
        'nex-blog' 		=>  'Next blog',
        'tags' 			=>  'Tags',
        'by'			=>  'By',
        'share' 		=>	'Share',
        'search-results'  => 'Search results',
        'looking-for-blogs'  => 'You are looking for blogs',    
    // End Blogs page
  

        // Start favourite page
        'removal'          =>  'Removal',
        'remove-from-cart' =>  'Remover from cart',
        'product-details'  =>  'Product details',
        'stock-status'     =>  'Stock Status',
        'add-to-cart'      =>  'Add to cart',
        'favorite'         =>  'Favorite',
        // End favourite page


        // Start privacy page
        'privacy'          =>  'Privacy policy',
        'terms'            =>  'Terms of use',
        // End privacy page



        // Start Footer 
        'important-link'          =>  'The most important links',
        'mailing-list'            =>  'Mailing List',
        'subscibe'               =>  'Subscribe to receive all new',
        'all-rights'            =>  'All rights reserved to',
        'nature-sound'            =>  'Nature Sound',
        'welcome-back'              => 'Welcome back!',
        // End Footer 


        // Start profile page
        'profile'          =>  'Profile',
        'shipping-addresses'  =>  'Shipping Addresses',
        'add'   => 'Add',
        'add-title'   => 'Add a title',
        'title-classification' => 'Title Classification',
        'region'  => 'Region',
        'city' => 'City',
        'location' =>'Location',
        'rest-address' => 'Address',
        'choose' => 'Choose ',
        // End profile page



        //Start Store Pages
        'store'   =>  'Store',
        'quantity' => 'Quantity',
        'add-cart'  => 'Add to cart',
        'del-cart' => 'Remove from cart',
        'category'  => 'Category',
        'product-code'  => 'Item code',
        'riyal' =>'SAR',
        'available-stock' => 'Available in stock',
        'shipping-info'  => 'Shipping information',
        'rating' => 'Rating',
        'product-description' => 'Product Description',
        'add-rating' => 'Add rating',
        'add-comment'  => 'Add Comment',
        'related-product'  => 'Related Products',
        'share-product'  => 'Share this product',
        //End Store Pages



        //Start hotel page
        'hotel'  => 'Hotel',
        'about' => 'About',
        'available-service' => 'Available services',
        'clinic'  => 'Clinic',
        'book-service' => 'Book a clinic service',
        'choose-service'  => 'Choose the service',
        'pet-type'  => 'Pet type',
        'choose-pet'=> 'Choose the pet',
        'write-number' => 'Write the number',
        'booking-date'  => 'Booking date',
        'pay'   => 'Pay',
        'gallery'  => 'Gallery',
        'picture-library' => 'Pictures library',
     
        //end hotel page

	];

?>