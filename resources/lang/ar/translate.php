<?php
	return [


      // Start navigation bar
		'home' 		    	=>  'الرئيسية',
		'about-us' 		   =>  'من نحن ',
		'more'  		      =>  'المزيد',
		'contact-us'	   =>  'تواصل معنا',
      'our-services'    =>  'خدماتنا',
      'clinic-service'  =>  'خدمه العياده',
      'hotel-service'   =>  'خدمه الفندقه',
      'shop'            =>  'المتجر',
      'my-account'      =>  'حسابي',
      'lang'            =>  'اللغه',
      'login'           =>  'تسجيل دخول',
      'logout'          =>  'تسجيل خروج',
      'signup'          =>  'تسجيل جديد',
      'my-cart'         =>  'السله',
      'my-fav'          =>  'المفضله',
      'menu'            =>  'القائمه',
      'setting'         =>  'الاعدادات',
     'discover'         =>  'اكتشف',
     'write-search'     => 'اكتب بحثك هنا',
      // end navigation bar

      // Strat Index Page
        'best-seller'   => 'الأفضل مبيعا ', 
        'best-exclusive'   => 'أفضل المنتجات الحصرية',
        'best-services'   => 'أفضل الخدمات على أيدي متخصصين',
        'advanced-clinic'   => 'عيادات متطوره',
        'shop-now'    => 'تسوق الان',
        'book-now'   => 'احجز الان',
        'top-rated'   => 'الأعلى تقييم',
        'more-seller'   => 'الأكثر مبيعا',
        'recently-arrived'   => 'وصل حديثا',
        'premium-products'   => 'منتجات متميزة',
        'everything-need'   => 'كل ما تريد معرفته عن حيوانك الأليف',
        'watch-all'   => 'مشاهده الكل',
        'said-about'   => 'كل ما قيل عنا',
        'customer-reviews'   => 'اراء العملاء',

      // End Index Page

		// Start About-Us page
        'our-mission' 	=>  'همتنا ورؤيتنا',
        'about-txt'  	=>  'نستطيع ان نجعلهم سعداء باستمرار',
        'photo-gallery' =>  'مكتبه الصور',
        'gallery' 		=>  'الجاليري',
		// End About-Us page

		// Start Contact-Us page
        'send' 			=>  'إرسال',
        'name' 			=>  'الاسم ',
        'email'			=>  'البريد الالكتروني',
        'phone-no' 		=>  'كلمنا علي رقم',
        'phone' 		   =>  'رقم الهاتف',
        'address' 		=>  'عنوان المقر',
        'message' 		=>  'رسالتك',
        'password'     =>  'كلمه المرور',
        'repassword'     =>  'تأكيد كلمه المرور',
        'update'     =>  'تعديل',

           //validation form

        'name-required'  => 'لاسم مطلوب',
        'phone-required' => 'رقم الهاتف مطلوب',
        'phone-regex'    => 'تأكد من ادخال رقم الهاتف بشكل صحيح',
        'email-email'    => 'تأكد من ادخال البريد الالكتروني بشكل صحيح',
        'name-min'       => 'The name must be more than two characters',
        'email-required' => 'البريد الالكتروني مطلوب',
        'password-required' => 'كلمه المرور مطلوبه',
        'message-required' => 'اترك رسالتك ',
        'singup' => 'تسجيل جديد',
        'have-account' => 'لدي حساب بالفعل ؟',
        'donthave-account' => 'ليس لدي حساب ؟',
        'cancel' => 'إلغاء',
        'remember-me'=> 'تذكرني',
        // end

		// End Contact-Us page


		// Start Blogs page
        'blog' 			=>  'المدونه',
        'details' 		=>  'التفاصيل',
        'n-t-o' 		=>  'من الأحدث للأقدم',
        'o-t-n' 		=>  'من الأقدم للأحدث',
 		     'search-blog'   =>  'ابحث في المدونه',
        'type-search' 	=>  'اكتب بحثك هنا',
        'search' 		=>  'بحث',
        'other-blogs' 	=>  'مقالات أخرى',
        'leave-comment' =>  'اترك تعليقا',
        'your-comment' 	=>  'تعليقك',
        'pre-blog' 		=>  'المقال السابق',
        'nex-blog' 		=>  'المقال التالي',
        'tags' 			=>  'الكلمات الدلالية',
        'by'			=>  'بواسطه ',
        'share' 		=>  'مشاركه ',
        'search-results'  => 'نتائج البحث',
        'looking-for-blogs'  => 'انت تبحث عن مدونات',
        // End Blogs page

  

        // Start favourite page
        'removal'          =>  'إزاله',
        'remove-from-cart' =>  'حذف من السله',
        'product-details'  =>  'تفاصيل المنتج',
        'stock-status'     =>  'حاله المخزون',
        'add-to-cart'      =>  'إضافه للسلة',
        'favorite'         =>  'المفضله',
        // End favourite page



        // Start privacy page
        'privacy'          =>  'سياسه الخصوصية',
        'terms'            =>  'شروط الاستخدام',
        // End privacy page




       // Start Footer 
        'important-link'          =>  'أهم الروابط',
        'mailing-list'            =>  'القائمه البريديه',
        'subscibe'               =>  'اشترك ليصلك كل جديد',
        'all-rights'            =>  'جميع الحقوق محفوظه لموقع ',
        'nature-sound'            =>  'صوت الطبيعه',
        'welcome-back'           => 'اهلا بعودتك !' ,
        // End Footer 



        // Start profile page
        'profile'          =>  'الملف الشخصي',
        'shipping-addresses'  =>  'عناوين الشحن',
        'add'   => 'إضافه',
        'add-title'   => 'إضافه عنوان',
        'title-classification' => 'تصنيف العنوان',
        'region'  => 'المنطقه',
        'city' => 'المدينه',
        'location' =>'الحي',
        'rest-address' => 'باقي العنوان',
        'choose' => 'اختر ',
        // End profile page



        //Start Store Pages
        'store'   =>  'المتجر',
        'quantity' => 'الكميه ',
        'add-cart'  => 'إضافه للسله',
        'del-cart' => 'حذف من السله',
        'category'  => 'قسم',
        'product-code'  => 'كود الصنف',
        'riyal' =>'ريال',
        'available-stock' => 'متاح فالمخزن',
        'shipping-info'  => 'معلومات الشحن',
        'rating' => 'التقييمات',
        'product-description' => 'وصف المنتج',
        'add-rating' => 'إضافه تقييم',
        'add-comment'  => 'إضافه تعليق',
        'related-product'  => 'منتجات ذات صلة',
        'share-product'  => 'مشاركة هذا المنتج',

        //End Store Pages



        //Start hotel page
        'hotel'  => 'الفندقه',
        'about' => 'نبذه عن ',
        'available-service' => 'الخدمات المتاحه',
        'clinic' => ' العياده' ,
        'book-service' => 'حجز خدمة عيادة',
        'choose-service'  => 'اختر الخدمة',
        'pet-type'  => 'نوع الحيوان الاليف',
        'choose-pet'=> 'اختر الحيوان الاليف',
        'write-number' => 'اكتب العدد',
        'booking-date'  => 'تاريخ الحجز',
        'pay'   => 'ادفع',
        'gallery'  => 'الجاليرى',
        'picture-library' => 'مكتبه الصور',

        //end hotel page

	];

?>