-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 04, 2021 at 10:58 AM
-- Server version: 10.3.31-MariaDB
-- PHP Version: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `naturesoundsa_nature_sound`
--

-- --------------------------------------------------------

--
-- Table structure for table `animal_categorys`
--

CREATE TABLE `animal_categorys` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `animal_type_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `animal_categorys`
--

INSERT INTO `animal_categorys` (`id`, `created_at`, `updated_at`, `deleted_at`, `animal_type_id`, `category_id`) VALUES
(3, '2021-08-11 08:16:27', '2021-08-11 08:16:27', NULL, 2, 2),
(4, '2021-08-28 20:09:00', '2021-08-28 20:08:57', NULL, 1, 2),
(5, '2021-08-28 20:09:02', '2021-08-30 09:27:14', '2021-08-30 09:27:14', 1, 1),
(6, '2021-08-28 14:45:06', '2021-08-30 09:27:14', '2021-08-30 09:27:14', 2, 1),
(7, '2021-08-28 17:07:55', '2021-08-30 09:37:57', '2021-08-30 09:37:57', 1, 4),
(8, '2021-08-28 17:07:55', '2021-08-28 17:15:55', '2021-08-28 17:15:55', 2, 4),
(9, '2021-08-28 17:15:55', '2021-08-28 17:15:55', '2021-08-29 08:49:10', 2, 4),
(10, '2021-08-30 09:27:14', '2021-08-30 09:39:09', '2021-08-30 09:39:09', 1, 1),
(11, '2021-08-30 09:27:14', '2021-08-30 09:39:09', '2021-08-30 09:39:09', 2, 1),
(12, '2021-08-30 09:37:57', '2021-08-30 09:37:57', '2021-08-30 09:37:57', 2, 4),
(13, '2021-08-30 09:37:57', '2021-08-30 09:37:57', '2021-08-30 09:37:57', 3, 4),
(14, '2021-08-30 09:37:57', '2021-08-30 09:37:57', '2021-08-30 09:37:57', 4, 4),
(15, '2021-08-30 09:37:57', '2021-08-30 09:37:57', NULL, 5, 4),
(16, '2021-08-30 09:39:09', '2021-08-30 09:39:09', NULL, 1, 1),
(17, '2021-08-30 09:39:09', '2021-08-30 09:39:09', NULL, 2, 1),
(18, '2021-08-30 09:39:09', '2021-08-30 09:39:09', NULL, 3, 1),
(19, '2021-08-30 09:39:09', '2021-08-30 09:39:09', NULL, 4, 1),
(20, '2021-08-30 09:39:09', '2021-08-30 09:39:09', NULL, 5, 1),
(21, '2021-08-30 09:39:09', '2021-08-30 09:39:09', NULL, 6, 1),
(22, '2021-08-30 09:41:51', '2021-08-30 09:41:51', NULL, 2, 5),
(23, '2021-08-30 09:41:51', '2021-08-30 09:41:51', NULL, 3, 5),
(24, '2021-08-30 09:42:19', '2021-08-30 09:42:19', NULL, 1, 6),
(25, '2021-08-30 09:42:19', '2021-08-30 09:42:19', NULL, 2, 6);

-- --------------------------------------------------------

--
-- Table structure for table `animal_types`
--

CREATE TABLE `animal_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name_ar` varchar(255) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `image` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `animal_types`
--

INSERT INTO `animal_types` (`id`, `created_at`, `updated_at`, `deleted_at`, `name_ar`, `name_en`, `status`, `image`) VALUES
(1, '2021-07-30 20:20:53', '2021-08-30 09:36:28', NULL, 'الكلاب', 'Dogs', 1, 'public/images/animals/L8MrMJ8nt1wDsMgvkMGhO9X4bVfFztiUsa1kqK3J.png'),
(2, '2021-08-10 18:46:36', '2021-08-30 09:36:49', NULL, 'القطط', 'Cats', 1, 'public/images/animals/7JwWr49s98cdw9WTx2ZAR4tGr9oSQMzQLjdehJ2B.png'),
(3, '2021-08-30 09:35:52', '2021-08-30 09:36:32', NULL, 'الزواحف', 'Reptiles', 1, 'public/images/animals/NNKxjUo9ih5rGThRDU4uCCDswj7n9fulNU5JvcRs.png'),
(4, '2021-08-30 09:36:12', '2021-08-30 09:36:34', NULL, 'الطيور', 'Birds', 1, 'public/images/animals/86uLb3pT9hgmesPyWGepvDYS5q3Lq9LZjxAfuiEn.png'),
(5, '2021-08-30 09:37:34', '2021-08-30 09:39:35', NULL, 'الأرانب', 'Rabbits', 1, 'public/images/animals/VE3CJMB8sUmpeaIVjqK44GN4eWtulH9YDgHqjKtp.png'),
(6, '2021-08-30 09:37:45', '2021-08-30 09:39:38', NULL, 'الأسماك', 'Fish', 1, 'public/images/animals/xQW3oh87yQfspQpvENKV25osHck4BBmksM1XGg9r.png');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `blog_title_en` varchar(255) NOT NULL,
  `blog_text_ar` varchar(255) NOT NULL,
  `blog_text_en` text NOT NULL,
  `blog_title_ar` text NOT NULL,
  `blog_description_ar` longtext NOT NULL,
  `blog_description_en` longtext NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `blog_image` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `created_at`, `updated_at`, `deleted_at`, `blog_title_en`, `blog_text_ar`, `blog_text_en`, `blog_title_ar`, `blog_description_ar`, `blog_description_en`, `user_id`, `status`, `blog_image`) VALUES
(2, '2021-07-30 18:12:10', '2021-09-08 06:38:07', NULL, 'Types of sharks', 'يتكون هيكل سمك القرش من مادة غضروفية وليس من العظام، ويتميز بوجود خمس فتحات خيشومية منفصلة على جانبه خلف الرأس.', 'The shark\'s skeleton is made of cartilaginous material rather than bone, and is characterized by the presence of five separate gill slits on its side behind the head.', 'أنواع أسماك القرش', '<h2 style=\"font-family: Nunito; margin: 30px 0px 10px; font-weight: 800; line-height: 48px; font-size: 32px; color: rgb(39, 48, 75); transition: all 0.3s ease 0s;\"><span id=\"i-2\"><span style=\"color: rgb(255, 0, 0);\"><span style=\"color: rgb(34, 54, 69); transition: all 0.3s ease 0s;\">انواع اسماك القرش</span></span></span></h2><h3 style=\"font-family: Nunito; margin: 30px 0px 10px; font-weight: 800; line-height: 40px; font-size: 28px; color: rgb(39, 48, 75); transition: all 0.3s ease 0s;\"><span id=\"i-3\"><span style=\"color: rgb(0, 0, 255);\">القرش الرملي :</span></span></h3><h2 style=\"margin: 30px 0px 10px; font-weight: 800; line-height: 48px; font-size: 32px; color: rgb(39, 48, 75); transition: all 0.3s ease 0s; font-family: Nunito;\"><div style=\"color: rgb(3, 27, 78); font-size: 14px; font-weight: 400;\"><p style=\"font-family: Nunito; color: rgb(85, 85, 85); font-size: 18px;\">القرش الرّمليّ سباح ماهر يندفع في مياه المحيط بسرعة كبيرة وهو إذ تتغير الفصول يرتحل مسافات واسعة، طلبا للطعام في بحار دافئة.</p></div></h2><h3 style=\"font-family: Nunito; margin: 30px 0px 10px; font-weight: 800; line-height: 40px; font-size: 28px; color: rgb(39, 48, 75); transition: all 0.3s ease 0s;\"><span id=\"i-4\"><span style=\"color: rgb(0, 0, 255);\">قرش النمر :</span></span></h3><h2 style=\"margin: 30px 0px 10px; font-weight: 800; line-height: 48px; font-size: 32px; color: rgb(39, 48, 75); transition: all 0.3s ease 0s; font-family: Nunito;\"><div style=\"color: rgb(3, 27, 78); font-size: 14px; font-weight: 400;\"><div></div><div><p style=\"font-family: Nunito; color: rgb(85, 85, 85); font-size: 18px;\">القرش النمري يأخذ اسمه من نمط من نمط جلده الذهبي المرقط، وفي هذا تمويه جيد في قاع البحر حيث يبحث عن طعامه المفضل ألا وهو المحار، ويشتهر بجرأته وشراسته حتى إنه يهاجم القروش الأخرى.</p></div></div></h2><h3 style=\"font-family: Nunito; margin: 30px 0px 10px; font-weight: 800; line-height: 40px; font-size: 28px; color: rgb(39, 48, 75); transition: all 0.3s ease 0s;\"><span id=\"i-7\"><span style=\"color: rgb(0, 0, 255);\">القرش الطحان :</span></span></h3><h2 style=\"margin: 30px 0px 10px; font-weight: 800; line-height: 48px; font-size: 32px; color: rgb(39, 48, 75); transition: all 0.3s ease 0s; font-family: Nunito;\"><div style=\"color: rgb(3, 27, 78); font-size: 14px; font-weight: 400;\"><div></div><div><p style=\"font-family: Nunito; color: rgb(85, 85, 85); font-size: 18px;\">يبلغ طول القرش الطحان 1.5 متر ويعيش في المياه التي لها قاع طيني، ويشتهر بغزارة الإنجاب، وقد يلد ثلاثين صغيرا في المرة الواحدة.</p></div></div></h2><div><div><div class=\"code-block code-block-10\" style=\"color: rgb(85, 85, 85); font-family: Nunito; font-size: 18px; margin: 8px 0px; clear: both;\"><ins class=\"adsbygoogle\" data-ad-format=\"fluid\" data-ad-layout-key=\"-5s+c0-2p-7u+14p\" data-ad-client=\"ca-pub-2319418021787841\" data-ad-slot=\"4168745498\" data-adsbygoogle-status=\"done\" data-ad-status=\"filled\" style=\"display: block; height: 499px; width: 750px;\"><ins id=\"aswift_8_expand\" tabindex=\"0\" title=\"Advertisement\" aria-label=\"Advertisement\" style=\"display: inline-table; border: none; height: 499px; margin: 0px; padding: 0px; position: relative; visibility: visible; width: 750px; background-color: transparent;\"></ins></ins></div></div><div class=\"code-block code-block-8\" style=\"color: rgb(85, 85, 85); font-family: Nunito; font-size: 18px; margin: 8px 0px; clear: both;\"><ins class=\"adsbygoogle\" data-ad-format=\"fluid\" data-ad-layout-key=\"-5s+c0-2p-7u+14p\" data-ad-client=\"ca-pub-2319418021787841\" data-ad-slot=\"4168745498\" data-adsbygoogle-status=\"done\" data-ad-status=\"filled\" style=\"display: block; height: 499px; width: 750px;\"><ins id=\"aswift_6_expand\" tabindex=\"0\" title=\"Advertisement\" aria-label=\"Advertisement\" style=\"display: inline-table; border: none; height: 499px; margin: 0px; padding: 0px; position: relative; visibility: visible; width: 750px; background-color: transparent;\"></ins></ins></div></div>', '<h1>Types of sharks</h1><p><br></p><h2><font color=\"#0000ff\">sand shark: </font></h2><p><span style=\"font-size: 0.875rem;\">The sand shark is a skilled swimmer, rushing in the ocean waters at great speed, and as the seasons change, it travels wide distances, seeking food in warm seas.</span><br></p><div><br></div><h2><font color=\"#0000ff\">tiger shark:</font></h2><div>The tiger shark takes its name from a pattern of its golden spotted skin pattern, and in this good camouflage at the bottom of the sea, where it searches for its favorite food, which is the oyster, and is famous for its boldness and ferocity, even attacking other sharks.</div><div><br></div><h2><font color=\"#0000ff\">Shark Mill:</font></h2><div>The miller shark is 1.5 meters long and lives in waters that have a muddy bottom. It is famous for its abundance of procreation, and it may give birth to thirty young at a time. </div><div>More about types</div>', 1, 1, 'public/images/blogs/5jgm1TNECLNpl9SruKgVHj2URU4XoeJWs5KxVTCX.jpg'),
(3, '2021-07-30 18:15:56', '2021-08-30 08:37:21', NULL, 'A new blog about 3', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 'This text is an example of a text that can be replaced in the same space. This text was generated from the Arabic text generator, where you can generate such text or many other texts in addition to increasing the number of characters generated by the application.', 'مقال جديد عن 3', '<div><div>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</div><div>إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.</div><div><br></div><div>ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</div><div>هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</div></div><div><br></div>', '<div><div>This text is an example of a text that can be replaced in the same space. This text was generated from the Arabic text generator, where you can generate such text or many other texts in addition to increasing the number of characters generated by the application.</div><div>If you need more paragraphs, the Arabic text generator allows you to increase the number of paragraphs as you want, the text will not appear divided and does not contain language errors, the Arabic text generator is useful for web designers in particular, where the customer often needs to see a real picture for site design.</div><div><br></div><div>Hence, the designer must put temporary texts on the design to show the client the complete form. The role of the Arabic text generator is to save the designer the trouble of searching for an alternative text that has nothing to do with the topic that the design is talking about, so it appears in an inappropriate manner.</div><div>This text can be installed on any design without a problem. It will not look like copied, disorganized, unformatted, or even incomprehensible text. Because it is still an alternative and temporary text.</div></div>', 1, 1, 'public/images/blogs/iwX63Eqfh9dpCCWOSBRG9pL7SIcnNNUsPCHgQ2H5.jpg'),
(4, '2021-07-30 18:17:48', '2021-08-30 08:36:12', NULL, 'Pet turtle', 'يمكن أن تكون إضافة سلحفاة من أنواع السلاحف الأليفة إلى منزلك طريقة رائعة للحصول على حيوان أليف جديد سيكون معك لسنوات. السلاحف مخلوقات صغيرة وهادئة وقليلة الحركة بشكل كبير ولا تسبب الكثير من الكوارث التي تسببها الحيوانات الأليفة الأكبر والأكثر نشاطًا مثل ا', 'Adding a pet tortoise species to your home can be a great way to get a new pet that will be with you for years. Turtles are small, quiet creatures that are largely immobile and do not cause as many disasters as larger, more active pets such as cats.', 'السلاحف الأليفة', '<div>يمكن أن تكون إضافة سلحفاة من أنواع السلاحف الأليفة إلى منزلك طريقة رائعة للحصول على حيوان أليف جديد سيكون معك لسنوات. السلاحف مخلوقات صغيرة وهادئة وقليلة الحركة بشكل كبير ولا تسبب الكثير من الكوارث التي تسببها الحيوانات الأليفة الأكبر والأكثر نشاطًا مثل القطط. العديد من الخيارات قد يكون اختيار سلحفاة أليف أمرًا مربكًا ، ولكل نوع من السلاحف متطلباته الفريدة من حيث المأوى والطعام ومصدر المياه وحتى مستويات درجة الحرارة والرطوبة ، وستحتاج إلى التفكير فيما إذا كان بإمكانك تزويد سلحفاتك بالسلاحف الخاصة بك. البيئة المناسبة لذلك في المنزل.</div><div><br></div><div><div><b>أنواع السلاحف المتوسطية من السلاحف الأليفة</b></div><div><br></div><div>سلحفاة البحر الأبيض المتوسط ​​هي نوع رائع من السلاحف البرية التي يمكنني أن أوصي بها ، وإذا كنت ترغب فقط في امتلاك سلحفاة ، فيمكنك امتلاك واحدة من السلاحف المتوسطية ، وعند الاحتفاظ بها في المكان المناسب ، من السهل جدًا العناية بها ، وبالنظر إلى أنها واحدة من أصغر أنواع السلاحف البرية (لا ينبغي الخلط بينها وبين السلحفاة الأفريقية) ، فمن السهل شراء مجموعة لمرة واحدة يمكن أن تدوم طوال الحياة ، وهذه السلاحف على وجه الخصوص أكثر نشاطًا خلال النهار مما يجعلهم حيوانًا أليفًا رائعًا للتفاعل ، ويحبون التفاعل مع مالكه ، واستكشاف العلبة وكذلك قضاء الوقت في الخارج في الأيام المشمسة طوال فصل الصيف.</div></div><div><br></div><div><br></div>', '<p>Adding a pet tortoise species to your home can be a great way to get a new pet that will be with you for years. Turtles are small, quiet creatures that are largely immobile and do not cause as many disasters as larger, more active pets such as cats. Many Choices Choosing a pet turtle can be overwhelming, and each type of turtle has its own unique requirements in terms of shelter, food, water source, and even temperature and humidity levels, and you\'ll need to consider whether you can provide your turtle with your own. The right environment for it at home.</p><p><b>Mediterranean turtle species of pet turtle</b></p><p>The Mediterranean tortoise is a wonderful type of land turtle that I can recommend, and if you just want to own a turtle, you can own one of the Mediterranean turtles, and when you keep it in the right place, it is very easy to take care of, given that it is one One of the smallest of the land turtles (not to be confused with the African tortoise), it is easy to buy a one time set that can last a lifetime, these turtles in particular are more active during the day which makes them a great interaction pet, they love interacting with their owner, and exploring the enclosure As well as spending time outside on sunny days throughout the summer.</p><p>                        </p>', 1, 1, 'public/images/blogs/eJHYOEMYpOCrolRpapypvLuLHCusQTybTulnJPdj.jpg'),
(5, '2021-07-29 20:44:25', '2021-08-30 08:38:28', NULL, 'A new blog about 4', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 'This text is an example of a text that can be replaced in the same space. This text was generated from the Arabic text generator, where you can generate such text or many other texts in addition to increasing the number of characters generated by the application.', 'مقال جديد عن 4', '<div>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</div><div>إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.</div><div><br></div><div>ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</div><div>هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</div><div><br></div>', '<p><font color=\"#000000\"><span style=\"font-size: 18px;\">This text is an example of a text that can be replaced in the same space. This text was generated from the Arabic text generator, where you can generate such text or many other texts in addition to increasing the number of characters generated by the application.<br></span></font><font color=\"#000000\"><span style=\"font-size: 18px;\">If you need more paragraphs, the Arabic text generator allows you to increase the number of paragraphs as you want, the text will not appear divided and does not contain language errors, the Arabic text generator is useful for web designers in particular, where the customer often needs to see a real picture for site design.</span></font><font color=\"#000000\"><span style=\"font-size: 18px;\"><br></span></font><font color=\"#000000\"><span style=\"font-size: 18px;\">Hence, the designer must put temporary texts on the design to show the client the complete form. The role of the Arabic text generator is to save the designer the trouble of searching for an alternative text that has nothing to do with the topic that the design is talking about, so it appears in an inappropriate manner.<br></span></font><font color=\"#000000\"><span style=\"font-size: 18px;\">This text can be installed on any design without a problem. It will not look like copied, disorganized, unformatted, or even incomprehensible text. Because it is still an alternative and temporary text.</span></font></p>', 1, 1, 'public/images/blogs/PxAlPqrSqkoNbWxtk1GtHsfGHgLpwd0rs7qQjRkY.png'),
(6, '2021-07-30 18:12:10', '2021-08-30 08:26:54', NULL, 'Article about Dogs', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 'This text is an example of a text that can be replaced in the same space. This text was generated from the Arabic text generator, where you can generate such text or many other texts in addition to increasing the number of characters generated by the application.', 'مقال عن الكلاب', '<div>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</div><div>إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.</div><div><br></div><div>ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</div><div>هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</div><div><br></div>', '<div>This text is an example of a text that can be replaced in the same space. This text was generated from the Arabic text generator, where you can generate such text or many other texts in addition to increasing the number of characters generated by the application.</div><div>If you need more paragraphs, the Arabic text generator allows you to increase the number of paragraphs as you want, the text will not appear divided and does not contain language errors, the Arabic text generator is useful for web designers in particular, where the customer often needs to see a real picture for site design.</div><div><br></div><div>Hence, the designer must put temporary texts on the design to show the client the complete form. The role of the Arabic text generator is to save the designer the trouble of searching for an alternative text that has nothing to do with the topic that the design is talking about, so it appears in an inappropriate manner.</div><div>This text can be installed on any design without a problem. It will not look like copied, disorganized, unformatted, or even incomprehensible text. Because it is still an alternative and temporary text.&nbsp;</div><div>More about an</div>', 1, 1, 'public/images/blogs/UztI8svOMMTsTuaYvIgZLHqCosN4eVI3lYfJ7YNU.jpg'),
(7, '2021-07-30 18:15:56', '2021-08-30 08:28:15', NULL, 'A new blog about 2', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 'This text is an example of a text that can be replaced in the same space. This text was generated from the Arabic text generator, where you can generate such text or many other texts in addition to increasing the number of characters generated by the application.', 'مقال جديد عن 2', '<div><div>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</div><div>إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.</div><div><br></div><div>ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</div><div>هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</div></div><div><br></div>', '<div><div>This text is an example of a text that can be replaced in the same space. This text was generated from the Arabic text generator, where you can generate such text or many other texts in addition to increasing the number of characters generated by the application.</div><div>If you need more paragraphs, the Arabic text generator allows you to increase the number of paragraphs as you want, the text will not appear divided and does not contain language errors, the Arabic text generator is useful for web designers in particular, where the customer often needs to see a real picture for site design.</div><div><br></div><div>Hence, the designer must put temporary texts on the design to show the client the complete form. The role of the Arabic text generator is to save the designer the trouble of searching for an alternative text that has nothing to do with the topic that the design is talking about, so it appears in an inappropriate manner.</div><div>This text can be installed on any design without a problem. It will not look like copied, disorganized, unformatted, or even incomprehensible text. Because it is still an alternative and temporary text.</div></div>', 1, 1, 'public/images/blogs/iwX63Eqfh9dpCCWOSBRG9pL7SIcnNNUsPCHgQ2H5.jpg'),
(8, '2021-07-30 18:17:48', '2021-08-30 08:24:40', NULL, 'A new blog about', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 'This text is an example of a text that can be replaced in the same space. This text was generated from the Arabic text generator, where you can generate such text or many other texts in addition to increasing the number of characters generated by the application.', 'مقال جديد عن', '<div>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</div><div>إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.</div><div><br></div><div>ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</div><div>هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</div><div><br></div>', '<div>This text is an example of a text that can be replaced in the same space. This text was generated from the Arabic text generator, where you can generate such text or many other texts in addition to increasing the number of characters generated by the application.</div><div>If you need more paragraphs, the Arabic text generator allows you to increase the number of paragraphs as you want, the text will not appear divided and does not contain language errors, the Arabic text generator is useful for web designers in particular, where the customer often needs to see a real picture for site design.</div><div><br></div><div>Hence, the designer must put temporary texts on the design to show the client the complete form. The role of the Arabic text generator is to save the designer the trouble of searching for an alternative text that has nothing to do with the topic that the design is talking about, so it appears in an inappropriate manner.</div><div>This text can be installed on any design without a problem. It will not look like copied, disorganized, unformatted, or even incomprehensible text. Because it is still an alternative and temporary text.</div>', 1, 1, 'public/images/blogs/KnFDa7r3QtQe6jKleF11IBMMHpDCKQ0iN4bm832s.jpg'),
(9, '2021-08-30 07:16:55', '2021-08-30 07:17:02', NULL, 'Different types of cats and various shapes', 'هناك العديد من أنواع القطط المختلفة والمتنوعة الأشكال، وتتفاوت أنواعها حسب طبيعة المناخ المتواجدة فيها، وقدرة تأقلمها مع الظروف المحيطة بها، ومنها المناسب لاقتنائه كحيوان أليف وغيرها مما لا يصلح لذلك', 'There are many different types of cats with various shapes, and their types vary according to the nature of the climate in which they are located, and their ability to adapt to the surrounding conditions, including those that are appropriate to be acquired as a pet and others that are not suitable for that.', 'أنواع القطط المختلفة والمتنوعة الأشكال', '<div>تعتبر من أهم أنواع القطط المنزلية أو المحلية , وهي الأنواع التي يمكن اقتنائها للتربية داخل المنزل، وهي المنتشرة منذ القدم، ويتوافر منها الكثير من الأشكال والفصائل المتعددة، وتتفاوت في الأحجام والنسب مما يجعل منها الأكثر تداولًا بين الأشخاص، حيث تتأقلم بشكل سريع مع طبيعة المنزل وآدابه، ويمكن تدريباها على العادات المنزلية.</div><div><br></div><div><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/jHWKtQHXVJg\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></div>', '<p>It is considered one of the most important types of domestic or domestic cats, and it is the species that can be acquired for breeding inside the house, and it is widespread since ancient times, and there are many shapes and multiple species, and they vary in sizes and proportions, which makes them the most traded among people, as they quickly adapt to the nature of the house and etiquette, and can be trained on household habits.</p><p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/jHWKtQHXVJg\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p><p>                        </p>', 1, 1, 'public/images/blogs/BLUIgfe0DO1qX3sdUreJztjqO9EnUk4kOhqEjzlL.jpg'),
(10, '2021-09-03 15:39:03', '2021-09-03 15:39:03', NULL, 'Call to a member function store() on string', 'Call to a member function store() on string\r\nCall to a member function store() on string', 'Call to a member function store() on string\r\nCall to a member function store() on string', 'Call to a member function store() on string', '<div class=\"exception-summary \" style=\"background: var(--background-error); border-bottom: 2px solid rgba(0, 0, 0, 0.1); border-top: 1px solid rgba(0, 0, 0, 0.3); flex: 0 0 auto; margin-bottom: 15px; color: rgb(34, 34, 34); font-family: Helvetica, Arial, sans-serif;\"><div class=\"exception-message-wrapper\"><div class=\"container\" style=\"max-width: 1024px; margin-top: 0px; margin-bottom: 0px; padding: 10px 15px 8px; display: flex; align-items: flex-start; min-height: 70px;\"><h1 class=\"break-long-words exception-message\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-size: 21px; overflow-wrap: break-word; hyphens: auto; min-width: 0px; flex-grow: 1; color: rgb(255, 255, 255);\">Call to a member function store() on string</h1><div><br></div><div class=\"exception-illustration hidden-xs-down\" style=\"display: initial; flex-basis: 111px; flex-shrink: 0; height: 66px; margin-left: 15px; opacity: 0.7;\"><svg viewBox=\"0 0 136 81\" xmlns=\"http://www.w3.org/2000/svg\" fill-rule=\"evenodd\" clip-rule=\"evenodd\" stroke-linejoin=\"round\" stroke-miterlimit=\"1.4\"><path d=\"M92.4 20.4a23.2 23.2 0 0 1 9 1.9 23.7 23.7 0 0 1 5.2 3 24.3 24.3 0 0 1 3.4 3.4 24.8 24.8 0 0 1 5 9.4c.5 1.7.8 3.4 1 5.2v14.5h.4l.5.2a7.4 7.4 0 0 0 2.5.2l.2-.2.6-.8.8-1.3-.2-.1a5.5 5.5 0 0 1-.8-.3 5.6 5.6 0 0 1-2.3-1.8 5.7 5.7 0 0 1-.9-1.6 6.5 6.5 0 0 1-.2-2.8 7.3 7.3 0 0 1 .5-2l.3-.3.8-.9.3-.3c.2-.2.5-.3.8-.3H120.7c.2 0 .3-.1.4 0h.4l.2.1.3.2.2-.4.3-.4.1-.1 1.2-1 .3-.2.4-.1.4-.1h.3l1.5.1.4.1.8.5.1.2 1 1.1v.2H129.4l.4-.2 1.4-.5h1.1c.3 0 .7.2 1 .4.2 0 .3.2.5.3l.2.2.5.3.4.6.1.3.4 1.4.1.4v.6a7.8 7.8 0 0 1-.1.6 9.9 9.9 0 0 1-.8 2.4 7.8 7.8 0 0 1-3 3.3 6.4 6.4 0 0 1-1 .5 6.1 6.1 0 0 1-.6.2l-.7.1h-.1a23.4 23.4 0 0 1-.2 1.7 14.3 14.3 0 0 1-.6 2.1l-.8 2a9.2 9.2 0 0 1-.4.6l-.7 1a9.1 9.1 0 0 1-2.3 2.2c-.9.5-2 .6-3 .7l-1.4.1h-.5l-.4.1a15.8 15.8 0 0 1-2.8-.1v4.2a9.7 9.7 0 0 1-.7 3.5 9.6 9.6 0 0 1-1.7 2.8 9.3 9.3 0 0 1-3 2.3 9 9 0 0 1-5.4.7 9 9 0 0 1-3-1 9.4 9.4 0 0 1-2.7-2.5 10 10 0 0 1-1 1.2 9.3 9.3 0 0 1-2 1.3 9 9 0 0 1-2.4 1 9 9 0 0 1-6.5-1.1A9.4 9.4 0 0 1 85 77V77a10.9 10.9 0 0 1-.6.6 9.3 9.3 0 0 1-2.7 2 9 9 0 0 1-6 .8 9 9 0 0 1-2.4-1 9.3 9.3 0 0 1-2.3-1.7 9.6 9.6 0 0 1-1.8-2.8 9.7 9.7 0 0 1-.8-3.7v-4a18.5 18.5 0 0 1-2.9.2l-1.2-.1c-1.9-.3-3.7-1-5.1-2.2a8.2 8.2 0 0 1-1.1-1 10.2 10.2 0 0 1-.9-1.2 15.3 15.3 0 0 1-.7-1.3 20.8 20.8 0 0 1-1.9-6.2v-.2a6.5 6.5 0 0 1-1-.3 6.1 6.1 0 0 1-.6-.3 6.6 6.6 0 0 1-.9-.6 8.2 8.2 0 0 1-2.7-3.7 10 10 0 0 1-.3-1 10.3 10.3 0 0 1-.3-1.9V47v-.4l.1-.4.6-1.4.1-.2a2 2 0 0 1 .8-.8l.3-.2.3-.2a3.2 3.2 0 0 1 1.8-.5h.4l.3.2 1.4.6.2.2.4.3.3.4.7-.7.2-.2.4-.2.6-.2h2.1l.4.2.4.2.3.2.8 1 .2-.1h.1v-.1H63l1.1.1h.3l.8.5.3.4.7 1 .2.3.1.5a11 11 0 0 1 .2 1.5c0 .8 0 1.6-.3 2.3a6 6 0 0 1-.5 1.2 5.5 5.5 0 0 1-3.3 2.5 12.3 12.3 0 0 0 1.4 3h.1l.2.1 1 .2h1.5l.5-.2H67.8l.5-.2h.1V44v-.4a26.7 26.7 0 0 1 .3-2.3 24.7 24.7 0 0 1 5.7-12.5 24.2 24.2 0 0 1 3.5-3.3 23.7 23.7 0 0 1 4.9-3 23.2 23.2 0 0 1 5.6-1.7 23.7 23.7 0 0 1 4-.3zm-.3 2a21.2 21.2 0 0 0-8 1.7 21.6 21.6 0 0 0-4.8 2.7 22.2 22.2 0 0 0-3.2 3 22.7 22.7 0 0 0-5 9.2 23.4 23.4 0 0 0-.7 4.9v15.7l-.5.1a34.3 34.3 0 0 1-1.5.3h-.2l-.4.1h-.4l-.9.2a10 10 0 0 1-1.9 0c-.5 0-1-.2-1.5-.4a1.8 1.8 0 0 1-.3-.2 2 2 0 0 1-.3-.3 5.2 5.2 0 0 1-.1-.2 9 9 0 0 1-.6-.9 13.8 13.8 0 0 1-1-2 14.3 14.3 0 0 1-.6-2 14 14 0 0 1-.1-.8v-.2h.3a12.8 12.8 0 0 0 1.4-.2 4.4 4.4 0 0 0 .3 0 3.6 3.6 0 0 0 1.1-.7 3.4 3.4 0 0 0 1.2-1.7l.2-1.2a5.1 5.1 0 0 0 0-.8 7.2 7.2 0 0 0-.1-.8l-.7-1-1.2-.2-1 .7-.1 1.3a5 5 0 0 1 .1.4v.6a1 1 0 0 1 0 .3c-.1.3-.4.4-.7.5l-1.2.4v-.7A9.9 9.9 0 0 1 60 49l.3-.6v-.2l.1-.1v-1.6l-1-1.2h-1.5l-1 1.1v.4a5.3 5.3 0 0 0-.2.6 5.5 5.5 0 0 0 0 .5c0 .7 0 1.4.3 2 0 .4.2.8.4 1.2L57 51a9.5 9.5 0 0 1-1.1-.5h-.2a2 2 0 0 1-.4-.3c-.4-.4-.5-1-.6-1.6a5.6 5.6 0 0 1 0-.5v-.5-.5l-.6-1.5-1.4-.6-.9.3s-.2 0-.3.2a2 2 0 0 1-.1 0l-.6 1.4v.7a8.5 8.5 0 0 0 .5 2c.4 1.1 1 2.1 2 2.8a4.7 4.7 0 0 0 2.1.9h1a22.8 22.8 0 0 0 .1 1 18.1 18.1 0 0 0 .8 3.8 18.2 18.2 0 0 0 1.6 3.7l1 1.3c1 1 2.3 1.6 3.7 2a11.7 11.7 0 0 0 4.8 0h.4l.5-.2.5-.1.6-.2v6.6a8 8 0 0 0 .1 1.3 7.5 7.5 0 0 0 2.4 4.3 7.2 7.2 0 0 0 2.3 1.3 7 7 0 0 0 7-1.1 7.5 7.5 0 0 0 2-2.6A7.7 7.7 0 0 0 85 72V71a8.2 8.2 0 0 0 .2 1.3c0 .7.3 1.4.6 2a7.5 7.5 0 0 0 1.7 2.3 7.3 7.3 0 0 0 2.2 1.4 7.1 7.1 0 0 0 4.6.2 7.2 7.2 0 0 0 2.4-1.2 7.5 7.5 0 0 0 2.1-2.7 7.8 7.8 0 0 0 .7-2.4V71a9.3 9.3 0 0 0 .1.6 7.6 7.6 0 0 0 .6 2.5 7.5 7.5 0 0 0 2.4 3 7.1 7.1 0 0 0 7 .8 7.3 7.3 0 0 0 2.3-1.5 7.5 7.5 0 0 0 1.6-2.3 7.6 7.6 0 0 0 .5-2l.1-1.1v-6.7l.4.1a12.2 12.2 0 0 0 2 .5 11.1 11.1 0 0 0 2.5 0h.8l1.2-.1a9.5 9.5 0 0 0 1.4-.2l.9-.3a3.5 3.5 0 0 0 .6-.4l1.2-1.4a12.2 12.2 0 0 0 .8-1.2c0-.3.2-.5.3-.7a15.9 15.9 0 0 0 .7-2l.3-1.6v-1.3l.2-.9V54.6a15.5 15.5 0 0 0 1.8 0 4.5 4.5 0 0 0 1.4-.5 5.7 5.7 0 0 0 2.5-3.2 7.6 7.6 0 0 0 .4-1.5v-.3l-.4-1.4a5.2 5.2 0 0 1-.2-.1l-.4-.4a3.8 3.8 0 0 0-.2 0 1.4 1.4 0 0 0-.5-.2l-1.4.4-.7 1.3v.7a5.7 5.7 0 0 1-.1.8l-.7 1.4a1.9 1.9 0 0 1-.5.3h-.3a9.6 9.6 0 0 1-.8.3 8.8 8.8 0 0 1-.6 0l.2-.4.2-.5.2-.3v-.4l.1-.2V50l.1-1 .1-.6v-.6a4.8 4.8 0 0 0 0-.8v-.2l-1-1.1-1.5-.2-1.1 1-.2 1.4v.1l.2.4.2.3v.4l.1 1.1v.3l.1.5v.8a9.6 9.6 0 0 1-.8-.3l-.2-.1h-.3l-.8-.1h-.2a1.6 1.6 0 0 1-.2-.2.9.9 0 0 1-.2-.2 1 1 0 0 1-.1-.5l.2-.9v-1.2l-.9-.8h-1.2l-.8.9v.3a4.8 4.8 0 0 0-.3 2l.3.9a3.5 3.5 0 0 0 1.2 1.6l1 .5.8.2 1.4.1h.4l.2.1a12.1 12.1 0 0 1-1 2.6 13.2 13.2 0 0 1-.8 1.5 9.5 9.5 0 0 1-1 1.2l-.2.3a1.7 1.7 0 0 1-.4.3 2.4 2.4 0 0 1-.7.2h-2.5a7.8 7.8 0 0 1-.6-.2l-.7-.2h-.2a14.8 14.8 0 0 1-.6-.2 23.4 23.4 0 0 1-.4-.1l-.4-.1-.3-.1V43.9a34.6 34.6 0 0 0 0-.6 23.6 23.6 0 0 0-.4-3 22.7 22.7 0 0 0-1.5-4.7 22.6 22.6 0 0 0-4.6-6.7 21.9 21.9 0 0 0-6.9-4.7 21.2 21.2 0 0 0-8.1-1.8H92zm9.1 33.7l.3.1a1 1 0 0 1 .6.8v.4a8.4 8.4 0 0 1 0 .5 8.8 8.8 0 0 1-1.6 4.2l-1 1.3A10 10 0 0 1 95 66c-1.3.3-2.7.4-4 .3a10.4 10.4 0 0 1-2.7-.8 10 10 0 0 1-3.6-2.5 9.3 9.3 0 0 1-.8-1 9 9 0 0 1-.7-1.2 8.6 8.6 0 0 1-.8-3.4V57a1 1 0 0 1 .3-.6 1 1 0 0 1 1.3-.2 1 1 0 0 1 .4.8v.4a6.5 6.5 0 0 0 .5 2.2 7 7 0 0 0 2.1 2.8l1 .6c2.6 1.6 6 1.6 8.5 0a8 8 0 0 0 1.1-.6 7.6 7.6 0 0 0 1.2-1.2 7 7 0 0 0 1-1.7 6.5 6.5 0 0 0 .4-2.5 1 1 0 0 1 .7-1h.4zM30.7 43.7c-15.5 1-28.5-6-30.1-16.4C-1.2 15.7 11.6 4 29 1.3 46.6-1.7 62.3 5.5 64 17.1c1.6 10.4-8.7 21-23.7 25a31.2 31.2 0 0 0 0 .9v.3a19 19 0 0 0 .1 1l.1.4.1.9a4.7 4.7 0 0 0 .5 1l.7 1a9.2 9.2 0 0 0 1.2 1l1.5.8.6.8-.7.6-1.1.3a11.2 11.2 0 0 1-2.6.4 8.6 8.6 0 0 1-3-.5 8.5 8.5 0 0 1-1-.4 11.2 11.2 0 0 1-1.8-1.2 13.3 13.3 0 0 1-1-1 18 18 0 0 1-.7-.6l-.4-.4a23.4 23.4 0 0 1-1.3-1.8l-.1-.1-.3-.5V45l-.3-.6v-.7zM83.1 36c3.6 0 6.5 3.2 6.5 7.1 0 4-3 7.2-6.5 7.2S76.7 47 76.7 43 79.6 36 83 36zm18 0c3.6 0 6.5 3.2 6.5 7.1 0 4-2.9 7.2-6.4 7.2S94.7 47 94.7 43s3-7.1 6.5-7.1zm-18 6.1c2 0 3.5 1.6 3.5 3.6S85 49.2 83 49.2s-3.4-1.6-3.4-3.6S81.2 42 83 42zm17.9 0c1.9 0 3.4 1.6 3.4 3.6s-1.5 3.6-3.4 3.6c-2 0-3.5-1.6-3.5-3.6S99.1 42 101 42zM17 28c-.3 1.6-1.8 5-5.2 5.8-2.5.6-4.1-.8-4.5-2.6-.4-1.9.7-3.5 2.1-4.5A3.5 3.5 0 0 1 8 24.6c-.4-2 .8-3.7 3.2-4.2 1.9-.5 3.1.2 3.4 1.5.3 1.1-.5 2.2-1.8 2.5-.9.3-1.6 0-1.7-.6a1.4 1.4 0 0 1 0-.7s.3.2 1 0c.7-.1 1-.7.9-1.2-.2-.6-1-.8-1.8-.6-1 .2-2 1-1.7 2.6.3 1 .9 1.6 1.5 1.8l.7-.2c1-.2 1.5 0 1.6.5 0 .4-.2 1-1.2 1.2a3.3 3.3 0 0 1-1.5 0c-.9.7-1.6 1.9-1.3 3.2.3 1.3 1.3 2.2 3 1.8 2.5-.7 3.8-3.7 4.2-5-.3-.5-.6-1-.7-1.6-.1-.5.1-1 .9-1.2.4 0 .7.2.8.8a2.8 2.8 0 0 1 0 1l.7 1c.6-2 1.4-4 1.7-4 .6-.2 1.5.6 1.5.6-.8.7-1.7 2.4-2.3 4.2.8.6 1.6 1 2.1 1 .5-.1.8-.6 1-1.2-.3-2.2 1-4.3 2.3-4.6.7-.2 1.3.2 1.4.8.1.5 0 1.3-.9 1.7-.2-1-.6-1.3-1-1.3-.4.1-.7 1.4-.4 2.8.2 1 .7 1.5 1.3 1.4.8-.2 1.3-1.2 1.7-2.1-.3-2.1.9-4.2 2.2-4.5.7-.2 1.2.1 1.4 1 .4 1.4-1 2.8-2.2 3.4.3.7.7 1 1.3.9 1-.3 1.6-1.5 2-2.5l-.5-3v-.3s1.6-.3 1.8.6v.1c.2-.6.7-1.2 1.3-1.4.8-.1 1.5.6 1.7 1.6.5 2.2-.5 4.4-1.8 4.7H33a31.9 31.9 0 0 0 1 5.2c-.4.1-1.8.4-2-.4l-.5-5.6c-.5 1-1.3 2.2-2.5 2.4-1 .3-1.6-.3-2-1.1-.5 1-1.3 2.1-2.4 2.4-.8.2-1.5-.1-2-1-.3.8-.9 1.5-1.5 1.7-.7.1-1.5-.3-2.4-1-.3.8-.4 1.6-.4 2.2 0 0-.7 0-.8-.4-.1-.5 0-1.5.3-2.7a10.3 10.3 0 0 1-.7-.8zm38.2-17.8l.2.9c.5 1.9.4 4.4.8 6.4 0 .6-.4 3-1.4 3.3-.2 0-.3 0-.4-.4-.1-.7 0-1.6-.3-2.6-.2-1.1-.8-1.6-1.5-1.5-.8.2-1.3 1-1.6 2l-.1-.5c-.2-1-1.8-.6-1.8-.6a6.2 6.2 0 0 1 .4 1.3l.2 1c-.2.5-.6 1-1.2 1l-.2.1a7 7 0 0 0-.1-.8c-.3-1.1-1-2-1.6-1.8a.7.7 0 0 0-.4.3c-1.3.3-2.4 2-2.1 3.9-.2.9-.6 1.7-1 1.9-.5 0-.8-.5-1.1-1.8l-.1-1.2a4 4 0 0 0 0-1.7c0-.4-.4-.7-.8-.6-.7.2-.9 1.7-.5 3.8-.2 1-.6 2-1.3 2-.4.2-.8-.2-1-1l-.2-3c1.2-.5 2-1 1.8-1.7-.1-.5-.8-.7-.8-.7s0 .7-1 1.2l-.2-1.4c-.1-.6-.4-1-1.7-.6l.4 1 .2 1.5h-1v.8c0 .3.4.3 1 .2 0 1.3 0 2.7.2 3.6.3 1.4 1.2 2 2 1.7 1-.2 1.6-1.3 2-2.3.3 1.2 1 2 1.9 1.7.7-.2 1.2-1.1 1.6-2.2.4.8 1.1 1.1 2 1 1.2-.4 1.7-1.6 1.8-2.8h.2c.6-.2 1-.6 1.3-1 0 .8 0 1.5.2 2.1.1.5.3.7.6.6.5-.1 1-.9 1-.9a4 4 0 0 1-.3-1c-.3-1.3.3-3.6 1-3.7.2 0 .3.2.5.7v.8l.2 1.5v.7c.2.7.7 1.3 1.5 1 1.3-.2 2-2.6 2.1-3.9.3.2.6.2 1 .1-.6-2.2 0-6.1-.3-7.9-.1-.4-1-.5-1.7-.5h-.4zm-21.5 12c.4 0 .7.3 1 1.1.2 1.3-.3 2.6-.9 2.8-.2 0-.7 0-1-1.2v-.4c0-1.3.4-2 1-2.2zm-5.2 1c.3 0 .6.2.6.5.2.6-.3 1.3-1.2 2-.3-1.4.1-2.3.6-2.5zm18-.4c-.5.2-1-.4-1.2-1.2-.2-1 0-2.1.7-2.5v.5c.2.7.6 1.5 1.3 1.9 0 .7-.2 1.2-.7 1.3zm10-1.6c0 .5.4.7 1 .6.8-.2 1-1 .8-1.6 0-.5-.4-1-1-.8-.5.1-1 .9-.8 1.8zm-14.3-5.5c0-.4-.5-.7-1-.5-.8.2-1 1-.9 1.5.2.6.5 1 1 .8.5 0 1.1-1 1-1.8z\" fill=\"#fff\" fill-opacity=\".6\"></path></svg></div></div></div></div><div class=\"container\" style=\"max-width: 1024px; margin-top: 0px; margin-bottom: 0px; padding-right: 15px; padding-left: 15px; color: rgb(34, 34, 34); font-family: Helvetica, Arial, sans-serif;\"><div class=\"sf-tabs\" data-processed=\"true\"></div></div>', '<div class=\"exception-summary \" style=\"background: var(--background-error); border-bottom: 2px solid rgba(0, 0, 0, 0.1); border-top: 1px solid rgba(0, 0, 0, 0.3); flex: 0 0 auto; margin-bottom: 15px; color: rgb(34, 34, 34); font-family: Helvetica, Arial, sans-serif;\"><div class=\"exception-message-wrapper\"><div class=\"container\" style=\"max-width: 1024px; margin-top: 0px; margin-bottom: 0px; padding: 10px 15px 8px; display: flex; align-items: flex-start; min-height: 70px;\"><h1 class=\"break-long-words exception-message\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-size: 21px; overflow-wrap: break-word; hyphens: auto; min-width: 0px; flex-grow: 1; color: rgb(255, 255, 255);\">Call to a member function store() on string</h1><div><br></div><div class=\"exception-illustration hidden-xs-down\" style=\"display: initial; flex-basis: 111px; flex-shrink: 0; height: 66px; margin-left: 15px; opacity: 0.7;\"><svg viewBox=\"0 0 136 81\" xmlns=\"http://www.w3.org/2000/svg\" fill-rule=\"evenodd\" clip-rule=\"evenodd\" stroke-linejoin=\"round\" stroke-miterlimit=\"1.4\"><path d=\"M92.4 20.4a23.2 23.2 0 0 1 9 1.9 23.7 23.7 0 0 1 5.2 3 24.3 24.3 0 0 1 3.4 3.4 24.8 24.8 0 0 1 5 9.4c.5 1.7.8 3.4 1 5.2v14.5h.4l.5.2a7.4 7.4 0 0 0 2.5.2l.2-.2.6-.8.8-1.3-.2-.1a5.5 5.5 0 0 1-.8-.3 5.6 5.6 0 0 1-2.3-1.8 5.7 5.7 0 0 1-.9-1.6 6.5 6.5 0 0 1-.2-2.8 7.3 7.3 0 0 1 .5-2l.3-.3.8-.9.3-.3c.2-.2.5-.3.8-.3H120.7c.2 0 .3-.1.4 0h.4l.2.1.3.2.2-.4.3-.4.1-.1 1.2-1 .3-.2.4-.1.4-.1h.3l1.5.1.4.1.8.5.1.2 1 1.1v.2H129.4l.4-.2 1.4-.5h1.1c.3 0 .7.2 1 .4.2 0 .3.2.5.3l.2.2.5.3.4.6.1.3.4 1.4.1.4v.6a7.8 7.8 0 0 1-.1.6 9.9 9.9 0 0 1-.8 2.4 7.8 7.8 0 0 1-3 3.3 6.4 6.4 0 0 1-1 .5 6.1 6.1 0 0 1-.6.2l-.7.1h-.1a23.4 23.4 0 0 1-.2 1.7 14.3 14.3 0 0 1-.6 2.1l-.8 2a9.2 9.2 0 0 1-.4.6l-.7 1a9.1 9.1 0 0 1-2.3 2.2c-.9.5-2 .6-3 .7l-1.4.1h-.5l-.4.1a15.8 15.8 0 0 1-2.8-.1v4.2a9.7 9.7 0 0 1-.7 3.5 9.6 9.6 0 0 1-1.7 2.8 9.3 9.3 0 0 1-3 2.3 9 9 0 0 1-5.4.7 9 9 0 0 1-3-1 9.4 9.4 0 0 1-2.7-2.5 10 10 0 0 1-1 1.2 9.3 9.3 0 0 1-2 1.3 9 9 0 0 1-2.4 1 9 9 0 0 1-6.5-1.1A9.4 9.4 0 0 1 85 77V77a10.9 10.9 0 0 1-.6.6 9.3 9.3 0 0 1-2.7 2 9 9 0 0 1-6 .8 9 9 0 0 1-2.4-1 9.3 9.3 0 0 1-2.3-1.7 9.6 9.6 0 0 1-1.8-2.8 9.7 9.7 0 0 1-.8-3.7v-4a18.5 18.5 0 0 1-2.9.2l-1.2-.1c-1.9-.3-3.7-1-5.1-2.2a8.2 8.2 0 0 1-1.1-1 10.2 10.2 0 0 1-.9-1.2 15.3 15.3 0 0 1-.7-1.3 20.8 20.8 0 0 1-1.9-6.2v-.2a6.5 6.5 0 0 1-1-.3 6.1 6.1 0 0 1-.6-.3 6.6 6.6 0 0 1-.9-.6 8.2 8.2 0 0 1-2.7-3.7 10 10 0 0 1-.3-1 10.3 10.3 0 0 1-.3-1.9V47v-.4l.1-.4.6-1.4.1-.2a2 2 0 0 1 .8-.8l.3-.2.3-.2a3.2 3.2 0 0 1 1.8-.5h.4l.3.2 1.4.6.2.2.4.3.3.4.7-.7.2-.2.4-.2.6-.2h2.1l.4.2.4.2.3.2.8 1 .2-.1h.1v-.1H63l1.1.1h.3l.8.5.3.4.7 1 .2.3.1.5a11 11 0 0 1 .2 1.5c0 .8 0 1.6-.3 2.3a6 6 0 0 1-.5 1.2 5.5 5.5 0 0 1-3.3 2.5 12.3 12.3 0 0 0 1.4 3h.1l.2.1 1 .2h1.5l.5-.2H67.8l.5-.2h.1V44v-.4a26.7 26.7 0 0 1 .3-2.3 24.7 24.7 0 0 1 5.7-12.5 24.2 24.2 0 0 1 3.5-3.3 23.7 23.7 0 0 1 4.9-3 23.2 23.2 0 0 1 5.6-1.7 23.7 23.7 0 0 1 4-.3zm-.3 2a21.2 21.2 0 0 0-8 1.7 21.6 21.6 0 0 0-4.8 2.7 22.2 22.2 0 0 0-3.2 3 22.7 22.7 0 0 0-5 9.2 23.4 23.4 0 0 0-.7 4.9v15.7l-.5.1a34.3 34.3 0 0 1-1.5.3h-.2l-.4.1h-.4l-.9.2a10 10 0 0 1-1.9 0c-.5 0-1-.2-1.5-.4a1.8 1.8 0 0 1-.3-.2 2 2 0 0 1-.3-.3 5.2 5.2 0 0 1-.1-.2 9 9 0 0 1-.6-.9 13.8 13.8 0 0 1-1-2 14.3 14.3 0 0 1-.6-2 14 14 0 0 1-.1-.8v-.2h.3a12.8 12.8 0 0 0 1.4-.2 4.4 4.4 0 0 0 .3 0 3.6 3.6 0 0 0 1.1-.7 3.4 3.4 0 0 0 1.2-1.7l.2-1.2a5.1 5.1 0 0 0 0-.8 7.2 7.2 0 0 0-.1-.8l-.7-1-1.2-.2-1 .7-.1 1.3a5 5 0 0 1 .1.4v.6a1 1 0 0 1 0 .3c-.1.3-.4.4-.7.5l-1.2.4v-.7A9.9 9.9 0 0 1 60 49l.3-.6v-.2l.1-.1v-1.6l-1-1.2h-1.5l-1 1.1v.4a5.3 5.3 0 0 0-.2.6 5.5 5.5 0 0 0 0 .5c0 .7 0 1.4.3 2 0 .4.2.8.4 1.2L57 51a9.5 9.5 0 0 1-1.1-.5h-.2a2 2 0 0 1-.4-.3c-.4-.4-.5-1-.6-1.6a5.6 5.6 0 0 1 0-.5v-.5-.5l-.6-1.5-1.4-.6-.9.3s-.2 0-.3.2a2 2 0 0 1-.1 0l-.6 1.4v.7a8.5 8.5 0 0 0 .5 2c.4 1.1 1 2.1 2 2.8a4.7 4.7 0 0 0 2.1.9h1a22.8 22.8 0 0 0 .1 1 18.1 18.1 0 0 0 .8 3.8 18.2 18.2 0 0 0 1.6 3.7l1 1.3c1 1 2.3 1.6 3.7 2a11.7 11.7 0 0 0 4.8 0h.4l.5-.2.5-.1.6-.2v6.6a8 8 0 0 0 .1 1.3 7.5 7.5 0 0 0 2.4 4.3 7.2 7.2 0 0 0 2.3 1.3 7 7 0 0 0 7-1.1 7.5 7.5 0 0 0 2-2.6A7.7 7.7 0 0 0 85 72V71a8.2 8.2 0 0 0 .2 1.3c0 .7.3 1.4.6 2a7.5 7.5 0 0 0 1.7 2.3 7.3 7.3 0 0 0 2.2 1.4 7.1 7.1 0 0 0 4.6.2 7.2 7.2 0 0 0 2.4-1.2 7.5 7.5 0 0 0 2.1-2.7 7.8 7.8 0 0 0 .7-2.4V71a9.3 9.3 0 0 0 .1.6 7.6 7.6 0 0 0 .6 2.5 7.5 7.5 0 0 0 2.4 3 7.1 7.1 0 0 0 7 .8 7.3 7.3 0 0 0 2.3-1.5 7.5 7.5 0 0 0 1.6-2.3 7.6 7.6 0 0 0 .5-2l.1-1.1v-6.7l.4.1a12.2 12.2 0 0 0 2 .5 11.1 11.1 0 0 0 2.5 0h.8l1.2-.1a9.5 9.5 0 0 0 1.4-.2l.9-.3a3.5 3.5 0 0 0 .6-.4l1.2-1.4a12.2 12.2 0 0 0 .8-1.2c0-.3.2-.5.3-.7a15.9 15.9 0 0 0 .7-2l.3-1.6v-1.3l.2-.9V54.6a15.5 15.5 0 0 0 1.8 0 4.5 4.5 0 0 0 1.4-.5 5.7 5.7 0 0 0 2.5-3.2 7.6 7.6 0 0 0 .4-1.5v-.3l-.4-1.4a5.2 5.2 0 0 1-.2-.1l-.4-.4a3.8 3.8 0 0 0-.2 0 1.4 1.4 0 0 0-.5-.2l-1.4.4-.7 1.3v.7a5.7 5.7 0 0 1-.1.8l-.7 1.4a1.9 1.9 0 0 1-.5.3h-.3a9.6 9.6 0 0 1-.8.3 8.8 8.8 0 0 1-.6 0l.2-.4.2-.5.2-.3v-.4l.1-.2V50l.1-1 .1-.6v-.6a4.8 4.8 0 0 0 0-.8v-.2l-1-1.1-1.5-.2-1.1 1-.2 1.4v.1l.2.4.2.3v.4l.1 1.1v.3l.1.5v.8a9.6 9.6 0 0 1-.8-.3l-.2-.1h-.3l-.8-.1h-.2a1.6 1.6 0 0 1-.2-.2.9.9 0 0 1-.2-.2 1 1 0 0 1-.1-.5l.2-.9v-1.2l-.9-.8h-1.2l-.8.9v.3a4.8 4.8 0 0 0-.3 2l.3.9a3.5 3.5 0 0 0 1.2 1.6l1 .5.8.2 1.4.1h.4l.2.1a12.1 12.1 0 0 1-1 2.6 13.2 13.2 0 0 1-.8 1.5 9.5 9.5 0 0 1-1 1.2l-.2.3a1.7 1.7 0 0 1-.4.3 2.4 2.4 0 0 1-.7.2h-2.5a7.8 7.8 0 0 1-.6-.2l-.7-.2h-.2a14.8 14.8 0 0 1-.6-.2 23.4 23.4 0 0 1-.4-.1l-.4-.1-.3-.1V43.9a34.6 34.6 0 0 0 0-.6 23.6 23.6 0 0 0-.4-3 22.7 22.7 0 0 0-1.5-4.7 22.6 22.6 0 0 0-4.6-6.7 21.9 21.9 0 0 0-6.9-4.7 21.2 21.2 0 0 0-8.1-1.8H92zm9.1 33.7l.3.1a1 1 0 0 1 .6.8v.4a8.4 8.4 0 0 1 0 .5 8.8 8.8 0 0 1-1.6 4.2l-1 1.3A10 10 0 0 1 95 66c-1.3.3-2.7.4-4 .3a10.4 10.4 0 0 1-2.7-.8 10 10 0 0 1-3.6-2.5 9.3 9.3 0 0 1-.8-1 9 9 0 0 1-.7-1.2 8.6 8.6 0 0 1-.8-3.4V57a1 1 0 0 1 .3-.6 1 1 0 0 1 1.3-.2 1 1 0 0 1 .4.8v.4a6.5 6.5 0 0 0 .5 2.2 7 7 0 0 0 2.1 2.8l1 .6c2.6 1.6 6 1.6 8.5 0a8 8 0 0 0 1.1-.6 7.6 7.6 0 0 0 1.2-1.2 7 7 0 0 0 1-1.7 6.5 6.5 0 0 0 .4-2.5 1 1 0 0 1 .7-1h.4zM30.7 43.7c-15.5 1-28.5-6-30.1-16.4C-1.2 15.7 11.6 4 29 1.3 46.6-1.7 62.3 5.5 64 17.1c1.6 10.4-8.7 21-23.7 25a31.2 31.2 0 0 0 0 .9v.3a19 19 0 0 0 .1 1l.1.4.1.9a4.7 4.7 0 0 0 .5 1l.7 1a9.2 9.2 0 0 0 1.2 1l1.5.8.6.8-.7.6-1.1.3a11.2 11.2 0 0 1-2.6.4 8.6 8.6 0 0 1-3-.5 8.5 8.5 0 0 1-1-.4 11.2 11.2 0 0 1-1.8-1.2 13.3 13.3 0 0 1-1-1 18 18 0 0 1-.7-.6l-.4-.4a23.4 23.4 0 0 1-1.3-1.8l-.1-.1-.3-.5V45l-.3-.6v-.7zM83.1 36c3.6 0 6.5 3.2 6.5 7.1 0 4-3 7.2-6.5 7.2S76.7 47 76.7 43 79.6 36 83 36zm18 0c3.6 0 6.5 3.2 6.5 7.1 0 4-2.9 7.2-6.4 7.2S94.7 47 94.7 43s3-7.1 6.5-7.1zm-18 6.1c2 0 3.5 1.6 3.5 3.6S85 49.2 83 49.2s-3.4-1.6-3.4-3.6S81.2 42 83 42zm17.9 0c1.9 0 3.4 1.6 3.4 3.6s-1.5 3.6-3.4 3.6c-2 0-3.5-1.6-3.5-3.6S99.1 42 101 42zM17 28c-.3 1.6-1.8 5-5.2 5.8-2.5.6-4.1-.8-4.5-2.6-.4-1.9.7-3.5 2.1-4.5A3.5 3.5 0 0 1 8 24.6c-.4-2 .8-3.7 3.2-4.2 1.9-.5 3.1.2 3.4 1.5.3 1.1-.5 2.2-1.8 2.5-.9.3-1.6 0-1.7-.6a1.4 1.4 0 0 1 0-.7s.3.2 1 0c.7-.1 1-.7.9-1.2-.2-.6-1-.8-1.8-.6-1 .2-2 1-1.7 2.6.3 1 .9 1.6 1.5 1.8l.7-.2c1-.2 1.5 0 1.6.5 0 .4-.2 1-1.2 1.2a3.3 3.3 0 0 1-1.5 0c-.9.7-1.6 1.9-1.3 3.2.3 1.3 1.3 2.2 3 1.8 2.5-.7 3.8-3.7 4.2-5-.3-.5-.6-1-.7-1.6-.1-.5.1-1 .9-1.2.4 0 .7.2.8.8a2.8 2.8 0 0 1 0 1l.7 1c.6-2 1.4-4 1.7-4 .6-.2 1.5.6 1.5.6-.8.7-1.7 2.4-2.3 4.2.8.6 1.6 1 2.1 1 .5-.1.8-.6 1-1.2-.3-2.2 1-4.3 2.3-4.6.7-.2 1.3.2 1.4.8.1.5 0 1.3-.9 1.7-.2-1-.6-1.3-1-1.3-.4.1-.7 1.4-.4 2.8.2 1 .7 1.5 1.3 1.4.8-.2 1.3-1.2 1.7-2.1-.3-2.1.9-4.2 2.2-4.5.7-.2 1.2.1 1.4 1 .4 1.4-1 2.8-2.2 3.4.3.7.7 1 1.3.9 1-.3 1.6-1.5 2-2.5l-.5-3v-.3s1.6-.3 1.8.6v.1c.2-.6.7-1.2 1.3-1.4.8-.1 1.5.6 1.7 1.6.5 2.2-.5 4.4-1.8 4.7H33a31.9 31.9 0 0 0 1 5.2c-.4.1-1.8.4-2-.4l-.5-5.6c-.5 1-1.3 2.2-2.5 2.4-1 .3-1.6-.3-2-1.1-.5 1-1.3 2.1-2.4 2.4-.8.2-1.5-.1-2-1-.3.8-.9 1.5-1.5 1.7-.7.1-1.5-.3-2.4-1-.3.8-.4 1.6-.4 2.2 0 0-.7 0-.8-.4-.1-.5 0-1.5.3-2.7a10.3 10.3 0 0 1-.7-.8zm38.2-17.8l.2.9c.5 1.9.4 4.4.8 6.4 0 .6-.4 3-1.4 3.3-.2 0-.3 0-.4-.4-.1-.7 0-1.6-.3-2.6-.2-1.1-.8-1.6-1.5-1.5-.8.2-1.3 1-1.6 2l-.1-.5c-.2-1-1.8-.6-1.8-.6a6.2 6.2 0 0 1 .4 1.3l.2 1c-.2.5-.6 1-1.2 1l-.2.1a7 7 0 0 0-.1-.8c-.3-1.1-1-2-1.6-1.8a.7.7 0 0 0-.4.3c-1.3.3-2.4 2-2.1 3.9-.2.9-.6 1.7-1 1.9-.5 0-.8-.5-1.1-1.8l-.1-1.2a4 4 0 0 0 0-1.7c0-.4-.4-.7-.8-.6-.7.2-.9 1.7-.5 3.8-.2 1-.6 2-1.3 2-.4.2-.8-.2-1-1l-.2-3c1.2-.5 2-1 1.8-1.7-.1-.5-.8-.7-.8-.7s0 .7-1 1.2l-.2-1.4c-.1-.6-.4-1-1.7-.6l.4 1 .2 1.5h-1v.8c0 .3.4.3 1 .2 0 1.3 0 2.7.2 3.6.3 1.4 1.2 2 2 1.7 1-.2 1.6-1.3 2-2.3.3 1.2 1 2 1.9 1.7.7-.2 1.2-1.1 1.6-2.2.4.8 1.1 1.1 2 1 1.2-.4 1.7-1.6 1.8-2.8h.2c.6-.2 1-.6 1.3-1 0 .8 0 1.5.2 2.1.1.5.3.7.6.6.5-.1 1-.9 1-.9a4 4 0 0 1-.3-1c-.3-1.3.3-3.6 1-3.7.2 0 .3.2.5.7v.8l.2 1.5v.7c.2.7.7 1.3 1.5 1 1.3-.2 2-2.6 2.1-3.9.3.2.6.2 1 .1-.6-2.2 0-6.1-.3-7.9-.1-.4-1-.5-1.7-.5h-.4zm-21.5 12c.4 0 .7.3 1 1.1.2 1.3-.3 2.6-.9 2.8-.2 0-.7 0-1-1.2v-.4c0-1.3.4-2 1-2.2zm-5.2 1c.3 0 .6.2.6.5.2.6-.3 1.3-1.2 2-.3-1.4.1-2.3.6-2.5zm18-.4c-.5.2-1-.4-1.2-1.2-.2-1 0-2.1.7-2.5v.5c.2.7.6 1.5 1.3 1.9 0 .7-.2 1.2-.7 1.3zm10-1.6c0 .5.4.7 1 .6.8-.2 1-1 .8-1.6 0-.5-.4-1-1-.8-.5.1-1 .9-.8 1.8zm-14.3-5.5c0-.4-.5-.7-1-.5-.8.2-1 1-.9 1.5.2.6.5 1 1 .8.5 0 1.1-1 1-1.8z\" fill=\"#fff\" fill-opacity=\".6\"></path></svg></div></div></div></div><div class=\"container\" style=\"max-width: 1024px; margin-top: 0px; margin-bottom: 0px; padding-right: 15px; padding-left: 15px; color: rgb(34, 34, 34); font-family: Helvetica, Arial, sans-serif;\"><div class=\"sf-tabs\" data-processed=\"true\"></div></div>', 1, 0, 'public/images/blogs/RoTTU8FcYmdP3byd7KfQ0IhTAq6M1qEPtARlwYEj.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `blog_blogtags`
--

CREATE TABLE `blog_blogtags` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `blog_id` int(10) UNSIGNED DEFAULT NULL,
  `blog_tag_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_blogtags`
--

INSERT INTO `blog_blogtags` (`id`, `created_at`, `updated_at`, `deleted_at`, `blog_id`, `blog_tag_id`) VALUES
(23, '2021-08-30 07:17:51', '2021-08-30 07:17:51', NULL, 9, 2),
(24, '2021-08-30 07:17:51', '2021-08-30 07:17:51', NULL, 9, 4),
(27, '2021-08-30 08:00:09', '2021-08-30 08:00:09', NULL, 2, 3),
(28, '2021-08-30 08:00:09', '2021-08-30 08:00:09', NULL, 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `blog_comments`
--

CREATE TABLE `blog_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `blog_id` int(10) UNSIGNED NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_comments`
--

INSERT INTO `blog_comments` (`id`, `created_at`, `updated_at`, `deleted_at`, `customer_id`, `blog_id`, `comment`) VALUES
(1, '2021-08-09 08:52:51', '2021-08-09 08:52:51', NULL, 4, 2, 'customer_id'),
(2, '2021-08-09 08:54:04', '2021-08-09 08:54:04', NULL, 4, 2, '<script src=\"https://unpkg.com/sweetalert/dist/sweetalert.min.js\"></script>\n<script src=\"https://unpkg.com/sweetalert/dist/sweetalert.min.js\"></script>'),
(3, '2021-08-09 08:54:14', '2021-08-09 08:54:14', NULL, 4, 2, '<script src=\"https://unpkg.com/sweetalert/dist/sweetalert'),
(4, '2021-08-09 08:55:13', '2021-08-09 08:55:13', NULL, 4, 2, '}\n        dd($comment);'),
(5, '2021-08-09 08:57:16', '2021-08-09 08:57:16', NULL, 4, 2, '</div>\n                                        </div>'),
(42, '2021-08-15 06:27:23', '2021-08-15 06:27:23', NULL, 4, 8, 'jugdr');

-- --------------------------------------------------------

--
-- Table structure for table `blog_tags`
--

CREATE TABLE `blog_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `tag_ar` varchar(255) NOT NULL,
  `tag_en` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_tags`
--

INSERT INTO `blog_tags` (`id`, `created_at`, `updated_at`, `deleted_at`, `tag_ar`, `tag_en`, `status`, `user_id`) VALUES
(1, '2021-07-29 20:14:29', '2021-09-06 14:53:37', NULL, 'كلاب', 'Dogs', 1, 1),
(2, '2021-07-29 20:14:46', '2021-07-29 20:15:10', NULL, 'قطط', 'Cats', 1, 1),
(3, '2021-08-30 07:06:19', '2021-08-30 07:07:48', NULL, 'حيوانات مائيه', 'aquatic animals', 1, 1),
(4, '2021-08-30 07:07:19', '2021-08-30 07:07:57', NULL, 'حيوانات أليفه', 'Pets', 1, 1),
(5, '2021-08-30 07:07:28', '2021-08-30 07:08:06', NULL, 'طيور', 'Birds', 1, 1),
(6, '2021-08-30 07:07:43', '2021-09-06 12:53:17', NULL, 'زواحف', 'ٌreptile', 1, 1),
(7, '2021-09-06 12:39:49', '2021-09-06 14:52:13', '2021-09-06 14:52:13', 'kjnhb', 'ccvbhnjm', 0, 1),
(8, '2021-09-06 18:41:34', '2021-09-06 14:52:29', '2021-09-06 14:52:29', 'قططgg', 'grtm', 0, 1),
(9, '2021-09-06 14:43:18', '2021-09-06 14:52:21', '2021-09-06 14:52:21', 'bbfg,mnjbgg', 'hnjmk,dlrfe', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `quantity` int(11) DEFAULT 1,
  `price_after_quantity` int(11) DEFAULT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `customer_id` int(10) DEFAULT NULL,
  `tax` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `created_at`, `updated_at`, `deleted_at`, `product_id`, `quantity`, `price_after_quantity`, `order_id`, `customer_id`, `tax`) VALUES
(244, '2021-09-05 10:20:05', '2021-09-26 11:23:16', '2021-09-26 11:23:16', 6, 1, 230, 4, 4, 10),
(246, '2021-09-05 10:23:26', '2021-09-07 06:21:32', '2021-09-07 06:21:32', 7, 1, 200, 4, 4, 10),
(247, '2021-09-12 07:09:14', '2021-11-02 04:57:50', '2021-11-02 04:57:50', 12, 3, 1140, 4, 4, 10),
(248, '2021-09-24 12:53:24', '2021-09-24 12:53:37', '2021-09-24 12:53:37', 7, 1, 200, 5, 24, 10),
(249, '2021-09-24 12:53:56', '2021-09-24 12:55:00', '2021-09-24 12:55:00', 7, 1, 200, 5, 24, 10),
(250, '2021-09-24 12:56:22', '2021-09-24 12:56:29', '2021-09-24 12:56:29', 7, 1, 200, 5, 24, 10),
(251, '2021-09-24 12:56:34', '2021-09-24 12:58:02', '2021-09-24 12:58:02', 7, 1, 200, 5, 24, 10),
(252, '2021-09-24 12:56:40', '2021-10-03 09:33:39', '2021-10-03 09:33:39', 8, 1, 111, 5, 24, 10),
(253, '2021-09-24 12:57:03', '2021-10-03 09:33:46', '2021-10-03 09:33:46', 12, 1, 380, 5, 24, 10),
(254, '2021-09-24 12:58:05', '2021-09-24 12:58:20', '2021-09-24 12:58:20', 7, 5, 1000, 5, 24, 10),
(255, '2021-09-24 12:59:53', '2021-09-24 13:00:38', '2021-09-24 13:00:38', 7, 1, 200, 5, 24, 10),
(256, '2021-09-24 13:21:54', '2021-09-24 13:21:58', '2021-09-24 13:21:58', 7, 1, 200, 5, 24, 10),
(257, '2021-09-26 06:32:51', '2021-09-26 06:32:51', '2021-09-26 13:20:51', 14, 1, 0, 6, 27, 10),
(258, '2021-09-26 06:33:45', '2021-09-26 06:40:26', '2021-09-26 06:40:26', 6, 1, 0, 6, 27, 10),
(259, '2021-09-26 06:34:08', '2021-09-26 06:34:08', '2021-09-26 13:20:47', 7, 1, 0, 6, 27, 10),
(260, '2021-09-26 06:34:36', '2021-09-26 06:34:42', '2021-09-26 06:34:42', 12, 1, 380, 6, 27, 10),
(261, '2021-09-26 06:34:48', '2021-09-26 06:42:00', '2021-09-26 06:42:00', 13, 1, 300, 6, 27, 10),
(262, '2021-09-26 06:34:59', '2021-09-26 06:37:16', '2021-09-26 06:37:16', 25, 1, 78, 6, 27, 10),
(263, '2021-09-26 06:35:08', '2021-09-26 06:35:08', '2021-09-26 13:20:43', 21, 1, 0, 6, 27, 10),
(264, '2021-09-26 06:37:18', '2021-09-26 06:37:31', '2021-09-26 06:37:31', 25, 1, 78, 6, 27, 10),
(265, '2021-09-26 06:37:34', '2021-09-26 06:37:34', '2021-09-26 13:20:41', 25, 1, 0, 6, 27, 10),
(266, '2021-09-26 06:38:51', '2021-09-26 06:40:35', '2021-09-26 06:40:35', 8, 1, 111, 6, 27, 10),
(267, '2021-09-26 06:40:22', '2021-11-02 04:58:27', NULL, 12, 1, 380, 4, 27, 10),
(268, '2021-09-26 06:40:29', '2021-11-02 04:54:28', NULL, 6, 1, 230, 4, 27, 10),
(269, '2021-09-26 06:40:38', '2021-09-26 06:40:48', '2021-09-26 06:40:48', 8, 1, 0, 6, 27, 10),
(270, '2021-09-26 06:40:51', '2021-09-26 06:41:38', '2021-09-26 06:41:38', 8, 1, 0, 6, 27, 10),
(271, '2021-09-26 06:41:43', '2021-09-26 06:41:43', '2021-09-26 13:20:36', 8, 1, 0, 6, 27, 10),
(272, '2021-09-26 06:42:03', '2021-10-06 05:07:55', NULL, 13, 2, 600, 5, 27, 10),
(273, '2021-09-26 06:45:30', '2021-09-26 06:46:20', '2021-09-09 11:28:27', 7, 1, 200, 7, 26, 10),
(274, '2021-09-26 09:17:31', '2021-09-26 09:34:13', '2021-09-26 09:34:13', 25, 1, 78, 7, 26, 10),
(275, '2021-09-26 09:40:56', '2021-11-02 04:54:28', NULL, 6, 1, 230, 4, 26, 10),
(276, '2021-09-26 11:23:19', '2021-11-02 04:57:50', '2021-11-02 04:57:50', 6, 1, 230, 4, 4, 10),
(277, '2021-09-26 11:28:25', '2021-09-26 11:28:31', '2021-09-26 11:28:31', 26, 1, 430, 4, 4, 10),
(278, '2021-09-27 12:19:09', '2021-11-02 04:54:28', NULL, 6, 1, 230, 4, 29, 10),
(279, '2021-09-28 08:32:47', '2021-10-06 05:20:05', NULL, 7, 1, 200, 5, 29, 10),
(280, '2021-09-28 09:07:21', '2021-11-02 04:58:27', NULL, 14, 2, 400, 4, 31, 10),
(281, '2021-09-30 09:33:01', '2021-10-06 05:20:05', NULL, 7, 1, 200, 5, 36, 10),
(282, '2021-09-30 09:33:18', '2021-10-06 05:20:05', NULL, 8, 1, 111, 5, 36, 10),
(283, '2021-09-30 09:33:59', '2021-10-06 05:20:05', NULL, 8, 1, 111, 5, 36, 10),
(284, '2021-09-30 09:34:53', '2021-11-02 04:58:27', NULL, 12, 1, 380, 4, 36, 10),
(285, '2021-09-30 09:37:29', '2021-11-02 04:58:27', NULL, 14, 2, 400, 4, 36, 10),
(286, '2021-09-30 09:37:44', '2021-09-30 09:37:44', NULL, 21, 1, 0, 10, 36, 10),
(287, '2021-09-30 09:41:09', '2021-09-30 09:41:09', NULL, 21, 1, 0, 10, 36, 10),
(288, '2021-09-30 09:41:44', '2021-09-30 09:41:44', NULL, 21, 1, 0, 10, 36, 10),
(289, '2021-09-30 09:43:14', '2021-09-30 09:43:14', NULL, 22, 1, 0, 10, 36, 10),
(290, '2021-09-30 09:45:10', '2021-09-30 09:45:10', NULL, 25, 1, 78, 10, 36, 10),
(291, '2021-10-03 09:33:56', '2021-10-03 09:34:41', '2021-10-03 09:34:41', 6, 1, 230, 5, 24, 10),
(292, '2021-10-03 09:34:01', '2021-10-03 09:34:41', '2021-10-03 09:34:41', 8, 1, 111, 5, 24, 10),
(293, '2021-10-03 09:34:05', '2021-10-03 09:34:41', '2021-10-03 09:34:41', 7, 1, 200, 5, 24, 10),
(294, '2021-10-03 09:34:55', '2021-10-03 09:41:08', '2021-10-03 09:41:08', 12, 1, 380, 5, 24, 10),
(295, '2021-10-03 09:34:59', '2021-10-03 09:41:02', '2021-10-03 09:41:02', 13, 1, 300, 5, 24, 10),
(296, '2021-10-03 09:35:01', '2021-10-03 09:40:42', '2021-10-03 09:40:42', 14, 2, 400, 5, 24, 10),
(297, '2021-10-03 09:43:05', '2021-10-03 09:44:00', '2021-10-03 09:44:00', 6, 2, 460, 5, 24, 10),
(298, '2021-10-03 09:43:09', '2021-10-03 09:44:00', '2021-10-03 09:44:00', 7, 2, 400, 5, 24, 10),
(299, '2021-10-03 09:44:26', '2021-10-06 05:06:41', '2021-10-06 05:06:41', 12, 1, 380, 5, 24, 10),
(300, '2021-10-03 09:44:30', '2021-10-06 05:06:42', '2021-10-06 05:06:42', 13, 4, 1200, 5, 24, 10),
(301, '2021-10-03 10:05:26', '2021-10-03 10:09:02', '2021-10-03 10:09:02', 6, 1, 230, 5, 24, 10),
(302, '2021-10-03 10:05:28', '2021-10-03 10:08:56', '2021-10-03 10:08:56', 8, 1, 111, 5, 24, 10),
(303, '2021-10-03 11:36:27', '2021-10-03 11:36:40', '2021-10-03 11:36:40', 12, 1, 380, 5, 24, 10),
(304, '2021-10-03 11:36:34', '2021-10-03 11:37:05', '2021-10-03 11:37:05', 13, 1, 300, 5, 24, 10),
(305, '2021-10-03 11:37:24', '2021-10-03 11:37:39', '2021-10-03 11:37:39', 13, 1, 300, 5, 24, 10),
(306, '2021-10-03 11:37:27', '2021-10-03 11:37:39', '2021-10-03 11:37:39', 12, 1, 380, 5, 24, 10),
(307, '2021-10-03 11:37:56', '2021-10-06 05:06:42', '2021-10-06 05:06:42', 12, 1, 380, 5, 24, 10),
(308, '2021-10-03 11:37:59', '2021-10-06 05:06:42', '2021-10-06 05:06:42', 13, 4, 1200, 5, 24, 10),
(309, '2021-10-03 11:38:05', '2021-10-03 11:38:18', '2021-10-03 11:38:18', 7, 1, 200, 5, 24, 10),
(310, '2021-10-06 05:07:34', '2021-10-06 05:08:05', '2021-10-06 05:08:05', 13, 2, 600, 5, 24, 10),
(311, '2021-10-06 05:08:14', '2021-10-06 05:21:28', '2021-10-06 05:21:28', 7, 1, 200, 5, 24, 10),
(312, '2021-10-06 05:08:17', '2021-10-06 05:21:22', '2021-10-06 05:21:22', 8, 1, 111, 5, 24, 10),
(313, '2021-10-06 05:21:40', '2021-11-02 04:54:28', NULL, 6, 1, 230, 4, 24, 10),
(314, '2021-11-02 04:58:10', '2021-11-02 05:07:28', '2021-11-02 05:07:28', 14, 2, 400, 4, 4, 10),
(315, '2021-11-02 04:58:12', '2021-11-02 05:00:57', '2021-11-02 05:00:57', 12, 1, 380, 4, 4, 10),
(316, '2021-11-02 05:07:40', '2021-11-02 05:07:40', NULL, 14, 1, 200, 4, 4, 10),
(317, '2021-11-02 05:07:42', '2021-11-02 05:22:45', '2021-11-02 05:22:45', 12, 1, 380, 4, 4, 10);

-- --------------------------------------------------------

--
-- Table structure for table `categorys`
--

CREATE TABLE `categorys` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `category_name_ar` varchar(255) NOT NULL,
  `category_name_en` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categorys`
--

INSERT INTO `categorys` (`id`, `created_at`, `updated_at`, `deleted_at`, `category_name_ar`, `category_name_en`, `status`) VALUES
(1, '2021-07-19 19:55:12', '2021-08-30 09:39:09', NULL, 'الالعاب', 'Games', 1),
(2, '2021-07-27 06:17:57', '2021-08-30 09:39:18', NULL, 'الطعام', 'Food', 1),
(3, '2021-07-28 17:48:54', '2021-07-28 18:28:42', '2021-07-28 18:28:42', 'قسم ثثمنت', 'ىلالربيبلاتن', 1),
(4, '2021-08-28 17:07:55', '2021-08-30 09:37:57', NULL, 'الاكسسوارات', 'Accessories', 1),
(5, '2021-08-30 09:41:51', '2021-08-30 09:42:23', NULL, 'التدريب والنظافه', 'Training and Grooming', 1),
(6, '2021-08-30 09:42:19', '2021-08-30 09:42:28', NULL, 'الصحه والجمال', 'Health and Beauty', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category_products`
--

CREATE TABLE `category_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `animal_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_products`
--

INSERT INTO `category_products` (`id`, `created_at`, `updated_at`, `deleted_at`, `product_id`, `category_id`, `animal_id`) VALUES
(2, '2021-07-29 19:46:30', '2021-09-01 09:15:11', NULL, 6, 1, 1),
(3, '2021-07-29 19:48:47', '2021-09-01 09:18:44', NULL, 7, 2, 1),
(4, '2021-07-29 19:49:57', '2021-09-01 09:21:27', NULL, 8, 2, 1),
(8, '2021-08-07 12:45:06', '2021-09-01 09:24:48', NULL, 12, 2, 1),
(9, '2021-08-10 07:56:31', '2021-09-01 09:27:41', NULL, 13, 2, 1),
(10, '2021-08-10 19:34:02', '2021-09-01 09:30:12', NULL, 14, 2, 2),
(15, '2021-08-11 08:16:27', '2021-09-01 09:33:16', NULL, 21, 2, 2),
(16, '2021-08-11 08:16:51', '2021-09-01 09:34:45', NULL, 22, 2, 2),
(17, '2021-08-26 08:23:15', '2021-09-01 09:37:22', NULL, 23, 2, 2),
(18, '2021-08-28 14:43:10', '2021-09-01 09:39:18', NULL, 24, 2, 2),
(19, '2021-08-28 14:45:06', '2021-09-01 09:41:47', NULL, 25, 1, 2),
(23, '2021-08-28 14:45:06', '2021-09-01 10:00:44', NULL, 26, 5, 2),
(24, '2021-09-01 10:03:47', '2021-09-01 10:12:53', NULL, 32, 4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `citys`
--

CREATE TABLE `citys` (
  `id` int(11) NOT NULL,
  `city_name_ar` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `citys`
--

INSERT INTO `citys` (`id`, `city_name_ar`, `created_at`, `updated_at`) VALUES
(1, 'الرياض', '2021-02-04 14:20:59', '2021-02-04 11:20:59'),
(2, 'مكة', '2021-01-10 10:08:29', '2021-01-10 10:08:29'),
(3, 'المدينة المنورة', '2021-01-12 08:32:32', '2021-01-12 08:32:32'),
(4, 'المنطقه الشرقيه', '2021-09-03 21:54:38', '2021-01-12 08:32:59'),
(5, 'الطائف', '2021-01-12 09:27:09', '2021-01-12 09:27:09'),
(6, 'تبوك', '2021-01-12 09:27:50', '2021-01-12 09:27:50'),
(48, 'جازان', '2021-02-04 14:20:59', '2021-02-04 11:20:59'),
(49, 'القصيم', '2021-01-10 10:08:29', '2021-01-10 10:08:29'),
(50, 'عسير', '2021-01-12 08:32:32', '2021-01-12 08:32:32'),
(51, 'الجوف', '2021-09-03 21:54:38', '2021-01-12 08:32:59'),
(52, 'الباحة', '2021-01-12 09:27:09', '2021-01-12 09:27:09'),
(53, 'حائل', '2021-09-09 10:34:27', '2021-09-09 08:34:27'),
(55, 'vbdgfgjf', '2021-09-15 10:32:21', '2021-09-15 10:32:21');

-- --------------------------------------------------------

--
-- Table structure for table `clinics`
--

CREATE TABLE `clinics` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `clinic_image` text DEFAULT NULL,
  `clinic_title_ar` varchar(255) NOT NULL,
  `clinic_title_en` varchar(255) NOT NULL,
  `clinic_description_ar` text NOT NULL,
  `clinic_description_en` text NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clinics`
--

INSERT INTO `clinics` (`id`, `created_at`, `updated_at`, `deleted_at`, `clinic_image`, `clinic_title_ar`, `clinic_title_en`, `clinic_description_ar`, `clinic_description_en`, `user_id`) VALUES
(1, '2021-08-02 18:54:12', '2021-09-07 11:08:41', NULL, 'public/images/clinics/Y5VYzGF3wuN0tfVrkUOQSSoTDuOiOwM4T8xYoLzp.png', 'المركز البيطري', 'The Veterinary Center', '<p style=\"margin-right: 0px; margin-bottom: 30px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: middle; background: transparent; line-height: 2;\"><font color=\"#7d7d7d\" face=\"c-rg\"><span style=\"text-transform: capitalize;\">المستشفي البيطري للعلاج و التحاليل المعملية هي احدي وحدات الرئاسة العامة التابعة لإدارة الخدمات البيطرية . تم إنشاء المستشفي البيطري للعلاج و التحاليل المعملية فى عام ٢٠٢٠&nbsp; . تقوم المستشفي بتقديم الخدمة العلاجية&nbsp; .</span></font></p><p style=\"text-transform: capitalize; font-family: c-rg; color: rgb(125, 125, 125); margin-right: 0px; margin-bottom: 30px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: middle; background: transparent; line-height: 2;\"><br></p>', 'The Veterinary Hospital for treatment and laboratory analysis is one of the units of the General Presidency of the Department of Veterinary Services. The Veterinary Hospital for treatment and laboratory analysis was established in 2020. The hospital provides the treatment service.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `clinic_reservations`
--

CREATE TABLE `clinic_reservations` (
  `id` int(10) NOT NULL,
  `customer_id` int(10) DEFAULT NULL,
  `clinic_id` int(10) DEFAULT 1,
  `service_id` int(10) DEFAULT NULL,
  `service_price` int(20) DEFAULT NULL,
  `animal_id` int(10) DEFAULT NULL,
  `animal_no` int(10) DEFAULT NULL,
  `reservation_date` varchar(250) DEFAULT NULL,
  `request_no` int(30) NOT NULL DEFAULT 0,
  `status` int(11) DEFAULT 0 COMMENT '0 => موجود فالسله 1 => انتظار الوصول 2 => مشترياتي	',
  `payment_method` int(11) NOT NULL DEFAULT 0 COMMENT '1 => from shop 2 => online',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clinic_reservations`
--

INSERT INTO `clinic_reservations` (`id`, `customer_id`, `clinic_id`, `service_id`, `service_price`, `animal_id`, `animal_no`, `reservation_date`, `request_no`, `status`, `payment_method`, `created_at`, `updated_at`) VALUES
(1, 4, 1, 1, 600, 6, 3, '2021-09-28', 123456, 0, 0, '2021-09-09 12:47:26', '2021-09-09 07:51:22'),
(2, 4, 1, 2, 200, 2, 2, '2021-09-28', 123456, 0, 0, '2021-09-09 12:47:30', '2021-09-09 07:51:22'),
(3, 26, 1, 1, 200, 6, 1, '2021-10-07', 314747, 0, 0, '2021-10-05 10:13:07', '2021-09-26 08:51:12'),
(5, 4, 1, 1, 200, 6, 1, '2021-10-22', 214747, 0, 0, '2021-10-05 10:12:34', '2021-10-05 07:07:13'),
(6, 4, 1, 2, 100, 4, 1, '2021-10-22', 214747, 0, 0, '2021-10-05 10:13:02', '2021-10-05 07:07:13');

-- --------------------------------------------------------

--
-- Table structure for table `clinic_services`
--

CREATE TABLE `clinic_services` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `clinic_id` int(10) UNSIGNED DEFAULT NULL,
  `service_description_ar` text DEFAULT NULL,
  `service_description_en` text DEFAULT NULL,
  `service_price` int(11) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clinic_services`
--

INSERT INTO `clinic_services` (`id`, `created_at`, `updated_at`, `deleted_at`, `clinic_id`, `service_description_ar`, `service_description_en`, `service_price`, `status`) VALUES
(1, '2021-08-02 19:07:01', '2021-08-02 19:21:46', NULL, 1, 'قص أظافر', 'nail trimming', 200, 1),
(2, '2021-08-02 19:08:54', '2021-08-02 19:17:36', NULL, 1, 'شهادات صحية', 'health record', 100, 1),
(3, '2021-08-02 19:16:17', '2021-08-02 19:21:51', NULL, 1, 'فحص موجات صوتية', 'Ultrasound examination', 300, 1),
(4, '2021-08-02 19:16:24', '2021-08-02 19:16:24', NULL, 1, 'تحديد موعد كشف', 'Determine an appointment date', 145, 1),
(5, '2021-09-06 09:26:22', '2021-09-06 09:27:32', '2021-09-06 09:27:32', 1, 'خدمه تنظيف', 'خدمخ', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `clinic_setting`
--

CREATE TABLE `clinic_setting` (
  `id` int(10) NOT NULL,
  `address` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `tax` int(11) DEFAULT NULL,
  `reserv_price` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `clinic_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clinic_setting`
--

INSERT INTO `clinic_setting` (`id`, `address`, `email`, `phone`, `tax`, `reserv_price`, `created_at`, `updated_at`, `clinic_id`) VALUES
(1, 'Jaddah, Sauide arabia', 'admin@domainname.com', '+966012345678', NULL, 500, '2021-08-14 00:12:00', '2021-08-14 00:12:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `created_at`, `updated_at`, `name`, `email`, `phone`, `message`) VALUES
(1, '2021-08-11 06:26:48', '2021-08-11 06:26:48', 'mai tarek', 'maitarekttt@gmail.com', '0123456789', '@error(\'name\')\r\n                                    <div class=\"error-area\">\r\n                                      <p>{{$message}}</p>\r\n                                    </div>\r\n                                    @enderror'),
(2, '2021-08-12 17:55:04', '2021-08-12 17:55:04', 'mai tarek', 'maitarekttt@gmail.com', '009283478292', 'نريد حجز الان'),
(3, '2021-08-12 17:56:06', '2021-08-12 17:56:06', 'mai tarek', 'maitarekttt@gmail.com', '009283478292', 'نريد حجز الان'),
(4, '2021-08-12 17:56:19', '2021-08-12 17:56:19', 'mai tarek', 'maitarekttt@gmail.com', '009283478292', 'نريد حجز الان'),
(5, '2021-08-12 17:56:28', '2021-08-12 17:56:28', 'mai tarek', 'maitarekttt@gmail.com', '0123456789', 'Alert::error(session(\'error\'),\'لابد من التسجيل أولا\');'),
(6, '2021-08-12 17:57:47', '2021-08-12 17:57:47', 'mai tarek', 'maitarekttt@gmail.com', '0123456789', '<script src=\"https://cdn.jsdelivr.net/npm/sweetalert2@9\"></script>\r\n@include(\'sweetalert::alert\')'),
(7, '2021-08-12 17:58:58', '2021-08-12 17:58:58', 'mai tarek', 'maitarekttt@gmail.com', '0123456789', '@if(Session::has(\'success\'))\r\n                <div class=\"alert alert-success\" role=\"alert\">{{Session::get(\'success\')}}</div>\r\n                @endif\r\n\r\n                @if(Session::has(\'error\'))\r\n                <div class=\"alert alert-danger\" role=\"alert\">{{Session::get(\'error\')}}</div>\r\n                @endif'),
(8, '2021-08-12 17:59:56', '2021-08-12 17:59:56', 'mai tarek', 'maitarekttt@gmail.com', '01323456322', '@if(Session::has(\'success\'))\r\n                <div class=\"alert alert-success\" role=\"alert\">{{Session::get(\'success\')}}</div>\r\n                @endif\r\n\r\n                @if(Session::has(\'error\'))\r\n                <div class=\"alert alert-danger\" role=\"alert\">{{Session::get(\'error\')}}</div>\r\n                @endif   @if(Session::has(\'success\'))\r\n                <div class=\"alert alert-success\" role=\"alert\">{{Session::get(\'success\')}}</div>\r\n                @endif\r\n\r\n                @if(Session::has(\'error\'))\r\n                <div class=\"alert alert-danger\" role=\"alert\">{{Session::get(\'error\')}}</div>\r\n                @endif'),
(9, '2021-08-12 18:00:57', '2021-08-12 18:00:57', 'mai tarek', 'maitarekttt@gmail.com', '01234567899', 'use RealRashid\\SweetAlert\\Facades\\Alert;\r\nuse RealRashid\\SweetAlert\\Facades\\Alert;'),
(10, '2021-08-12 18:02:32', '2021-08-12 18:02:32', 'mai tarek', 'maitarekttt@gmail.com', '0123456789', 'gt'),
(11, '2021-08-12 18:02:56', '2021-08-12 18:02:56', 'mai tarek', 'maitarekttt@gmail.com', '009283478292', 'use RealRashid\\SweetAlert\\Facades\\Alert;'),
(12, '2021-09-24 12:45:26', '2021-09-24 12:45:26', 'Ahmed Ragheb', 'a2ragheb@gmail.com', '01092221615', 'nnnn'),
(13, '2021-10-09 03:25:19', '2021-10-09 03:25:19', 'JamesBek', 'no-replyAnync@gmail.com', '85166782974', 'Hello!  nature-sound-sa.com \r\n \r\nWe make offer for you \r\n \r\nSending your business proposition through the Contact us form which can be found on the sites in the contact section. Contact form are filled in by our software and the captcha is solved. The superiority of this method is that messages sent through feedback forms are whitelisted. This method improve the odds that your message will be read. \r\n \r\nOur database contains more than 27 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 49 USD \r\n \r\nFREE TEST mailing Up to 50,000 messages. \r\n \r\n \r\nThis letter is created automatically.  Use our contacts for communication. \r\n \r\nContact us. \r\nTelegram - @FeedbackMessages \r\nSkype  live:contactform_18 \r\nWhatsApp - +375259112693 \r\nWe only use chat.'),
(14, '2021-10-19 04:26:01', '2021-10-19 04:26:01', 'Mike Conors', 'no-replyAnync@gmail.com', '87444923826', 'Hi there \r\n \r\nDo you want a quick boost in ranks and sales for your nature-sound-sa.com website? \r\nHaving a high DA score, always helps \r\n \r\nGet your nature-sound-sa.com to have a DA between 50 to 60 points in Moz with us today and reap the benefits of such a great feat. \r\n \r\nSee our offers here: \r\nhttps://www.monkeydigital.co/product/moz-da50-seo-plan/ \r\n \r\nNEW: \r\nhttps://www.monkeydigital.co/product/ahrefs-dr60/ \r\n \r\n \r\nthank you \r\nMike Conors\r\n \r\nsupport@monkeydigital.co'),
(15, '2021-10-20 01:58:37', '2021-10-20 01:58:37', 'Mohammed Koofee', 'noormohammedali966@gmail.com', '88836462366', 'Hello Dear, \r\nAs-salamu alaykum \r\nFirst let me introduce myself, My name is Noor Mohammed Ali Al-Koofee from Iraq. \r\n \r\nI am married in Saudi Arabia. My Husband has been domestically abusive lately, the rate of abuse has attracted public attention since 2019 after a popular television presenter, Rania al-Baz, was severely beaten by her own husband too, I am interested in investing in your country through your personal guidelines. Before the Covid-19 Pandemic started, I saved a total of $20 Million currently deposited in the bank ready to be wire transferred to your country for possible investment & migration opportunities. \r\n \r\nIf interested, kindly contact me to send all proof of funds for your consideration. I cannot say everything here but I can be reached directly on WhatsApp only: +966592291747, or mailto:contact@noormohammedali.com or Email: noormohammedali966@gmail.com \r\n \r\nSincerely yours, \r\nNoor Mohammed Ali Al-Koofee \r\nSaudi Arabia'),
(16, '2021-10-21 02:33:50', '2021-10-21 02:33:50', 'Andrew Goldenberge', 'martinbr@consultant.com', '88325293575', 'Hello, \r\nWe provide funding through our venture capital company to both start-up and existing companies either looking for funding for expansion or to accelerate growth in their company. \r\n \r\nWe have a structured joint venture investment plan in which we are interested in an annual return on investment not more than 10% ROI. \r\n \r\nWe are also currently structuring a convertible debt and loan financing of 3% interest repayable annually with no early repayment penalties. \r\n \r\nIf you have a business plan or executive summary I can review to understand a much better idea of your business and what you are looking to do, this will assist in determining the best possible investment structure we can pursue and discuss more extensively. \r\n \r\nIf you are interested in any of the above, kindly respond to us via this email andrew.goldenberg@castleprojectservicesltd.com \r\n \r\nI wait to hear from you. \r\n \r\nSincerely, \r\n \r\nAndrew Goldenberge \r\n \r\nInvestment/Wealth Manager \r\nCastle Project Services Ltd. \r\nE-Mail: andrew.goldenberg@castleprojectservicesltd.com'),
(17, '2021-10-21 22:42:25', '2021-10-21 22:42:25', 'Eric Jones', 'eric.jones.z.mail@gmail.com', '555-555-1212', 'Good day, \r\n\r\nMy name is Eric and unlike a lot of emails you might get, I wanted to instead provide you with a word of encouragement – Congratulations\r\n\r\nWhat for?  \r\n\r\nPart of my job is to check out websites and the work you’ve done with nature-sound-sa.com definitely stands out. \r\n\r\nIt’s clear you took building a website seriously and made a real investment of time and resources into making it top quality.\r\n\r\nThere is, however, a catch… more accurately, a question…\r\n\r\nSo when someone like me happens to find your site – maybe at the top of the search results (nice job BTW) or just through a random link, how do you know? \r\n\r\nMore importantly, how do you make a connection with that person?\r\n\r\nStudies show that 7 out of 10 visitors don’t stick around – they’re there one second and then gone with the wind.\r\n\r\nHere’s a way to create INSTANT engagement that you may not have known about… \r\n\r\nTalk With Web Visitor is a software widget that’s works on your site, ready to capture any visitor’s Name, Email address and Phone Number.  It lets you know INSTANTLY that they’re interested – so that you can talk to that lead while they’re literally checking out nature-sound-sa.com.\r\n\r\nCLICK HERE https://talkwithwebvisitors.com to try out a Live Demo with Talk With Web Visitor now to see exactly how it works.\r\n\r\nIt could be a game-changer for your business – and it gets even better… once you’ve captured their phone number, with our new SMS Text With Lead feature, you can automatically start a text (SMS) conversation – immediately (and there’s literally a 100X difference between contacting someone within 5 minutes versus 30 minutes.)\r\n\r\nPlus then, even if you don’t close a deal right away, you can connect later on with text messages for new offers, content links, even just follow up notes to build a relationship.\r\n\r\nEverything I’ve just described is simple, easy, and effective. \r\n\r\nCLICK HERE https://talkwithwebvisitors.com to discover what Talk With Web Visitor can do for your business.\r\n\r\nYou could be converting up to 100X more leads today!\r\n\r\nEric\r\nPS: Talk With Web Visitor offers a FREE 14 days trial – and it even includes International Long Distance Calling. \r\nYou have customers waiting to talk with you right now… don’t keep them waiting. \r\nCLICK HERE https://talkwithwebvisitors.com to try Talk With Web Visitor now.\r\n\r\nIf you\'d like to unsubscribe click here http://talkwithwebvisitors.com/unsubscribe.aspx?d=nature-sound-sa.com'),
(18, '2021-10-22 07:39:44', '2021-10-22 07:39:44', 'Donald Cole', 'lanj7962@gmail.com', '81953365657', 'Good day \r\n \r\nI contacted you some days back seeking your cooperation in a matter regarding funds worth $24 Million, I urge you to  get back to me through this email coledd11@cloedcolela.com if you\'re still interested. \r\n \r\nI await your response. \r\n \r\nThanks, \r\n \r\nDonald Cole'),
(19, '2021-10-30 00:09:01', '2021-10-30 00:09:01', 'Mike Donaldson', 'no-replyAnync@gmail.com', '82212579231', 'Hi there \r\n \r\nWe will increase your Local Ranks organically and safely, using only whitehat methods, while providing Google maps and website offsite work at the same time. \r\n \r\nPlease check our services below, we offer Local SEO at cheap rates. \r\nhttps://speed-seo.net/product/local-seo-package/ \r\n \r\nNEW: \r\nhttps://www.speed-seo.net/product/zip-codes-gmaps-citations/ \r\n \r\nregards \r\nMike Donaldson\r\n \r\nSpeed SEO Digital Agency'),
(20, '2021-11-01 06:59:21', '2021-11-01 06:59:21', 'Chau Cheung', 'noreply@googlemail.com', '83143475626', 'Hello, \r\nI am contacting you regarding a transaction of mutual benefit, I am an attorney who managed a client\'s account that passed away many years ago, he passed away without any known relative. \r\nWe can work together mutually to process and receive the funds, let me know if you wish to know more about my proposal and I shall provide you with more information. \r\n \r\nRegards, \r\nMr Chau Shiu Cheung \r\nchaushuicheung@hongkongsolicitors.org'),
(21, '2021-11-03 11:21:15', '2021-11-03 11:21:15', 'Mike Paterson', 'no-replyAnync@gmail.com', '82187743318', 'Howdy \r\n \r\nI have just took a look on your SEO for  nature-sound-sa.com for its SEO Trend and saw that your website could use a push. \r\n \r\nWe will improve your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly reports and outstanding support. \r\n \r\nPlease check our pricelist here, we offer SEO at cheap rates. \r\nhttps://www.hilkom-digital.de/cheap-seo-packages/ \r\n \r\nStart increasing your sales and leads with us, today! \r\n \r\nregards \r\nMike Paterson\r\n \r\nHilkom Digital Team \r\nsupport@hilkom-digital.de');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `forget_password` int(1) NOT NULL DEFAULT 0,
  `code` int(5) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `repassword` varchar(255) DEFAULT NULL,
  `type` varchar(191) DEFAULT NULL,
  `photo` text DEFAULT NULL,
  `spare_email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `created_at`, `forget_password`, `code`, `updated_at`, `deleted_at`, `name`, `email`, `phone`, `password`, `repassword`, `type`, `photo`, `spare_email`) VALUES
(4, '2021-08-08 08:12:12', 0, NULL, '2021-09-03 16:19:43', NULL, 'muhamed22', 'muhamed@yahoo.com', '0123456789', '$2y$10$G1BW46r/NLRjCftAjHkZrOI7YBos8KG3b75pxe6B87bVJtJ4GoFiW', '$2y$10$08HXxooThozr4p2NhK36AOc3KJaMw8a.Rt63t5zjloC2bGg2TBUx2', NULL, 'public/images/customers/crC6RuZtVRPxyAB3rQlQXOF6gfDXuQaL11dHbdvN.jpg', NULL),
(5, '2021-08-08 08:26:01', 0, NULL, '2021-08-08 08:26:02', NULL, 'testc', NULL, '009283478292', '$2y$10$dQ8Rj2hwSuqUqKzkOdzwIO04y106q6tCa8jIX4qUJEJoWPYj32j4q', '$2y$10$ShYwpz5V4UHDC0aT3R3MA.ZBVqguwM9RmN67ZXgxW7gnMgBea5El6', NULL, '/public/images/avatar.png', NULL),
(6, '2021-08-08 08:54:56', 0, NULL, '2021-08-08 08:54:56', NULL, 'dvxj', NULL, '012345678933', '$2y$10$TQRsYrJggHc.PI7WI3sajex.x7xLdIj01nsNul0AICevp5k47WJLq', '$2y$10$IRsvE7UveEj9RKtedj.VFelR1XxuKXsauHN.YPMqApZxt3v9ptGqO', NULL, '/public/images/avatar.png', NULL),
(8, '2021-08-26 14:01:31', 0, NULL, '2021-08-26 14:01:31', NULL, 'muhamed11', NULL, '0111111111111', '$2y$10$c/f9Rm.nYr42.N0RVcu/z.0GcJ0BBNW2Dh2/IZi.27iRfY/PKOPdC', '$2y$10$oCaCl5C30O0bUpa1d2ElYOl9gVGMea872twYZfdtT5/4BH54MlUBO', NULL, '/public/images/avatar.png', NULL),
(9, '2021-08-26 14:04:00', 0, NULL, '2021-08-26 14:04:01', NULL, 'ahmed1111', NULL, '0102373837373', '$2y$10$tme6xu.m9AmUAe1cXJu05eS8a4thaLPf5v1ayz7HCqonqTeIoILqW', '$2y$10$SF5/xIcL2Yv6byChOQ1z6u6E2tTPjnQoXE.JxZ4kB74jgx7g7LZeC', NULL, '/public/images/avatar.png', NULL),
(10, '2021-08-26 14:09:47', 0, NULL, '2021-08-26 14:09:47', NULL, 'lwkhje', NULL, '010101191191999', '$2y$10$.8kyIukhxE5hVyA862zIAe/mVh0.YgLTgxFKlh.QkZLLybK/l8aRO', '$2y$10$NKvrCfKctIRpOYOLEQI8PePZqeemM6JuBa.SF5ZG53bWWAQVYprp2', NULL, '/public/images/avatar.png', NULL),
(11, '2021-08-26 14:11:30', 0, NULL, '2021-08-26 14:11:31', NULL, '23r4tyukh', NULL, '918922829292', '$2y$10$dOFjCc1OfCHGXGopntC8oOKc7EyfkxSELWrzuXBbMRGIvSND5CcJ.', '$2y$10$TYnHqnQpcC4yUi0aKFWvTu3Aqp9a5rpsnnc.mdFe208FkAYcx8W8W', NULL, '/public/images/avatar.png', NULL),
(12, '2021-08-26 14:12:37', 0, NULL, '2021-08-26 14:12:37', NULL, '123etyhu', NULL, '0092834782921', '$2y$10$GD6h6h/jMGQdVQaMR1FCHOnCtB9liaXULtf0VIgskIu70cPw1eyDC', '$2y$10$uVVmLNyu1Xc1jp9xSxUTAOuqDoSWkGqTJJh1mBfgEg/OiFYYN6oVK', NULL, '/public/images/avatar.png', NULL),
(13, '2021-08-26 14:13:03', 0, NULL, '2021-08-26 14:13:03', NULL, 'wedfgyui', NULL, '0192837289111', '$2y$10$SvDTFmza1YUn5iSQz76dmeg7EAWAWSg.9p7cgOsrlIDleG4BkfuSm', '$2y$10$65SVaJE11vz0FCIXNnqkXu72Y4lwEGK41mxzvpGoDBbwoD1x7M7r.', NULL, '/public/images/avatar.png', NULL),
(14, '2021-08-26 14:57:27', 0, NULL, '2021-08-26 14:57:27', NULL, '1234588888', NULL, '0019283738383', '$2y$10$GmH4fIk.NFgIquvbX/AtYua/6N4OifZybqJMlTVRIlh6.BGiB5ar2', '$2y$10$6fJatNBdfAUohbDpsUAntet.FR7P4w4Z9bNcgIgB.wMbtr4K3PBti', NULL, '/public/images/avatar.png', NULL),
(15, '2021-08-26 14:59:02', 0, NULL, '2021-08-26 14:59:02', NULL, ',kmgyfcvgbhk,', NULL, '138393938393', '$2y$10$WWHsTgIAyuw/286XwCsFsO9zx61uqiVAT5EDicmFdgPQ2ry7zplZW', '$2y$10$szAdNiVv06zjzkY.I35mRezefQdxe60nNYfjNKU.RdUfMYeXQuPbe', NULL, '/public/images/avatar.png', NULL),
(16, '2021-08-26 15:00:40', 0, NULL, '2021-08-26 15:00:40', NULL, 'lkjhgghjkl', NULL, '0123456789222', '$2y$10$l9n5jpK7AG1MTxqUhHKVpe1kRX8Ma7Py4cfHLHTMqLke5DXTsLZA2', '$2y$10$ocVFH8p3upf793edyplGLuHCH9h6XKvLSmvWXsXwpqaeGFNj3bg92', NULL, '/public/images/avatar.png', NULL),
(17, '2021-09-01 06:23:19', 0, NULL, '2021-09-01 06:23:19', NULL, 'foad123', NULL, NULL, '$2y$10$pTIRPWxoi7KneG05gKs/QuyN9At61kWbtUrYxzKHLYK/6t2d6KFyq', '$2y$10$frF5GlkK.l8oFYBq.zUTsOvvCZT6V5VRr6Ye44ZwG6e.sPwk4Memm', NULL, '/public/images/avatar.png', NULL),
(18, '2021-09-01 06:28:57', 0, NULL, '2021-09-01 06:28:57', NULL, 'foad1234', NULL, '012345678999', '$2y$10$eNhurAvIE8oDMIlokyFM9u2t9w32aDtb3aSKkoF/QyW3TpK4spTKK', '$2y$10$2bMGWP/mkofReZXgpXPbPenQ7AopjPU/aeemcs8li8zPmMQfjb0Wi', NULL, '/public/images/avatar.png', NULL),
(19, '2021-09-01 10:46:00', 0, NULL, '2021-09-01 10:46:01', NULL, 'mohamed ibrahim', NULL, '01006670297', '$2y$10$lMy4B3t9inDNji3T14UDg.7GoQkbZ3GJZwDJcnzL9VS61UHmnaXAS', '$2y$10$PTcI3GpL63mKb8RI4bzPNeF.n8U2dJcCrWGi/ydnSc0OIpMkotfkO', NULL, '/public/images/avatar.png', NULL),
(20, '2021-09-02 20:31:38', 0, NULL, '2021-09-02 20:31:38', NULL, 'Aa', NULL, '0113456789', '$2y$10$wtcykSeOvm5jq5ufo.HnEeRJt8agwpTXdMzOG/gT5jDHIRU9OG5QC', '$2y$10$.uw6JERHlRtbMiEKuieV5.fFBL0rByHSXt62NjgWgTdt1OjGuiU82', NULL, '/public/images/avatar.png', NULL),
(21, '2021-09-05 17:02:04', 0, NULL, '2021-09-05 17:02:04', NULL, 'لمياء عبدالحميد أحمد', NULL, '01063788727', '$2y$10$DzoJX3xrpmddn8.YeJX.m.mccImDvCLicGjhFM/D23Da1fwjpvGPW', '$2y$10$eNJH/4RdU.ExB4my7YUyNey55MUIXIXPL4Lsgumtr1Iy4P1O/85be', NULL, '/public/images/avatar.png', NULL),
(22, '2021-09-07 07:00:56', 0, NULL, '2021-09-07 07:00:56', NULL, 'mahmoud', 'mahmoud@yahoo.com', '010029282922', '$2y$10$6bMq58Eyyi36n5Ra5Czgv.1Ws833YcIKb2gEPPmw1UNWVk6b0rFsu', '$2y$10$tXqI8pfX5BAQvI5csUpGY.Mxq9qZ0aGDzMNByeag4YQJbMdCCduh2', NULL, '/public/images/avatar.png', NULL),
(23, '2021-09-21 15:25:01', 0, NULL, '2021-09-21 15:47:05', NULL, 'a-ragheb', 'a2ragheb@gmail.com', '010666666333', '$2y$10$hUM7pAzJK3.cfVjKWqnLTevlro6aW7XpU.R5zTYSlMRuJWpHdlvLK', '$2y$10$w2fKeF/iaUCYJZWFYAw7JOG5PGrWPgvCOpO2odQ.oR5sVjjVb4ELu', NULL, 'public/images/customers/z0vXoUNQqSRzHF5bjITb5hdX3dHKtaskjEpasLCo.jpg', NULL),
(24, '2021-09-21 15:25:01', 0, NULL, '2021-09-29 16:53:11', NULL, 'a2ragheb', 'a2ragheb@gmail.com', '01092221615', '$2y$10$Pm8hoFJouMySrQXCeF9yXulDcIHRt1gg6nPS/ufwd5PuImE2TqLTq', '$2y$10$2w5WZVCkf58nY7lbqSw2eueratCUqfZbcB30Z.t.Id87E7LQYzvSS', NULL, '/public/images/avatar.png', 'ahmedragheb.work@gmail.com'),
(25, '2021-09-24 17:24:47', 0, NULL, '2021-09-24 17:24:48', NULL, 'مستخدم', 'a@admin.com', '10123456789', '$2y$10$vifNmPlirpjJaBebMrvCKesz8VbZGriMi4IjtTfF49XE6zGz8SRji', '$2y$10$5GGIPz0EniLoYoyjCLJ2NesBZqCONOoCBS5JMPm8q9pXlgASCiS6e', NULL, '/public/images/avatar.png', NULL),
(26, '2021-09-26 05:36:57', 0, NULL, '2021-09-26 11:12:47', NULL, 'asmaa123', 'saidd@gmail.com', '0122334455', '$2y$10$JFQWItjGKNyxH/FWt6iXeedJ.V.G6wSBJVtw7pEjKXeYnr8naRiNy', '$2y$10$SgLTEBH7SVZqZ4yLMUiso.63rDaA36PtnqPfmuIPlfU1dZu8RI7jS', NULL, '/public/images/avatar.png', 'saidd@gmail.com'),
(27, '2021-09-26 05:38:03', 0, NULL, '2021-09-26 05:38:03', NULL, 'muhand123', 'muhand123@yahoo.com', '012223455566', '$2y$10$bgEUkgFxSwd9r4JMqFk74uIAuRSNRcNon968FBBo.gKu30Exrcnoa', '$2y$10$ND3p7t1lC3uRQW8yk6GfketGp4FphFzwDwORrbYFHIVzRWuHyxPZ2', NULL, '/public/images/avatar.png', NULL),
(28, '2021-09-26 10:05:47', 0, NULL, '2021-09-26 10:05:47', NULL, 'ahmed', 'data@admin.com', '5284176932', '$2y$10$U/k7.i7Fs9GjLWhTr/KPmetiZs0BPoik3Y6N2/bHKXK4UzJZTE2Hy', '$2y$10$Bg7PyIRXpMaCtysJU8U7quadOZLhr9IOIC1ZJVf/jMlIIhMggSdia', NULL, '/public/images/avatar.png', NULL),
(29, '2021-09-27 12:09:53', 0, NULL, '2021-09-27 12:09:53', NULL, 'amir nageh', 'amir.nageh15@gmail.com', '01024517052', '$2y$10$XXUJBUKTqwelF.lB/ks9.Ons9QF2NEjO4C9e/SNInrHgZdDwyctLO', '$2y$10$xDDSbCeZUICc2k6PlvUrXOpC5Q.zWl99N91LnrTm5OjzXw7483idq', NULL, '/public/images/avatar.png', NULL),
(30, '2021-09-28 07:55:56', 0, NULL, '2021-09-28 07:55:56', NULL, 'amir nageh', 'amir.nageh16@gmail.com', '01027928812', '$2y$10$xv1r5v888CuUWTr3hTSg5u.CBGY19xHu7MN6bAjkRUx3O7Oj.ekjG', '$2y$10$LEOZ0sXk009HKQQLTZdefe2hZOzj3yKz1j9dNlDcyY1qacpj01Jlm', NULL, '/public/images/avatar.png', NULL),
(31, '2021-09-28 09:05:45', 0, NULL, '2021-09-28 09:05:45', NULL, 'mostafa', 'mostafa@gmail.com', '01020292292', '$2y$10$ij757yYu8I7az.EvqKXose.636dC96v2QdXT01UgJb78VloSaoH7m', '$2y$10$AOHOueAcUPOC9iCcAAXrfu69oW1Ung6XnWs5p7cwVi7F3vbQ80.K2', NULL, '/public/images/avatar.png', NULL),
(32, '2021-09-28 10:25:50', 0, NULL, '2021-09-28 10:25:50', NULL, 'ahmedd1234', 'ahmedd1234@yahoo.comm', '010292929377', '$2y$10$.FjHmGEUxROqSU3YvO3bkuWF5ZB3awKzkIdCwGx9nmM2b7gyMmyxu', '$2y$10$WkxIGqZM8i9dZrdXLmVKAezz1B4VExjgYqezjU3hDlXHWrWIbm96S', NULL, '/public/images/avatar.png', NULL),
(33, '2021-09-28 10:27:05', 0, NULL, '2021-09-28 10:27:05', NULL, 'kdjhghd', 'kdjhghd@yhaoo.com', '0123458933', '$2y$10$TyINuEHdZBzyqPMfrcpl.OMqAwndM.FbGLLMN1qkr.nVCr6v4/f0u', '$2y$10$jI9kHerzP8lOt6XdiDMlle8Bzaf7/xgSl1FyBh1kEg7ueAxkVY5Mq', NULL, '/public/images/avatar.png', NULL),
(34, '2021-09-28 10:28:53', 0, NULL, '2021-09-28 10:28:53', NULL, 'fhhfhfhhj', 'fhhfhfhhj@yahoo.com', '10237833833', '$2y$10$UK0vQZLll16Ku4NSHKYt1OBlRL.Uz0EWoTrp7/9OH2gM7BngE0YW.', '$2y$10$bDduZd0tdkq5aAlqZCqud.LPQbjNP8oNOZ/k3phx2QKr38jh306K6', NULL, '/public/images/avatar.png', NULL),
(35, '2021-09-28 10:30:15', 0, NULL, '2021-09-28 10:30:15', NULL, 'fawzy', 'fawzy@yahoo.com', '012237333373', '$2y$10$1nQnW6cBk0v728PJYWxuuOaa8XPiIwtmLq52tWdDOg1iuECKCa.wC', '$2y$10$poQoAKh2RxdoygsPu71zs.g6Cv.1bWqDPWYASsQOvNPgO9jKjSlxq', NULL, '/public/images/avatar.png', NULL),
(36, '2021-09-30 06:40:55', 0, 5735, '2021-09-30 08:59:08', NULL, 'maitarek', 'maitarekttt@gmail.com', '01097987069', '$2y$10$OezkdW7NNWyYrQAjc85c8Oaspb7S1bJBRNGwtP5pgC./kUZXgqWUy', '$2y$10$NaWMeetCclXSSxFdpuRYFOZaUUVv4psmChjui4dwBm.d7ZodOTcQ6', NULL, '/public/images/avatar.png', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer_reviews`
--

CREATE TABLE `customer_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `customer_name` varchar(50) NOT NULL,
  `job_title` varchar(191) NOT NULL,
  `customer_review` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_reviews`
--

INSERT INTO `customer_reviews` (`id`, `created_at`, `updated_at`, `deleted_at`, `customer_name`, `job_title`, `customer_review`) VALUES
(1, '2021-08-14 09:37:06', '2021-08-14 09:57:09', NULL, 'محمد أحمد', 'مهندس', 'customer_reviewcustomer_review11111111111111111');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hotels`
--

CREATE TABLE `hotels` (
  `id` int(10) NOT NULL,
  `hotel_image` text DEFAULT NULL,
  `hotel_title_ar` text NOT NULL,
  `hotel_title_en` text NOT NULL,
  `hotel_description_ar` longtext NOT NULL,
  `hotel_description_en` longtext NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hotels`
--

INSERT INTO `hotels` (`id`, `hotel_image`, `hotel_title_ar`, `hotel_title_en`, `hotel_description_ar`, `hotel_description_en`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'public/images/hotels/IHtpJpceFb4OxGvN53cyGBMInWRTSmHrSQcHzKvR.png', 'فندق Family Holidays with Pets', 'Family Holidays with Pets Hotel', '<div><div style=\"\">جميع خدمات الحيوانات أستضافة اقامة علاج نظافة نقل متابعة علاجية رش مزارع وتنظيف ...</div><div style=\"font-size: 0.875rem;\"><br></div></div>', 'All animal services Hosting, cleaning treatment, transporting, therapeutic follow-up, spraying farms, cleaning...', 1, NULL, '2021-09-06 20:32:46', '2021-09-08 07:08:49');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_reservations`
--

CREATE TABLE `hotel_reservations` (
  `id` int(10) NOT NULL,
  `customer_id` int(10) DEFAULT NULL,
  `service_id` int(10) DEFAULT NULL,
  `service_price` int(11) DEFAULT NULL,
  `animal_id` int(10) DEFAULT NULL,
  `animal_no` int(11) DEFAULT NULL,
  `reservation_start_date` varchar(100) DEFAULT NULL,
  `reservation_end_date` varchar(200) DEFAULT NULL,
  `request_no` int(30) NOT NULL DEFAULT 0,
  `status` int(11) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hotel_reservations`
--

INSERT INTO `hotel_reservations` (`id`, `customer_id`, `service_id`, `service_price`, `animal_id`, `animal_no`, `reservation_start_date`, `reservation_end_date`, `request_no`, `status`, `created_at`, `updated_at`) VALUES
(1, 4, 1, 1000, 5, 1, '2021-09-28', '2021-10-01', 111222, 0, '2021-09-09 07:50:59', '2021-09-09 07:50:59'),
(2, 4, 8, 1400, 4, 2, '2021-09-28', '2021-10-01', 111222, 0, '2021-09-09 07:50:59', '2021-09-09 07:50:59');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_services`
--

CREATE TABLE `hotel_services` (
  `id` int(10) NOT NULL,
  `hotel_id` int(10) DEFAULT NULL,
  `service_description_ar` longtext NOT NULL,
  `service_description_en` longtext NOT NULL,
  `service_price` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hotel_services`
--

INSERT INTO `hotel_services` (`id`, `hotel_id`, `service_description_ar`, `service_description_en`, `service_price`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'غرف مكيفة لإقامة الحيوانات الأليفة', 'Air-conditioned rooms for pets.', 1000, 1, '2021-09-06 20:26:48', '2021-09-06 20:26:48', NULL),
(2, 1, 'إعطاء التطعيمات اللازمة للحيوان قبل إقامته', 'Giving the necessary vaccinations to the animal before its residence.', 200, 1, '2021-09-06 20:26:48', '2021-09-06 20:26:48', NULL),
(4, 1, 'غرف سنجل لإقامة الحيوانات الأليفة', 'Single rooms for pets', 500, 1, '2021-09-06 20:26:48', '2021-09-06 20:26:48', NULL),
(5, 1, 'رعاية خاصة بالحيوانات الكبيرة في السن', 'Healing sessions for older animals.', 643, 1, '2021-09-06 20:26:48', '2021-09-06 20:26:48', NULL),
(6, 1, 'غرف مزدوجه لإقامة الحيوانات الأليفة', 'Double rooms for pets', 123, 1, '2021-09-06 20:26:48', '2021-09-06 20:26:48', NULL),
(7, 1, 'قص أظافر', 'nail trimming', 300, 1, '2021-09-07 11:10:06', '2021-09-07 11:10:06', NULL),
(8, 1, 'سبا للحيوانات الأليفه', 'Spa for pets', 700, 1, '2021-09-07 11:10:35', '2021-09-07 11:10:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hotel_setting`
--

CREATE TABLE `hotel_setting` (
  `id` int(10) NOT NULL,
  `hotel_id` int(10) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` int(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hotel_setting`
--

INSERT INTO `hotel_setting` (`id`, `hotel_id`, `address`, `email`, `phone`, `created_at`, `updated_at`) VALUES
(1, 1, 'cvhkljhgfdfghjk', 'lkjhgfdcfvghjkl', 198373930, '2021-09-06 20:35:03', '2021-09-06 20:35:03');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(10) NOT NULL,
  `location_name_ar` varchar(255) NOT NULL,
  `region_id` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `location_name_ar`, `region_id`, `created_at`, `updated_at`) VALUES
(1, 'حي العليا\r\n', 1, '2021-09-03 21:49:07', '2021-09-03 21:49:07'),
(2, 'حي السليمانية', 1, '2021-09-03 21:49:30', '2021-09-03 21:49:07'),
(3, 'حي الملك عبد العزيز\r\n', 1, '2021-09-03 21:49:07', '2021-09-03 21:49:07'),
(4, 'حي الملك عبد الله الجنوبي\r\n', 1, '2021-09-03 21:49:30', '2021-09-03 21:49:07'),
(5, 'حي الملك عبد الله الشمالي\r\n', 1, '2021-09-03 21:49:07', '2021-09-03 21:49:07'),
(6, 'حي الواحة\r\n', 1, '2021-09-03 21:49:30', '2021-09-03 21:49:07'),
(7, 'حي صلاح الدين\r\n', 1, '2021-09-03 21:49:07', '2021-09-03 21:49:07'),
(8, 'حي المصيف', 1, '2021-09-03 21:49:30', '2021-09-03 21:49:07'),
(9, 'حي الورود\r\n', 1, '2021-09-03 21:49:07', '2021-09-03 21:49:07'),
(10, 'حي الملك فهد\r\n', 1, '2021-09-03 21:49:30', '2021-09-03 21:49:07'),
(11, 'حي المرسلات\r\n', 1, '2021-09-03 21:49:07', '2021-09-03 21:49:07'),
(12, 'حي النزهة\r\n', 1, '2021-09-03 21:49:30', '2021-09-03 21:49:07'),
(13, 'حي المغرزات\r\n', 1, '2021-09-03 21:49:07', '2021-09-03 21:49:07'),
(14, 'حي الازدهار\r\n', 1, '2021-09-03 21:49:30', '2021-09-03 21:49:07'),
(15, 'حي التعاون\r\n', 1, '2021-09-03 21:49:07', '2021-09-03 21:49:07'),
(16, 'حي النخيل\r\n', 1, '2021-09-03 21:51:07', '2021-09-03 21:49:07'),
(17, 'حى الروضه', 6, '2021-09-03 21:49:07', '2021-09-03 21:49:07'),
(18, 'حى العزيزيه', 6, '2021-09-03 21:49:30', '2021-09-03 21:49:07'),
(19, 'حى الخنساء', 6, '2021-09-03 21:49:07', '2021-09-03 21:49:07'),
(20, 'حى المعيصم', 6, '2021-09-03 21:49:30', '2021-09-03 21:49:07'),
(21, 'حى المرسلات', 6, '2021-09-03 21:49:07', '2021-09-03 21:49:07'),
(22, 'حى النسيم', 6, '2021-09-03 21:49:30', '2021-09-03 21:49:07'),
(23, 'حى الشرائع', 6, '2021-09-03 21:49:07', '2021-09-03 21:49:07'),
(24, 'حى الحسنان', 6, '2021-09-03 21:49:30', '2021-09-03 21:49:07'),
(25, 'الصفا، المروة', 5, '2021-09-03 22:05:00', '2021-09-03 21:49:07'),
(26, 'مشرفة', 5, '2021-09-03 22:05:00', '2021-09-03 21:49:07'),
(27, 'الرحاب', 5, '2021-09-03 22:05:00', '2021-09-03 21:49:07'),
(28, 'حي الحجاز', 5, '2021-09-03 22:05:00', '2021-09-03 21:49:07'),
(29, 'البغدادية الشرقية', 5, '2021-09-03 22:05:00', '2021-09-03 21:49:07'),
(30, 'حى الرويس', 5, '2021-09-03 22:05:00', '2021-09-03 21:49:07'),
(31, 'حي بني مالك', 5, '2021-09-03 22:05:00', '2021-09-03 21:49:07'),
(32, 'الاندلس', 5, '2021-09-09 11:06:43', '2021-09-09 09:06:43');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_07_17_203522_create_animal_categorys_table', 1),
(5, '2021_07_17_203522_create_animal_types_table', 1),
(6, '2021_07_17_203522_create_blog_blogtags_table', 1),
(7, '2021_07_17_203522_create_blog_comments_table', 1),
(8, '2021_07_17_203522_create_blog_tags_table', 1),
(9, '2021_07_17_203522_create_blogs_table', 1),
(10, '2021_07_17_203522_create_category_products_table', 1),
(11, '2021_07_17_203522_create_categorys_table', 1),
(12, '2021_07_17_203522_create_clinic_services_table', 1),
(13, '2021_07_17_203522_create_clinics_table', 1),
(14, '2021_07_17_203522_create_contacts_table', 1),
(15, '2021_07_17_203522_create_customers_table', 1),
(16, '2021_07_17_203522_create_privacy_policys_table', 1),
(17, '2021_07_17_203522_create_product_appraisals_table', 1),
(18, '2021_07_17_203522_create_product_discount_table', 1),
(19, '2021_07_17_203522_create_product_favourites_table', 1),
(20, '2021_07_17_203522_create_product_images_table', 1),
(21, '2021_07_17_203522_create_products_table', 1),
(22, '2021_07_17_203522_create_settings_table', 1),
(23, '2021_07_17_203522_create_site_gallery_table', 1),
(24, '2021_07_17_203522_create_stores_table', 1),
(25, '2021_07_17_203522_create_trademarks_table', 1),
(26, '2021_07_17_203523_create_carts_table', 1),
(27, '2021_07_17_203523_create_customer_reviews_table', 1),
(28, '2021_07_17_203523_create_newsletters_table', 1),
(29, '2021_07_17_203523_create_shop_bills_table', 1),
(30, '2021_07_17_203523_create_shop_coupons_table', 1),
(31, '2021_07_17_203523_create_shop_settings_table', 1),
(32, '2021_07_17_203533_create_foreign_keys', 1),
(33, '2021_08_03_213047_laratrust_setup_tables', 2);

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE `newsletters` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`id`, `created_at`, `updated_at`, `email`) VALUES
(1, '2021-09-01 08:12:36', '2021-09-01 08:12:36', 'maitarekttt@gmail.com'),
(2, '2021-09-01 08:12:46', '2021-09-01 08:12:46', 'maitarekttt@gmai');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) NOT NULL,
  `customer_id` int(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `created_at`, `updated_at`) VALUES
(4, 4, '2021-09-05 07:28:31', '2021-09-05 07:28:31'),
(5, 24, '2021-09-24 12:53:24', '2021-09-24 12:53:24'),
(6, 27, '2021-09-26 06:32:51', '2021-09-26 06:32:51'),
(7, 26, '2021-09-26 06:45:29', '2021-09-26 06:45:29'),
(8, 29, '2021-09-27 12:19:09', '2021-09-27 12:19:09'),
(9, 31, '2021-09-28 09:07:21', '2021-09-28 09:07:21'),
(10, 36, '2021-09-30 09:33:01', '2021-09-30 09:33:01');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) NOT NULL,
  `token` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `paysuccess`
--

CREATE TABLE `paysuccess` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `products` varchar(255) NOT NULL,
  `fatora_link` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `done` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `paysuccess`
--

INSERT INTO `paysuccess` (`id`, `user_id`, `products`, `fatora_link`, `price`, `done`, `created_at`, `updated_at`) VALUES
(2, 4, '[6,7]', 'https://demo.MyFatoorah.com/KWT/ie/0106296936137', 430, 0, '2021-09-06 20:05:51', '2021-09-06 20:05:51'),
(3, 4, '[6]', 'https://demo.MyFatoorah.com/KWT/ie/0106296967638', 230, 0, '2021-09-07 06:47:12', '2021-09-07 06:47:12'),
(4, 4, '[6]', 'https://demo.MyFatoorah.com/KWT/ie/0106296967738', 230, 0, '2021-09-07 06:47:28', '2021-09-07 06:47:28'),
(5, 4, '[6]', 'https://demo.MyFatoorah.com/KWT/ie/0106296967938', 230, 0, '2021-09-07 06:48:09', '2021-09-07 06:48:09'),
(6, 31, '[14]', 'https://demo.MyFatoorah.com/KWT/ie/01072103896141', 200, 0, '2021-09-28 09:42:20', '2021-09-28 09:42:20');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(3, 'create-blog', 'إضافه مقال', 'إضافه مقال', '2021-08-30 19:04:02', '2021-08-30 19:04:02'),
(4, 'edit-blog', 'تعديل مقال', 'تعديل مقال', '2021-08-30 19:05:09', '2021-08-30 19:05:09'),
(5, 'delete-blog', 'حذف مقال', 'حذف مقال', '2021-08-30 19:05:26', '2021-08-30 19:05:26'),
(6, 'read-blog', 'Read Blog', 'kfjhb', '2021-08-31 07:12:04', '2021-08-31 07:12:04'),
(7, 'activate-blog', 'blog activate', 'vvv', '2021-08-31 07:39:51', '2021-08-31 07:39:51'),
(8, 'add-tag-blog', 'blog add tag', 'ff', '2021-08-31 07:40:10', '2021-08-31 07:40:10'),
(9, 'create-products', 'create-products', 'create-products', '2021-08-31 08:31:04', '2021-08-31 08:31:04'),
(10, 'edit-products', 'edit-products', 'edit-products', '2021-08-31 08:31:18', '2021-08-31 08:31:18'),
(11, 'delete-products', 'delete-products', 'delete-products', '2021-08-31 08:31:33', '2021-08-31 08:31:33'),
(12, 'activate-products', 'products-activate', 'products-activate', '2021-08-31 08:32:00', '2021-08-31 08:32:00'),
(13, 'star-products', 'products-star', 'products-star', '2021-08-31 08:32:18', '2021-08-31 08:32:18'),
(14, 'create-sellers-products', 'create-product_sellers', 'create-product_sellers', '2021-08-31 08:32:36', '2021-08-31 08:32:36'),
(15, 'edit-sellers-products', 'edit-product_sellers', 'edit-product_sellers', '2021-08-31 08:32:44', '2021-08-31 08:32:44'),
(16, 'delete-sellers-products', 'delete-product_sellers', 'delete-product_sellers', '2021-08-31 08:32:56', '2021-08-31 08:32:56'),
(17, 'activate-sellers-products', 'product_sellers-activate', 'product_sellers-activate', '2021-08-31 08:33:16', '2021-08-31 08:33:16'),
(18, 'create-arrivals-products', 'create-product_arrivals', 'create-product_arrivals', '2021-08-31 08:32:36', '2021-08-31 08:32:36'),
(19, 'edit-arrivals-products', 'edit-product_arrivals', 'edit-product_arrivals', '2021-08-31 08:32:44', '2021-08-31 08:32:44'),
(20, 'delete-arrivals-products', 'delete-product_arrivals', 'delete-product_arrivals', '2021-08-31 08:32:56', '2021-08-31 08:32:56'),
(21, 'activate-arrivals-products', 'product_arrivals-activate', 'product_arrivals-activate', '2021-08-31 08:33:16', '2021-08-31 08:33:16'),
(22, 'create-categorys', 'create-categorys', 'create-categorys', '2021-08-30 19:04:02', '2021-08-30 19:04:02'),
(23, 'edit-categorys', 'edit-categorys', 'edit-categorys', '2021-08-30 19:05:09', '2021-08-30 19:05:09'),
(24, 'delete-categorys', 'delete-categorys', 'delete-categorys', '2021-08-30 19:05:26', '2021-08-30 19:05:26'),
(25, 'activate-categorys', 'activate-categorys', 'activate-categorys', '2021-08-31 08:33:16', '2021-08-31 08:33:16'),
(26, 'read-products', 'read-products', 'read-products', '2021-08-31 08:33:16', '2021-08-31 08:33:16'),
(31, 'create-shop-coupons', 'create-shop-coupons', 'create-shop-coupons', '2021-09-12 06:58:03', '2021-09-12 06:58:03'),
(32, 'edit-shop-coupons', 'edit-shop-coupons', 'edit-shop-coupons', '2021-09-12 07:01:16', '2021-09-12 07:01:16'),
(33, 'delete-shop-coupons', 'delete-shop-coupons', 'delete-shop-coupons', '2021-09-12 07:01:31', '2021-09-12 07:01:31'),
(34, 'read-shop-coupons', 'read-shop-coupons', 'read-shop-coupons', '2021-09-12 07:01:52', '2021-09-12 07:01:52'),
(35, 'activate-shop-coupons', 'activate-shop-coupons', 'activate-shop-coupons', '2021-09-12 07:02:07', '2021-09-12 07:02:07'),
(36, 'create-tags', 'create-tags', 'create-tags', '2021-09-15 10:11:57', '2021-09-15 10:11:57'),
(37, 'edit-tags', 'edit-tags', 'edit-tags', '2021-09-15 10:12:14', '2021-09-15 10:12:14'),
(38, 'read-tags', 'read-tags', 'read-tags', '2021-09-15 10:12:31', '2021-09-15 10:12:31'),
(39, 'delete-tags', 'delete-tags', 'read-tags', '2021-09-15 10:12:45', '2021-09-15 10:12:45'),
(40, 'activate-tags', 'activate-tags', 'read-tags', '2021-09-15 10:12:56', '2021-09-15 10:18:34'),
(41, 'read-citys', 'read-citys', 'read-tags', '2021-09-15 10:19:29', '2021-09-15 10:19:29'),
(42, 'create-citys', 'create-citys', 'read-tags', '2021-09-15 10:19:41', '2021-09-15 10:19:41'),
(43, 'edit-citys', 'edit-citys', 'read-tags', '2021-09-15 10:19:50', '2021-09-15 10:19:50'),
(44, 'delete-citys', 'delete-citys', 'read-tags', '2021-09-15 10:20:03', '2021-09-15 10:20:03'),
(46, 'read-regions', 'read-regions', 'read-tags', '2021-09-15 10:20:42', '2021-09-15 10:20:42'),
(47, 'create-regions', 'create-regions', 'read-tags', '2021-09-15 10:20:53', '2021-09-15 10:20:53'),
(48, 'edit-regions', 'edit-regions', 'read-tags', '2021-09-15 10:21:02', '2021-09-15 10:21:02'),
(49, 'delete-regions', 'delete-regions', 'read-tags', '2021-09-15 10:21:12', '2021-09-15 10:21:12'),
(51, 'read-locations', 'read-locations', 'read-tags', '2021-09-15 10:21:37', '2021-09-15 10:21:37'),
(52, 'create-locations', 'create-locations', 'read-tags', '2021-09-15 10:21:46', '2021-09-15 10:21:46'),
(53, 'edit-locations', 'edit-locations', 'read-tags', '2021-09-15 10:22:42', '2021-09-15 10:22:42'),
(54, 'delete-locations', 'delete-locations', 'read-tags', '2021-09-15 10:22:59', '2021-09-15 10:22:59'),
(56, 'read-gallerys', 'read-gallerys', 'read-tags', '2021-09-15 10:53:29', '2021-09-15 10:53:29'),
(57, 'create-gallerys', 'create-gallerys', 'read-tags', '2021-09-15 10:53:41', '2021-09-15 10:53:41'),
(58, 'delete-gallerys', 'delete-gallerys', 'read-tags', '2021-09-15 10:54:19', '2021-09-15 10:54:19'),
(59, 'read-animal_types', 'read-animal_types', 'read-tags', '2021-09-15 10:54:36', '2021-09-15 10:54:36'),
(60, 'create-animal_types', 'create-animal_types', 'read-tags', '2021-09-15 10:54:46', '2021-09-15 10:54:46'),
(61, 'edit-animal_types', 'edit-animal_types', 'read-tags', '2021-09-15 10:55:04', '2021-09-15 10:55:04'),
(62, 'delete-animal_types', 'delete-animal_types', 'read-tags', '2021-09-15 10:55:14', '2021-09-15 10:55:14'),
(63, 'activate-animal_types', 'activate-animal_types', 'read-tags', '2021-09-15 10:55:24', '2021-09-15 10:55:24'),
(64, 'read-clinics', 'read-clinics', 'read-tags', '2021-09-15 10:55:43', '2021-09-15 10:55:43'),
(65, 'edit-clinics', 'edit-clinics', 'read-tags', '2021-09-15 10:56:01', '2021-09-15 10:56:01'),
(66, 'delete-clinic_service', 'delete-clinic_service', 'read-tags', '2021-09-15 10:56:34', '2021-09-15 10:56:34'),
(67, 'activate-clinic_service', 'activate-clinic_service', 'read-tags', '2021-09-15 10:56:50', '2021-09-15 10:56:50'),
(68, 'create-clinic_service', 'create-clinic_service', 'read-tags', '2021-09-15 11:08:06', '2021-09-15 11:08:06'),
(69, 'read-sliders', 'read-sliders', 'read-tags', '2021-09-15 12:04:48', '2021-09-15 12:04:48'),
(70, 'create-sliders', 'create-sliders', 'create-sliders', '2021-09-15 12:05:17', '2021-09-15 12:05:17'),
(71, 'edit-sliders', 'edit-sliders', 'edit-sliders', '2021-09-15 12:05:27', '2021-09-15 12:05:27'),
(72, 'delete-sliders', 'delete-sliders', 'create-sliders', '2021-09-15 12:05:53', '2021-09-15 12:05:53'),
(73, 'activate-sliders', 'activate-sliders', 'create-sliders', '2021-09-15 12:06:29', '2021-09-15 12:06:29'),
(74, 'read-trademarks', 'read-trademarks', 'edit-sliders', '2021-09-15 12:08:35', '2021-09-15 12:08:35'),
(75, 'create-trademarks', 'create-trademarks', 'create-sliders', '2021-09-15 12:08:43', '2021-09-15 12:08:43'),
(76, 'edit-trademarks', 'edit-trademarks', 'edit-sliders', '2021-09-15 12:08:55', '2021-09-15 12:08:55'),
(77, 'delete-trademarks', 'delete-trademarks', 'create-sliders', '2021-09-15 12:09:01', '2021-09-15 12:09:01'),
(78, 'activate-trademarks', 'activate-trademarks', 'edit-sliders', '2021-09-15 12:09:11', '2021-09-15 12:09:11'),
(79, 'read-stores', 'read-stores', 'create-sliders', '2021-09-15 12:09:31', '2021-09-15 12:09:31'),
(81, 'edit-stores', 'edit-stores', 'create-sliders', '2021-09-15 12:09:52', '2021-09-15 12:09:52'),
(82, 'read-customers', 'read-customers', 'edit-sliders', '2021-09-15 12:10:09', '2021-09-15 12:10:09'),
(84, 'create-customers', 'create-customers', 'edit-sliders', '2021-09-15 12:10:30', '2021-09-15 12:10:30'),
(85, 'edit-settings', 'edit-settings', 'edit-sliders', '2021-09-15 12:11:10', '2021-09-15 12:11:10');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(3, 1),
(3, 2),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(81, 1),
(82, 1),
(84, 1),
(85, 1);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `privacy_policys`
--

CREATE TABLE `privacy_policys` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `privacy_policy_ar` text NOT NULL,
  `privacy_policy_en` text NOT NULL,
  `term_of_use_ar` longtext NOT NULL,
  `term_of_use_en` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `privacy_policys`
--

INSERT INTO `privacy_policys` (`id`, `created_at`, `updated_at`, `privacy_policy_ar`, `privacy_policy_en`, `term_of_use_ar`, `term_of_use_en`) VALUES
(1, NULL, '2021-08-13 22:35:09', 'Route::resource(\'shop_settings\', \'Dashboard\\ShopSettingController\');\r\n        Route::resource(\'shop_settings\', \'Dashboard\\ShopSettingController\');\r\n        Route::resource(\'shop_settings\', \'Dashboard\\ShopSettingController\');', 'Route::resource(\'shop_settings\', \'Dashboard\\ShopSettingController\');\r\n        Route::resource(\'shop_settings\', \'Dashboard\\ShopSettingController\');', 'Route::resource(\'shop_settings\', \'Dashboattrd\\ShopSettingController\');', 'Route::resource(\'shop_settings\', \'Dashboard\\ShopSettingController\');\r\n        Route::resource(\'shop_settings\', \'Dashboard\\ShopSettingController\');');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `product_code` varchar(30) NOT NULL,
  `product_name_ar` varchar(255) NOT NULL,
  `product_name_en` varchar(255) NOT NULL,
  `product_unitprice` varchar(20) NOT NULL,
  `product_oldprice` int(11) DEFAULT 0,
  `product_description_ar` text NOT NULL,
  `product_description_en` text NOT NULL,
  `product_longdescription_ar` longtext NOT NULL,
  `product_longdescription_en` longtext NOT NULL,
  `views` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL,
  `star` bigint(20) NOT NULL DEFAULT 0,
  `exclusive` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `created_at`, `updated_at`, `deleted_at`, `product_code`, `product_name_ar`, `product_name_en`, `product_unitprice`, `product_oldprice`, `product_description_ar`, `product_description_en`, `product_longdescription_ar`, `product_longdescription_en`, `views`, `status`, `user_id`, `star`, `exclusive`) VALUES
(6, '2021-07-29 19:46:30', '2021-09-01 09:15:11', NULL, '57577', 'التحفيز الذهني للكلاب من نونبل', 'Nunbell Mental Stimulation For Dogs', '230', 0, 'يعمل Nunbell مع غريزة كلبك الطبيعية للبحث عن الطعام والعمل من أجله ، وتحديهم لإلغاء تأمين المكافآت في الداخل.', '<div>Nunbell work with your dog’s natural instinct to hunt and work for food, challenging them to unlock the treats inside.</div><div><br></div>', '<div>يعمل Nunbell مع غريزة كلبك الطبيعية للبحث عن الطعام والعمل من أجله ، وتحديهم لإلغاء تأمين المكافآت في الداخل.</div><div><br></div><div><br></div><div><br></div><div>لماذا أخترتنا؟</div><div><br></div><div>تتوافق جميع ألعاب ألغاز الكلاب لدينا مع CFR21 ، وهي خالية من المواد السامة بنسبة 100٪ ، وخالية من الرصاص ، وخالية من المواد الكيميائية ، وآمنة لحيواناتك الأليفة للمضغ واللعب. يتوافق مع نفس معايير السلامة تمامًا مثل ألعاب الأطفال.</div>', '<div>Nunbell work with your dog’s natural instinct to hunt and work for food, challenging them to unlock the treats inside.</div><div><br></div><div><br></div><div><br></div><div>Why Choose Us?</div><div><br></div><div>All our dog puzzle toys are meet CFR21 compliant, 100% toxic-free, Lead-free, chemical-free, safe for your pets to chew and play. Complies with the exact same safety standard as children’s toys.</div><div><br></div>', 0, 1, 1, 1, 1),
(7, '2021-07-29 19:48:47', '2021-09-01 09:18:44', NULL, '123224', 'Groovy فريش فود من جروفي 400جرام', 'Groovy Fresh Food 400gm', '200', 225, 'الغذاء الامثل لحيوانك الاليف حيث انه يحتوي علي وجبة كاملة محفوظة بدون اضافة اي مواد حافظة داخل عبوة سهلة الاستخدام والتخزين في درجة الحرارة العادية المكونات : مشتقات فراخ (تركي5% ) - ارز - بطاطس- كوسة- جزر - بسلة', 'The ideal food for your pet, as it contains a complete meal preserved without adding any preservatives inside an easy-to-use package and storage at normal temperature Ingredients: Chicken derivatives (Turkish 5%) - rice - potatoes - zucchini - carrots - peas', '<div>Groovy فريش فود من جروفي 400جرام</div><div><br></div><div><br></div><div>الغذاء الامثل لحيوانك الاليف حيث انه يحتوي علي وجبة كاملة محفوظة بدون اضافة اي مواد حافظة داخل عبوة سهلة الاستخدام والتخزين في درجة الحرارة العادية المكونات : مشتقات فراخ (تركي5% ) - ارز - بطاطس- كوسة- جزر - بسلة - بروتينات نباتية -معادن -اضافات تكنولوجية مكثفات و مواد تخثير&nbsp; الاضافات الغذائية : فيتامين أ 131 وحدة دولية - فيتامين د3 160 وحدة دولية - فيتامين ه 10ملجم - كبريتات النحاس بيتاهيدريت 7.6 ملجمالمقومات التحليلية : البروتين الخام بنسبة 12.5% الالياف الخام بنسبة 2 % محتوي الدهون 5% الرماد الخام بنسبة 3% الرطوبة 55% ز</div><div><br></div>', '<div>Groovy Fresh Food 400gm</div><div><br></div><div><br></div><div>The perfect food for your pet, as it contains a complete meal preserved without adding any preservatives inside an easy-to-use package and storage at normal temperature Ingredients: Chicken derivatives (Turkish 5%) - rice - potatoes - zucchini - carrots - peas - vegetable proteins - minerals - additives Technological thickeners and coagulants Food additives: Vitamin A 131 IU - Vitamin D3 160 IU - Vitamin E 10 mg - Copper sulfate betahydrate 7.6 mg Analytical ingredients: Crude protein 12.5%, crude fiber 2%, fat content 5%, raw ash 3% Humidity 55% g</div>', 0, 1, 1, 1, 1),
(8, '2021-07-29 19:49:57', '2021-09-01 09:21:27', NULL, '98765', 'بلوبو طعام رطب للكلاب 415 جرام - 4 قطع', 'lkjhgfcfgbhjk', '111', 130, '<div>طعام رطب للكلاب، غني ومتوازن من بلابو بقطع اللحم الغنية بالفيتامينات والمعادن المهمة لصحة الكلاب البالغة، العبوة 415 جم، بلد المنشأ: بولندا</div><div><br></div>', 'Wet Food for Dogs, Rich and Balanced from Blabo with Cuts of Meat Rich in Vitamins and Minerals Important for the Health of Adult Dogs, Pack 415g, Country of Origin: Poland', '<div>مواصفات المنتج</div><div>طعام رطب للكلاب، غني ومتوازن من بلابو بقطع اللحم الغنية بالفيتامينات والمعادن المهمة لصحة الكلاب البالغة، العبوة 415 جم، بلد المنشأ: بولندا</div><div>العلامة التجارية : بلابو</div><div>نوع التغليف : علبة</div><div>نوع الطعام : الطعام الرطب</div><div>الحجم : 415 جم</div><div>الفصيلة : كلاب</div><div>المرتبة : مميز</div><div>المرحلة العمرية : بالغ</div><div>نكهة الطعام : لحم</div><div>بنية الطعام : مفتت</div><div><br></div>', '<div>Product specification</div><div>Wet Food for Dogs, Rich and Balanced from Blabo with Cuts of Meat Rich in Vitamins and Minerals Important for the Health of Adult Dogs, Pack 415g, Country of Origin: Poland</div><div>Brand: Blabo</div><div>Packaging type: box</div><div>Food type: wet food</div><div>Size: 415 gm</div><div>Kind: dogs</div><div>Rank: Outstanding</div><div>Age group: adult</div><div>Food flavor: meat</div><div>Food texture: crumbly</div>', 0, 1, 1, 1, 0),
(12, '2021-08-07 12:45:06', '2021-09-01 09:24:48', NULL, '2456', 'فيتا ماكس طعام جاف بالدجاج والبط للكلاب البالغة - 20 كجم', 'Vita Maxx Dry Food with Chicken & Duck For Adult Dogs - 20Kg', '380', 410, 'فيتا ماكس طعام جاف بالدجاج والبط للكلاب البالغة - 20 كجم', '<div>Vita Maxx Dry Food with Chicken &amp; Duck For Adult Dogs - 20Kg</div><div><br></div>', '<div>المرتبة : مميز</div><div>نوع التغليف : كيس</div><div>بنية الطعام : جاف</div><div>الحجم : 20كغ</div><div>المرحلة العمرية : بالغ</div><div>نكهة الطعام : متعدد</div><div>نوع الطعام : الطعام الجاف</div><div>الفصيلة : كلاب</div><div><br></div>', '<div>Rank: Outstanding</div><div>Packaging type: sachet</div><div>Food texture: dry</div><div>Size: 20 kg</div><div>Age group: adult</div><div>Food flavor: multi</div><div>Food type: dry food</div><div>Kind: dogs</div>', 0, 1, 1, 0, 1),
(13, '2021-08-10 07:56:31', '2021-09-01 09:27:41', NULL, '55553', 'Mera Dog Pure Sensitive Goody Snacks With Turkey & Potato - 600g', 'Mera Dog Pure Sensitive Goody Snacks With Turkey & Potato - 600g', '300', 0, '<div class=\"-df -j-bet\" style=\"display: flex; justify-content: space-between; color: rgb(40, 40, 40); font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, &quot;Helvetica Neue&quot;, Arial, sans-serif;\"><div class=\"-fs0 -pls -prl\" style=\"padding-left: 24px; padding-right: 8px; font-size: 0px;\"><h1 class=\"-fs20 -pts -pbxs\" style=\"padding: 8px 0px 4px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-size: 1.25rem;\">Mera Dog Pure Sensitive Goody Snacks With Turkey &amp; Potato - 600g</h1></div><a id=\"wishlist\" href=\"https://www.jumia.com.eg/ar/customer/account/login/?tkWl=ME244OT06BR5SNAFAMZ-38530158&amp;return=%2Far%2Fpure-sensitive-goody-snacks-with-turkey-potato-600g-mera-dog-mpg188494.html\" class=\"btn _def _i _rnd -mas -fsh0 -me-start\" data-simplesku=\"ME244OT06BR5SNAFAMZ-38530158\" data-track-onclick=\"wishlist\" data-track-onclick-bound=\"true\" style=\"display: flex; color: rgb(246, 139, 30); border-radius: 99px; border-style: initial; border-color: initial; outline: 0px; padding: 0px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, &quot;Helvetica Neue&quot;, Arial, sans-serif; text-transform: uppercase; line-height: 1rem; cursor: pointer; align-items: center; position: relative; text-indent: 8px; flex-shrink: 0; align-self: flex-start; margin: 8px;\"><svg aria-label=\"اضافة لقائمة الأمنيات\" viewBox=\"0 0 24 24\" class=\"ic -f-or5\" width=\"24\" height=\"24\"><use xlink:href=\"https://www.jumia.com.eg/assets_he/images/i-icons.e70b7734.svg#saved-items\"></use></svg></a></div><div class=\"-phs\" style=\"padding-left: 8px; padding-right: 8px; color: rgb(40, 40, 40); font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, &quot;Helvetica Neue&quot;, Arial, sans-serif;\"><br class=\"Apple-interchange-newline\"></div>', '<div class=\"-df -j-bet\" style=\"display: flex; justify-content: space-between; color: rgb(40, 40, 40); font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, &quot;Helvetica Neue&quot;, Arial, sans-serif;\"><div class=\"-fs0 -pls -prl\" style=\"padding-left: 24px; padding-right: 8px; font-size: 0px;\"><h1 class=\"-fs20 -pts -pbxs\" style=\"padding: 8px 0px 4px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-size: 1.25rem;\">Mera Dog Pure Sensitive Goody Snacks With Turkey &amp; Potato - 600g</h1></div><a id=\"wishlist\" href=\"https://www.jumia.com.eg/ar/customer/account/login/?tkWl=ME244OT06BR5SNAFAMZ-38530158&amp;return=%2Far%2Fpure-sensitive-goody-snacks-with-turkey-potato-600g-mera-dog-mpg188494.html\" class=\"btn _def _i _rnd -mas -fsh0 -me-start\" data-simplesku=\"ME244OT06BR5SNAFAMZ-38530158\" data-track-onclick=\"wishlist\" data-track-onclick-bound=\"true\" style=\"display: flex; color: rgb(246, 139, 30); border-radius: 99px; border-style: initial; border-color: initial; outline: 0px; padding: 0px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, &quot;Helvetica Neue&quot;, Arial, sans-serif; text-transform: uppercase; line-height: 1rem; cursor: pointer; align-items: center; position: relative; text-indent: 8px; flex-shrink: 0; align-self: flex-start; margin: 8px;\"><svg aria-label=\"اضافة لقائمة الأمنيات\" viewBox=\"0 0 24 24\" class=\"ic -f-or5\" width=\"24\" height=\"24\"><use xlink:href=\"https://www.jumia.com.eg/assets_he/images/i-icons.e70b7734.svg#saved-items\"></use></svg></a></div><div class=\"-phs\" style=\"padding-left: 8px; padding-right: 8px; color: rgb(40, 40, 40); font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, &quot;Helvetica Neue&quot;, Arial, sans-serif;\"><br class=\"Apple-interchange-newline\"></div>', '<div>أليفك من اهم الحاجات في حياتك و عشان احنا عارفين ده كويس و مهتمين بصحة أليفك بنقدملك افضل انواع اكل و مستلزمات جميع الحيوانات الأليفة (كلاب - قطط - طيور - سمك) و اتأكد انك هتحصل علي افضل المميزات عندنا كعميل أوسكار المميز هتقدر تحصل علي خصومات كتير ده غير التوصيل المجاني لحد باب بيتك.</div><div><br></div>', 'Your pet is one of the most important needs in your life, and because we know this well and are interested in the health of your pets, we offer you the best types of food and supplies for all pets (dogs - cats - birds - fish) and I am sure that you will get the best features with us as a special Oscar customer, you will be able to get many discounts This is not free delivery to your doorstep.', 0, 1, 1, 1, 0),
(14, '2021-08-10 19:34:02', '2021-09-01 09:30:12', NULL, '88888', 'بيوي قطط كروسينز مكس طعام جاف للقطط - 20 كجم', 'Bewi Cat Crocins Mix Dry Food For Cats - 20kg', '200', 0, 'يتمتع الطعام الجاف أيضًا بميزة أنه عندما يكون متاحًا مجانًا ، يمكن للقط أن يأكل عدة وجبات صغيرة يوميًا وفقًا لعاداته الطبيعية.&nbsp;', 'Dry food also has the advantage that when it is freely available, the cat can eat several small meals a day in accordance with its natural habits', 'بسبب إزالة الماء ، يكون تركيز المغذيات في الطعام الجاف أعلى بأربع مرات من تركيزه في الطعام الرطب. يمكن تقليل تكاليف التغذية بشكل كبير عن طريق إعطاء قطتك طعامًا جافًا. يتمتع الطعام الجاف أيضًا بميزة أنه عندما يكون متاحًا مجانًا ، يمكن للقط أن يأكل عدة وجبات صغيرة يوميًا وفقًا لعاداته الطبيعية. تساعد الكروكيت المقرمشة أيضًا في تنظيف أسنان القط ، بينما يعمل نشاط المضغ على شد لثة القط.', '<div>Due to water removal, the nutrient concentration in dry food is approximately 4 times as high as that in moist food. Feeding costs can be considerably reduced by giving your cat dry food. Dry food also has the advantage that when it is freely available, the cat can eat several small meals a day in accordance with its natural habits. The crispy croquettes also help to clean the cat\'s teeth, while the chewing activity firms up the cat\'s gums.</div><div><br></div>', 0, 1, 1, 0, 1),
(21, '2021-08-11 08:16:27', '2021-09-01 09:33:16', NULL, '22227', 'ليوناردو طعام جاف للقطط - 2 كجم', 'Leonardo Dry Cat Food - 2 kg', '230', 280, 'يعتبر LEONARDO® Kitten سهل الهضم بشكل خاص لأنه يحتوي على المزيد من الدواجن الطازجة ، بالإضافة إلى بذور الشيا لتنظيم القناة الهضمية. في الأشهر القليلة الأولى من حياة القطة', 'animal_type_idanimal_type_idanimal_type_idLEONARDO® Kitten is particularly easy to digest because it contains more fresh poultry, aswell as Shea seeds to regulate the gut. In the first few months of a kitten’s life,', '<div><span style=\"font-size: 0.875rem;\">مواصفات المنتج</span><br></div><div>يعتبر LEONARDO® Kitten سهل الهضم بشكل خاص لأنه يحتوي على المزيد من الدواجن الطازجة ، بالإضافة إلى بذور الشيا لتنظيم القناة الهضمية. في الأشهر القليلة الأولى من حياة القطة ، يعتبر الهضم الجيد أمرًا مهمًا بشكل خاص للرفاهية. يحتوي LEONARDO® Kitten على ProVital ، وهو مزيج من المكونات النشطة من مستخلصات الخميرة الطبيعية ، لدعم وتقوية وظيفة المناعة.</div><div>ما هو مصدر البروتين؟</div><div>بروتين حيواني 85٪ بروتين نباتي 15٪</div><div>المكونات تصنع الفرق!</div>', '<div><span style=\"font-size: 0.875rem;\">Product features</span><br></div><div><br></div><div>LEONARDO® Kitten is particularly easy to digest because it contains more fresh poultry, aswell as Shea seeds to regulate the gut. In the first few months of a kitten’s life, good digestion is particularly important for wellbeing. LEONARDO® Kitten contains ProVital, a combination of active ingredients from natural yeast extracts, to support and strengthen the immune function.</div><div>What is the protein source?</div><div>Animal protein 85%Vegetable protein 15%</div><div>The ingredients makes the difference!</div><div><br></div>', 0, 1, 1, 0, 0),
(22, '2021-08-11 08:16:51', '2021-09-01 09:34:45', NULL, '747932', 'بورينا دراى فود للقطط - عادى', 'Purina Dry Food For Cats - Regular', '340', 390, '<div><br></div><div>يوفر Purina Cat Chow Indoor تغذية كاملة ومتوازنة بنسبة 100٪ للقطط البالغة. إنه طعام جاف للقطط للتغذية اليومية&nbsp;<br></div>', '<div><br></div><div>Purina Cat Chow Indoor provides 100% complete &amp; balanced nutrition for adult cats. It’s a dry cat food for everyday feeding, formulated to suit an indoor adult cat’s life.<br></div>', '<div>بورينا دراى فود للقطط - عادى</div><div><br></div><div>يوفر Purina Cat Chow Indoor تغذية كاملة ومتوازنة بنسبة 100٪ للقطط البالغة. إنه طعام جاف للقطط للتغذية اليومية ، وقد تم تركيبه ليناسب حياة القطط البالغة داخل المنزل. تمنحهم كل وجبة السعرات الحرارية المناسبة التي يحتاجونها بالإضافة إلى 25 من الفيتامينات والمعادن الأساسية ومضادات الأكسدة للمساعدة في دعم صحة المناعة ومزيج خاص من الألياف للمساعدة في التحكم في كرات الشعر.</div>', '<div>Purina Dry Food For Cats - Regular</div><div><br></div><div>Purina Cat Chow Indoor provides 100% complete &amp; balanced nutrition for adult cats. It’s a dry cat food for everyday feeding, formulated to suit an indoor adult cat’s life. Every meal gives them just the right calories they need as well as 25 essential vitamins and minerals, antioxidants to help support immune health and a special fiber blend to help control hairballs.</div><div><br></div>', 0, 1, 1, 0, 1),
(23, '2021-08-26 08:23:15', '2021-09-01 09:37:22', NULL, '2222222', 'بيربو طعام جاف للقطط - 1 كجم', 'Birbo dry food for cats - 1 kg', '66', 75, '<div>يخضع لإنتهاء الصلاحية : نعم ،&nbsp;<span style=\"font-size: 0.875rem;\">العلامة التجارية : Birbo ،&nbsp;</span><span style=\"font-size: 0.875rem;\">المرتبة : مميز ،&nbsp;</span><span style=\"font-size: 0.875rem;\">نوع التغليف : كيس</span></div><div><br></div>', '<div>Subject to Expiration: Yes,&nbsp;<span style=\"font-size: 0.875rem;\">Brand: Birbo,&nbsp;</span><span style=\"font-size: 0.875rem;\">Rank: Outstanding,&nbsp;</span><span style=\"font-size: 0.875rem;\">Packaging type: sachet</span></div><div><br></div>', '<div>يخضع لإنتهاء الصلاحية : نعم</div><div>العلامة التجارية : Birbo</div><div>المرتبة : مميز</div><div>نوع التغليف : كيس</div><div>بنية الطعام : مقرمش</div><div>الحجم : 1 كغ</div><div>المرحلة العمرية : بالغ</div><div>نكهة الطعام : سمك ولحم</div><div>نوع الطعام : الطعام الجاف</div><div>الفصيلة : قطط</div><div>يحتوي علي مكافات</div><div><br></div>', '<div><span style=\"font-size: 0.875rem;\">Subject to Expiration: Yes</span><br></div><div>Brand: Birbo</div><div>Rank: Outstanding</div><div>Packaging type: sachet</div><div>Food texture: crunchy</div><div>Size: 1 kg</div><div>Age group: adult</div><div>Food flavor: fish and meat</div><div>Food type: dry food</div><div>Family: cats</div><div>Contains rewards</div>', 0, 1, 1, 0, 1),
(24, '2021-08-28 14:43:10', '2021-09-01 10:09:50', NULL, '93878', 'بيكارت ميجابون بوينت طعام جاف للقطط - 18 كجم', 'Pickart Megabon Point Dry Food For Cats - 18 kg', '500', 600, '<div>بيكارت ميجابون بوينت طعام جاف للقطط - 18 كجم</div><div>العلامة التجارية : ميجابون</div><div>نوع التغليف : جرة</div><div>نوع الطعام : الطعام الجاف</div><div><span style=\"font-size: 0.875rem;\">الفصيلة : قطط</span><br></div><div><span style=\"font-size: 0.875rem;\"><br></span></div>', '<div>Pickart Megabon Point Dry Food For Cats - 18 kg</div><div>Brand: Megabon</div><div>Packaging Type : Jar</div><div>Food type: dry food</div><div><span style=\"font-size: 0.875rem;\">Family: cats</span><br></div><div><span style=\"font-size: 0.875rem;\"><br></span></div>', '<div>بيكارت ميجابون بوينت طعام جاف للقطط - 18 كجم</div><div>العلامة التجارية : ميجابون</div><div>نوع التغليف : جرة</div><div>نوع الطعام : الطعام الجاف</div><div><span style=\"font-size: 0.875rem;\">الفصيلة : قطط</span><br></div><div>المرتبة : مميز جدا</div><div>المرحلة العمرية : بالغ</div><div>نكهة الطعام : لحم</div><div>بنية الطعام : صلب</div><div><br></div>', '<div>Pickart Megabon Point Dry Food For Cats - 18 kg</div><div>Brand: Megabon</div><div>Packaging Type : Jar</div><div>Food type: dry food</div><div><span style=\"font-size: 0.875rem;\">Family: cats</span><br></div><div>Rank: very special</div><div>Age group: adult</div><div>Food flavor: meat</div><div>Food texture: solid</div>', 0, 1, 1, 0, 1),
(25, '2021-08-28 14:45:06', '2021-09-01 10:09:44', NULL, '98784', 'ألعاب قطط ، لعبة قطط تفاعلية مع كوب شفط', 'Cat Toys, Turntable Interactive Cat Toy with Suction Cup', '78', 99, '<div>ألعاب Exboard Windmill للقطط ، لعبة قطط تفاعلية قابلة للدوران مع فرشاة كوب شفط لتنظيف أسنان القطط ، أداة تثبيت على الحائط للقطط مع كرة لعبة قابلة للدوران (أزرق)</div><div><br></div>', '<div><span style=\"font-size: 0.875rem;\">Exboard Windmill Cat Toys, Turntable Interactive Cat Toy with Suction Cup Brush for Cat Tooth Cleaning Scratching,Wall Mount Cat Spinner with Rotatable Toy ball (Blue)</span><br></div><div><span style=\"font-size: 0.875rem;\"><br></span></div>', '<div><span style=\"font-size: 0.875rem;\">ألعاب Exboard Windmill للقطط ، لعبة قطط تفاعلية قابلة للدوران مع فرشاة كوب شفط لتنظيف أسنان القطط ، أداة تثبيت على الحائط للقطط مع كرة لعبة قابلة للدوران (أزرق)</span><br></div><div>مصنوعة من مادة TPR الناعمة ، عديمة النكهة ، آمنة ، صديقة للبيئة ، متينة ، مقاومة للعض ، قابلة للمط. تصميم كوب شفط كرة لعبة قط ، مثالية للعض والمضغ واللعب لقطتك.</div>', '<div><span style=\"font-size: 0.875rem;\">Exboard Windmill Cat Toys, Turntable Interactive Cat Toy with Suction Cup Brush for Cat Tooth Cleaning Scratching,Wall Mount Cat Spinner with Rotatable Toy ball (Blue)</span><br></div><div>Made of soft TPR material,flavorless, safe, eco-friendly,Durable, bite-resistant, stretchable.Suction cup design cat toy ball , Perfect for biting, chewing and playing for your cat.</div><div><br></div>', 0, 1, 1, 0, 1),
(26, '2021-09-01 09:48:49', '2021-09-01 10:09:57', NULL, '23443', 'Dogs Cats Treat Launcher Snack Food Feeder Catapult Pet Training Tool rose red', 'Dogs Cats Treat Launcher Snack Food Feeder Catapult Pet Training Tool rose red', '430', 460, 'الأوصاف:\r\nيأتي مع حزام معصم يجعله سهل الحمل للغاية.\r\nيمكنك الآن إبقاء يديك حرتين لمداعبة كلبك.', 'Descriptions:\r\nIt comes with a wrist strap, that makes it very portable.\r\nYou can now keep your hands free to pet your dog.', 'سمات:\r\nتم تصميم المقبض ليمنحك قبضة مريحة.\r\nمثالي للكلاب والقطط ، سهل التحميل والاستخدام ممتع.\r\nهذه اللعبة هي متعة تفاعلية رائعة بالنسبة لك ولكلبك / قطتك.\r\nمن السهل تحميلها مع حيواناتك الأليفة المفضلة ولديها مشغل زنبركي لإطلاق المكافآت.\r\nلا داعي للاحتفاظ بهذه المكافآت والقلق بشأن إزالة الفتات من يديك.\r\n\r\nالأوصاف:\r\nيأتي مع حزام معصم يجعله سهل الحمل للغاية.\r\nيمكنك الآن إبقاء يديك حرتين لمداعبة كلبك.', 'Features:\r\nThe handle is designed to give you comfortable grip.\r\nPerfect for dogs and cats, easy to load and fun to use.\r\nThis toy is great interactive fun for you and your dog/cat.\r\nIt is easy to load with your pets favorite treats and has a spring loaded trigger to launch treats.\r\nNo need to hold those treats and worry about getting the crumbs off your hands.\r\n\r\nDescriptions:\r\nIt comes with a wrist strap, that makes it very portable.\r\nYou can now keep your hands free to pet your dog.', 0, 1, 1, 0, 0),
(32, '2021-09-01 10:03:47', '2021-09-01 10:12:52', NULL, '88676', ',طوق كلب مخصص بطبعة نايلون أطواق كلب صغير ورصاص للحيوانات الأليفة الصغيرة', 'Personalized Dog Collar Leash Nylon Print Small Dog Collars And Lead', '878', 977, '<div>طوق كلب مخصص بطبعة نايلون أطواق كلب صغير ورصاص للحيوانات الأليفة الصغيرة والمتوسطة والكبيرة جرو كلب الراعي الألماني (# زهرة زرقاء داكنة C)</div><div><br></div>', '<div>Personalized Dog Collar Leash Nylon Print Small Dog Collars And Lead For Small Medium Large Pet Pitbull German Shepherd Puppy(#Dark Blue Flower C)</div><div><br></div>', '<div><span style=\"font-size: 0.875rem;\">الاسم: شخصية طوق الكلب المقود نايلون طباعة أطواق الكلاب الصغيرة والرصاص للحيوانات الأليفة الصغيرة والمتوسطة الكبيرة بيتبول كلب الراعي الألماني جرو</span><br></div><div><br></div><div>دعوى ل: الكلاب الصغيرة والمتوسطة والكبيرة</div><div><br></div><div>سلالة الكلاب القابلة للتطبيق: كلاب صغيرة متوسطة وكبيرة</div><div><br></div><div>الميزة: شخصية / مخصصة / قابلة للتعديل<br></div>', '<div><span style=\"font-size: 0.875rem;\">Name: Personalized Dog Collar Leash Nylon Print Small Dog Collars and Lead for Small Medium Large Pet Pitbull German Shepherd Puppy</span><br></div><div><br></div><div>Suit for: Small Medium Large Dogs</div><div><br></div><div>Applicable Dog Breed: Small Medium Large Dogs</div><div><br></div><div>Feature: Personalized/Customized/Adjustable</div><div><br></div>', 0, 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_appraisals`
--

CREATE TABLE `product_appraisals` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `appraisal_content` text DEFAULT NULL,
  `appraisal_num` varchar(11) DEFAULT NULL,
  `customer_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_appraisals`
--

INSERT INTO `product_appraisals` (`id`, `created_at`, `updated_at`, `deleted_at`, `product_id`, `appraisal_content`, `appraisal_num`, `customer_id`) VALUES
(1, '2021-08-09 19:20:27', '2021-08-09 19:20:27', NULL, 12, 'ProductAppraisal', '1', 4),
(2, '2021-08-09 19:20:46', '2021-08-09 19:20:46', NULL, 12, 'ProductAppraisal', '3', 4),
(3, '2021-08-09 19:21:21', '2021-08-09 19:21:21', NULL, 12, 'ProductAppraisalProductAppraisal', '2', 4),
(4, '2021-08-09 19:22:45', '2021-08-09 19:22:45', NULL, 12, 'ProductAppraisalProductAppraisal', '2', 4),
(5, '2021-08-09 19:24:13', '2021-08-09 19:24:13', NULL, 12, 'appraisal_numappraisal_numappraisal_num', '1', 4),
(6, '2021-08-09 19:24:44', '2021-08-09 19:24:44', NULL, 12, 'appraisal_numappraisal_numappraisal_numappraisal_numappraisal_num', '2', 4),
(7, '2021-08-09 19:28:19', '2021-08-09 19:28:19', NULL, 12, '<div class=\"rating\"></div>\n                                            </div>\n                                            <input type=\"hidden\" name=\"appraisal_num\" id=\"val\">\n                                                <div class=\"rating\"></div>\n                                            </div>\n                                            <input type=\"hidden\" name=\"appraisal_num\" id=\"val\">', '2', 4),
(8, '2021-08-09 19:29:11', '2021-08-09 19:29:11', NULL, 12, 'appraisal_numappraisal_num', '2', 4),
(9, '2021-08-09 19:29:39', '2021-08-09 19:29:39', NULL, 12, 'appraisal_numappraisal_numappraisal_numappraisal_numappraisal_numappraisal_num', '4', 4),
(10, '2021-08-09 19:29:50', '2021-08-09 19:29:50', NULL, 12, 'appraisal_numappraisal_numappraisal_numappraisal_numappraisal_numappraisal_num', '3', 4),
(11, '2021-08-09 19:30:21', '2021-08-09 19:30:21', NULL, 12, 'appraisal_numappraisal_numappraisal_num', '1', 4),
(12, '2021-08-09 20:06:03', '2021-08-09 20:06:03', NULL, 12, '<?php echo $value3->created_at->toFormattedDateString(); ?> <?php echo $value3->created_at->toFormattedDateString(); ?> <?php echo $value3->created_at->toFormattedDateString(); ?>', '1', 4),
(13, '2021-08-11 19:10:31', '2021-08-11 19:10:31', NULL, 6, 'hhhhhhhhhhhhhhhhhhhhhhhh', '2', 4),
(14, '2021-09-01 12:10:32', '2021-09-01 12:10:32', NULL, 12, NULL, NULL, 18),
(15, '2021-09-01 12:11:45', '2021-09-01 12:11:45', NULL, 12, NULL, NULL, 18),
(16, '2021-09-01 12:12:05', '2021-09-01 12:12:05', NULL, 12, NULL, NULL, 18),
(17, '2021-09-24 13:02:12', '2021-09-24 13:02:12', NULL, 7, 'mmm', '2.5', 24);

-- --------------------------------------------------------

--
-- Table structure for table `product_best_sellers`
--

CREATE TABLE `product_best_sellers` (
  `id` int(10) NOT NULL,
  `product_id` int(10) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `user_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_best_sellers`
--

INSERT INTO `product_best_sellers` (`id`, `product_id`, `status`, `created_at`, `updated_at`, `user_id`) VALUES
(10, 7, 0, '2021-08-28 15:51:00', '2021-08-28 13:51:00', 1),
(11, 8, 1, '2021-08-28 13:21:52', '2021-08-28 13:21:52', 1),
(12, 22, 1, '2021-08-28 13:22:01', '2021-08-28 13:22:01', 1),
(13, 23, 1, '2021-08-28 13:22:01', '2021-08-28 13:22:01', 1),
(14, 8, 1, '2021-08-28 13:26:55', '2021-08-28 13:26:55', 1),
(15, 8, 1, '2021-08-28 13:29:27', '2021-08-28 13:29:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_discount`
--

CREATE TABLE `product_discount` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_discount_rate` int(11) NOT NULL,
  `discount_expiredate` date NOT NULL,
  `discount_notes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_favourites`
--

CREATE TABLE `product_favourites` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_favourites`
--

INSERT INTO `product_favourites` (`id`, `created_at`, `updated_at`, `deleted_at`, `product_id`, `customer_id`) VALUES
(37, '2021-08-26 15:53:28', '2021-08-26 15:54:36', '2021-08-26 15:54:36', 7, 4),
(38, '2021-08-26 15:54:40', '2021-08-26 15:54:47', '2021-08-26 15:54:47', 7, 4),
(39, '2021-08-26 15:55:06', '2021-08-26 15:55:09', '2021-08-26 15:55:09', 7, 4),
(40, '2021-08-26 16:01:16', '2021-08-26 16:29:04', '2021-08-26 16:29:04', 7, 4),
(41, '2021-08-26 16:01:20', '2021-08-26 16:01:27', '2021-08-26 16:01:27', 6, 4),
(42, '2021-08-26 16:01:30', '2021-08-31 13:03:58', '2021-08-31 13:03:58', 6, 4),
(43, '2021-08-26 16:01:30', '2021-08-31 13:03:58', '2021-08-31 13:03:58', 7, 4),
(44, '2021-08-26 16:01:30', '2021-08-31 13:03:58', '2021-08-31 13:03:58', 8, 4),
(45, '2021-08-26 16:01:30', '2021-08-31 10:06:36', '2021-08-31 10:06:36', 12, 4),
(46, '2021-08-26 16:01:30', '2021-08-26 16:01:30', NULL, 7, 8),
(47, '2021-08-26 16:01:30', '2021-08-26 16:01:30', NULL, 8, 8),
(48, '2021-08-26 16:01:30', '2021-08-26 16:01:30', NULL, 12, 8),
(49, '2021-08-26 16:01:30', '2021-08-26 16:01:30', NULL, 8, 14),
(50, '2021-08-26 16:01:30', '2021-08-26 16:01:30', NULL, 12, 14),
(51, '2021-08-31 10:06:32', '2021-08-31 10:06:43', '2021-08-31 10:06:43', 14, 4),
(52, '2021-08-31 10:06:39', '2021-08-31 10:06:49', '2021-08-31 10:06:49', 12, 4),
(53, '2021-08-31 10:06:53', '2021-08-31 10:08:30', '2021-08-31 10:08:30', 12, 4),
(54, '2021-08-31 10:08:26', '2021-08-31 10:08:40', '2021-08-31 10:08:40', 14, 4),
(55, '2021-08-31 10:10:10', '2021-08-31 10:10:13', '2021-08-31 10:10:13', 23, 4),
(56, '2021-08-31 10:10:19', '2021-08-31 10:10:22', '2021-08-31 10:10:22', 14, 4),
(57, '2021-08-31 10:11:17', '2021-08-31 10:11:20', '2021-08-31 10:11:20', 14, 4),
(58, '2021-08-31 10:11:52', '2021-08-31 10:11:54', '2021-08-31 10:11:54', 14, 4),
(59, '2021-08-31 10:11:59', '2021-08-31 10:12:07', '2021-08-31 10:12:07', 14, 4),
(60, '2021-08-31 10:13:01', '2021-08-31 10:13:03', '2021-08-31 10:13:03', 12, 4),
(61, '2021-08-31 10:13:08', '2021-08-31 10:13:36', '2021-08-31 10:13:36', 12, 4),
(62, '2021-08-31 10:13:41', '2021-08-31 10:13:44', '2021-08-31 10:13:44', 12, 4),
(63, '2021-08-31 10:14:54', '2021-08-31 10:15:00', '2021-08-31 10:15:00', 12, 4),
(64, '2021-08-31 10:16:46', '2021-08-31 10:17:53', '2021-08-31 10:17:53', 12, 4),
(65, '2021-08-31 10:18:07', '2021-08-31 10:18:12', '2021-08-31 10:18:12', 12, 4),
(66, '2021-08-31 10:18:33', '2021-08-31 10:18:39', '2021-08-31 10:18:39', 12, 4),
(67, '2021-08-31 10:18:43', '2021-08-31 13:03:58', '2021-08-31 13:03:58', 12, 4),
(68, '2021-08-31 12:33:46', '2021-08-31 13:03:58', '2021-08-31 13:03:58', 14, 4),
(69, '2021-09-01 06:34:25', '2021-09-01 06:34:25', NULL, 6, 18),
(70, '2021-09-05 08:45:39', '2021-09-05 10:06:47', '2021-09-05 10:06:47', 13, 4),
(71, '2021-09-05 08:45:59', '2021-09-05 10:07:55', '2021-09-05 10:07:55', 23, 4),
(72, '2021-09-05 08:46:30', '2021-09-05 10:11:44', '2021-09-05 10:11:44', 8, 4),
(73, '2021-09-05 16:59:46', '2021-09-05 16:59:46', NULL, 12, 4),
(74, '2021-09-24 12:53:04', '2021-09-24 12:55:47', '2021-09-24 12:55:47', 7, 24),
(75, '2021-09-24 12:55:59', '2021-09-24 12:56:03', '2021-09-24 12:56:03', 7, 24),
(76, '2021-09-24 12:56:09', '2021-09-24 12:56:16', '2021-09-24 12:56:16', 7, 24),
(77, '2021-09-24 13:21:41', '2021-09-24 13:22:02', '2021-09-24 13:22:02', 7, 24),
(78, '2021-09-26 06:32:48', '2021-09-26 06:32:48', NULL, 14, 27),
(79, '2021-09-30 09:33:45', '2021-09-30 09:33:53', '2021-09-30 09:33:53', 7, 36),
(80, '2021-09-30 09:33:45', '2021-09-30 09:33:45', NULL, 8, 36),
(81, '2021-09-30 09:41:09', '2021-09-30 09:41:09', NULL, 21, 36);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_image` text NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `created_at`, `updated_at`, `product_image`, `product_id`) VALUES
(22, '2021-09-01 09:15:11', '2021-09-01 09:15:11', '163049491119487.jpg', 6),
(23, '2021-09-01 09:15:11', '2021-09-01 09:15:11', '163049491112910.jpg', 6),
(24, '2021-09-01 09:18:44', '2021-09-01 09:18:44', '163049512420108.jpg', 7),
(25, '2021-09-01 09:21:27', '2021-09-01 09:21:27', '163049528718447.jpg', 8),
(26, '2021-09-01 09:24:48', '2021-09-01 09:24:48', '163049548869764.jpg', 12),
(27, '2021-09-01 09:24:48', '2021-09-01 09:24:48', '163049548842053.jpg', 12),
(28, '2021-09-01 09:24:48', '2021-09-01 09:24:48', '163049548839689.jpg', 12),
(29, '2021-09-01 09:27:41', '2021-09-01 09:27:41', '163049566197060.jpg', 13),
(30, '2021-09-01 09:27:41', '2021-09-01 09:27:41', '163049566137906.jpg', 13),
(31, '2021-09-01 09:30:12', '2021-09-01 09:30:12', '163049581282238.jpg', 14),
(32, '2021-09-01 09:33:16', '2021-09-01 09:33:16', '163049599676205.jpg', 21),
(33, '2021-09-01 09:34:45', '2021-09-01 09:34:45', '163049608556498.jpg', 22),
(34, '2021-09-01 09:37:22', '2021-09-01 09:37:22', '163049624244275.jpg', 23),
(35, '2021-09-01 09:39:18', '2021-09-01 09:39:18', '163049635857195.jpg', 24),
(36, '2021-09-01 09:41:47', '2021-09-01 09:41:47', '163049650788895.jpg', 25),
(37, '2021-09-01 09:41:47', '2021-09-01 09:41:47', '163049650782833.jpg', 25),
(38, '2021-09-01 09:41:47', '2021-09-01 09:41:47', '163049650789568.jpg', 25),
(39, '2021-09-01 09:41:47', '2021-09-01 09:41:47', '163049650766265.jpg', 25),
(40, '2021-09-01 09:41:47', '2021-09-01 09:41:47', '163016910695783.jpg', 26),
(41, '2021-09-01 10:00:44', '2021-09-01 10:00:44', '163049764424693.jpg', 26),
(42, '2021-09-01 10:03:47', '2021-09-01 10:03:47', '163049782713153.jpg', 32),
(43, '2021-09-01 10:12:53', '2021-09-01 10:12:53', '163049837370577.jpg', 32),
(44, '2021-09-01 10:12:53', '2021-09-01 10:12:53', '163049837377236.jpg', 32);

-- --------------------------------------------------------

--
-- Table structure for table `product_new_arrivals`
--

CREATE TABLE `product_new_arrivals` (
  `id` int(10) NOT NULL,
  `product_id` int(10) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `user_id` int(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_new_arrivals`
--

INSERT INTO `product_new_arrivals` (`id`, `product_id`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 6, 1, 1, '2021-08-28 14:01:53', '2021-08-28 14:01:53'),
(2, 7, 1, 1, '2021-08-28 14:01:53', '2021-08-28 14:01:53'),
(3, 8, 1, 1, '2021-08-28 14:01:53', '2021-08-28 14:01:53'),
(4, 12, 1, 1, '2021-08-28 14:01:53', '2021-08-28 14:01:53'),
(5, 13, 1, 1, '2021-08-28 14:01:53', '2021-08-28 14:01:53'),
(6, 14, 1, 1, '2021-08-28 14:01:53', '2021-08-28 14:01:53'),
(7, 21, 1, 1, '2021-08-28 14:01:53', '2021-08-28 14:01:53'),
(8, 22, 1, 1, '2021-08-28 14:01:53', '2021-08-28 14:01:53'),
(9, 23, 1, 1, '2021-08-28 14:01:53', '2021-08-28 14:01:53');

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE `regions` (
  `id` int(10) NOT NULL,
  `region_name_ar` varchar(255) DEFAULT NULL,
  `city_id` int(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `region_name_ar`, `city_id`, `created_at`, `updated_at`) VALUES
(1, 'الرياض ', 1, '2021-09-03 21:52:42', '2021-09-03 20:51:16'),
(2, 'الدوادمي', 1, '2021-09-03 21:52:38', '2021-09-03 20:51:16'),
(3, 'المجمعه', 1, '2021-09-03 21:53:10', '2021-09-03 20:51:16'),
(4, 'عفيف', 1, '2021-09-03 21:53:13', '2021-09-03 20:51:16'),
(5, 'جده', 2, '2021-09-03 21:52:20', '2021-09-03 20:51:16'),
(6, 'مكه المكرمه', 2, '2021-09-03 21:53:42', '2021-09-03 20:51:16'),
(7, 'الطائف', 2, '2021-09-03 21:53:56', '2021-09-03 20:51:16'),
(8, 'الجموم', 2, '2021-09-09 10:47:19', '2021-09-09 08:47:12');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'superadministrator', 'Superadministrator', 'Superadministrator', NULL, NULL),
(2, 'administrator', 'Administrator', 'Administrator', NULL, NULL),
(4, 'admin', 'admin', 'g', '2021-09-15 11:13:11', '2021-09-15 11:13:11');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`, `user_type`) VALUES
(1, 1, 'App\\Models\\User'),
(1, 3, 'App\\Models\\User'),
(2, 4, 'App\\Models\\User'),
(4, 5, 'App\\Models\\User');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `about_us_ar` longtext NOT NULL,
  `our_vision_en` longtext NOT NULL,
  `video` text NOT NULL,
  `phone_no` varchar(40) NOT NULL,
  `address_ar` varchar(255) NOT NULL,
  `address_en` varchar(255) NOT NULL,
  `email` varchar(200) NOT NULL,
  `facebook_link` text NOT NULL,
  `about_us_en` longtext NOT NULL,
  `snapchat_link` text NOT NULL,
  `our_vision_ar` longtext NOT NULL,
  `twitter_link` text NOT NULL,
  `instagram_link` varchar(191) NOT NULL,
  `youtube_link` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `created_at`, `updated_at`, `about_us_ar`, `our_vision_en`, `video`, `phone_no`, `address_ar`, `address_en`, `email`, `facebook_link`, `about_us_en`, `snapchat_link`, `our_vision_ar`, `twitter_link`, `instagram_link`, `youtube_link`) VALUES
(1, NULL, '2021-08-30 06:13:17', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها في أي محرك بحث \"Lorem Ipsum\" نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض .العبارات الفكاهية إليها', 'There is a proven fact from a long time ago that the readable content of a page will distract the reader from focusing on the external form of the text or the form of paragraphs placed on the page he reads. Therefore, the Lorem Ipsum method is used because it gives a natural distribution - to some extent - of the characters instead of using \"here there is text content, here there is text content\", making it look (that is, the characters) as if in any search engine \"Lorem Ipsum\" is a readable text. Many desktop publishing and web page editing programs use Lorem Ipsum by default as a sample text, and if you enter many new sites will appear in the search results. Over the years, new and different versions of Lorem Ipsum\'s text have appeared, sometimes by accident, sometimes deliberately as humorous phrases were inserted into it.', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/krDWc30PAGg\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '+971 012 345 6789', 'المملكة العربية السعودية مدينة الرياض - شارع 15', 'Saudi Arabia', 'company_mail@gmail.com', 'https://www.facebook.com/', 'There is a proven fact from a long time ago that the readable content of a page will distract the reader from focusing on the external form of the text or the form of paragraphs placed on the page he reads. Therefore, the Lorem Ipsum method is used because it gives a natural distribution - to some extent - of the characters instead of using \"here there is text content, here there is text content\", making it look (that is, the characters) as if in any search engine \"Lorem Ipsum\" is a readable text. Many desktop publishing and web page editing programs use Lorem Ipsum by default as a sample text, and if you enter many new sites will appear in the search results. Over the years, new and different versions of Lorem Ipsum\'s text have appeared, sometimes by accident, sometimes deliberately as humorous phrases were inserted into it.', 'https://www.snapchat.com/', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها في أي محرك بحث \"Lorem Ipsum\" نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض .العبارات الفكاهية إليها', 'https://twitter.com/?lang=en', 'https://www.instagram.com/', 'https://www.youtube.com');

-- --------------------------------------------------------

--
-- Table structure for table `shippment_addresses`
--

CREATE TABLE `shippment_addresses` (
  `id` int(10) NOT NULL,
  `customer_id` int(10) DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  `location_id` int(10) DEFAULT NULL,
  `region_id` int(10) DEFAULT NULL,
  `city_id` int(10) DEFAULT NULL,
  `phone` int(30) DEFAULT NULL,
  `address_type` varchar(20) DEFAULT NULL,
  `address_details` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shippment_addresses`
--

INSERT INTO `shippment_addresses` (`id`, `customer_id`, `status`, `location_id`, `region_id`, `city_id`, `phone`, `address_type`, `address_details`, `created_at`, `updated_at`) VALUES
(3, 4, 0, 1, 1, 1, 2147483647, 'work', 'rfthjkjhgfd', '2021-09-03 21:39:32', '2021-09-03 21:39:32'),
(8, 6, 0, 1, 1, 1, 2147483647, 'work', 'rfthjkjhgfd', '2021-09-03 21:39:32', '2021-09-03 21:39:32'),
(9, 23, 0, 2, 1, 1, 1092221615, 'home', NULL, '2021-09-21 16:05:59', '2021-09-21 16:05:59'),
(10, 23, 0, 2, 1, 1, 2147483647, 'home', NULL, '2021-09-21 16:07:14', '2021-09-21 16:07:14'),
(11, 29, 0, 1, 1, 1, 1024517052, 'home', 'elmahala', '2021-09-28 08:28:09', '2021-09-28 08:28:09'),
(12, 31, 0, 2, 1, 1, 2147483647, 'home', NULL, '2021-09-28 09:37:07', '2021-09-28 09:37:07'),
(13, 24, 0, 2, 1, 1, 1092224645, 'home', NULL, '2021-10-06 05:55:08', '2021-10-06 05:55:08');

-- --------------------------------------------------------

--
-- Table structure for table `shop_bills`
--

CREATE TABLE `shop_bills` (
  `id` int(10) UNSIGNED NOT NULL,
  `request_no` int(40) DEFAULT 0,
  `order_total_price` int(11) DEFAULT NULL,
  `order_id` int(10) DEFAULT NULL,
  `customer_id` int(10) DEFAULT NULL,
  `price_after_tax` int(11) DEFAULT NULL,
  `price_after_coupon` int(10) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 => موجود فالسله\r\n1 => انتظار الوصول\r\n2 => مشترياتي ',
  `payment_method` int(11) DEFAULT 0 COMMENT '1 => from shop\r\n2 => online',
  `coupon_id` int(10) UNSIGNED DEFAULT NULL,
  `receive_type` int(11) NOT NULL DEFAULT 0 COMMENT '1 => توصيل عادي &&   \r\n2 => استلام من المتجر',
  `shippment_address` int(11) DEFAULT 0,
  `bill_address` int(11) DEFAULT 0 COMMENT '1 => عنوان الفاتوره هوا عنوان الشحن\r\n2 => عنوان جديد',
  `new_address` varchar(191) DEFAULT '0' COMMENT 'عنوان الفاتوره',
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shop_bills`
--

INSERT INTO `shop_bills` (`id`, `request_no`, `order_total_price`, `order_id`, `customer_id`, `price_after_tax`, `price_after_coupon`, `status`, `payment_method`, `coupon_id`, `receive_type`, `shippment_address`, `bill_address`, `new_address`, `updated_at`, `deleted_at`, `created_at`) VALUES
(8, 1234567, 1370, 4, 4, 1507, 523, 1, 2, 1, 1, 3, 1, NULL, '2021-11-02 04:57:50', '2021-11-02 04:57:50', '2021-09-02 12:44:44'),
(9, 2147483647, 2630, 5, 24, 2893, 0, 0, 0, NULL, 0, 0, 0, '0', '2021-10-03 09:34:41', '2021-10-03 09:34:41', '2021-09-24 12:53:24'),
(10, 2147483647, 1857, 6, 27, 2043, 0, 0, 0, NULL, 0, 0, 0, '0', '2021-09-26 06:42:03', NULL, '2021-09-26 06:34:36'),
(12, 1064957063, 230, 7, 26, 253, 0, 0, 0, NULL, 0, 0, 0, '0', '2021-09-26 09:40:56', NULL, '2021-09-26 09:17:31'),
(13, 2147483647, 430, 8, 29, 473, 0, 1, 1, NULL, 1, NULL, 1, NULL, '2021-09-28 08:32:47', NULL, '2021-09-27 12:19:09'),
(14, 2147483647, 200, 9, 31, 220, 0, 1, 2, NULL, 1, 12, 1, NULL, '2021-09-28 09:42:20', NULL, '2021-09-28 09:07:21'),
(15, 2147483647, 658, 10, 36, 724, 0, 0, 0, NULL, 0, 0, 0, '0', '2021-09-30 09:45:10', NULL, '2021-09-30 09:34:53'),
(16, 2147483647, 860, 5, 24, 946, 0, 0, 0, NULL, 0, 0, 0, '0', '2021-10-03 09:44:00', '2021-10-03 09:44:00', '2021-10-03 09:34:55'),
(17, 2147483647, 1249, 5, 24, 1374, 0, 0, 0, NULL, 0, 0, 0, '0', '2021-10-03 11:37:39', '2021-10-03 11:37:39', '2021-10-03 09:44:26'),
(18, 2147483647, 3100, 5, 24, 3410, 0, 1, 1, NULL, 1, NULL, 1, NULL, '2021-10-06 05:06:42', '2021-10-06 05:06:42', '2021-10-03 11:37:56'),
(19, 1571913511, 230, 5, 24, 253, 0, 1, 0, NULL, 1, 13, 1, NULL, '2021-10-06 05:56:03', NULL, '2021-10-06 05:07:34'),
(20, 2147483647, 20, 4, 4, 418, 0, 0, 0, NULL, 0, 0, 0, '0', '2021-11-02 05:07:28', '2021-11-02 05:07:28', '2021-11-02 04:58:10'),
(21, 2147483647, 200, 4, 4, 220, 0, 1, 0, NULL, 1, 3, 1, NULL, '2021-11-02 05:23:30', NULL, '2021-11-02 05:07:40');

-- --------------------------------------------------------

--
-- Table structure for table `shop_coupons`
--

CREATE TABLE `shop_coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `coupon_name` varchar(200) NOT NULL,
  `coupon_code` varchar(20) NOT NULL,
  `expire_date` date NOT NULL,
  `status` varchar(191) NOT NULL,
  `coupon_discount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shop_coupons`
--

INSERT INTO `shop_coupons` (`id`, `created_at`, `updated_at`, `deleted_at`, `coupon_name`, `coupon_code`, `expire_date`, `status`, `coupon_discount`) VALUES
(1, '2021-09-12 08:50:51', NULL, NULL, 'كوبون خصم أول ', '22first', '0000-00-00', '1', 22);

-- --------------------------------------------------------

--
-- Table structure for table `shop_settings`
--

CREATE TABLE `shop_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `shipping_info_en` longtext DEFAULT NULL,
  `shipping_info_ar` longtext DEFAULT NULL,
  `delivery_price` varchar(191) NOT NULL,
  `tax` varchar(191) NOT NULL,
  `bank_account` varchar(191) NOT NULL,
  `bank_name` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shop_settings`
--

INSERT INTO `shop_settings` (`id`, `created_at`, `updated_at`, `shipping_info_en`, `shipping_info_ar`, `delivery_price`, `tax`, `bank_account`, `bank_name`) VALUES
(1, NULL, '2021-07-30 20:48:48', 'There is a proven fact from a long time ago that the readable content of a page will distract the reader from focusing on the external form of the text or the form of paragraphs placed on the page he reads. Therefore, the Lorem Ipsum method is used because it gives a natural distribution - to a certain extent - for the characters instead of using \"here there is text content, here there is text content\", making it look (that is, the letters) as if it were readable text. Many desktop publishing and web page editing programs use Lorem Ipsum by default as a sample text.\r\n\r\nThere is a proven fact from a long time ago that the readable content of a page will distract the reader from focusing on the external form of the text or the form of paragraphs placed on the page he reads. Therefore, the Lorem Ipsum method is used because it gives a natural distribution - to a certain extent - for the characters instead of using \"here there is text content, here there is text content\", making it look (that is, the letters) as if it were readable text. Many desktop publishing and web page editing programs use Lorem Ipsum by default as a sample text.', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال\r\n\r\nهناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال\r\n\r\n', '6787654345', '10', 'هعبي4قف567890', 'بنك 1');

-- --------------------------------------------------------

--
-- Table structure for table `site_gallery`
--

CREATE TABLE `site_gallery` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `setting_id` int(10) UNSIGNED DEFAULT NULL,
  `site_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_gallery`
--

INSERT INTO `site_gallery` (`id`, `created_at`, `updated_at`, `setting_id`, `site_image`) VALUES
(16, '2021-07-30 19:33:21', '2021-07-30 19:33:21', NULL, 'public/images/site_gallerys/162768080128216.jpg'),
(17, '2021-07-30 19:33:21', '2021-07-30 19:33:21', NULL, 'public/images/site_gallerys/162768080176123.jpg'),
(18, '2021-07-30 19:33:21', '2021-07-30 19:33:21', NULL, 'public/images/site_gallerys/162768080121233.jpg'),
(19, '2021-07-30 19:33:21', '2021-07-30 19:33:21', NULL, 'public/images/site_gallerys/162768080155350.jpg'),
(20, '2021-07-30 19:33:21', '2021-07-30 19:33:21', NULL, 'public/images/site_gallerys/162768080134893.jpg'),
(21, '2021-07-30 19:33:21', '2021-07-30 19:33:21', NULL, 'public/images/site_gallerys/162768080126663.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) NOT NULL,
  `slider_img` text DEFAULT NULL,
  `status` bigint(20) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `slider_text_en` text DEFAULT NULL,
  `slider_text_ar` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `slider_img`, `status`, `created_at`, `updated_at`, `slider_text_en`, `slider_text_ar`) VALUES
(4, 'public/images/sliders/L53BBgO5XSBS9nsEFRAW7Y1AsSpzdivGWrnVVdHy.jpg', 1, '2021-08-08 13:14:43', '2021-08-08 11:14:43', '<p><br></p>', '<p><br></p>'),
(5, 'public/images/sliders/NEExF1Pf8kjy7vQlbDTzoGIwYNSjcQTJ4t3ShKeT.jpg', 1, '2021-08-30 11:43:14', '2021-08-30 09:43:14', '<p><br></p><p><br></p>', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `current_quantity` int(11) NOT NULL DEFAULT 0,
  `status` varchar(191) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `created_at`, `updated_at`, `deleted_at`, `product_id`, `product_quantity`, `current_quantity`, `status`) VALUES
(2, '2021-07-29 19:46:30', '2021-09-01 09:15:11', NULL, 6, 30, 0, '0'),
(3, '2021-07-29 19:48:47', '2021-09-01 09:18:44', NULL, 7, 10, 0, '0'),
(4, '2021-07-29 19:49:57', '2021-09-01 09:21:27', NULL, 8, 56, 0, '0'),
(8, '2021-08-07 12:45:06', '2021-09-01 09:24:48', NULL, 12, 37, 0, '0'),
(9, '2021-08-10 07:56:31', '2021-09-01 09:27:41', NULL, 13, 9, 0, '0'),
(10, '2021-08-10 19:34:02', '2021-09-01 09:30:12', NULL, 14, 80, 0, '0'),
(15, '2021-08-11 08:16:27', '2021-09-01 09:33:16', NULL, 21, 8, 0, '0'),
(16, '2021-08-11 08:16:51', '2021-09-01 09:34:45', NULL, 22, 20, 0, '0'),
(17, '2021-08-26 08:23:15', '2021-09-01 09:37:22', NULL, 23, 11, 0, '0'),
(18, '2021-08-28 14:43:10', '2021-09-01 09:39:18', NULL, 24, 9, 0, '0'),
(19, '2021-08-28 14:45:06', '2021-09-01 09:41:47', NULL, 25, 21, 0, '0'),
(20, '2021-09-01 09:48:49', '2021-09-01 10:00:44', NULL, 26, 2, 0, '0'),
(23, '2021-09-01 10:03:47', '2021-09-01 10:12:52', NULL, 32, 8, 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `trademarks`
--

CREATE TABLE `trademarks` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `image` text DEFAULT NULL,
  `status` bigint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trademarks`
--

INSERT INTO `trademarks` (`id`, `created_at`, `updated_at`, `deleted_at`, `name`, `image`, `status`) VALUES
(1, '2021-08-11 07:42:10', '2021-08-11 07:42:29', NULL, 'علامه 1', 'public/images/trademarks/zpYXuF5DKoNCRBZjzghg5qurncPiZe0u9cRsJCRb.png', 1),
(2, '2021-08-11 07:49:55', '2021-08-30 10:40:46', NULL, 'علامه 2', 'public/images/trademarks/mj0W3C4JEQMhKY02j7sn0BkFE7KK34NJsNQB2ZCX.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `photo` text DEFAULT NULL,
  `phone` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `photo`, `phone`) VALUES
(1, 'admin', 'admin@admin.com', NULL, '$2y$10$SDs7d0Z4AC6i2ofbG6s4tOkbPYpBIVq0yNNlY3.ABrMzuyoPIHXKq', NULL, '2021-07-19 19:08:14', '2021-09-15 19:06:24', '/public/images/animals/XND1IWFHNxzlE6fgbj12fMmfOBCTQn0SKaKrUjpG.png', 107585185),
(2, 'ahmed', 'ahmed123@yahoo.com', NULL, '$2y$10$VJ7kQclO0NYGRAWPUd6g7eQl03t2QyptWXvJtzKjd2Rha0Q3A49a.', NULL, '2021-08-03 20:34:40', '2021-08-03 20:34:40', NULL, 0),
(3, 'mai tarek', 'maitarekttt@gmail.com', NULL, '$2y$10$yi5hCfm7zJDDnnQJC4hhUuQZgA2UWvUGegV2dAVCJOu4u/Cq9pGU.', NULL, '2021-08-30 18:11:56', '2021-08-30 18:11:56', NULL, 0),
(4, 'maitarek', 'maitarektt3t@gmail.com', NULL, '$2y$10$lX5b3pb7oNSHrkI45N2J4uS0JIXVjOFzG7PL1FoEKFF2EztXbsFIy', NULL, '2021-08-31 05:55:31', '2021-08-31 05:55:31', NULL, 0),
(5, 'foas123', 'foad@yahoo.com', NULL, '$2y$10$MnLzDkiPxu7b2/QpVejXqeRseBAAYpwsnoIBdrgOh.K92kGRXZbBm', NULL, '2021-09-15 11:13:49', '2021-09-15 11:13:49', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `id` int(10) NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `visitor_count` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`id`, `ip_address`, `visitor_count`, `created_at`, `updated_at`) VALUES
(1, '197.61.13.102', 3, '2021-10-06 14:12:03', '2021-10-06 11:12:03'),
(2, '3.238.146.203', 0, '2021-10-06 11:09:35', '2021-10-06 11:09:35'),
(3, '66.249.89.217', 0, '2021-10-06 11:12:45', '2021-10-06 11:12:45'),
(4, '156.199.124.194', 5, '2021-10-06 21:55:01', '2021-10-06 18:55:01'),
(5, '51.222.253.16', 6, '2021-10-27 16:46:31', '2021-10-27 13:46:31'),
(6, '144.202.103.47', 0, '2021-10-06 15:14:07', '2021-10-06 15:14:07'),
(7, '66.249.64.217', 11, '2021-10-14 11:22:20', '2021-10-14 08:22:20'),
(8, '66.249.64.215', 17, '2021-10-24 23:13:58', '2021-10-24 20:13:58'),
(9, '66.249.64.219', 16, '2021-10-25 09:42:40', '2021-10-25 06:42:40'),
(10, '196.158.207.154', 1, '2021-10-06 20:37:52', '2021-10-06 17:37:52'),
(11, '20.112.100.210', 1, '2021-10-06 22:28:42', '2021-10-06 19:28:42'),
(12, '34.220.245.243', 0, '2021-10-06 21:11:52', '2021-10-06 21:11:52'),
(13, '34.222.67.187', 0, '2021-10-06 21:11:55', '2021-10-06 21:11:55'),
(14, '34.217.40.7', 0, '2021-10-06 21:11:56', '2021-10-06 21:11:56'),
(15, '54.189.39.199', 0, '2021-10-06 21:12:45', '2021-10-06 21:12:45'),
(16, '51.222.253.7', 8, '2021-10-29 08:33:57', '2021-10-29 05:33:57'),
(17, '124.71.180.89', 7, '2021-11-04 07:42:09', '2021-11-04 04:42:09'),
(18, '165.22.246.229', 5, '2021-10-07 09:01:22', '2021-10-07 06:01:22'),
(19, '154.239.191.150', 2, '2021-10-07 09:07:28', '2021-10-07 06:07:28'),
(20, '23.146.241.19', 0, '2021-10-07 07:30:04', '2021-10-07 07:30:04'),
(21, '167.71.214.220', 17, '2021-11-02 02:17:07', '2021-11-01 23:17:07'),
(22, '51.222.253.2', 8, '2021-10-26 18:41:36', '2021-10-26 15:41:36'),
(23, '193.56.29.48', 0, '2021-10-07 16:03:40', '2021-10-07 16:03:40'),
(24, '137.226.113.44', 2, '2021-10-28 11:04:05', '2021-10-28 08:04:05'),
(25, '161.35.91.105', 0, '2021-10-07 21:35:00', '2021-10-07 21:35:00'),
(26, '34.216.204.126', 0, '2021-10-07 21:35:32', '2021-10-07 21:35:32'),
(27, '54.148.132.253', 0, '2021-10-07 21:35:34', '2021-10-07 21:35:34'),
(28, '51.222.253.8', 3, '2021-10-28 04:29:49', '2021-10-28 01:29:49'),
(29, '146.70.31.119', 2, '2021-10-08 07:48:21', '2021-10-08 04:48:21'),
(30, '192.71.103.173', 0, '2021-10-08 07:41:39', '2021-10-08 07:41:39'),
(31, '34.222.236.126', 0, '2021-10-08 21:10:53', '2021-10-08 21:10:53'),
(32, '54.191.86.212', 0, '2021-10-08 21:11:10', '2021-10-08 21:11:10'),
(33, '54.202.132.3', 0, '2021-10-08 21:20:43', '2021-10-08 21:20:43'),
(34, '167.94.138.60', 3, '2021-10-09 01:52:14', '2021-10-08 22:52:14'),
(35, '162.142.125.60', 3, '2021-10-09 01:52:14', '2021-10-08 22:52:14'),
(36, '162.142.125.59', 6, '2021-10-09 01:52:14', '2021-10-08 22:52:14'),
(37, '139.162.7.175', 0, '2021-10-08 22:52:15', '2021-10-08 22:52:15'),
(38, '3.235.51.96', 0, '2021-10-08 23:54:18', '2021-10-08 23:54:18'),
(39, '34.86.35.28', 1, '2021-10-29 09:28:11', '2021-10-29 06:28:11'),
(40, '20.88.58.130', 2, '2021-10-09 03:42:53', '2021-10-09 00:42:53'),
(41, '185.220.101.62', 0, '2021-10-09 00:44:51', '2021-10-09 00:44:51'),
(42, '185.220.101.45', 0, '2021-10-09 00:45:03', '2021-10-09 00:45:03'),
(43, '23.154.177.134', 0, '2021-10-09 00:45:24', '2021-10-09 00:45:24'),
(44, '198.98.48.203', 0, '2021-10-09 00:45:26', '2021-10-09 00:45:26'),
(45, '20.85.212.2', 2, '2021-10-09 05:31:07', '2021-10-09 02:31:07'),
(46, '84.247.48.9', 0, '2021-10-09 03:25:16', '2021-10-09 03:25:16'),
(47, '65.154.226.165', 0, '2021-10-09 03:31:01', '2021-10-09 03:31:01'),
(48, '3.70.17.244', 0, '2021-10-09 04:11:09', '2021-10-09 04:11:09'),
(49, '40.77.167.27', 0, '2021-10-09 04:27:45', '2021-10-09 04:27:45'),
(50, '3.235.250.158', 0, '2021-10-09 11:57:59', '2021-10-09 11:57:59'),
(51, '34.94.183.25', 2, '2021-10-09 16:19:49', '2021-10-09 13:19:49'),
(52, '51.222.253.14', 4, '2021-10-28 16:59:44', '2021-10-28 13:59:44'),
(53, '92.118.160.17', 1, '2021-10-23 03:58:21', '2021-10-23 00:58:21'),
(54, '147.182.242.131', 0, '2021-10-09 20:34:42', '2021-10-09 20:34:42'),
(55, '54.69.0.183', 0, '2021-10-09 21:05:03', '2021-10-09 21:05:03'),
(56, '35.86.101.119', 0, '2021-10-09 21:05:14', '2021-10-09 21:05:14'),
(57, '51.222.253.10', 6, '2021-10-26 07:56:25', '2021-10-26 04:56:25'),
(58, '212.83.146.233', 1, '2021-10-14 05:52:21', '2021-10-14 02:52:21'),
(59, '92.118.160.5', 2, '2021-10-30 00:36:06', '2021-10-29 21:36:06'),
(60, '195.154.61.206', 0, '2021-10-10 06:48:06', '2021-10-10 06:48:06'),
(61, '51.254.49.100', 0, '2021-10-10 07:58:03', '2021-10-10 07:58:03'),
(62, '54.202.55.252', 0, '2021-10-10 21:35:46', '2021-10-10 21:35:46'),
(63, '18.236.128.196', 0, '2021-10-10 21:36:25', '2021-10-10 21:36:25'),
(64, '51.222.253.19', 4, '2021-10-27 13:50:59', '2021-10-27 10:50:59'),
(65, '5.181.235.74', 1, '2021-10-11 01:36:06', '2021-10-10 22:36:06'),
(66, '18.233.162.36', 0, '2021-10-10 23:59:50', '2021-10-10 23:59:50'),
(67, '139.59.241.57', 2, '2021-10-11 03:19:22', '2021-10-11 00:19:22'),
(68, '51.158.97.148', 0, '2021-10-11 00:32:32', '2021-10-11 00:32:32'),
(69, '18.223.107.87', 0, '2021-10-11 04:26:18', '2021-10-11 04:26:18'),
(70, '34.126.121.221', 2, '2021-10-11 10:19:18', '2021-10-11 07:19:18'),
(71, '156.207.223.235', 6, '2021-10-11 17:19:12', '2021-10-11 14:19:12'),
(72, '5.41.223.241', 0, '2021-10-11 14:29:33', '2021-10-11 14:29:33'),
(73, '45.129.18.18', 0, '2021-10-11 14:41:36', '2021-10-11 14:41:36'),
(74, '51.222.253.9', 8, '2021-10-28 15:14:02', '2021-10-28 12:14:02'),
(75, '45.129.18.88', 0, '2021-10-11 17:49:02', '2021-10-11 17:49:02'),
(76, '61.135.15.196', 0, '2021-10-11 18:09:23', '2021-10-11 18:09:23'),
(77, '18.237.136.204', 0, '2021-10-11 21:14:35', '2021-10-11 21:14:35'),
(78, '34.218.223.30', 0, '2021-10-11 21:14:38', '2021-10-11 21:14:38'),
(79, '54.203.126.61', 0, '2021-10-11 21:15:10', '2021-10-11 21:15:10'),
(80, '178.128.112.131', 2, '2021-10-12 00:21:36', '2021-10-11 21:21:36'),
(81, '124.126.78.151', 1, '2021-10-23 02:15:24', '2021-10-22 23:15:24'),
(82, '42.83.147.34', 1, '2021-10-28 10:30:12', '2021-10-28 07:30:12'),
(83, '34.86.35.30', 0, '2021-10-12 10:20:04', '2021-10-12 10:20:04'),
(84, '104.211.34.172', 2, '2021-10-12 13:39:04', '2021-10-12 10:39:04'),
(85, '51.222.253.20', 8, '2021-10-29 17:03:22', '2021-10-29 14:03:22'),
(86, '51.222.253.6', 6, '2021-10-28 03:04:59', '2021-10-28 00:04:59'),
(87, '64.246.165.50', 0, '2021-10-12 13:44:17', '2021-10-12 13:44:17'),
(88, '88.202.184.23', 4, '2021-10-12 21:46:17', '2021-10-12 18:46:17'),
(89, '34.96.130.19', 0, '2021-10-12 17:48:46', '2021-10-12 17:48:46'),
(90, '192.99.18.122', 0, '2021-10-12 20:24:21', '2021-10-12 20:24:21'),
(91, '54.191.79.142', 0, '2021-10-12 21:30:44', '2021-10-12 21:30:44'),
(92, '144.76.67.169', 2, '2021-10-13 05:43:27', '2021-10-13 02:43:27'),
(93, '54.218.54.221', 0, '2021-10-13 04:58:56', '2021-10-13 04:58:56'),
(94, '167.172.80.218', 5, '2021-10-17 13:14:05', '2021-10-17 10:14:05'),
(95, '35.236.96.231', 2, '2021-10-13 09:04:53', '2021-10-13 06:04:53'),
(96, '51.222.253.5', 7, '2021-10-27 15:08:16', '2021-10-27 12:08:16'),
(97, '52.161.23.42', 0, '2021-10-13 08:38:04', '2021-10-13 08:38:04'),
(98, '165.232.165.207', 2, '2021-10-13 11:38:25', '2021-10-13 08:38:25'),
(99, '51.222.253.18', 3, '2021-10-30 07:16:10', '2021-10-30 04:16:10'),
(100, '51.222.253.3', 8, '2021-10-26 15:18:18', '2021-10-26 12:18:18'),
(101, '18.224.39.239', 0, '2021-10-13 14:00:52', '2021-10-13 14:00:52'),
(102, '167.172.82.191', 2, '2021-10-13 22:35:29', '2021-10-13 19:35:29'),
(103, '51.222.253.13', 3, '2021-10-28 12:16:38', '2021-10-28 09:16:38'),
(104, '3.249.74.65', 0, '2021-10-13 21:37:27', '2021-10-13 21:37:27'),
(105, '54.191.56.56', 0, '2021-10-13 21:42:50', '2021-10-13 21:42:50'),
(106, '54.189.85.176', 0, '2021-10-13 21:42:53', '2021-10-13 21:42:53'),
(107, '54.184.244.62', 0, '2021-10-13 22:41:29', '2021-10-13 22:41:29'),
(108, '34.218.232.133', 0, '2021-10-13 22:41:31', '2021-10-13 22:41:31'),
(109, '18.237.240.169', 0, '2021-10-13 22:45:39', '2021-10-13 22:45:39'),
(110, '3.230.172.16', 0, '2021-10-14 00:02:19', '2021-10-14 00:02:19'),
(111, '51.15.191.81', 0, '2021-10-14 03:27:54', '2021-10-14 03:27:54'),
(112, '51.222.253.17', 6, '2021-10-29 05:06:48', '2021-10-29 02:06:48'),
(113, '51.254.49.103', 0, '2021-10-14 05:25:10', '2021-10-14 05:25:10'),
(114, '65.155.30.101', 0, '2021-10-14 08:33:46', '2021-10-14 08:33:46'),
(115, '34.126.137.241', 2, '2021-10-14 14:45:50', '2021-10-14 11:45:50'),
(116, '65.154.226.100', 0, '2021-10-14 17:34:58', '2021-10-14 17:34:58'),
(117, '54.187.51.48', 0, '2021-10-14 21:32:32', '2021-10-14 21:32:32'),
(118, '34.210.167.137', 0, '2021-10-14 21:32:35', '2021-10-14 21:32:35'),
(119, '35.203.109.226', 2, '2021-10-15 12:17:21', '2021-10-15 09:17:21'),
(120, '51.222.253.4', 6, '2021-10-29 03:00:58', '2021-10-29 00:00:58'),
(121, '51.222.253.15', 5, '2021-10-29 15:28:04', '2021-10-29 12:28:04'),
(122, '130.255.166.79', 0, '2021-10-15 18:37:26', '2021-10-15 18:37:26'),
(123, '34.219.225.3', 0, '2021-10-15 21:04:10', '2021-10-15 21:04:10'),
(124, '34.96.130.13', 0, '2021-10-15 22:16:47', '2021-10-15 22:16:47'),
(125, '45.129.18.184', 0, '2021-10-15 22:19:26', '2021-10-15 22:19:26'),
(126, '34.96.130.29', 0, '2021-10-16 01:17:08', '2021-10-16 01:17:08'),
(127, '92.118.160.1', 3, '2021-11-01 19:57:01', '2021-11-01 16:57:01'),
(128, '92.118.160.57', 0, '2021-10-16 05:30:21', '2021-10-16 05:30:21'),
(129, '217.160.174.204', 0, '2021-10-16 09:24:28', '2021-10-16 09:24:28'),
(130, '185.247.225.43', 0, '2021-10-16 09:25:01', '2021-10-16 09:25:01'),
(131, '163.172.56.74', 0, '2021-10-16 09:25:34', '2021-10-16 09:25:34'),
(132, '23.129.64.133', 0, '2021-10-16 09:25:57', '2021-10-16 09:25:57'),
(133, '155.254.223.184', 0, '2021-10-16 15:49:12', '2021-10-16 15:49:12'),
(134, '92.118.160.41', 2, '2021-10-23 01:24:19', '2021-10-22 22:24:19'),
(135, '68.183.188.112', 2, '2021-10-16 19:52:50', '2021-10-16 16:52:50'),
(136, '52.89.220.50', 0, '2021-10-16 21:38:05', '2021-10-16 21:38:05'),
(137, '35.166.60.162', 0, '2021-10-16 21:38:08', '2021-10-16 21:38:08'),
(138, '143.198.198.80', 8, '2021-10-20 10:05:45', '2021-10-20 07:05:45'),
(139, '51.222.253.12', 5, '2021-10-30 02:07:41', '2021-10-29 23:07:41'),
(140, '154.239.218.39', 1, '2021-10-17 11:42:22', '2021-10-17 08:42:22'),
(141, '54.69.203.10', 0, '2021-10-17 22:18:36', '2021-10-17 22:18:36'),
(142, '35.165.82.128', 0, '2021-10-17 22:18:37', '2021-10-17 22:18:37'),
(143, '20.106.56.22', 2, '2021-10-18 01:25:04', '2021-10-17 22:25:04'),
(144, '103.73.95.129', 0, '2021-10-18 01:22:25', '2021-10-18 01:22:25'),
(145, '35.245.75.202', 1, '2021-10-18 05:25:05', '2021-10-18 02:25:05'),
(146, '54.198.252.134', 5, '2021-10-18 12:44:29', '2021-10-18 09:44:29'),
(147, '54.145.90.209', 0, '2021-10-18 09:44:27', '2021-10-18 09:44:27'),
(148, '66.102.8.24', 2, '2021-10-18 12:44:59', '2021-10-18 09:44:59'),
(149, '66.102.8.22', 0, '2021-10-18 09:44:36', '2021-10-18 09:44:36'),
(150, '54.224.191.110', 1, '2021-10-18 14:02:10', '2021-10-18 11:02:10'),
(151, '66.249.81.186', 0, '2021-10-18 17:46:55', '2021-10-18 17:46:55'),
(152, '66.249.93.3', 0, '2021-10-18 17:46:55', '2021-10-18 17:46:55'),
(153, '66.249.81.188', 0, '2021-10-18 17:46:59', '2021-10-18 17:46:59'),
(154, '66.249.93.5', 0, '2021-10-18 17:47:00', '2021-10-18 17:47:00'),
(155, '217.52.184.29', 4, '2021-10-18 20:49:19', '2021-10-18 17:49:19'),
(156, '52.10.82.62', 0, '2021-10-18 21:39:09', '2021-10-18 21:39:09'),
(157, '18.236.113.162', 0, '2021-10-18 21:39:40', '2021-10-18 21:39:40'),
(158, '54.69.11.151', 0, '2021-10-18 21:41:45', '2021-10-18 21:41:45'),
(159, '82.80.249.214', 0, '2021-10-19 03:49:31', '2021-10-19 03:49:31'),
(160, '82.80.230.228', 6, '2021-10-19 06:50:28', '2021-10-19 03:50:28'),
(161, '185.189.114.124', 1, '2021-10-22 10:39:41', '2021-10-22 07:39:41'),
(162, '18.144.65.252', 2, '2021-10-19 08:59:39', '2021-10-19 05:59:39'),
(163, '103.13.106.150', 0, '2021-10-19 08:16:49', '2021-10-19 08:16:49'),
(164, '44.236.207.248', 0, '2021-10-19 13:34:14', '2021-10-19 13:34:14'),
(165, '51.222.253.11', 5, '2021-10-30 04:55:33', '2021-10-30 01:55:33'),
(166, '34.136.129.254', 4, '2021-10-19 22:58:13', '2021-10-19 19:58:13'),
(167, '34.96.130.4', 0, '2021-10-19 15:50:46', '2021-10-19 15:50:46'),
(168, '34.132.108.121', 2, '2021-10-20 02:09:35', '2021-10-19 23:09:35'),
(169, '35.86.145.2', 0, '2021-10-19 21:46:50', '2021-10-19 21:46:50'),
(170, '35.86.100.0', 0, '2021-10-19 21:46:52', '2021-10-19 21:46:52'),
(171, '88.202.184.17', 6, '2021-10-20 07:55:42', '2021-10-20 04:55:42'),
(172, '46.183.220.237', 0, '2021-10-20 01:58:34', '2021-10-20 01:58:34'),
(173, '34.77.162.17', 0, '2021-10-20 02:31:01', '2021-10-20 02:31:01'),
(174, '134.119.184.170', 2, '2021-10-20 07:57:50', '2021-10-20 04:57:50'),
(175, '35.160.177.243', 0, '2021-10-20 08:51:52', '2021-10-20 08:51:52'),
(176, '51.222.253.1', 3, '2021-10-26 20:23:16', '2021-10-26 17:23:16'),
(177, '34.252.26.215', 0, '2021-10-20 14:06:53', '2021-10-20 14:06:53'),
(178, '34.240.108.254', 0, '2021-10-20 14:15:29', '2021-10-20 14:15:29'),
(179, '52.18.155.69', 0, '2021-10-20 14:19:48', '2021-10-20 14:19:48'),
(180, '34.240.217.225', 0, '2021-10-20 14:29:24', '2021-10-20 14:29:24'),
(181, '54.171.151.147', 0, '2021-10-20 15:02:33', '2021-10-20 15:02:33'),
(182, '34.241.208.251', 0, '2021-10-20 15:02:40', '2021-10-20 15:02:40'),
(183, '54.212.216.139', 0, '2021-10-20 21:29:50', '2021-10-20 21:29:50'),
(184, '35.166.127.20', 0, '2021-10-20 21:30:02', '2021-10-20 21:30:02'),
(185, '143.198.86.11', 2, '2021-10-21 05:07:32', '2021-10-21 02:07:32'),
(186, '84.17.51.21', 0, '2021-10-21 02:33:48', '2021-10-21 02:33:48'),
(187, '128.199.199.126', 2, '2021-10-21 15:30:20', '2021-10-21 12:30:20'),
(188, '34.87.128.205', 2, '2021-10-21 15:44:20', '2021-10-21 12:44:20'),
(189, '196.244.200.179', 0, '2021-10-21 22:42:19', '2021-10-21 22:42:19'),
(190, '34.86.35.15', 0, '2021-10-22 00:44:22', '2021-10-22 00:44:22'),
(191, '34.86.35.12', 0, '2021-10-22 05:08:37', '2021-10-22 05:08:37'),
(192, '51.158.118.231', 0, '2021-10-22 09:49:42', '2021-10-22 09:49:42'),
(193, '34.77.162.18', 0, '2021-10-22 13:10:39', '2021-10-22 13:10:39'),
(194, '34.96.130.10', 0, '2021-10-22 16:51:31', '2021-10-22 16:51:31'),
(195, '52.212.221.68', 0, '2021-10-22 17:15:24', '2021-10-22 17:15:24'),
(196, '100.21.218.158', 1, '2021-10-26 01:40:52', '2021-10-25 22:40:52'),
(197, '54.200.237.148', 0, '2021-10-22 21:39:05', '2021-10-22 21:39:05'),
(198, '34.209.23.188', 0, '2021-10-22 21:41:02', '2021-10-22 21:41:02'),
(199, '52.41.98.30', 0, '2021-10-22 21:54:10', '2021-10-22 21:54:10'),
(200, '34.86.35.9', 0, '2021-10-23 01:13:32', '2021-10-23 01:13:32'),
(201, '178.128.22.18', 2, '2021-10-23 04:17:30', '2021-10-23 01:17:30'),
(202, '143.110.229.58', 2, '2021-10-23 09:59:14', '2021-10-23 06:59:14'),
(203, '92.118.160.61', 0, '2021-10-23 18:22:29', '2021-10-23 18:22:29'),
(204, '54.190.2.125', 0, '2021-10-23 21:28:22', '2021-10-23 21:28:22'),
(205, '35.86.113.65', 0, '2021-10-23 21:28:26', '2021-10-23 21:28:26'),
(206, '93.158.91.184', 0, '2021-10-23 21:29:51', '2021-10-23 21:29:51'),
(207, '35.240.178.22', 2, '2021-10-24 03:02:13', '2021-10-24 00:02:13'),
(208, '92.118.160.45', 0, '2021-10-24 17:48:06', '2021-10-24 17:48:06'),
(209, '54.186.104.113', 0, '2021-10-24 21:53:41', '2021-10-24 21:53:41'),
(210, '54.218.102.205', 0, '2021-10-24 21:53:48', '2021-10-24 21:53:48'),
(211, '178.128.117.204', 2, '2021-10-25 06:23:05', '2021-10-25 03:23:05'),
(212, '41.34.55.163', 2, '2021-10-25 10:23:48', '2021-10-25 07:23:48'),
(213, '114.119.129.14', 0, '2021-10-25 07:45:19', '2021-10-25 07:45:19'),
(214, '34.86.35.13', 0, '2021-10-25 17:24:59', '2021-10-25 17:24:59'),
(215, '92.118.160.9', 0, '2021-10-25 17:40:42', '2021-10-25 17:40:42'),
(216, '5.181.235.72', 2, '2021-10-25 20:59:24', '2021-10-25 17:59:24'),
(217, '34.220.155.2', 0, '2021-10-25 21:22:04', '2021-10-25 21:22:04'),
(218, '34.87.78.236', 2, '2021-10-26 05:11:47', '2021-10-26 02:11:47'),
(219, '34.77.162.31', 0, '2021-10-26 12:09:07', '2021-10-26 12:09:07'),
(220, '34.77.162.1', 0, '2021-10-26 13:19:35', '2021-10-26 13:19:35'),
(221, '114.119.129.143', 1, '2021-10-28 19:44:14', '2021-10-28 16:44:14'),
(222, '143.198.233.6', 2, '2021-10-26 21:32:30', '2021-10-26 18:32:30'),
(223, '185.65.246.127', 0, '2021-10-26 19:24:23', '2021-10-26 19:24:23'),
(224, '34.212.23.217', 0, '2021-10-26 21:26:44', '2021-10-26 21:26:44'),
(225, '34.209.78.15', 0, '2021-10-26 21:26:45', '2021-10-26 21:26:45'),
(226, '34.212.176.136', 0, '2021-10-26 21:52:13', '2021-10-26 21:52:13'),
(227, '18.237.112.0', 0, '2021-10-26 21:52:15', '2021-10-26 21:52:15'),
(228, '78.47.91.251', 0, '2021-10-26 22:15:59', '2021-10-26 22:15:59'),
(229, '51.15.205.3', 0, '2021-10-26 23:49:19', '2021-10-26 23:49:19'),
(230, '34.77.162.0', 0, '2021-10-27 00:32:12', '2021-10-27 00:32:12'),
(231, '34.86.35.31', 0, '2021-10-27 00:58:33', '2021-10-27 00:58:33'),
(232, '34.77.162.23', 1, '2021-10-27 05:10:42', '2021-10-27 02:10:42'),
(233, '117.194.14.238', 0, '2021-10-27 08:37:47', '2021-10-27 08:37:47'),
(234, '54.77.135.196', 0, '2021-10-27 08:40:23', '2021-10-27 08:40:23'),
(235, '3.250.238.136', 0, '2021-10-27 08:56:10', '2021-10-27 08:56:10'),
(236, '18.237.116.139', 0, '2021-10-27 11:26:10', '2021-10-27 11:26:10'),
(237, '34.213.245.191', 0, '2021-10-27 21:44:30', '2021-10-27 21:44:30'),
(238, '114.119.162.80', 0, '2021-10-27 21:55:19', '2021-10-27 21:55:19'),
(239, '93.158.92.205', 0, '2021-10-27 23:23:10', '2021-10-27 23:23:10'),
(240, '114.119.157.118', 0, '2021-10-28 00:52:58', '2021-10-28 00:52:58'),
(241, '114.119.129.147', 1, '2021-11-02 07:18:43', '2021-11-02 04:18:43'),
(242, '114.119.129.128', 1, '2021-11-02 10:17:39', '2021-11-02 07:17:39'),
(243, '107.172.89.151', 0, '2021-10-28 02:15:15', '2021-10-28 02:15:15'),
(244, '54.155.229.241', 0, '2021-10-28 04:59:39', '2021-10-28 04:59:39'),
(245, '45.129.18.48', 0, '2021-10-28 05:07:18', '2021-10-28 05:07:18'),
(246, '34.94.116.176', 2, '2021-10-28 10:05:54', '2021-10-28 07:05:54'),
(247, '54.170.247.95', 0, '2021-10-28 09:25:59', '2021-10-28 09:25:59'),
(248, '34.77.162.3', 0, '2021-10-28 20:34:35', '2021-10-28 20:34:35'),
(249, '35.86.146.226', 0, '2021-10-28 21:53:05', '2021-10-28 21:53:05'),
(250, '52.27.34.102', 0, '2021-10-28 21:55:38', '2021-10-28 21:55:38'),
(251, '54.183.177.145', 0, '2021-10-28 22:45:41', '2021-10-28 22:45:41'),
(252, '107.172.75.133', 0, '2021-10-29 16:22:47', '2021-10-29 16:22:47'),
(253, '18.236.127.222', 0, '2021-10-29 21:21:55', '2021-10-29 21:21:55'),
(254, '54.190.224.105', 0, '2021-10-29 21:22:05', '2021-10-29 21:22:05'),
(255, '8.41.221.63', 0, '2021-10-29 22:01:20', '2021-10-29 22:01:20'),
(256, '34.77.162.11', 0, '2021-10-29 22:25:53', '2021-10-29 22:25:53'),
(257, '114.119.129.115', 0, '2021-10-29 23:34:46', '2021-10-29 23:34:46'),
(258, '138.199.56.228', 0, '2021-10-30 00:08:59', '2021-10-30 00:08:59'),
(259, '34.86.35.26', 0, '2021-10-30 01:48:04', '2021-10-30 01:48:04'),
(260, '206.189.122.10', 0, '2021-10-30 03:47:10', '2021-10-30 03:47:10'),
(261, '185.27.99.123', 0, '2021-10-31 09:31:42', '2021-10-31 09:31:42'),
(262, '159.65.170.33', 0, '2021-10-31 19:17:42', '2021-10-31 19:17:42'),
(263, '8.41.221.48', 0, '2021-10-31 21:18:09', '2021-10-31 21:18:09'),
(264, '52.25.155.235', 0, '2021-10-31 21:25:24', '2021-10-31 21:25:24'),
(265, '104.131.64.236', 0, '2021-10-31 22:19:19', '2021-10-31 22:19:19'),
(266, '114.119.156.247', 0, '2021-10-31 22:44:25', '2021-10-31 22:44:25'),
(267, '114.119.155.24', 0, '2021-10-31 23:59:36', '2021-10-31 23:59:36'),
(268, '216.145.5.42', 0, '2021-11-01 04:30:06', '2021-11-01 04:30:06'),
(269, '92.118.160.13', 0, '2021-11-01 05:35:26', '2021-11-01 05:35:26'),
(270, '195.181.161.10', 0, '2021-11-01 06:59:19', '2021-11-01 06:59:19'),
(271, '34.214.120.227', 0, '2021-11-01 21:17:06', '2021-11-01 21:17:06'),
(272, '18.237.130.4', 0, '2021-11-01 21:20:13', '2021-11-01 21:20:13'),
(273, '54.214.158.128', 0, '2021-11-01 21:31:13', '2021-11-01 21:31:13'),
(274, '34.219.114.47', 0, '2021-11-01 21:31:30', '2021-11-01 21:31:30'),
(275, '157.55.39.132', 0, '2021-11-02 02:22:33', '2021-11-02 02:22:33'),
(276, '41.233.69.58', 7, '2021-11-02 08:28:37', '2021-11-02 05:28:37'),
(277, '68.183.35.135', 2, '2021-11-02 09:28:19', '2021-11-02 06:28:19'),
(278, '199.249.230.66', 0, '2021-11-02 06:47:26', '2021-11-02 06:47:26'),
(279, '185.220.100.240', 0, '2021-11-02 06:47:44', '2021-11-02 06:47:44'),
(280, '185.220.101.46', 0, '2021-11-02 06:47:47', '2021-11-02 06:47:47'),
(281, '45.129.56.200', 0, '2021-11-02 06:47:54', '2021-11-02 06:47:54'),
(282, '35.198.225.72', 2, '2021-11-02 20:40:35', '2021-11-02 17:40:35'),
(283, '52.12.133.143', 0, '2021-11-02 21:23:41', '2021-11-02 21:23:41'),
(284, '34.126.87.98', 2, '2021-11-03 01:41:07', '2021-11-02 22:41:07'),
(285, '128.199.134.191', 2, '2021-11-03 02:42:02', '2021-11-02 23:42:02'),
(286, '146.59.206.243', 1, '2021-11-03 11:45:07', '2021-11-03 08:45:07'),
(287, '37.120.235.164', 0, '2021-11-03 11:21:13', '2021-11-03 11:21:13'),
(288, '34.101.142.218', 2, '2021-11-03 15:25:02', '2021-11-03 12:25:02'),
(289, '34.222.139.92', 0, '2021-11-03 15:55:24', '2021-11-03 15:55:24'),
(290, '18.237.71.15', 0, '2021-11-03 15:58:55', '2021-11-03 15:58:55'),
(291, '54.191.46.226', 0, '2021-11-03 21:16:53', '2021-11-03 21:16:53'),
(292, '54.218.172.65', 0, '2021-11-03 21:17:05', '2021-11-03 21:17:05'),
(293, '2.57.122.156', 1, '2021-11-04 00:45:50', '2021-11-03 21:45:50'),
(294, '114.119.159.40', 0, '2021-11-04 00:38:09', '2021-11-04 00:38:09'),
(295, '114.119.162.62', 0, '2021-11-04 01:32:37', '2021-11-04 01:32:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `animal_categorys`
--
ALTER TABLE `animal_categorys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `animal_categorys_animal_type_id_index` (`animal_type_id`),
  ADD KEY `animal_categorys_category_id_index` (`category_id`);

--
-- Indexes for table `animal_types`
--
ALTER TABLE `animal_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blogs_user_id_index` (`user_id`);

--
-- Indexes for table `blog_blogtags`
--
ALTER TABLE `blog_blogtags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_blogtags_blog_id_index` (`blog_id`),
  ADD KEY `blog_blogtags_blog_tag_id_index` (`blog_tag_id`);

--
-- Indexes for table `blog_comments`
--
ALTER TABLE `blog_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_comments_customer_id_index` (`customer_id`),
  ADD KEY `blog_comments_blog_id_index` (`blog_id`);

--
-- Indexes for table `blog_tags`
--
ALTER TABLE `blog_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_tags_user_id_index` (`user_id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_product_id_index` (`product_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `categorys`
--
ALTER TABLE `categorys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_products`
--
ALTER TABLE `category_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_products_product_id_index` (`product_id`),
  ADD KEY `category_products_category_id_index` (`category_id`),
  ADD KEY `animal_id` (`animal_id`);

--
-- Indexes for table `citys`
--
ALTER TABLE `citys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clinics`
--
ALTER TABLE `clinics`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clinics_user_id_index` (`user_id`);

--
-- Indexes for table `clinic_reservations`
--
ALTER TABLE `clinic_reservations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `animal_id` (`animal_id`),
  ADD KEY `clinic_id` (`clinic_id`);

--
-- Indexes for table `clinic_services`
--
ALTER TABLE `clinic_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clinic_services_clinic_id_index` (`clinic_id`);

--
-- Indexes for table `clinic_setting`
--
ALTER TABLE `clinic_setting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clinic_id` (`clinic_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_reviews`
--
ALTER TABLE `customer_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotels`
--
ALTER TABLE `hotels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `hotel_reservations`
--
ALTER TABLE `hotel_reservations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `animal_id` (`animal_id`);

--
-- Indexes for table `hotel_services`
--
ALTER TABLE `hotel_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_id` (`hotel_id`);

--
-- Indexes for table `hotel_setting`
--
ALTER TABLE `hotel_setting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_id` (`hotel_id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `region_id` (`region_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`customer_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `paysuccess`
--
ALTER TABLE `paysuccess`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `privacy_policys`
--
ALTER TABLE `privacy_policys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_code` (`product_code`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `product_appraisals`
--
ALTER TABLE `product_appraisals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_appraisals_product_id_index` (`product_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `product_best_sellers`
--
ALTER TABLE `product_best_sellers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `product_discount`
--
ALTER TABLE `product_discount`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_discount_product_id_index` (`product_id`);

--
-- Indexes for table `product_favourites`
--
ALTER TABLE `product_favourites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_favourites_product_id_index` (`product_id`),
  ADD KEY `product_favourites_customer_id_index` (`customer_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_product_id_index` (`product_id`);

--
-- Indexes for table `product_new_arrivals`
--
ALTER TABLE `product_new_arrivals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `city_id` (`city_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shippment_addresses`
--
ALTER TABLE `shippment_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY ` region_id` (`region_id`),
  ADD KEY `city_id` (`city_id`);

--
-- Indexes for table `shop_bills`
--
ALTER TABLE `shop_bills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shop_bills_coupon_id_index` (`coupon_id`),
  ADD KEY `cart_id` (`order_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `address_type` (`shippment_address`);

--
-- Indexes for table `shop_coupons`
--
ALTER TABLE `shop_coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_settings`
--
ALTER TABLE `shop_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_gallery`
--
ALTER TABLE `site_gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `site_gallery_setting_id_index` (`setting_id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stores_product_id_index` (`product_id`);

--
-- Indexes for table `trademarks`
--
ALTER TABLE `trademarks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `animal_categorys`
--
ALTER TABLE `animal_categorys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `animal_types`
--
ALTER TABLE `animal_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `blog_blogtags`
--
ALTER TABLE `blog_blogtags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `blog_comments`
--
ALTER TABLE `blog_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `blog_tags`
--
ALTER TABLE `blog_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=318;

--
-- AUTO_INCREMENT for table `categorys`
--
ALTER TABLE `categorys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `category_products`
--
ALTER TABLE `category_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `citys`
--
ALTER TABLE `citys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `clinics`
--
ALTER TABLE `clinics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `clinic_reservations`
--
ALTER TABLE `clinic_reservations`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `clinic_services`
--
ALTER TABLE `clinic_services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `clinic_setting`
--
ALTER TABLE `clinic_setting`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `customer_reviews`
--
ALTER TABLE `customer_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hotels`
--
ALTER TABLE `hotels`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `hotel_reservations`
--
ALTER TABLE `hotel_reservations`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `hotel_services`
--
ALTER TABLE `hotel_services`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `hotel_setting`
--
ALTER TABLE `hotel_setting`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `paysuccess`
--
ALTER TABLE `paysuccess`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `privacy_policys`
--
ALTER TABLE `privacy_policys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `product_appraisals`
--
ALTER TABLE `product_appraisals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `product_best_sellers`
--
ALTER TABLE `product_best_sellers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `product_discount`
--
ALTER TABLE `product_discount`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_favourites`
--
ALTER TABLE `product_favourites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `product_new_arrivals`
--
ALTER TABLE `product_new_arrivals`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `regions`
--
ALTER TABLE `regions`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shippment_addresses`
--
ALTER TABLE `shippment_addresses`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `shop_bills`
--
ALTER TABLE `shop_bills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `shop_coupons`
--
ALTER TABLE `shop_coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shop_settings`
--
ALTER TABLE `shop_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `site_gallery`
--
ALTER TABLE `site_gallery`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `trademarks`
--
ALTER TABLE `trademarks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=296;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `animal_categorys`
--
ALTER TABLE `animal_categorys`
  ADD CONSTRAINT `animal_categorys_animal_type_id_foreign` FOREIGN KEY (`animal_type_id`) REFERENCES `animal_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `animal_categorys_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categorys` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `blog_blogtags`
--
ALTER TABLE `blog_blogtags`
  ADD CONSTRAINT `blog_blogtags_blog_id_foreign` FOREIGN KEY (`blog_id`) REFERENCES `blogs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blog_blogtags_blog_tag_id_foreign` FOREIGN KEY (`blog_tag_id`) REFERENCES `blog_tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `blog_comments`
--
ALTER TABLE `blog_comments`
  ADD CONSTRAINT `blog_comments_blog_id_foreign` FOREIGN KEY (`blog_id`) REFERENCES `blogs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blog_comments_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `category_products`
--
ALTER TABLE `category_products`
  ADD CONSTRAINT `category_products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categorys` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `category_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `clinic_services`
--
ALTER TABLE `clinic_services`
  ADD CONSTRAINT `clinic_services_clinic_id_foreign` FOREIGN KEY (`clinic_id`) REFERENCES `clinics` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_appraisals`
--
ALTER TABLE `product_appraisals`
  ADD CONSTRAINT `product_appraisals_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_discount`
--
ALTER TABLE `product_discount`
  ADD CONSTRAINT `product_discount_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_favourites`
--
ALTER TABLE `product_favourites`
  ADD CONSTRAINT `product_favourites_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_favourites_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `shop_bills`
--
ALTER TABLE `shop_bills`
  ADD CONSTRAINT `shop_bills_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `shop_coupons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `site_gallery`
--
ALTER TABLE `site_gallery`
  ADD CONSTRAINT `site_gallery_setting_id_foreign` FOREIGN KEY (`setting_id`) REFERENCES `settings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `stores`
--
ALTER TABLE `stores`
  ADD CONSTRAINT `stores_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
