<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('blog_blogtags', function(Blueprint $table) {
			$table->foreign('blog_id')->references('id')->on('blogs')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('blog_blogtags', function(Blueprint $table) {
			$table->foreign('blog_tag_id')->references('id')->on('blog_blogtags')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('blog_comments', function(Blueprint $table) {
			$table->foreign('customer_id')->references('id')->on('customers')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('blog_comments', function(Blueprint $table) {
			$table->foreign('blog_id')->references('id')->on('blogs')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('site_gallery', function(Blueprint $table) {
			$table->foreign('setting_id')->references('id')->on('settings')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('product_appraisals', function(Blueprint $table) {
			$table->foreign('product_id')->references('id')->on('products')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('category_products', function(Blueprint $table) {
			$table->foreign('product_id')->references('id')->on('products')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('category_products', function(Blueprint $table) {
			$table->foreign('category_id')->references('id')->on('categorys')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('stores', function(Blueprint $table) {
			$table->foreign('product_id')->references('id')->on('products')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('product_images', function(Blueprint $table) {
			$table->foreign('product_id')->references('id')->on('products')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('product_discount', function(Blueprint $table) {
			$table->foreign('product_id')->references('id')->on('products')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('animal_categorys', function(Blueprint $table) {
			$table->foreign('animal_type_id')->references('id')->on('animal_types')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('animal_categorys', function(Blueprint $table) {
			$table->foreign('category_id')->references('id')->on('categorys')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('clinic_services', function(Blueprint $table) {
			$table->foreign('clinic_id')->references('id')->on('clinics')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('product_favourites', function(Blueprint $table) {
			$table->foreign('product_id')->references('id')->on('products')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('product_favourites', function(Blueprint $table) {
			$table->foreign('customer_id')->references('id')->on('customers')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('carts', function(Blueprint $table) {
			$table->foreign('product_id')->references('id')->on('products')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('carts', function(Blueprint $table) {
			$table->foreign('bill_id')->references('id')->on('shop_bills')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('shop_bills', function(Blueprint $table) {
			$table->foreign('coupon_id')->references('id')->on('shop_coupons')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('blog_blogtags', function(Blueprint $table) {
			$table->dropForeign('blog_blogtags_blog_id_foreign');
		});
		Schema::table('blog_blogtags', function(Blueprint $table) {
			$table->dropForeign('blog_blogtags_blog_tag_id_foreign');
		});
		Schema::table('blog_comments', function(Blueprint $table) {
			$table->dropForeign('blog_comments_customer_id_foreign');
		});
		Schema::table('blog_comments', function(Blueprint $table) {
			$table->dropForeign('blog_comments_blog_id_foreign');
		});
		Schema::table('site_gallery', function(Blueprint $table) {
			$table->dropForeign('site_gallery_setting_id_foreign');
		});
		Schema::table('product_appraisals', function(Blueprint $table) {
			$table->dropForeign('product_appraisals_product_id_foreign');
		});
		Schema::table('category_products', function(Blueprint $table) {
			$table->dropForeign('category_products_product_id_foreign');
		});
		Schema::table('category_products', function(Blueprint $table) {
			$table->dropForeign('category_products_category_id_foreign');
		});
		Schema::table('stores', function(Blueprint $table) {
			$table->dropForeign('stores_product_id_foreign');
		});
		Schema::table('product_images', function(Blueprint $table) {
			$table->dropForeign('product_images_product_id_foreign');
		});
		Schema::table('product_discount', function(Blueprint $table) {
			$table->dropForeign('product_discount_product_id_foreign');
		});
		Schema::table('animal_categorys', function(Blueprint $table) {
			$table->dropForeign('animal_categorys_animal_type_id_foreign');
		});
		Schema::table('animal_categorys', function(Blueprint $table) {
			$table->dropForeign('animal_categorys_category_id_foreign');
		});
		Schema::table('clinic_services', function(Blueprint $table) {
			$table->dropForeign('clinic_services_clinic_id_foreign');
		});
		Schema::table('product_favourites', function(Blueprint $table) {
			$table->dropForeign('product_favourites_product_id_foreign');
		});
		Schema::table('product_favourites', function(Blueprint $table) {
			$table->dropForeign('product_favourites_customer_id_foreign');
		});
		Schema::table('carts', function(Blueprint $table) {
			$table->dropForeign('carts_product_id_foreign');
		});
		Schema::table('carts', function(Blueprint $table) {
			$table->dropForeign('carts_bill_id_foreign');
		});
		Schema::table('shop_bills', function(Blueprint $table) {
			$table->dropForeign('shop_bills_coupon_id_foreign');
		});
	}
}