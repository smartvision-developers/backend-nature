<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClinicServicesTable extends Migration {

	public function up()
	{
		Schema::create('clinic_services', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('clinic_id')->unsigned()->index();
			$table->text('service_description_ar');
			$table->text('service_description_en');
		});
	}

	public function down()
	{
		Schema::drop('clinic_services');
	}
}