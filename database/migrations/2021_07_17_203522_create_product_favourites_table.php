<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductFavouritesTable extends Migration {

	public function up()
	{
		Schema::create('product_favourites', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('product_id')->unsigned()->index();
			$table->integer('customer_id')->unsigned()->index();
		});
	}

	public function down()
	{
		Schema::drop('product_favourites');
	}
}