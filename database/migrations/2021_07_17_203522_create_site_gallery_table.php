<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSiteGalleryTable extends Migration {

	public function up()
	{
		Schema::create('site_gallery', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('setting_id')->unsigned()->index();
			$table->text('site_image');
		});
	}

	public function down()
	{
		Schema::drop('site_gallery');
	}
}