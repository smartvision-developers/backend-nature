<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCartsTable extends Migration {

	public function up()
	{
		Schema::create('carts', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('product_id')->unsigned()->index();
			$table->integer('quantity');
			$table->integer('price_after_quantity')->nullable();
			$table->integer('bill_id')->unsigned()->index();
		});
	}

	public function down()
	{
		Schema::drop('carts');
	}
}