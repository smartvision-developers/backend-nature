<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductDiscountTable extends Migration {

	public function up()
	{
		Schema::create('product_discount', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('product_id')->unsigned()->index();
			$table->integer('product_discount_rate');
			$table->date('discount_expiredate');
			$table->text('discount_notes');
		});
	}

	public function down()
	{
		Schema::drop('product_discount');
	}
}