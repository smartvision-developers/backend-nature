<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlogTagsTable extends Migration {

	public function up()
	{
		Schema::create('blog_tags', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('blog_tag_ar', 255);
			$table->string('blog_tag_en', 255);
			$table->boolean('status')->default(0);
			$table->integer('user_id')->unsigned()->index();
		});
	}

	public function down()
	{
		Schema::drop('blog_tags');
	}
}