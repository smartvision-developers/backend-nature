<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClinicsTable extends Migration {

	public function up()
	{
		Schema::create('clinics', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->text('clinic_image');
			$table->string('clinic_title_ar', 255);
			$table->string('clinic_title_en', 255);
			$table->text('clinic_description_ar');
			$table->text('clinic_description_en');
			$table->integer('user_id')->unsigned()->index();
		});
	}

	public function down()
	{
		Schema::drop('clinics');
	}
}