<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShopBillsTable extends Migration {

	public function up()
	{
		Schema::create('shop_bills', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('f_name', 50);
			$table->string('l_name', 50);
			$table->string('city');
			$table->string('governorate');
			$table->string('country');
			$table->string('email');
			$table->string('phone');
			$table->string('address');
			$table->string('received_type');
			$table->string('new_address');
			$table->string('city_code');
			$table->integer('coupon_id')->unsigned()->index();
			$table->boolean('status');
		});
	}

	public function down()
	{
		Schema::drop('shop_bills');
	}
}