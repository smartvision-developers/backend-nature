<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlogCommentsTable extends Migration {

	public function up()
	{
		Schema::create('blog_comments', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('customer_id')->unsigned()->index();
			$table->integer('blog_id')->unsigned()->index();
			$table->text('comment');
		});
	}

	public function down()
	{
		Schema::drop('blog_comments');
	}
}