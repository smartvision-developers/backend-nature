<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePrivacyPolicysTable extends Migration {

	public function up()
	{
		Schema::create('privacy_policys', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->text('privacy_policy_ar');
			$table->text('privacy_policy_en');
			$table->longText('term_of_use_ar');
			$table->longText('term_of_use_en');
		});
	}

	public function down()
	{
		Schema::drop('privacy_policys');
	}
}