<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShopSettingsTable extends Migration {

	public function up()
	{
		Schema::create('shop_settings', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->longText('shipping_info_en');
			$table->longText('shipping_info_ar');
			$table->string('delivery_price');
			$table->string('tax');
			$table->string('bank_account');
			$table->string('bank_name');
		});
	}

	public function down()
	{
		Schema::drop('shop_settings');
	}
}