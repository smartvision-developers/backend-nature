<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlogsTable extends Migration {

	public function up()
	{
		Schema::create('blogs', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('blog_title_en', 255);
			$table->string('blog_text_ar', 255);
			$table->string('blog_text_en', 255);
			$table->string('blog_title_ar', 255);
			$table->longText('blog_description_ar');
			$table->longText('blog_description_en');
			$table->integer('user_id')->unsigned()->index();
			$table->boolean('status')->default(0);
			$table->text('blog_image');
		});
	}

	public function down()
	{
		Schema::drop('blogs');
	}
}