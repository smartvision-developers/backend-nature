<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShopCouponsTable extends Migration {

	public function up()
	{
		Schema::create('shop_coupons', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('coupon_name', 200);
			$table->string('coupon_code', 20);
			$table->date('expire_date');
			$table->string('status');
			$table->integer('coupon_discount');
		});
	}

	public function down()
	{
		Schema::drop('shop_coupons');
	}
}