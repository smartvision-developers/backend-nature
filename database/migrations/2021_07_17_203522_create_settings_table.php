<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingsTable extends Migration {

	public function up()
	{
		Schema::create('settings', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->longText('about_us_ar');
			$table->longText('our_vision_en');
			$table->text('video');
			$table->string('phone_no', 40);
			$table->string('address', 255);
			$table->string('email', 200);
			$table->text('facebook_link');
			$table->longText('about_us_en');
			$table->text('snapchat_link');
			$table->longText('our_vision_ar');
			$table->text('twitter_link');
			$table->string('instagram_link');
		});
	}

	public function down()
	{
		Schema::drop('settings');
	}
}