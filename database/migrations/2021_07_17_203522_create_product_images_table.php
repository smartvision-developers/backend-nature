<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductImagesTable extends Migration {

	public function up()
	{
		Schema::create('product_images', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->text('product_image');
			$table->integer('product_id')->unsigned()->index();
		});
	}

	public function down()
	{
		Schema::drop('product_images');
	}
}