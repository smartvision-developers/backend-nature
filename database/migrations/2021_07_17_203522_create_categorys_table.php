<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategorysTable extends Migration {

	public function up()
	{
		Schema::create('categorys', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('category_name_ar', 255);
			$table->string('category_name_en', 255);
			$table->boolean('status')->default(0);
		});
	}

	public function down()
	{
		Schema::drop('categorys');
	}
}