<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomersTable extends Migration {

	public function up()
	{
		Schema::create('customers', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('name', 255);
			$table->string('email', 255);
			$table->string('phone', 30);
			$table->string('password', 30);
			$table->string('repassword', 30);
			$table->string('type');
		});
	}

	public function down()
	{
		Schema::drop('customers');
	}
}