<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrademarksTable extends Migration {

	public function up()
	{
		Schema::create('trademarks', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('name', 255);
			$table->text('image');
		});
	}

	public function down()
	{
		Schema::drop('trademarks');
	}
}