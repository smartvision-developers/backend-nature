<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	public function up()
	{
		Schema::create('products', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('product_code', 30);
			$table->string('product_name_ar', 255);
			$table->string('product_name_en', 255);
			$table->string('product_unitprice', 20);
			$table->text('product_description_ar');
			$table->text('product_description_en');
			$table->longText('product_longdescription_ar');
			$table->longText('product_longdescription_en');
			$table->integer('views');
		});
	}

	public function down()
	{
		Schema::drop('products');
	}
}