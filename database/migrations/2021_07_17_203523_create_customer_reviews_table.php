<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomerReviewsTable extends Migration {

	public function up()
	{
		Schema::create('customer_reviews', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('customer_name', 50);
			$table->string('job_title');
			$table->text('customer_review');
		});
	}

	public function down()
	{
		Schema::drop('customer_reviews');
	}
}