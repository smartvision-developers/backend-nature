<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnimalTypesTable extends Migration {

	public function up()
	{
		Schema::create('animal_types', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('name_ar', 255);
			$table->string('name_en', 255);
			$table->boolean('status')->default(0);
		});
	}

	public function down()
	{
		Schema::drop('animal_types');
	}
}