<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlogBlogtagsTable extends Migration {

	public function up()
	{
		Schema::create('blog_blogtags', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('blog_id')->unsigned()->index();
			$table->integer('blog_tag_id')->unsigned()->index();
		});
	}

	public function down()
	{
		Schema::drop('blog_blogtags');
	}
}