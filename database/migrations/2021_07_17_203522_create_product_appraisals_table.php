<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductAppraisalsTable extends Migration {

	public function up()
	{
		Schema::create('product_appraisals', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('product_id')->unsigned()->index();
			$table->text('appraisal_content');
			$table->integer('appraisal_num');
		});
	}

	public function down()
	{
		Schema::drop('product_appraisals');
	}
}